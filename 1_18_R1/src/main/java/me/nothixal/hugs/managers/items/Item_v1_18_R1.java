package me.nothixal.hugs.managers.items;

import com.cryptomorin.xseries.XMaterial;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.protocol.game.ClientboundOpenBookPacket;
import net.minecraft.world.InteractionHand;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_18_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_18_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Item_v1_18_R1 implements ItemManager {

  @Override
  public ItemStack createItem(ItemStack item) {
    return item;
  }

  @Override
  public ItemStack createItem(ItemStack item, String displayName) {
    ItemStack is = item.clone();
    ItemMeta meta = is.getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createItem(ItemStack item, String displayName, List<String> lore) {
    ItemStack is = item.clone();
    ItemMeta meta = is.getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    meta.setLore(colorizeLore(lore));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createItem(Material material) {
    return new ItemStack(material);
  }

  @Override
  public ItemStack createItem(Material material, String displayName) {
    ItemStack item = new ItemStack(material);
    ItemMeta meta = item.getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    item.setItemMeta(meta);
    return item;
  }

  @Override
  public ItemStack createItem(Material material, String displayName, List<String> lore) {
    ItemStack item = new ItemStack(material, 1);
    ItemMeta meta = item.getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    meta.setLore(colorizeLore(lore));
    item.setItemMeta(meta);
    return item;
  }

  @Override
  public ItemStack createItem(XMaterial material) {
    return material.parseItem();
  }

  @Override
  public ItemStack createItem(XMaterial material, String displayName) {
    ItemStack item = material.parseItem();
    ItemMeta meta = item.getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    item.setItemMeta(meta);
    return item;
  }

  @Override
  public ItemStack createItem(XMaterial material, String displayName, List<String> lore) {
    ItemStack item = material.parseItem();
    ItemMeta meta = item.getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    meta.setLore(colorizeLore(lore));
    item.setItemMeta(meta);
    return item;
  }

  @Override
  public ItemStack createItemNoAttrib(XMaterial material) {
    return hideAllAttributes(material.parseItem());
  }

  @Override
  public ItemStack createItemNoAttrib(XMaterial material, String displayName) {
    ItemStack is = hideAllAttributes(material.parseItem());
    ItemMeta meta = is.getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createItemNoAttrib(XMaterial material, String displayName, List<String> lore) {
    ItemStack is = hideAllAttributes(material.parseItem());
    ItemMeta meta = is.getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    meta.setLore(colorizeLore(lore));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createItemGlowing(XMaterial material) {
    return null;
  }

  @Override
  public ItemStack createItemGlowing(XMaterial material, String displayName) {
    return null;
  }

  @Override
  public ItemStack createItemGlowing(XMaterial material, String displayName, List<String> lore) {
    return null;
  }

  @Override
  public ItemStack createItemGlowing(ItemStack item) {
    return null;
  }

  @Override
  public ItemStack createItemGlowing(ItemStack item, String displayName) {
    return null;
  }

  @Override
  public ItemStack createItemGlowing(ItemStack item, String displayName, List<String> lore) {
    return null;
  }

  @Override
  @SuppressWarnings("deprecation")
  public ItemStack createSkullItem(String owner) {
    ItemStack is = XMaterial.PLAYER_HEAD.parseItem();
    SkullMeta meta = (SkullMeta) is.getItemMeta();
    meta.setOwningPlayer(Bukkit.getOfflinePlayer(owner));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  @SuppressWarnings("deprecation")
  public ItemStack createSkullItem(String owner, String displayName) {
    ItemStack is = XMaterial.PLAYER_HEAD.parseItem();
    SkullMeta meta = (SkullMeta) is.getItemMeta();
    meta.setOwningPlayer(Bukkit.getOfflinePlayer(owner));
    meta.setDisplayName(displayName);
    is.setItemMeta(meta);
    return is;
  }

  @Override
  @SuppressWarnings("deprecation")
  public ItemStack createSkullItem(String owner, String displayName, List<String> lore) {
    ItemStack is = XMaterial.PLAYER_HEAD.parseItem();
    SkullMeta meta = (SkullMeta) is.getItemMeta();
    meta.setOwningPlayer(Bukkit.getOfflinePlayer(owner));
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    meta.setLore(colorizeLore(lore));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createSkullItem(UUID owner) {
    ItemStack is = XMaterial.PLAYER_HEAD.parseItem();
    SkullMeta meta = (SkullMeta) is.getItemMeta();
    meta.setOwningPlayer(Bukkit.getOfflinePlayer(owner));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createSkullItem(UUID owner, String displayName) {
    ItemStack is = XMaterial.PLAYER_HEAD.parseItem();
    SkullMeta meta = (SkullMeta) is.getItemMeta();
    meta.setOwningPlayer(Bukkit.getPlayer(owner));
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createSkullItem(UUID owner, String displayName, List<String> lore) {
    ItemStack is = XMaterial.PLAYER_HEAD.parseItem();
    SkullMeta meta = (SkullMeta) is.getItemMeta();
    meta.setOwningPlayer(Bukkit.getPlayer(owner));
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    meta.setLore(colorizeLore(lore));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createSkullItem(ItemStack item, String displayName) {
    ItemStack is = item.clone();
    SkullMeta meta = (SkullMeta) is.getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createSkullItem(ItemStack item, String displayName, List<String> lore) {
    ItemStack is = item.clone();
    SkullMeta meta = (SkullMeta) is.getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    meta.setLore(colorizeLore(lore));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createSkullItem(ItemStack item, List<String> lore) {
    ItemStack is = item.clone();
    SkullMeta meta = (SkullMeta) is.getItemMeta();
    meta.setLore(colorizeLore(lore));
    is.setItemMeta(meta);
    return is;
  }

  @Override
  public ItemStack createNMSSkullItem(String textureValue, String displayName) {
    ItemStack item = createItem(Material.PLAYER_HEAD, displayName);
    ItemMeta meta = item.getItemMeta();
    item.setItemMeta(meta);

    net.minecraft.world.item.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);

    CompoundTag compound = nmsStack.getTag();
    if (compound == null) {
      compound = new CompoundTag();
      nmsStack.setTag(compound);
      compound = nmsStack.getTag();
    }

    UUID uuid = UUID.randomUUID();

    long least = uuid.getMostSignificantBits();
    long most = uuid.getLeastSignificantBits();

    int[] test = new int[]{(int) (least >> 32), (int) least, (int) (most >> 32), (int) most};


    CompoundTag skullOwner = new CompoundTag();
    skullOwner.putIntArray("Id", test);
    CompoundTag properties = new CompoundTag();
    CompoundTag value = new CompoundTag();
    value.putString("Value", textureValue);
    ListTag textures = new ListTag();
    textures.add(value);
    properties.put("textures", textures);
    skullOwner.put("Properties", properties);

    compound.put("SkullOwner", skullOwner);
    nmsStack.setTag(compound);

    return CraftItemStack.asBukkitCopy(nmsStack);
  }

  @Override
  public ItemStack createNMSSkullItem(String textureValue, String displayName, List<String> lore) {
    ItemStack item = createItem(Material.PLAYER_HEAD, displayName);
    ItemMeta meta = item.getItemMeta();
    meta.setLore(colorizeLore(lore));
    item.setItemMeta(meta);

    net.minecraft.world.item.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);

    CompoundTag compound = nmsStack.getTag();
    if (compound == null) {
      compound = new CompoundTag();
      nmsStack.setTag(compound);
      compound = nmsStack.getTag();
    }

    UUID uuid = UUID.randomUUID();

    long least = uuid.getMostSignificantBits();
    long most = uuid.getLeastSignificantBits();

    int[] test = new int[]{(int) (least >> 32), (int) least, (int) (most >> 32), (int) most};


    CompoundTag skullOwner = new CompoundTag();
    skullOwner.putIntArray("Id", test);
    CompoundTag properties = new CompoundTag();
    CompoundTag value = new CompoundTag();
    value.putString("Value", textureValue);
    ListTag textures = new ListTag();
    textures.add(value);
    properties.put("textures", textures);
    skullOwner.put("Properties", properties);

    compound.put("SkullOwner", skullOwner);
    nmsStack.setTag(compound);

    return CraftItemStack.asBukkitCopy(nmsStack);
  }

  @Override
  public ItemStack createNMSSkullItem(String textureValue, ItemStack item) {
    net.minecraft.world.item.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);

    CompoundTag compound = nmsStack.getTag();
    if (compound == null) {
      compound = new CompoundTag();
      nmsStack.setTag(compound);
      compound = nmsStack.getTag();
    }

    UUID uuid = UUID.randomUUID();

    long least = uuid.getMostSignificantBits();
    long most = uuid.getLeastSignificantBits();

    int[] test = new int[]{(int) (least >> 32), (int) least, (int) (most >> 32), (int) most};

    CompoundTag skullOwner = new CompoundTag();
    skullOwner.putIntArray("Id", test);
    CompoundTag properties = new CompoundTag();
    CompoundTag value = new CompoundTag();
    value.putString("Value", textureValue);
    CompoundTag textures = new CompoundTag();
    textures.merge(value);
    properties.put("textures", textures);
    skullOwner.put("Properties", properties);

    compound.put("SkullOwner", skullOwner);
    nmsStack.setTag(compound);

    return CraftItemStack.asBukkitCopy(nmsStack);
  }

  @Override
  public ItemStack createNMSSkullItem(String textureValue, ItemStack item, String displayName) {
    net.minecraft.world.item.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);

    CompoundTag compound = nmsStack.getTag();
    if (compound == null) {
      compound = new CompoundTag();
      nmsStack.setTag(compound);
      compound = nmsStack.getTag();
    }

    UUID uuid = UUID.randomUUID();

    long least = uuid.getMostSignificantBits();
    long most = uuid.getLeastSignificantBits();

    int[] test = new int[]{(int) (least >> 32), (int) least, (int) (most >> 32), (int) most};

    CompoundTag skullOwner = new CompoundTag();
    skullOwner.putIntArray("Id", test);
    CompoundTag properties = new CompoundTag();
    CompoundTag value = new CompoundTag();
    value.putString("Value", textureValue);
    CompoundTag textures = new CompoundTag();
    textures.merge(value);
    properties.put("textures", textures);
    skullOwner.put("Properties", properties);

    compound.put("SkullOwner", skullOwner);
    nmsStack.setTag(compound);

//        createSkullItem(CraftItemStack.asBukkitCopy(nmsStack), "");

    ItemStack itemStack = CraftItemStack.asBukkitCopy(nmsStack);
    ItemMeta meta = itemStack.getItemMeta();
    meta.setDisplayName(displayName);
    itemStack.setItemMeta(meta);

    return itemStack;
  }

  @Override
  public ItemStack getEros() {
    return createNMSSkullItem(PluginConstants.EROS_TEXTURE_VALUE, "&eEros");
  }

  @Override
  public ItemStack getSilvia() {
    return createNMSSkullItem(PluginConstants.SILVIA_TEXTURE_VALUE, "&eSilvia");
  }

  @Override
  public ItemStack getRandomLovebird() {
    if (Math.random() > 0.5) {
      return getEros();
    } else {
      return getSilvia();
    }
  }

  @Override
  public void openBook(JavaPlugin plugin, Player player, ItemStack book) {
    int slot = player.getInventory().getHeldItemSlot();
    ItemStack old = player.getInventory().getItem(slot);
    player.getInventory().setItem(slot, book);
    new BukkitRunnable() {

      @Override
      public void run() {
        ClientboundOpenBookPacket packet = new ClientboundOpenBookPacket(InteractionHand.MAIN_HAND);
        ((CraftPlayer) player).getHandle().connection.send(packet);

        player.getInventory().setItem(slot, old);
      }
    }.runTaskLater(plugin, 1);
  }

  private ItemStack hideAttribute(ItemStack item, ItemFlag flag) {
    ItemMeta meta = item.getItemMeta();
    meta.addItemFlags(flag);
    item.setItemMeta(meta);
    return item;
  }

  private ItemStack hideAttributes(ItemStack item, ItemFlag... flags) {
    ItemMeta meta = item.getItemMeta();
    meta.addItemFlags(flags);
    item.setItemMeta(meta);
    return item;
  }

  private ItemStack hideAllAttributes(ItemStack item) {
    ItemMeta meta = item.getItemMeta();
    meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
    meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
    meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
    meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
    meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
    meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
    item.setItemMeta(meta);
    return item;
  }

  private List<String> colorizeLore(List<String> lore) {
    List<String> newLore = new ArrayList<>();

    for (String s : lore) {
      newLore.add(ChatUtils.colorChat(s));
    }

    return newLore;
  }

}
