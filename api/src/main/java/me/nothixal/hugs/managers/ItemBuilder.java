package me.nothixal.hugs.managers;

import com.cryptomorin.xseries.XMaterial;
import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class ItemBuilder {

  private ItemStack item;

  public ItemBuilder(ItemStack item) {
    this.item = item.clone();
  }

  public ItemBuilder(Material material) {
    item = new ItemStack(material);
  }

  public ItemBuilder(XMaterial material) {
    item = new ItemStack(material.parseMaterial());
  }

  public ItemBuilder setAmount(int amount) {
    item.setAmount(amount);
    return this;
  }

  public ItemBuilder setDisplayName(String displayName) {
    ItemMeta meta = getItemMeta();
    meta.setDisplayName(ChatUtils.colorChat(displayName));
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder setLocalizedName(String localizedName) {
    ItemMeta meta = getItemMeta();
    meta.setLocalizedName(localizedName);
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder setLore(List<String> lore) {
    ItemMeta meta = getItemMeta();
    meta.setLore(colorizeLore(lore));
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder setLore(String... lore) {
    ItemMeta meta = getItemMeta();
    meta.setLore(colorizeLore(lore));
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder setLore(String lore) {
    List<String> loreList = new ArrayList<>();
    loreList.add(lore);
    ItemMeta meta = getItemMeta();
    meta.setLore(loreList);
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder setColor(Color color) {
    LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
    meta.setColor(color);
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder setGlowing(boolean glow) {
    if (glow) {
      addEnchant(Enchantment.KNOCKBACK, 1);
      addItemFlag(ItemFlag.HIDE_ENCHANTS);
    } else {
      ItemMeta meta = getItemMeta();
      for (Enchantment enchantment : meta.getEnchants().keySet()) {
        meta.removeEnchant(enchantment);
      }
    }
    return this;
  }

  public ItemBuilder setUnbreakable(boolean unbreakable) {
    ItemMeta meta = item.getItemMeta();
    meta.setUnbreakable(unbreakable);
    item.setItemMeta(meta);
    return this;
  }

  public ItemBuilder setBannerColor(DyeColor color) {
    BannerMeta meta = (BannerMeta) item.getItemMeta();
    meta.setBaseColor(color);
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder setHead(String owner) {
    SkullMeta meta = (SkullMeta) item.getItemMeta();
    meta.setOwner(owner);
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder setOwningPlayer(OfflinePlayer player) {
    SkullMeta meta = (SkullMeta) getItemMeta();
    meta.setOwningPlayer(player);
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder setItemStack(ItemStack stack) {
    this.item = stack;
    return this;
  }

  public ItemBuilder addEnchant(Enchantment enchantment, int level) {
    ItemMeta meta = getItemMeta();
    meta.addEnchant(enchantment, level, true);
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder addItemFlag(ItemFlag flag) {
    ItemMeta meta = getItemMeta();
    meta.addItemFlags(flag);
    setItemMeta(meta);
    return this;
  }

  private ItemBuilder hideAttribute(ItemFlag flag) {
    ItemMeta meta = getItemMeta();
    meta.addItemFlags(flag);
    setItemMeta(meta);
    return this;
  }

  private ItemBuilder hideAttributes(ItemFlag... flags) {
    ItemMeta meta = getItemMeta();
    meta.addItemFlags(flags);
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder hideAllAttributes() {
    ItemMeta meta = getItemMeta();
    meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
    meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
    meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
    meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
    meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
    meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
    setItemMeta(meta);
    return this;
  }

  public ItemBuilder addPersistentData(NamespacedKey namespacedKey, String value) {
    ItemMeta meta = getItemMeta();
    PersistentDataContainer pdc = meta.getPersistentDataContainer();
    pdc.set(namespacedKey, PersistentDataType.STRING, value);

    setItemMeta(meta);
    return this;
  }

  public ItemBuilder addPersistentData(NamespacedKey namespacedKey, double value) {
    ItemMeta meta = getItemMeta();
    PersistentDataContainer pdc = meta.getPersistentDataContainer();
    pdc.set(namespacedKey, PersistentDataType.DOUBLE, value);

    setItemMeta(meta);
    return this;
  }

  public ItemBuilder addPersistentData(NamespacedKey namespacedKey, byte value) {
    ItemMeta meta = getItemMeta();
    PersistentDataContainer pdc = meta.getPersistentDataContainer();
    pdc.set(namespacedKey, PersistentDataType.BYTE, value);

    setItemMeta(meta);
    return this;
  }

  public ItemBuilder addPersistentData(NamespacedKey namespacedKey, float value) {
    ItemMeta meta = getItemMeta();
    PersistentDataContainer pdc = meta.getPersistentDataContainer();
    pdc.set(namespacedKey, PersistentDataType.FLOAT, value);

    setItemMeta(meta);
    return this;
  }

  public ItemMeta getItemMeta() {
    return item.getItemMeta();
  }

  public ItemBuilder setItemMeta(ItemMeta meta) {
    item.setItemMeta(meta);
    return this;
  }

  public ItemStack build() {
    return item;
  }


  // Private Methods
  private List<String> colorizeLore(List<String> lore) {
    List<String> newLore = new ArrayList<>();

    for (String s : lore) {
      newLore.add(ChatUtils.colorChat(s));
    }

    return newLore;
  }

  private List<String> colorizeLore(String... lore) {
    List<String> newLore = new ArrayList<>();

    for (String s : lore) {
      newLore.add(ChatUtils.colorChat(s));
    }

    return newLore;
  }

}
