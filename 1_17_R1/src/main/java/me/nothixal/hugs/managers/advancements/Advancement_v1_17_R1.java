package me.nothixal.hugs.managers.advancements;

import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.enums.AdvancementBackground;
import me.nothixal.hugs.utils.json.JSONUtils;
import me.nothixal.hugs.utils.text.TextUtils;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementDisplay;
import net.minecraft.advancements.AdvancementFrameType;
import net.minecraft.advancements.AdvancementProgress;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.CriterionInstance;
import net.minecraft.advancements.critereon.LootSerializationContext;
import net.minecraft.network.chat.IChatBaseComponent.ChatSerializer;
import net.minecraft.network.protocol.game.PacketPlayOutAdvancements;
import net.minecraft.resources.MinecraftKey;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_17_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Advancement_v1_17_R1 implements AdvancementManager {

  @Override
  public void sendToast(ItemStack item, UUID uuid, String title, String description) {
    Player player = Bukkit.getPlayer(uuid);

    MinecraftKey pluginKey = new MinecraftKey("hugs.plugin", "notification");

    AdvancementRewards advRewards = new AdvancementRewards(0, new MinecraftKey[0], new MinecraftKey[0], null);

    Map<String, Criterion> advCriteria = new HashMap<>();
    String[][] advRequirements;

    advCriteria.put("for_free", new Criterion(new CriterionInstance() {

      @Override
      public JsonObject a(LootSerializationContext lootSerializationContext) {
        return null;
      }

      @Override
      public MinecraftKey a() {
        return new MinecraftKey("minecraft", "impossible");
      }
    }));

    ArrayList<String[]> fixedRequirements = new ArrayList<>();

    fixedRequirements.add(new String[]{"for_free"});

    advRequirements = Arrays.stream(fixedRequirements.toArray()).toArray(String[][]::new);

    AdvancementDisplay display = new AdvancementDisplay(CraftItemStack.asNMSCopy(item),
        ChatSerializer.a(JSONUtils.formatSimpleJSON(TextUtils.colorText(title))),
        ChatSerializer.a(JSONUtils.formatSimpleJSON(TextUtils.colorText(description))),
        new MinecraftKey(AdvancementBackground.SNOW.getValue()),
        AdvancementFrameType.c, true, true, true);
    Advancement advancement = new Advancement(pluginKey, null, display, advRewards, advCriteria, advRequirements);

    HashMap<MinecraftKey, AdvancementProgress> prg = new HashMap<>();

    AdvancementProgress advPrg = new AdvancementProgress();
    advPrg.a(advCriteria, advRequirements);
    advPrg.getCriterionProgress("for_free").b();
    prg.put(pluginKey, advPrg);

    PacketPlayOutAdvancements packet = new PacketPlayOutAdvancements(false, Collections.singletonList(advancement), new HashSet<>(), prg);
    ((CraftPlayer) player).getHandle().b.sendPacket(packet);

    HashSet<MinecraftKey> rm = new HashSet<>();
    rm.add(pluginKey);
    prg.clear();
    packet = new PacketPlayOutAdvancements(false, new ArrayList<>(), rm, prg);
    ((CraftPlayer) player).getHandle().b.sendPacket(packet);
  }

  @Override
  public void sendToast(UUID uuid, String title, String description) {

  }

  @Override
  public void sendToast(Player player, String title, String description) {

  }

  @Override
  public void sendToast(String playerName, String title, String description) {

  }

  @Override
  public void sendGoalToast(ItemStack item, UUID uuid, String title, String description) {

  }

  @Override
  public void sendChallengeToast(ItemStack item, UUID uuid, String title, String description) {
    Player player = Bukkit.getPlayer(uuid);

    MinecraftKey notName = new MinecraftKey("hugs.plugin", "notification");

    AdvancementRewards advRewards = new AdvancementRewards(0, new MinecraftKey[0], new MinecraftKey[0], null);

    Map<String, Criterion> advCriteria = new HashMap<>();
    String[][] advRequirements;

    advCriteria.put("for_free", new Criterion(new CriterionInstance() {

      @Override
      public JsonObject a(LootSerializationContext lootSerializationContext) {
        return null;
      }

      @Override
      public MinecraftKey a() {
        return new MinecraftKey("minecraft", "impossible");
      }
    }));

    ArrayList<String[]> fixedRequirements = new ArrayList<>();

    fixedRequirements.add(new String[]{"for_free"});

    advRequirements = Arrays.stream(fixedRequirements.toArray()).toArray(String[][]::new);

    AdvancementDisplay display = new AdvancementDisplay(CraftItemStack.asNMSCopy(item),
        ChatSerializer.a(JSONUtils.formatSimpleJSON(TextUtils.colorText(title))),
        ChatSerializer.a(JSONUtils.formatSimpleJSON(TextUtils.colorText(description))),
        new MinecraftKey("minecraft:textures/block/dirt.png"),
        AdvancementFrameType.b, true, true, true);
    Advancement advancement = new Advancement(notName, null, display, advRewards, advCriteria, advRequirements);

    HashMap<MinecraftKey, AdvancementProgress> prg = new HashMap<>();

    AdvancementProgress advPrg = new AdvancementProgress();
    advPrg.a(advCriteria, advRequirements);
    advPrg.getCriterionProgress("for_free").b();
    prg.put(notName, advPrg);

    PacketPlayOutAdvancements packet = new PacketPlayOutAdvancements(false, Arrays.asList(advancement), new HashSet<>(), prg);
    ((CraftPlayer) player).getHandle().b.sendPacket(packet);
  }
}
