# ██████╗  ██████╗ ██╗     ██╗███████╗██╗  ██╗
# ██╔══██╗██╔═══██╗██║     ██║██╔════╝██║  ██║
# ██████╔╝██║   ██║██║     ██║███████╗███████║
# ██╔═══╝ ██║   ██║██║     ██║╚════██║██╔══██║
# ██║     ╚██████╔╝███████╗██║███████║██║  ██║
# ╚═╝      ╚═════╝ ╚══════╝╚═╝╚══════╝╚═╝  ╚═╝
#
# Available Placeholders:
#   Global: Can be used anywhere in this file. (Even in places it normally shouldn't be)
#     - EXAMPLE: %prefix%
#
#   Case Specific: Can only be used in certain messages. Supported placeholders are listed above the path name.
#     - EXAMPLES: %sender%, %target%, %permission%

# Internal placeholders used by the plugin.
# These placeholders are global and can be used anywhere in this file.
# You can also create your own custom placeholders as well!
# Just create a unique section name and use it anywhere you would like.
#
# EXAMPLE:
#  plugin_placeholders:
#    # This is a custom placeholder that I created. I can now use it anywhere by using %gui_prefix%
#    gui_prefix: "&3&lHugs &8&l>"
#
#    # This placeholder is just a color code. I can use it anywhere by using %cool_color%
#    cool_color: "#03A9F4"
plugin_placeholders:
  # The prefix for the plugin. Can be used globally using %prefix%.
  prefix: "&8[#03A9F4Hugs&8]"

  # An alternative prefix to be used in certain places. Can be used globally using %alt_prefix%.
  alt_prefix: "#03A9F4&lHugs &8&l>>"

  # The prefix for the plugin when showing errors. Can be used globally using %error_prefix%.
  error_prefix: "&8[&cHugs&8]"

  # The header that is put above a message. Can be used in message lists using %header%.
  header: '&8&l<&8&m&l-------&8&l<&3+&8&l>&m&l--------&8&l<<&3&l #03A9F4&lHugs &8&l>>&m&l--------&8&l<&3+&8&l>&m&l-------&8&l>'

  # The footer that is put below a message. Can be used in message lists using %footer%.
  footer: '&8&l<&8&m&l--------&8&l<&3+&8&l>&m&l---------&8&l<<&3+&8&l>>&m&l---------&8&l<&3+&8&l>&m&l--------&8&l>'

#
# Indicator Messages
#
indicators:
  # The messages that are sent upon sending and receiving hugs.
  chat_messages:
    # The message that you receive when enabling chat indicators with commands.
    # Supported Placeholders: N/A
    enabled: "%prefix% &aTeraz Będziesz Otrzymywał Indykatory Chatu."
    # The message that you receive when disabling chat indicators with commands.
    # Supported Placeholders: N/A
    disabled: "%prefix% &cJuż Nie Będziesz Otrzymywał Indykatorów Chatu."
    # The message that the sender receives upon giving a hug.
    # Supported Placeholders: %target%
    message_to_sender: '%prefix% &3Dałeś &7%target% &3Przytulasa!'
    # The message that the target receives upon receiving a hug.
    # Supported Placeholders: %sender%
    message_to_receiver: '%prefix% &3Zostałeś Przytulony Przez &7%sender%&3!'
    # The message that the sender receives if they hug themself.
    # Supported Placeholders: N/A
    self_hug: "%prefix% &7Dałeś Sobie Potrzebnego Przytulasa."

  # The title and subtitle that the target receives upon receiving a hug.
  # Only supports the %sender% placeholder in the title variable.
  title_messages:
    # The message that you receive when enabling title indicators with commands.
    # Supported Placeholders: N/A
    enabled: "%prefix% &aBędziesz Teraz Otrzymywał Indykatory Title."
    # The message that you receive when disabling title indicators with commands.
    # Supported Placeholders: N/A
    disabled: "%prefix% &cJuż Nie Będziesz Otrzymywał Indykatorów Title."

    # Self hug title messages.
    self_hug:
      title: "&3Przytuliłeś"
      subtitle: "&7Siebie."

    # Normal hug title messages.
    normal_hug:
      title: "&3%sender%"
      subtitle: "&7Przytulił ciebie."

    # Mass hug title messages.
    mass_hug:
      title: "&3%sender%"
      subtitle: "&7Przytulił Wszystkich!"

  actionbar_messages:
    # The message that you receive when enabling actionbar indicators with commands.
    # Supported Placeholders: N/A
    enabled: "%prefix% &aBędziesz Otrzymywał Indykatory Actionbar."
    # The message that you receive when disabling actionbar indicators with commands.
    # Supported Placeholders: N/A
    disabled: "%prefix% &cJuż Nie Będziesz Otrzymywał Indykatorów Actionbar."
    # The message that you receive when you hug yourself.
    # Supported Placeholders: N/A
    self_hug: "&7Dałeś sobie Potrzebnego Przytulasa."
    # The message that the target receives upon receiving a hug.
    # Supported Placeholders: %player%
    normal_hug: "&e%player% &7Dał Ci Ciepłego Przytulasa. Awww &c<3"
    # The message that the target 
    # Supported Placeholders: %player%
    mass_hug: "&e%player% &7Dał Każdemu Przytulasa!"

  bossbar_messages:
    # The message that you receive when enabling bossbar indicators with commands.
    # Supported Placeholders: N/A
    enabled: "%prefix% &aBędziesz Otrzymywał Indykatory BossBar."
    # The message that you receive when disabling bossbar indicators with commands.
    # Supported Placeholders: N/A
    disabled: "%prefix% &cJuż Nie Będziesz Otrzymywał Indykatorów BossBar."
    # Supported Placeholders: N/A
    self_hug: "&7Dałeś Sobie Ciepłego Przytulasa."
    # Supported Placeholders: %player%
    normal_hug: "&e%player% &7Dał Ci Ciepłego Przytulasa. Awww &c<3"
    # Supported Placeholders: %player%
    mass_hug: "&e%player% &7Dał Każdemu Przytulasa!"

  toast_messages:
    # The message that you receive when enabling toast indicators with commands.
    # Supported Placeholders: N/A
    enabled: "%prefix% &aBędziesz Otrzymywał Indykatory Toast."
    # The message that you receive when disabling toast indicators with commands.
    # Supported Placeholders: N/A
    disabled: "%prefix% &cJuż Nie Będziesz Otrzymywał Indykatorów Toast."
    # Supported Placeholders: N/A
    self_hug: "&7Dałeś Sobie Ciepłego Przytulasa."
    # Supported Placeholders: %player%
    normal_hug: "&e%player% &7Dał Ci Ciepłego Przytulasa. Awww &c<3"
    # Supported Placeholders: %player%
    mass_hug: "&e%player% &7Dał Wszystkim Przytulasa!"

#
# General Messages
#
messages:
  huggability:
    all_hugs: "%prefix% &aMożna Cie Przytulać!"
    no_hugs: "%prefix% &cJuż Nie Można Cie Przytulać!"
    realistic_hugs: "%prefix% &aMożesz Tylko Dawać Przytulasy Graczą Blisko Ciebie."

  menus:
    enabled: "%prefix% &aMenu Będą Użyte Zamiast Textu."
    disabled: "%prefix% &cMenu Już Nie Będą Pokazane. Text Będzie Używany Zamiast Menu."

  language_changed: "%prefix% &7Set language to &a%language%."

  # The messages that are broadcast to the server when someone gives a hug.
  broadcasts:
    # The broadcast that is sent when a player hugs themself.
    # Supported Placeholders: %sender%
    self_hugs: "%prefix% &7%sender% &3Przytulił Siebie."
    # The broadcast that is sent when a player hugs another player.
    # Supported Placeholders: %sender%, %target%
    normal_hugs: '%prefix% &7%sender% &3Dał &7%target% &3Ciepłego Przytulasa. Awww <3'
    # The broadcast that is sent when a player hugs everyone.
    # Supported Placeholders: %sender%
    mass_hugs: '%prefix% &7%sender% &3gave everyone a hug!'

  # The messages for data purges.
  confirmations:
    # Supported Placeholders: N/A
    confirmation_all_message:
      - "%prefix% &7Jesteś Pewien? To Usunie Statystyki Każdego Gracza!"
      - "&7Napisz \"tak\" By Kontynuować albo \"nie\" by anulować."
    # Supported Placeholders: %player%
    confirmation_message:
      - "%prefix% &7Jesteś Pewien? To Usunie Statystyki %player%!"
      - "&7Napisz \"tak\" By Kontynuować albo \"nie\" by anulować."
    # Supported Placeholders: N/A
    cancelled_purge: "%prefix% &7Anulował Usunięcie Daty."
    # Supported Placeholders: %target%
    player_data_purged: "%prefix% &7Statystyki &3%target% Zostały &cUsunięte&7!"
    # Supported Placeholders: N/A
    all_data_purged: "%prefix% &7Data Wszystkich Graczy Została &Usunięta&7!"

  # The message that the sender receives when they use the reload command.
  # Supported Placeholders: N/A
  reload_message:
    - "%header%"
    - "%prefix% &7Configuracja Przeładowana &7Pomyślnie."
    - "%footer%"

#
# Data Messages
#
data:
  # The messages that the sender receives when they use the /hugs total command.
  # Supported Placeholders: %self_hugs_total%, %normal_hugs_total%, %mass_hugs_total%
  total_hugs_message:
    - "%alt_prefix% #67cbf8Server Totals"
    - "%prefix% &3Sobie &3%self_hugs_total% &7Samo Przytulasy!"
    - "%prefix% &3%normal_hugs_total% &7Przytulasy!"
    - "%prefix% &3%mass_hugs_total% &7Masywne Przytulasy!"

  # The messages that appear when a player types /hugs info <player>.
  # Supported Placeholders: %total_hugs_given%, %total_hugs_received%, %total_mass_hugs_given%, %total_mass_hugs_received%
  your_profile:
    - "%alt_prefix% #67cbf8Viewing your profile."
    - "&7Samo Przytulasy: &b%total_self_hugs_given%"
    - "&7Przytulasy Dane: &e%total_hugs_given%"
    - "&7Przytulasy Otrzymane: &e%total_hugs_received%"
    - "&7Masywne Przytulasy Dane: &6%total_mass_hugs_given%"
    - "&7Masywne Przytulasy Otrzymane: &6%total_mass_hugs_received%"
    - "&7Specjalne Przytulasy Dane: &d%total_unique_hugs%"

  # The messages that appear when a player types /hugs info <player>.
  # Supported Placeholders: %total_hugs_given%, %total_hugs_received%, %total_mass_hugs_given%, %total_mass_hugs_received%
  player_profile:
    - "%alt_prefix% #67cbf8Viewing %target%'s profile."
    - "&7Samo Przytulasy: &b%total_self_hugs_given%"
    - "&7Przytulasy Dane: &e%total_hugs_given%"
    - "&7Przytulasy Otrzymane: &e%total_hugs_received%"
    - "&7Masywne Przytulasy Dane: &6%total_mass_hugs_given%"
    - "&7Masywne Przytulasy Otrzymane: &6%total_mass_hugs_received%"
    - "&7SPecjalne Przytulasy Dane: &d%total_unique_hugs%"

  leaderboard:
    - "%alt_prefix% #67cbf8Leaderboard"
    - ""

#
# Error Messages
#
errors:
  # The message that the sender receives if they try to hug themselves, but have set their huggable preference to false.
  # Supported Placeholders: N/A
  self_not_huggable: "%error_prefix% &7Nie Możesz Siebie Przytulić Bo Wyłączyłeś Przytulasy!"

  # The message that the sender receives if they try to hug themself and the "allow_self_hugging" setting is set to false.
  # Supported Placeholders: N/A
  self_hug_disallowed: "%error_prefix% &c&lHmm &7więc... To Jest Wstydliwe, Coś Cie Powstrzymuje Od Przytulania Siebie"

  # The message that the sender receives if the receiver doesn't want hugs.
  # Supported Placeholders: %target%
  player_not_huggable: "%error_prefix% &6%target% &7Nie Chce Być Przytulany. :'( Powiedz Im żeby Włączyli Przytulanie!,"

  # The message that the sender receives if they haven't been hugged recently.
  # Supported Placeholders: N/A
  no_recent_hug_given: "%error_prefix% &c&lERROR: &7No one has given you a hug recently!"

  # The message that the sender receives when they check for a player's info and the player's name is longer than 16 characters.
  # Supported Placeholders: N/A
  invalid_player: "%error_prefix% &c&lERROR: &7Zły Nick. Nicki są, Maksymalnie, Długości 16 Liter."

  # The message that the sender receives if the name is spelled incorrectly or if the target is offline.
  # Supported Placeholders: %target%
  player_not_found: "%error_prefix% &6%target% &7Nie Mogą Być Znalezieni! Czy Są Online?"

  # The message that the sender receives if they are the only one online and try to send a mass hug.
  # Supported Placeholders: N/A
  not_enough_players: "%error_prefix% &c&lERROR: &7Muszą Być Chociaż 3 Gracze Online By Masowo Przytulić!"

  # The message that the sender recieves when a random hug could not be determined.
  # Supported Placeholders: N/A
  no_random_players: "%error_prefix% &c&lERROR: &7Nikt Nie Chce Być Przytulony. :("

  # The message that the sender receives if they type a subcommand incorrectly.
  # Supported Placeholders: N/A
  invalid_arguments: "%error_prefix% &7Napisz &b/hugs help &7Dla Pomocy !"

  # The message that the sender receives if they have too many arguments.
  # Supported Placeholders: N/A
  too_many_arguments: "%error_prefix% &c&lERROR: &7Zbyt Dużo Argumentów. Napisz &b/hugs help &7Dla Poprawnej Komendy!"

  # The message that the sender receives if they don't have enough arguments.
  # Supported Placeholders: N/A
  too_few_arguments: "%error_prefix% &c&lERROR: &7Za Mało Argumentów. Napisz &b/hugs help &7Dla Poprawnej Komendy!"

  # The message that the sender receives if they use /hugs info <player> and the receiver's data isn't found.
  # Supported Placeholders: %target%
  data_not_found: "%error_prefix% &c&lERROR: &7%target% Nie Ma Żadnych Statystyk. :'( \nJaką Część o \"Rozprowadź Miłość\" Nie Zrozumiałeś?"

  # The message that the sender receives if they do not have the correct permissions.
  # Supported Placeholders: %permission%
  no_permission: "%error_prefix% &cNie Masz Permisji Do Tej Komendy. Potrzebujesz &e%permission% &cBy Użyć Tej Komendy!"

  # The cooldown message sent to players on cooldown.
  # Supported Placeholders: %cooldown%
  cooldown_message: "%error_prefix% &cMusisz Poczekać &e%cooldown% &cSekund By Przytulić Kogoś"

  # The message sent to players when the server version is too old to support the GUIs
  server_version_too_old:
    - "&c&lERROR: &7Ten Server Nie Wspiera Wersji Tego Plugina."
    - "&7Poproś Admina O Zmienienie Ustawień &euse_help_menu &7to &cfalse&7."

#
# Eros Messages
#
eros:
  not_huggable:
    - "&7&o\"Pssst, Masz Wyłączone Przytulasy!"
    - "&7&oTe Ustawienia Inaczej Nie Mają Sensu!"

#
# Silvia Messages
#
silvia:
  settings_overridden:
    - "&7&o\"Twoje Ustawienia Zostały Przesłonięte."
    - "&7&oSkontaktuj Administratora By To Naprawił\""

#
# Experimental Messages
#
experimental:
  passive_mode: "%prefix% &3Tryb Pasywny Jest Włączony. Nie Możesz Skrzywdzić Innych Graczy!"

#
# The command usage messages.
#
usages:
  hug: "&3&lProper Usage &8&l>> &b/hug <player>"
  info: "&3&lProper Usage &8&l>> &b/hugs info <player>"
  total: "&3&lProper Usage &8&l>> &b/hugs total"
  purge: "&3&lProper Usage &8&l>> &b/hugs purge <player>"

output:
  base_command:
    - "%alt_prefix% #03A9F4Rozprowadź Miłość."
    - "%prefix% &7Version: &f%version%"
    - "%prefix% &7Developed By: #03A9F4%author%"
    - "%prefix% &7Type #03A9F4%help_command% &7Dla Wszystkich Komend Plugina!"

#
# GUI Titles
#
menu_titles:
  # Main Menu
  help_menu: "%alt_prefix% &8Menu Pomocy"

  # Plugin Menus
  eros_stats_menu: "%alt_prefix% &8Eros"
  silvia_stats_menu: "%alt_prefix% &8Silvia"

  leaderboard_menu: "%alt_prefix% &8Tabela Wyników"
  plugin_archives_menu: "%alt_prefix% &8Archivy"
  commands_menu: "%alt_prefix% &8Komendy"

  # Player Panel
  your_stats_menu: "%alt_prefix% &8Twoje Statystyki"
  player_stats_menu: "%alt_prefix% &8Statystyki Gracza"
  global_stats_menu: "%alt_prefix% &8Globalne Statystyki"
  your_preferences_menu: "%alt_prefix% &8Twoje Preferencje"
  your_visuals_menu: "%alt_prefix% &8Your Visuals"
  your_sounds_menu: "%alt_prefix% &8Twoje Dźwięki"
  your_indicators_menu: "%alt_prefix% &8Twój Indykator"

  # Admin Panel
  admin_panel: "%alt_prefix% &8Panel Admina"
  general_settings: "%alt_prefix% &8Ogólne Ustawienia"
  advanced_settings: "%alt_prefix% &8Zaawansowane Ustawienia"
  technical_settings: "%alt_prefix% &8Techniczne Ustawienia"
  defaults: "%alt_prefix% &8Domyślne"
  overrides: "%alt_prefix% &8Nadpisy"
  experimental_settings: "%alt_prefix% &8Experymentalne Ustawienia"

  select_language: "%alt_prefix% &8Select Language"

#
# GUI Items
#
common_items:
  close_menu: "&cZamknij Menu"
  go_back: "&ePowrót"
  next_page: "&aNastępna Strona"
  previous_page: "&aPoprzednia Strona"
  switch_filter: "&eKliknij By Zmienić Filter!"
  toggle: "&eKliknij By Włączyć."
  empty_space: "&cPusto"
  coming_soon: "&7Wkrótce"

  normal_hugs_given: "&ePrzytulasy Dane"
  normal_hugs_received: "&ePrzytulasy Otrzymano"
  mass_hugs_given: "&ePrzytulasy Masowo Dano"
  mass_hugs_received: "&ePrzytulasy Masowo Otrzymano"
  self_hugs: "&eSamo Przytulasy"
  unique_hugs: "&eSpecjalne Przytulasy"

  first_hug:
    title: "&ePierwszy Przytulas"
    lore:
      - "&7Date: &f%date%"
      - "&7Given To: &b%target%"

  no_first_hug:
    title: "&eBrak Pierwszego Przytulasa"
    lore:
      - "&7Dano: &bNikomu"

#
# GUI Menus
#
menus:
  # Fully Supported
  help_menu:
    leaderboards:
      title: "&aTabela Wyników"
      lore: "&7Kliknij By Zobaczyć Ranking."
    global_stats:
      title: "&aStatystyki Servera"
      lore: "&7Kliknij By Zobaczyć."
    your_stats:
      title: "&aTwoje Statystyki"
      lore: "&7Kliknij By Zobaczyć Swoje Statystyki."
    achievements:
      title: "&aOsiągniecia"
      lore: "&7Wkrótce"
    language_select:
      title: "&aUstaw Język"
      lore: "&7Wkrótce"
    plugin_archives:
      title: "&aArchiwy Pluginów"
      lore: "&7Kliknij By Zobaczyć Info."
    commands:
      title: "&aKomendy"
      lore: "&7Kliknij By Zobaczyć Komendy."
    your_preferences:
      title: "&aTwoje Preferencje"
      lore: "&7Kliknij By Ustawić Preferencje."
    admin_panel:
      title: "&aPanel Admina"
      lore: "&7Kliknij By Ustawić."
    donations:
      title: "&dDonacje"
      lore: "&7Kliknij By Zdonejtować!"

  # Not Supported Yet
  leaderboard_menu:
    top_item:
      title: "&aTabela Wyników"
      lore:
        - ""

    no_normal_hugs_given: "&cNikt Nikomu Nie Dał Przytulasa! :("
    no_normal_hugs_received: "&cNikt Nie Dostał Przytulasa! :("
    no_mass_hugs_given: "&cNikt Nie Dał Masywnego Przytulasa! :("
    no_mass_hugs_received: "&cNikt Nie Dostał Masywnego Przytulasa! :("
    no_self_hugs_given: "&cNikt Siebie Nie Przytulił! :("

    global_total:
      title: "&eGlobalne Statystyki"
      lore: ""

    change_filter:
      title: "&aZmień Filter"
      lore:
        - ""
        - ""
        - ""

  # Fully Supported
  global_stats_menu:
    top_item:
      title: "&aGlobalne Statystyki"
      lore:
        - "&7Samo Przytulasy: &e%self_given%"
        - "&7Przytulasy Dane: &e%normal_given%"
        - "&7Masywne Prztulasy Dane: &6%mass_given%"
        - "&7Specjalne Przytulasy: &dWIP"

  # Fully Supported
  your_stats_menu:
    top_item:
      title: "&aTwoje Statystki"
      lore:
        - "&7Samo Przytulasy: &b%self_given%"
        - ""
        - "&7Przytulasy Dane: &e%normal_given%"
        - "&7Przytulasy Otrzymane: &e%normal_received%"
        - ""
        - "&7Masywne Przytulasy Dane: &6%mass_given%"
        - "&7Masywne Przytulasy Otrzymane: &6%mass_received%"
        - ""
        - "&7Specjalne Przytulasy: &d%unique_hugs%"

  # Partially Supported
  plugin_archives_menu:
    top_item:
      title: "&aArchivy"
      lore:
        - ""

    plugin_website:
      title: "&aStrona Plugina"
      lore:
        - "&7Podczas Odwiedzania, Prosze Daj"
        - "&7Pluginowi, &e✰✰✰✰✰ &7ocenę!"
        - ""
        - "&eKliknij By Odwiedzić Stronę!"
    wiki:
      title: "&aWiki"
      lore:
        - "&7Potrzebujesz Pomocy Z Configiem?"
        - "&7Nie Rozumiesz Wszystkich Ustawień?"
        - ""
        - "&eKliknij By Odwiedzić Wikipedię!"
    source_code:
      title: "&aKod Zródłowy"
      lore:
        - "&7Zainteresowany Jak Działa Plugin?"
        - "&7Zródło Jest Otwarte!"
        - ""
        - "&eKliknij By Odwiedzić GitLab Repo!"
    bug_tracker:
      title: "&aWykrywacz Bugów"
      lore:
        - "&7Znalazłeś Błąd?"
        - "&7Zgłoś Go Do Wykrywacza Bugów!"
        - ""
        - "&eKliknij By Odwiedzić Wykrywacza Bugów."
    discord:
      title: "&9Discord"
      lore:
        - "&7Potrzebna Pomoc Z Discorda?"
        - "&7Czy Chcesz Wychillować?"
        - "&7Dołącz Na Nasz Server Discord"
        - ""
        - "&eKliknij By Dołączyć Do Naszego Społeczeństwa!"
    donate:
      title: "&dDonacja"
      lore:
        - ""
        - ""

  # Not Supported Yet
  commands_menu:
    title: "%alt_prefix% Komendy"

  # Not Supported Yet
  your_preferences_menu:
    huggability_icon:
      title: "&aHuggability"
      lore:
        - "&7Adjust your interaction settings"
        - "&7for how you want to receive hugs."

    huggability:
      title: "&aHuggability"
      lore:
        - "%huggability_description%"
        - ""
        - "%huggability_states%"
        - ""
        - "&eClick to toggle."
      indicator:
        selected_format: "%color_prefix%➡ %display_name%"
        unselected_format: "%color_prefix%   %display_name%"
      states:
        all:
          color_prefix: "&a"
          display_name: "All"
          description:
            - "&7Everyone is able to give you hugs via"
            - "&7commands and personal interaction."
        realistic:
          color_prefix: "&d"
          display_name: "Realistic"
          description:
            - "&7Players are able to give you hugs"
            - "&7via personal interaction only."
        none:
          color_prefix: "&c"
          display_name: "None"
          description:
            - "&7No one is able to give you any"
            - "&7form of hugs at all. :("

  # Not Supported Yet
  your_indicators_menu:
    top_item:
      title: "&eYour Indicators"
      lore:
        - ""

    chat_icon:
      title_enabled: "&aChat Indicator"
      title_disabled: "&cChat Indicator"
      lore:
        - "&7Toggles whether you will"
        - "&7receive &echat &7notifications"
        - "&7when receiving hugs."

    chat_button:
      title_enabled: "&aChat Indicator"
      title_disabled: "&cChat Indicator"
      lore_enabled:
        - "&7Click to disable!"
      lore_disabled:
        - "&7Click to enable!"

    titles_icon:
      title_enabled: "&aTitles Indicator"
      title_disabled: "&cTitles Indicator"
      lore:
        - "&7Toggles whether you will"
        - "&7receive &etitle &7notifications"
        - "&7when receiving hugs."

    titles_button:
      title_enabled: "&aTitles Indicator"
      title_disabled: "&cTitles Indicator"
      lore_enabled:
        - "&7Click to disable!"
      lore_disabled:
        - "&7Click to enable!"

    actionbar_icon:
      title_enabled: "&aActionbar Indicator"
      title_disabled: "&cActionbar Indicator"
      lore:
        - "&7Toggles whether you will"
        - "&7receive &eactionbar &7notifications"
        - "&7when receiving hugs."

    actionbar_button:
      title_enabled: "&aActionbar Indicator"
      title_disabled: "&cActionbar Indicator"
      lore_enabled:
        - "&7Click to disable!"
      lore_disabled:
        - "&7Click to enable!"

    bossbar_icon:
      title_enabled: "&aBossbar Indicator"
      title_disabled: "&cBossbar Indicator"
      lore:
        - "&7Toggles whether you will"
        - "&7receive &ebossbar &7notifications"
        - "&7when receiving hugs."

    bossbar_button:
      title_enabled: "&aBossbar Indicator"
      title_disabled: "&cBossbar Indicator"
      lore_enabled:
        - "&7Click to disable!"
      lore_disabled:
        - "&7Click to enable!"

    toasts_icon:
      title_enabled: "&aToast Indicator"
      title_disabled: "&cToast Indicator"
      lore:
        - "&7Toggles whether you will"
        - "&7receive &etoast &7notifications"
        - "&7when receiving hugs."

    toasts_button:
      title_enabled: "&aToast Indicator"
      title_disabled: "&cToast Indicator"
      lore_enabled:
        - "&7Click to disable!"
      lore_disabled:
        - "&7Click to enable!"

  # Not Supported Yet
  your_sounds_menu:
    top_item:
      title: "&eYour Sounds"
      lore:
        - ""
    menu_sounds_enabled_icon:
      title: "&aMenu Sounds"
      lore:
        - "&7Sounds played when clicking"
        - "&7on buttons in the GUIs."
    menu_sounds_disabled_icon:
      title: "&cMenu Sounds"
      lore:
        - "&7Sounds played when clicking"
        - "&7on buttons in the GUIs."
    menu_sounds_enabled_button:
      title: "&aMenu Sounds"
      lore:
        - "&7Click to disable!"
    menu_sounds_disabled_button:
      title: "&cMenu Sounds"
      lore:
        - "&7Click to enable!"

    command_sounds_enabled_icon:
      title: "&aCommand Sounds"
      lore:
        - "&7Sounds played when commands fail."
    command_sounds_disabled_icon:
      title: "&cCommand Sounds"
      lore:
        - "&7Sounds played when commands fail."
    command_sounds_enabled_button:
      title: "&aCommand Sounds"
      lore:
        - "&7Click to disable!"
    command_sounds_disabled_button:
      title: "&cCommand Sounds"
      lore:
        - "&7Click to enable!"

    hug_sounds_enabled_icon:
      title: "&aHug Sounds"
      lore:
        - "&7Sounds played when receiving hugs."
    hug_sounds_disabled_icon:
      title: "&cHug Sounds"
      lore:
        - "&7Sounds played when receiving hugs."
    hug_sounds_enabled_button:
      title: "&aHug Sounds"
      lore:
        - "&7Click to disable!"
    hug_sounds_disabled_button:
      title: "&cHug Sounds"
      lore:
        - "&7Click to enable!"

  # Not Supported Yet
  your_visuals_menu:
    top_item:
      title: "&eVisual Preferences"
      lore:
        - ""

    particle_quality_icon:
      title: "%color_prefix%Particle Quality"
      lore:
        - "&7Settings related to"
        - "&7particle spawning."

    particle_quality:
      title: "&aParticle Quality"
      lore:
        - "%particle_quality_description%"
        - ""
        - "%particle_quality_states%"
        - ""
        - "&eClick to change quality!"
      indicator:
        selected_format: "%color_prefix%➡ %display_name%"
        unselected_format: "%color_prefix%   %display_name%"
      states:
        none:
          color_prefix: "&c"
          display_name: "Off"
          description:
            - "&7No particles will be displayed."
        low:
          color_prefix: "&e"
          display_name: "Low"
          description:
            - "&7Very few particles will be displayed."
        medium:
          color_prefix: "&2"
          display_name: "Medium"
          description:
            - "&7Some particles will be displayed."
        high:
          color_prefix: "&3"
          display_name: "High"
          description:
            - "&7A moderate amount of particles"
            - "&7will be displayed."
        extreme:
          color_prefix: "&d"
          display_name: "Extreme"
          description:
            - "&7A significant amount of particles"
            - "&7will be displayed."

  # Not Supported Yet
  admin_panel_menu:
    all_settings:
      title: "&aAll Settings"
      lore:
        - "&7Click to view settings."
    modify_player:
      title: "&aModify Player"
      lore:
        - "&7Select a player to modify!"
    select_language:
      title: "&aSelect Language"
      lore:
        - "&7Change the plugin's language."
    experimental_settings:
      title: "&aExperimental Settings"
      lore:
        - "&7Click to edit."


donations:
