package me.nothixal.hugs.guis;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Filter;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class LeaderboardGUI implements Listener {

  private final HugsPlugin plugin;
  private final Inventory inventory;
  private final ItemManager itemManager;

  public LeaderboardGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();

    String invName = Messages.LEADERBOARD_GUI_TITLE.getLangValue();
    this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    String displayName = plugin.getLangFile().getString("menus.leaderboard_menu.top_item.title");

    inventory.setItem(4, itemManager.createItem(XMaterial.ELYTRA, displayName));

    for (int i = 20; i < 24; i++) {
      inventory.setItem(20, itemManager.createItem(XMaterial.PLAYER_HEAD, ChatUtils.colorChat("&cLoading...")));
    }

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    inventory.setItem(51, getFilterItem(player));
    doWork(inventory, player);

    plugin.getGUIManager().getLeaderboardGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getLeaderboardGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();

    if (inv != plugin.getGUIManager().getLeaderboardGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (event.getSlot() == 49) {
      player.closeInventory();
      return;
    }

    if (event.getSlot() == 48) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }

    if (!item.hasItemMeta()) {
      return;
    }

    if (!item.getItemMeta().hasLore()) {
      return;
    }

    if (event.isLeftClick() && item.getType() == XMaterial.HOPPER.parseMaterial()) {
      if (plugin.getFilterList().containsKey(player.getUniqueId())) {
        plugin.getFilterList().put(player.getUniqueId(), plugin.getFilterList().get(player.getUniqueId()).getNext());
      } else {
        plugin.getFilterList().put(player.getUniqueId(), Filter.HUGS_RECEIVED);
      }

      doWork(plugin.getGUIManager().getLeaderboardGUIs().get(player.getUniqueId()), player);
      playSound(player, XSound.UI_BUTTON_CLICK, 2);
      return;
    }

    if (event.isRightClick() && item.getType() == XMaterial.HOPPER.parseMaterial()) {
      if (plugin.getFilterList().containsKey(player.getUniqueId())) {
        plugin.getFilterList().put(player.getUniqueId(), plugin.getFilterList().get(player.getUniqueId()).getPrevious());
      } else {
        plugin.getFilterList().put(player.getUniqueId(), Filter.SELF_GIVEN);
      }

      doWork(plugin.getGUIManager().getLeaderboardGUIs().get(player.getUniqueId()), player);
      playSound(player, XSound.UI_BUTTON_CLICK, 2);
      return;
    }
  }

  private ItemStack getFilterItem(Player player) {
    ItemStack item;
    Filter filter = plugin.getFilterList().get(player.getUniqueId());

    switch (filter) {
      case HUGS_RECEIVED:
        item = itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
            Arrays.asList(
                "",
                "&7Hugs Given",
                "&bHugs Received",
                "&7Mass Hugs Given",
                "&7Mass Hugs Received",
                "&7Self Hugs",
                "",
                "&eClick to switch filter!"
            ));
        break;
      case MASS_HUGS_GIVEN:
        item = itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
            Arrays.asList(
                "",
                "&7Hugs Given",
                "&7Hugs Received",
                "&bMass Hugs Given",
                "&7Mass Hugs Received",
                "&7Self Hugs",
                "",
                "&eClick to switch filter!"
            ));
        break;
      case MASS_HUGS_RECEIVED:
        item = itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
            Arrays.asList(
                "",
                "&7Hugs Given",
                "&7Hugs Received",
                "&7Mass Hugs Given",
                "&bMass Hugs Received",
                "&7Self Hugs",
                "",
                "&eClick to switch filter!"
            ));
        break;
      case SELF_GIVEN:
        item = itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
            Arrays.asList(
                "",
                "&7Hugs Given",
                "&7Hugs Received",
                "&7Mass Hugs Given",
                "&7Mass Hugs Received",
                "&bSelf Hugs",
                "",
                "&eClick to switch filter!"
            ));
        break;
      case HUGS_GIVEN:
      default:
        item = itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
            Arrays.asList(
                "",
                "&bHugs Given",
                "&7Hugs Received",
                "&7Mass Hugs Given",
                "&7Mass Hugs Received",
                "&7Self Hugs",
                "",
                "&eClick to switch filter!"
            ));
    }

    return item;
  }

  //TODO: Change what is being looped over.
  // This method ignores any storage option that the server owner has set.
  // The data should be grabbed from the PlayerDataManager
  private void doWork(Inventory inventory, Player player) {
    new BukkitRunnable() {

      @Override
      public void run() {
        File playerDataDirectory = new File(plugin.getFileManager().getPlayerDataDirectory());

        File[] files = playerDataDirectory.listFiles();

        if (files == null) {
          return;
        }

        Map<UUID, Integer> unSortedMap = new HashMap<>();

        for (File file : files) {
          if (file.isDirectory()) {
            continue;
          }

          if (!file.getName().endsWith(".yml")) {
            continue;
          }

          UUID uuid = UUID.fromString(file.getName().replace(".yml", ""));

          if (!plugin.getFilterList().containsKey(uuid)) {
            continue;
          }

          if (plugin.getFilterList().get(uuid) == Filter.HUGS_GIVEN) {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getHugsGiven(uuid));

            inventory.setItem(51, getFilterItem(player));

            test(inventory, player, unSortedMap, plugin.getLangFile().getString("menus.leaderboard_menu.no_normal_hugs_given"));
          } else if (plugin.getFilterList().get(uuid) == Filter.HUGS_RECEIVED) {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getHugsReceived(uuid));

            inventory.setItem(51, getFilterItem(player));

            test(inventory, player, unSortedMap, plugin.getLangFile().getString("menus.leaderboard_menu.no_normal_hugs_received"));
          } else if (plugin.getFilterList().get(uuid) == Filter.MASS_HUGS_GIVEN) {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getMassHugsGiven(uuid));

            inventory.setItem(51, getFilterItem(player));

            test(inventory, player, unSortedMap, plugin.getLangFile().getString("menus.leaderboard_menu.no_mass_hugs_given"));
          } else if (plugin.getFilterList().get(uuid) == Filter.MASS_HUGS_RECEIVED) {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getMassHugsReceived(uuid));

            inventory.setItem(51, getFilterItem(player));

            test(inventory, player, unSortedMap, plugin.getLangFile().getString("menus.leaderboard_menu.no_mass_hugs_received"));
          } else if(plugin.getFilterList().get(uuid) == Filter.SELF_GIVEN) {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getSelfHugs(uuid));

            inventory.setItem(51, getFilterItem(player));

            test(inventory, player, unSortedMap, plugin.getLangFile().getString("menus.leaderboard_menu.no_self_hugs_given"));
          } else {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getHugsGiven(uuid));
          }
        }
      }
    }.runTaskAsynchronously(plugin);
  }

  private void test(Inventory inventory, Player player, Map<UUID, Integer> unsortedMap, String noDataFound) {
    //LinkedHashMap preserves the ordering of elements in which they are inserted.
    LinkedHashMap<UUID, Integer> sortedMap = new LinkedHashMap<>();

    unsortedMap.entrySet()
        .stream()
        .limit(5)
        .sorted(Entry.<UUID, Integer>comparingByValue().reversed())
        .forEachOrdered(uuidIntegerEntry -> {
          if (uuidIntegerEntry.getValue() > 0) {
            sortedMap.put(uuidIntegerEntry.getKey(), uuidIntegerEntry.getValue());
          }
        });

    //System.out.println("Sorted Map   : " + sortedMap);

    int slot = 20;
    int ranking = 1;

    if (sortedMap.isEmpty()) {
      for (int i = 0; i < 5 - sortedMap.size(); i++) {
        inventory.setItem(slot++, itemManager
            .createItem(XMaterial.BARRIER, ChatUtils.colorChat(noDataFound), Collections.emptyList()));
      }

      return;
    }

    for (Entry<UUID, Integer> entry : sortedMap.entrySet()) {
      inventory.setItem(slot++, itemManager.createSkullItem(entry.getKey(),
          ChatUtils.colorChat("&a" + ranking++ + ". " + Bukkit.getServer().getOfflinePlayer(entry.getKey()).getName()),
          Collections.singletonList("&7" + plugin.getFilterList().get(player.getUniqueId()).getValue() + ": &e" + entry.getValue())));
    }

    if (sortedMap.size() < 5) {
      String displayName = plugin.getLangFile().getString(Messages.EMPTY_SPACE.getValue());

      for (int i = 0; i < 5 - sortedMap.size(); i++) {
        inventory.setItem(slot++, itemManager.createItem(XMaterial.PLAYER_HEAD, displayName));
      }
    }

    player.updateInventory();
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }

  private void playSound(Player player, XSound sound, int pitch) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, pitch);
    }
  }
}
