package me.nothixal.hugs.guis.stats;

import com.cryptomorin.xseries.XMaterial;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GlobalStatsGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public GlobalStatsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    String invName = Messages.GLOBAL_STATS_GUI_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

//    if (plugin.getPlayerDataManager().getDeveloperHugsGiven(player.getUniqueId()) > 0) {
//      inventory.setItem(2, itemManager.getEros(player.getUniqueId()));
//    }

    int normalHugs = plugin.getPlayerDataManager().getTotalHugsGiven();
    int massHugs = plugin.getPlayerDataManager().getTotalMassHugsGiven();
    int selfHugs = plugin.getPlayerDataManager().getTotalSelfHugsGiven();

    String name = plugin.getLangFile().getString("menus.global_stats_menu.top_item.title");
    List<String> lore = plugin.getLangFile().getStringList("menus.global_stats_menu.top_item.lore");
    List<String> updateLore = new ArrayList<>();

    for (String s : lore) {
      updateLore.add(s
          .replace("%self_given%", formatValue(selfHugs))
          .replace("%normal_given%", formatValue(normalHugs))
          .replace("%mass_given%", formatValue(massHugs))
      );
    }

    inventory.setItem(4, itemManager.createItem(XMaterial.SPRUCE_SIGN, name, updateLore));

    DecimalFormat numberFormat = new DecimalFormat("###,###");
    String displayName = plugin.getLangFile().getString(Messages.NORMAL_HUGS_GIVEN.getValue());
    inventory.setItem(20, itemManager.createItem(XMaterial.POPPY, displayName,
        Collections.singletonList("&7" + numberFormat.format(normalHugs))));

    displayName = plugin.getLangFile().getString(Messages.MASS_HUGS_GIVEN.getValue());
    inventory.setItem(21, itemManager.createItem(XMaterial.ROSE_BUSH, displayName,
        Collections.singletonList("&7" + numberFormat.format(massHugs))));

    displayName = plugin.getLangFile().getString(Messages.SELF_HUGS_GIVEN.getValue());
    inventory.setItem(22, itemManager.createItem(XMaterial.ARMOR_STAND, displayName,
        Collections.singletonList("&7" + numberFormat.format(selfHugs))));

    displayName = plugin.getLangFile().getString(Messages.UNIQUE_HUGS_GIVEN.getValue());
    inventory.setItem(24, itemManager.createItem(XMaterial.SUNFLOWER, displayName,
        Collections.singletonList(Messages.COMING_SOON.getLangValue())));

//    inventory.setItem(30, itemManager.createItem(XMaterial.RABBIT_HIDE, "&eReceived",
//        Collections.singletonList("&7Coming Soon")));
//
//    inventory.setItem(31, itemManager.createItem(XMaterial.LEATHER, "&eMass Received",
//        Collections.singletonList("&7Coming Soon")));

//    inventory.setItem(32, itemManager.createItem(XMaterial.SUNFLOWER, "&eComing Soon"));


//    inventory.setItem(32, itemManager.createItem(XMaterial.OAK_SAPLING, "&eFirst Hug",
//        Arrays.asList("&7Date: &f<Date>", "&7Given To: &b<First>")));

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    plugin.getGUIManager().getGlobalStatsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getGlobalStatsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getGlobalStatsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new HelpGUI(plugin).openGUI(player);
    }
  }

  public String formatValue(float value) {
    String[] arr = {"", "k", "M", "B", "T", "P", "E"};
    int index = 0;
    while ((value / 1000) >= 1) {
      value = value / 1000;
      index++;
    }
    DecimalFormat decimalFormat;

    if (index == 1) {
      decimalFormat = new DecimalFormat("#.##");
    } else {
      decimalFormat = new DecimalFormat("#.#");
    }

    return String.format("%s%s", decimalFormat.format(value), arr[index]);
  }
}
