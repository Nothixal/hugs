package me.nothixal.hugs.guis.settings.player;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.enums.preferences.HuggableState;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.Indicator;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class YourPreferencesGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public YourPreferencesGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    String invName = Messages.YOUR_PREFERENCES_GUI_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    inventory.setItem(4, itemManager.createSkullItem(player.getUniqueId(), "&e" + player.getName()));

    String displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    boolean overridden = plugin.getConfig().getBoolean(Settings.OVERRIDES.getValue());

    if (overridden) {
      setupOverriddenGUI(inventory);
    } else {
      setupDefaultGUI(player, inventory);
    }

    plugin.getGUIManager().getYourPreferencesGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getYourPreferencesGUIs().get(player.getUniqueId()));
  }

  private void setupOverriddenGUI(Inventory inventory) {
    ItemStack silvia = itemManager.createSkullItem(itemManager.getSilvia(), "&eSilvia", Arrays.asList(
        "&7&o\"Your settings have been overridden.",
        "&7&oContact an administrator to change it.\""));

    inventory.setItem(2, silvia);

    ItemStack huggable = itemManager.createItemGlowing(plugin.getItemStates().HUGGABLE_ENABLED_ICON, "&4Huggable",
        Collections.singletonList("&7Setting Overridden"));

    ItemStack particles = itemManager.createItemGlowing(XMaterial.POPPY, "&4Particles",
        Collections.singletonList("&7Setting Overridden"));

    ItemStack sounds = itemManager.createItemGlowing(XMaterial.BELL, "&4Sounds",
        Collections.singletonList("&7Setting Overridden"));

    ItemStack indicators = itemManager.createItemGlowing(XMaterial.PAINTING, "&4Indicators",
        Collections.singletonList("&7Setting Overridden"));

    ItemStack overriddenButton = itemManager.createItemGlowing(XMaterial.RED_GLAZED_TERRACOTTA, "&cOverridden");

    inventory.setItem(20, huggable);
    inventory.setItem(29, overriddenButton);

    inventory.setItem(22, particles);
    inventory.setItem(31, overriddenButton);

    inventory.setItem(23, sounds);
    inventory.setItem(32, overriddenButton);

    inventory.setItem(24, indicators);
    inventory.setItem(33, overriddenButton);
  }

  private void setupDefaultGUI(Player player, Inventory inventory) {
    HuggableState state = plugin.getPlayerDataManager().getHuggability(player.getUniqueId());

    if (state == HuggableState.ALL) {
      inventory.setItem(20, plugin.getItemStates().HUGGABILITY_ALL_ICON);
      inventory.setItem(29, plugin.getItemStates().HUGGABILITY_ALL_BUTTON);
    } else if (state == HuggableState.REALISTIC) {
      inventory.setItem(20, plugin.getItemStates().HUGGABILITY_REALISTIC_ICON);
      inventory.setItem(29, plugin.getItemStates().HUGGABILITY_REALISTIC_BUTTON);
    } else {
      inventory.setItem(20, plugin.getItemStates().HUGGABILITY_OFF_ICON);
      inventory.setItem(29, plugin.getItemStates().HUGGABILITY_OFF_BUTTON);
    }

    if (state == HuggableState.NONE) {
      List<String> erosLore = plugin.getLangFile().getStringList("eros.not_huggable");
      ItemStack eros = itemManager.createSkullItem(itemManager.getEros(), "&eEros", erosLore);
      inventory.setItem(2, eros);
    }

    String displayName = plugin.getLangFile().getString("menus.your_preferences_menu.visuals_icon.title");
    List<String> soundLore = plugin.getLangFile().getStringList("menus.your_preferences_menu.visuals_icon.lore");

    // Visual Settings
    inventory.setItem(22, plugin.getItemManager().createItem(XMaterial.PRISMARINE_CRYSTALS, displayName, soundLore));

    displayName = plugin.getLangFile().getString("menus.your_preferences_menu.visuals_button.title");
    soundLore = plugin.getLangFile().getStringList("menus.your_preferences_menu.visuals_button.lore");
    inventory.setItem(31, plugin.getItemManager().createItem(XMaterial.ORANGE_DYE, displayName, soundLore));

    // Sound Settings
    displayName = plugin.getLangFile().getString("menus.your_preferences_menu.sounds_icon.title");
    soundLore = plugin.getLangFile().getStringList("menus.your_preferences_menu.sounds_icon.lore");
    inventory.setItem(23, itemManager.createItem(XMaterial.BELL, displayName, soundLore));

    displayName = plugin.getLangFile().getString("menus.your_preferences_menu.sounds_button.title");
    soundLore = plugin.getLangFile().getStringList("menus.your_preferences_menu.sounds_button.lore");
    inventory.setItem(32, itemManager.createItem(XMaterial.ORANGE_DYE, displayName, soundLore));

    // Feedback Settings
    displayName = plugin.getLangFile().getString("menus.your_preferences_menu.your_indicators_icon.title");
    ItemStack indicatorItem = itemManager.createItemNoAttrib(XMaterial.GLOBE_BANNER_PATTERN, displayName);
    ItemMeta meta = indicatorItem.getItemMeta();

    List<Indicator> indicatorList = plugin.getPlayerDataManager().getIndicators(player.getUniqueId());
    indicatorList.sort(Comparator.comparing(v -> indicatorList.indexOf(v.getPriority())));

    List<String> indicatorLore = plugin.getLangFile().getStringList("menus.your_preferences_menu.your_indicators_icon.lore");
    List<String> updatedLore = new ArrayList<>();

    for (String s : indicatorLore) {
      for (Indicator indicator : indicatorList) {
        s = s.replace("%" + indicator.getType().toString().toLowerCase() + "_indicator%",
            indicator.isEnabled() ? " &a&l" + PluginConstants.CHECKMARK + " &a" : " &c&l" + PluginConstants.X + " &c");
      }

      updatedLore.add(s);
    }

    List<String> newLore = new ArrayList<>();

    for (String s : updatedLore) {
      newLore.add(ChatUtils.colorChat(s));
    }

    meta.setLore(newLore);
    indicatorItem.setItemMeta(meta);

    inventory.setItem(24, indicatorItem);

    displayName = plugin.getLangFile().getString("menus.your_preferences_menu.your_indicators_button.title");
    List<String> lore = plugin.getLangFile().getStringList("menus.your_preferences_menu.your_indicators_button.lore");
    inventory.setItem(33, itemManager.createItem(XMaterial.ORANGE_DYE, displayName, lore));
  }

//  private void setupNotHuggableGUI(Inventory inventory) {
//    ItemStack eros = itemManager.createSkullItem(itemManager.getEros(), "&eEros", Arrays.asList(
//        "&7&o\"Hey, you can't modify your settings",
//        "&7&obecause you're not huggable!\""));
//
//    inventory.setItem(2, eros);
//
////    if (dataManager.wantsHugs(player.getUniqueId())) {
////      inventory.setItem(20, plugin.getItemStates().HUGGABLE_ENABLED_ICON);
////      inventory.setItem(29, plugin.getItemStates().HUGGABLE_ENABLED_BUTTON);
////    } else {
//      inventory.setItem(20, plugin.getItemStates().HUGGABLE_OFF_ICON);
//      inventory.setItem(29, plugin.getItemStates().HUGGABLE_OFF_BUTTON);
////    }
//
//    ItemStack particles = itemManager.createItemGlowing(XMaterial.POPPY, "&4Particles",
//        Collections.singletonList("&7Setting Disabled"));
//
//    ItemStack sounds = itemManager.createItemGlowing(XMaterial.BELL, "&4Sounds",
//        Collections.singletonList("&7Setting Disabled"));
//
//    ItemStack indicators = itemManager.createItemGlowing(XMaterial.PAINTING, "&4Indicators",
//        Collections.singletonList("&7Setting Disabled"));
//
//    ItemStack unmodifiableButton = itemManager.createItemGlowing(XMaterial.RED_DYE, "&cUnmodifiable");
//
//    inventory.setItem(22, particles);
//    inventory.setItem(31, unmodifiableButton);
//
//    inventory.setItem(23, sounds);
//    inventory.setItem(32, unmodifiableButton);
//
//    inventory.setItem(24, indicators);
//    inventory.setItem(33, unmodifiableButton);
//  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getView().getTopInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getRawSlot();

    if (inv != plugin.getGUIManager().getYourPreferencesGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }

    boolean overridden = plugin.getConfig().getBoolean(Settings.OVERRIDES.getValue());
    if (overridden) {
      return;
    }

    // Wants Hugs Button
    if (slot == 20 || slot == 29) {
      if (event.isLeftClick()) {
        plugin.getPlayerDataManager().setHuggability(player.getUniqueId(), plugin.getPlayerDataManager().getHuggability(player.getUniqueId()).getNext());
        InventoryUtils.togglePair(inv, 20, 29, plugin.getItemStates().getHuggableItems());
      }

      if (event.isRightClick()) {
        plugin.getPlayerDataManager().setHuggability(player.getUniqueId(), plugin.getPlayerDataManager().getHuggability(player.getUniqueId()).getPrevious());
        InventoryUtils.togglePairReverse(inv, 20, 29, plugin.getItemStates().getHuggableItems());
      }

      openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK, 2);
      return;
    }

    // Particles Button
    if (slot == 22 || slot == 31) {
      new YourVisualsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);

//      if (event.isLeftClick()) {
//        plugin.getPlayerDataManager().increaseParticleQuality(player.getUniqueId());
//        InventoryUtils.togglePair(inv, 22, 31, plugin.getItemStates().getParticleSettingsItems());
//      }
//
//      if (event.isRightClick()) {
//        plugin.getPlayerDataManager().decreaseParticleQuality(player.getUniqueId());
//        InventoryUtils.togglePairReverse(inv, 22, 31, plugin.getItemStates().getParticleSettingsItems());
//      }
//
//      playSound(player, XSound.UI_BUTTON_CLICK, 2);
      return;
    }

    // Sounds Button
    if (slot == 23 || slot == 32) {
//      plugin.getPlayerDataManager().setWantsSounds(player.getUniqueId(), !plugin.getPlayerDataManager().wantsSounds(player.getUniqueId()));
//      InventoryUtils.togglePair(inv, 23, 32, plugin.getItemToggles().getSoundSettingsItems());
//      player.updateInventory();

      new YourSoundsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Your Indicators Button
    if (slot == 24 || slot == 33) {
      new YourIndicatorsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }

  private void playSound(Player player, XSound sound, int pitch) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, pitch);
    }
  }

}
