package me.nothixal.hugs.guis.adminpanel.overrides;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.io.IOException;
import java.util.Collections;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class OverrideIndicatorsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public OverrideIndicatorsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6,
        ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Overrides > Indicators"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.RED_STAINED_GLASS_PANE, " "));

    inventory.setItem(4, itemManager.createItemNoAttrib(XMaterial.FLOWER_BANNER_PATTERN, "&eIndicator Overrides"));

    boolean chat = plugin.getFileManager().getOverridesYMLData().getBoolean("overrides.indicators.chat");
    boolean titles = plugin.getFileManager().getOverridesYMLData().getBoolean("overrides.indicators.titles");
    boolean bossbar = plugin.getFileManager().getOverridesYMLData().getBoolean("overrides.indicators.bossbar");
    boolean actionbar = plugin.getFileManager().getOverridesYMLData().getBoolean("overrides.indicators.actionbar");
    boolean toasts = plugin.getFileManager().getOverridesYMLData().getBoolean("overrides.indicators.toasts");

    if (chat) {
      inventory.setItem(20, plugin.getItemStates().CHAT_INDICATOR_ENABLED_ICON);
      inventory.setItem(29, plugin.getItemStates().CHAT_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(20, plugin.getItemStates().CHAT_INDICATOR_DISABLED_ICON);
      inventory.setItem(29, plugin.getItemStates().CHAT_INDICATOR_DISABLED_BUTTON);
    }

    if (titles) {
      inventory.setItem(21, plugin.getItemStates().TITLES_INDICATOR_ENABLED_ICON);
      inventory.setItem(30, plugin.getItemStates().TITLES_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(21, plugin.getItemStates().TITLES_INDICATOR_DISABLED_ICON);
      inventory.setItem(30, plugin.getItemStates().TITLES_INDICATOR_DISABLED_BUTTON);
    }

    if (actionbar) {
      inventory.setItem(22, plugin.getItemStates().ACTIONBAR_INDICATOR_ENABLED_ICON);
      inventory.setItem(31, plugin.getItemStates().ACTIONBAR_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(22, plugin.getItemStates().ACTIONBAR_INDICATOR_DISABLED_ICON);
      inventory.setItem(31, plugin.getItemStates().ACTIONBAR_INDICATOR_DISABLED_BUTTON);
    }

    if (bossbar) {
      inventory.setItem(23, plugin.getItemStates().BOSSBAR_INDICATOR_ENABLED_ICON);
      inventory.setItem(32, plugin.getItemStates().BOSSBAR_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(23, plugin.getItemStates().BOSSBAR_INDICATOR_DISABLED_ICON);
      inventory.setItem(32, plugin.getItemStates().BOSSBAR_INDICATOR_DISABLED_BUTTON);
    }

    if (toasts) {
      inventory.setItem(24, plugin.getItemStates().TOAST_INDICATOR_ENABLED_ICON);
      inventory.setItem(33, plugin.getItemStates().TOAST_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(24, plugin.getItemStates().TOAST_INDICATOR_DISABLED_ICON);
      inventory.setItem(33, plugin.getItemStates().TOAST_INDICATOR_DISABLED_BUTTON);
    }

    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose"));

    plugin.getGUIManager().getOverridesSettingsIndicatorsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getOverridesSettingsIndicatorsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getOverridesSettingsIndicatorsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new OverridesGUI(plugin).openGUI(player);
      return;
    }

    FileConfiguration config = plugin.getFileManager().getOverridesYMLData();

    // Chat Indicator Check
    if (slot == 20 || slot == 29) {
      config.set("overrides.indicators.chat", !config.getBoolean("overrides.indicators.chat"));
      InventoryUtils.togglePair(inv, 20, 29, plugin.getItemStates().getChatIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Titles Indicator Check
    if (slot == 21 || slot == 30) {
      config.set("overrides.indicators.titles", !config.getBoolean("overrides.indicators.titles"));
      InventoryUtils.togglePair(inv, 21, 30, plugin.getItemStates().getTitlesIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Actionbar Indicator Check
    if (slot == 22 || slot == 31) {
      config.set("overrides.indicators.actionbar", !config.getBoolean("overrides.indicators.actionbar"));
      InventoryUtils.togglePair(inv, 22, 31, plugin.getItemStates().getActionbarIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Bossbar Indicator Check
    if (slot == 23 || slot == 32) {
      config.set("overrides.indicators.bossbar", !config.getBoolean("overrides.indicators.bossbar"));
      InventoryUtils.togglePair(inv, 23, 32, plugin.getItemStates().getBossbarIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Toast Indicator Check
    if (slot == 24 || slot == 33) {
      config.set("overrides.indicators.toasts", !config.getBoolean("overrides.indicators.toasts"));
      InventoryUtils.togglePair(inv, 24, 33, plugin.getItemStates().getToastIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }
  }

  @EventHandler
  public void onInventoryClose(InventoryCloseEvent event) {
    Inventory inv = event.getInventory();
    Player player = (Player) event.getPlayer();

    if (inv != plugin.getGUIManager().getOverridesSettingsIndicatorsGUIs().get(player.getUniqueId())) {
      return;
    }

    try {
      plugin.getFileManager().getOverridesYMLData().save(plugin.getFileManager().getOverridesYMLFile());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 2);
    }
  }
}
