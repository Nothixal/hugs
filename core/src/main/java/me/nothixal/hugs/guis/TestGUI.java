package me.nothixal.hugs.guis;

import me.nothixal.hugs.managers.guis.InventoryHandler;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;

public class TestGUI implements InventoryHandler {

  @Override
  public void onOpen(InventoryOpenEvent event) {
    event.getPlayer().sendMessage("You opened the Test GUI");
  }

  @Override
  public void onClick(InventoryClickEvent event) {
    int slot = event.getSlot();
    Player player = (Player) event.getWhoClicked();
    float pitch = 0.5F + slot * 0.1F;
    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1F, pitch);
  }

  @Override
  public void onClose(InventoryCloseEvent event) {
    event.getPlayer().sendMessage("You closed the Test GUI");
  }
}
