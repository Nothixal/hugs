package me.nothixal.hugs.guis;

import com.cryptomorin.xseries.XMaterial;
import java.io.File;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.utils.BookUtils;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PluginArchivesGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public PluginArchivesGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    String invName = Messages.PLUGIN_ARCHIVES_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    String displayName = plugin.getLangFile().getString("menus.plugin_archives_menu.top_item.title");

    inventory.setItem(4, itemManager.createItem(XMaterial.BOOKSHELF, displayName));

    displayName = plugin.getLangFile().getString("menus.plugin_archives_menu.plugin_website.title");
    List<String > lore = plugin.getLangFile().getStringList("menus.plugin_archives_menu.plugin_website.lore");

    // Row 3
    inventory.setItem(20, itemManager.createNMSSkullItem(PluginConstants.WEBSITE_TEXTURE, displayName, lore));

    displayName = plugin.getLangFile().getString("menus.plugin_archives_menu.wiki.title");
    lore = plugin.getLangFile().getStringList("menus.plugin_archives_menu.wiki.lore");

    inventory.setItem(21, itemManager.createNMSSkullItem(PluginConstants.WIKI_TEXTURE, displayName, lore));

    displayName = plugin.getLangFile().getString("menus.plugin_archives_menu.source_code.title");
    lore = plugin.getLangFile().getStringList("menus.plugin_archives_menu.source_code.lore");

    inventory.setItem(22, itemManager.createItem(XMaterial.REDSTONE, displayName, lore));

    displayName = plugin.getLangFile().getString("menus.plugin_archives_menu.bug_tracker.title");
    lore = plugin.getLangFile().getStringList("menus.plugin_archives_menu.bug_tracker.lore");

    inventory.setItem(23, itemManager.createNMSSkullItem(PluginConstants.BUG_TRACKER_TEXTURE, displayName, lore));

    displayName = plugin.getLangFile().getString("menus.plugin_archives_menu.discord.title");
    lore = plugin.getLangFile().getStringList("menus.plugin_archives_menu.discord.lore");

    inventory.setItem(24, itemManager.createNMSSkullItem(PluginConstants.DISCORD_TEXTURE, displayName, lore));

    inventory.setItem(31, getRandomDonateItem());

    // Row 6
    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    inventory.setItem(51, itemManager.createItem(XMaterial.KNOWLEDGE_BOOK, "&aInstalled Version",
        Arrays.asList(
            "&7This information is important when reporting bugs!",
            "",
            "&fServer: &a" + plugin.getVersionChecker().getServerVersion().getDisplayValue(),
            "&fPlugin Version: &a" + plugin.getDescription().getVersion(),
            "",
            "&eClick to output information.")));

    plugin.getGUIManager().getArchivesGUI().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getArchivesGUI().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getArchivesGUI().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    if (!item.hasItemMeta()) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 51) {
      player.closeInventory();
      sendPluginInfo(player);
      return;
    }

    if (slot == 31) {
      BookUtils.openDonateBook(plugin, player);
      return;
    }

    if (slot == 20) {
      BookUtils.showWebsiteBook(plugin, player);
      return;
    }

    if (slot == 21) {
      BookUtils.showWikiBook(plugin, player);
      return;
    }

    if (slot == 22) {
      BookUtils.showSourceCodeBook(plugin, player);
      return;
    }

    if (slot == 23) {
      BookUtils.showBugTrackerBook(plugin, player);
      return;
    }

    if (slot == 24) {
      BookUtils.showDiscordBook(plugin, player);
      return;
    }
  }

  private void sendPluginInfo(Player player) {
    String primaryColor = PluginConstants.DEFAULT_PREFIX_COLOR_HEX;
    String tertiaryColor = PluginConstants.TERTIARY_COLOR_HEX;
    String configPrefix = Messages.PREFIX.getLangValue() + " ";
    String prefix = primaryColor + "&lHugs &8&l>> ";
    String padding = "  ";

    String header = prefix + tertiaryColor + "Running " + primaryColor + "v" + plugin.getDescription().getVersion() + tertiaryColor + " by " + primaryColor + PluginConstants.PLUGIN_CREATOR;
    String version = configPrefix + "&f&bServer Version: &f" + plugin.getVersionChecker().getServerVersion().getDisplayValue();

    String storage = configPrefix + "&f&bStorage:";
    String storageType = configPrefix + padding + "&f- &3Type: &f" + plugin.getPlayerDataManager().getDatabaseType().getValue();
    String fileSize = configPrefix + padding + "&f- &3File Size: &a" + humanReadableByteCountSI(getFolderSize(plugin.getFileManager().getPlayerDataDirectory()));

    String hooks = configPrefix + "&bHooks:";

    List<String> pluginHooks = plugin.getHookManager().getHooks();

    player.sendMessage(ChatUtils.colorChat(header));
    player.sendMessage(ChatUtils.colorChat(version));
    player.sendMessage(ChatUtils.colorChat(storage));
    player.sendMessage(ChatUtils.colorChat(storageType));
    player.sendMessage(ChatUtils.colorChat(fileSize));
    player.sendMessage(ChatUtils.colorChat(hooks));

    if (pluginHooks.isEmpty()) {
      player.sendMessage(ChatUtils.colorChat(configPrefix + padding + "&f- &7None"));
    } else {
      for (String s : pluginHooks) {
        player.sendMessage(ChatUtils.colorChat(configPrefix + padding + "&7- &a" + s));
      }
    }
  }

  public static String humanReadableByteCountSI(long bytes) {
    if (-1000 < bytes && bytes < 1000) {
      return bytes + " B";
    }
    CharacterIterator ci = new StringCharacterIterator("kMGTPE");
    while (bytes <= -999_950 || bytes >= 999_950) {
      bytes /= 1000;
      ci.next();
    }
    return String.format("%.1f %cB", bytes / 1000.0, ci.current());
  }

  private static long getFolderSize(String path) {
    return getFolderSize(new File(path));
  }

  private static long getFolderSize(File folder) {
    long length = 0;

    // listFiles() is used to list the
    // contents of the given folder
    File[] files = folder.listFiles();

    // loop for traversing the directory
    for (File file : files) {
      if (file.isFile()) {
        length += file.length();
      } else {
        length += getFolderSize(file);
      }
    }
    return length;
  }

  private ItemStack getRandomDonateItem() {
    List<ItemStack> donateItems = new ArrayList<>();
    donateItems.add(itemManager.createItem(Material.RABBIT_STEW, "&dDonate",
        Collections.singletonList("&7Feed the developer!")));
    donateItems.add(itemManager.createItem(Material.HEART_OF_THE_SEA, "&dDonate",
        Collections.singletonList("&7Kindness Donation!")));
    donateItems.add(itemManager.createItem(Material.DRAGON_BREATH, "&dDonate",
        Collections.singletonList("&7Buy me a coffee!")));
//    donateItems.add(itemManager.createItem(Material.JUKEBOX, "&dDonate",
//        Collections.singletonList("&7Support me directly!")));
    return donateItems.get(new Random().nextInt(donateItems.size()));
  }

}
