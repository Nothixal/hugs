package me.nothixal.hugs.guis.adminpanel;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.guis.adminpanel.defaults.DefaultsGUI;
import me.nothixal.hugs.guis.adminpanel.overrides.OverridesGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ExperimentalSettingsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public ExperimentalSettingsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6,
        ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Experimental Settings"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createActiveHeader(inventory, 5,
        itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "),
        itemManager.createItem(XMaterial.LIGHT_BLUE_STAINED_GLASS_PANE, " "));

    inventory.setItem(0, itemManager.createItem(XMaterial.CHEST, "&aGeneral Settings"));
    inventory.setItem(1, itemManager.createItem(XMaterial.ENDER_CHEST, "&aAdvanced Settings"));
    inventory.setItem(2, itemManager.createItem(XMaterial.ANVIL, "&aTechnical Settings"));
    inventory.setItem(3, itemManager.createItem(XMaterial.KNOWLEDGE_BOOK, "&aDefaults"));
    inventory.setItem(4, itemManager.createItem(XMaterial.COMPARATOR, "&aOverrides"));
    inventory.setItem(5, itemManager.createItem(XMaterial.BREWING_STAND, "&aExperimental Settings"));

    if (plugin.getConfig().getBoolean(Settings.PASSIVE_MODE_ENABLED.getValue())) {
      inventory.setItem(29, plugin.getItemStates().PASSIVE_MODE_ENABLED_ICON);
      inventory.setItem(38, plugin.getItemStates().PASSIVE_MODE_ENABLED_BUTTON);
    } else {
      inventory.setItem(29, plugin.getItemStates().PASSIVE_MODE_DISABLED_ICON);
      inventory.setItem(38, plugin.getItemStates().PASSIVE_MODE_DISABLED_BUTTON);
    }

    if (plugin.getConfig().getBoolean(Settings.TEMPORARY_INVINCIBILITY_ENABLED.getValue())) {
      inventory.setItem(31, plugin.getItemStates().TEMPORARY_INVINCIBILITY_ENABLED_ICON);
      inventory.setItem(40, plugin.getItemStates().TEMPORARY_INVINCIBILITY_ENABLED_BUTTON);
    } else {
      inventory.setItem(31, plugin.getItemStates().TEMPORARY_INVINCIBILITY_DISABLED_ICON);
      inventory.setItem(40, plugin.getItemStates().TEMPORARY_INVINCIBILITY_DISABLED_BUTTON);
    }

    if (plugin.getConfig().getBoolean(Settings.REALISTIC_HUGS.getValue())) {
      inventory.setItem(33, plugin.getItemStates().REALISTIC_HUGS_ENABLED_ICON);
      inventory.setItem(42, plugin.getItemStates().REALISTIC_HUGS_ENABLED_BUTTON);
    } else {
      inventory.setItem(33, plugin.getItemStates().REALISTIC_HUGS_DISABLED_ICON);
      inventory.setItem(42, plugin.getItemStates().REALISTIC_HUGS_DISABLED_BUTTON);
    }

    String displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(45, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(46, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    plugin.getGUIManager().getExperimentalSettingsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getExperimentalSettingsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getExperimentalSettingsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 45) {
      player.closeInventory();
      return;
    }

    if (slot == 46) {
      new AdminPanelGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 0) {
      new GeneralSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 1) {
      new AdvancedSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 2) {
      new TechnicalSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 3) {
      new DefaultsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 4) {
      new OverridesGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 5) {
      new ExperimentalSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 29 || slot == 38) {
      plugin.getConfig().set(Settings.PASSIVE_MODE_ENABLED.getValue(), !plugin.getConfig().getBoolean(Settings.PASSIVE_MODE_ENABLED.getValue()));
      plugin.saveConfig();
      InventoryUtils.togglePair(inv, 29, 38, plugin.getItemStates().getPassiveModeItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 31 || slot == 40) {
      plugin.getConfig().set(Settings.TEMPORARY_INVINCIBILITY_ENABLED.getValue(), !plugin.getConfig().getBoolean(Settings.TEMPORARY_INVINCIBILITY_ENABLED.getValue()));
      plugin.saveConfig();
      InventoryUtils.togglePair(inv, 31, 40, plugin.getItemStates().getTemporaryInvincibilityItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 33 || slot == 42) {
      plugin.getConfig().set(Settings.REALISTIC_HUGS.getValue(), !plugin.getConfig().getBoolean(Settings.REALISTIC_HUGS.getValue()));
      plugin.saveConfig();
      InventoryUtils.togglePair(inv, 33, 42, plugin.getItemStates().getRealisticHugsItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }

}
