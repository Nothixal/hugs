package me.nothixal.hugs.guis.stats;

import com.cryptomorin.xseries.XMaterial;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PlayerStatsGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public PlayerStatsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player, OfflinePlayer target) {
    String invName = Messages.PLAYER_STATS_GUI_TITLE.getLangValue().replace("%target%", target.getName());
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    inventory.setItem(4, itemManager.createSkullItem(target.getUniqueId(), "&e" + target.getName() + "'s Stats"));

    DecimalFormat numberFormat = new DecimalFormat("###,###");

    String displayName = plugin.getLangFile().getString(Messages.NORMAL_HUGS_GIVEN.getValue());
    inventory.setItem(21, itemManager.createItem(XMaterial.POPPY, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getHugsGiven(target.getUniqueId())))));

    displayName = plugin.getLangFile().getString(Messages.MASS_HUGS_GIVEN.getValue());
    inventory.setItem(22, itemManager.createItem(XMaterial.ROSE_BUSH, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getMassHugsGiven(target.getUniqueId())))));

    displayName = plugin.getLangFile().getString(Messages.SELF_HUGS_GIVEN.getValue());
    inventory.setItem(23, itemManager.createItem(XMaterial.ARMOR_STAND, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getSelfHugs(target.getUniqueId())))));

    displayName = plugin.getLangFile().getString(Messages.NORMAL_HUGS_RECEIVED.getValue());
    inventory.setItem(30, itemManager.createItem(XMaterial.RABBIT_HIDE, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getHugsReceived(target.getUniqueId())))));

    displayName = plugin.getLangFile().getString(Messages.MASS_HUGS_RECEIVED.getValue());
    inventory.setItem(31, itemManager.createItem(XMaterial.LEATHER, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getMassHugsReceived(target.getUniqueId())))));

    Date firstHugGiven = new Date(plugin.getPlayerDataManager().getFirstHugGivenTimestamp(target.getUniqueId()));
    SimpleDateFormat format = new SimpleDateFormat("MMM d, yyyy");

    if (plugin.getPlayerDataManager().getFirstHugGivenTimestamp(player.getUniqueId()) == 0L) {
      displayName = plugin.getLangFile().getString("common_items.no_first_hug.title");
      List<String> originalLore = plugin.getLangFile().getStringList("common_items.no_first_hug.lore");

      inventory.setItem(32, itemManager.createItem(XMaterial.OAK_SAPLING, displayName, originalLore));
    } else {
      String playerName = Bukkit.getOfflinePlayer(UUID.fromString(plugin.getPlayerDataManager().getFirstHugGivenTo(target.getUniqueId()))).getName();
      displayName = plugin.getLangFile().getString("common_items.first_hug.title");
      List<String> originalLore = plugin.getLangFile().getStringList("common_items.first_hug.lore");
      List<String> updatedLore = new ArrayList<>();

      for (String s : originalLore) {
        updatedLore.add(s
            .replace("%date%", format.format(firstHugGiven))
            .replace("%target%", playerName));
      }

      inventory.setItem(32, itemManager.createItem(XMaterial.OAK_SAPLING, displayName, updatedLore));
    }

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    plugin.getGUIManager().getPlayerStatsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getPlayerStatsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getPlayerStatsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new HelpGUI(plugin).openGUI(player);
    }
  }

  private void acknowledgePlayer(Player player, Inventory inventory) {
    // Has contributed to the project.

    // Has hugged the developer before.

    // Is the top player on one of the leaderboards.
  }

}
