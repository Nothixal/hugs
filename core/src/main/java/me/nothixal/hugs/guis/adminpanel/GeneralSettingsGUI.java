package me.nothixal.hugs.guis.adminpanel;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.guis.adminpanel.defaults.DefaultsGUI;
import me.nothixal.hugs.guis.adminpanel.overrides.OverridesGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GeneralSettingsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public GeneralSettingsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6,
        ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8General Settings"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createActiveHeader(inventory, 0,
        itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "),
        itemManager.createItem(XMaterial.LIGHT_BLUE_STAINED_GLASS_PANE, " "));

    inventory.setItem(0, itemManager.createItem(XMaterial.CHEST, "&aGeneral Settings"));
    inventory.setItem(1, itemManager.createItem(XMaterial.ENDER_CHEST, "&aAdvanced Settings"));
    inventory.setItem(2, itemManager.createItem(XMaterial.ANVIL, "&aTechnical Settings"));
    inventory.setItem(3, itemManager.createItem(XMaterial.KNOWLEDGE_BOOK, "&aDefaults"));
    inventory.setItem(4, itemManager.createItem(XMaterial.COMPARATOR, "&aOverrides"));
    inventory.setItem(5, itemManager.createItem(XMaterial.BREWING_STAND, "&aExperimental Settings"));

//    inventory.setItem(6, itemManager.getEros(UUID.randomUUID()));
//    inventory.setItem(7, itemManager.getSilvia(UUID.randomUUID()));

//    inventory.setItem(31, itemManager.createItem(XMaterial.HONEYCOMB, "&aPlugin Settings"));
    if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HEALTH.getValue())) {
      inventory.setItem(29, plugin.getItemStates().RESTORE_HEALTH_ENABLED_ICON);
      inventory.setItem(38, plugin.getItemStates().RESTORE_HEALTH_ENABLED_BUTTON);
    } else {
      inventory.setItem(29, plugin.getItemStates().RESTORE_HEALTH_DISABLED_ICON);
      inventory.setItem(38, plugin.getItemStates().RESTORE_HEALTH_DISABLED_BUTTON);
    }

    if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HUNGER.getValue())) {
      inventory.setItem(30, plugin.getItemStates().RESTORE_HUNGER_ENABLED_ICON);
      inventory.setItem(39, plugin.getItemStates().RESTORE_HUNGER_ENABLED_BUTTON);
    } else {
      inventory.setItem(30, plugin.getItemStates().RESTORE_HUNGER_DISABLED_ICON);
      inventory.setItem(39, plugin.getItemStates().RESTORE_HUNGER_DISABLED_BUTTON);
    }

    if (plugin.getConfig().getBoolean(Settings.ALLOW_SELF_HUG.getValue())) {
      inventory.setItem(32, plugin.getItemStates().SELF_HUGS_ENABLED_ICON);
      inventory.setItem(41, plugin.getItemStates().SELF_HUGS_ENABLED_BUTTON);
    } else {
      inventory.setItem(32, plugin.getItemStates().SELF_HUGS_DISABLED_ICON);
      inventory.setItem(41, plugin.getItemStates().SELF_HUGS_DISABLED_BUTTON);
    }

    if (plugin.getConfig().getBoolean(Settings.ALLOW_SHIFT_HUG.getValue())) {
      inventory.setItem(33, plugin.getItemStates().SHIFT_HUGS_ENABLED_ICON);
      inventory.setItem(42, plugin.getItemStates().SHIFT_HUGS_ENABLED_BUTTON);
    } else {
      inventory.setItem(33, plugin.getItemStates().SHIFT_HUGS_DISABLED_ICON);
      inventory.setItem(42, plugin.getItemStates().SHIFT_HUGS_DISABLED_BUTTON);
    }

    String displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(45, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(46, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    plugin.getGUIManager().getGeneralSettingsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getGeneralSettingsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getGeneralSettingsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 45) {
      player.closeInventory();
      return;
    }

    if (slot == 46) {
      new AdminPanelGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 0) {
      new GeneralSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 1) {
      new AdvancedSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 2) {
      new TechnicalSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 3) {
      new DefaultsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 4) {
      new OverridesGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 5) {
      new ExperimentalSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 29 || slot == 38) {
      plugin.getConfig().set(Settings.RESTORE_PLAYER_HEALTH.getValue(), !plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HEALTH.getValue()));
      plugin.saveConfig();
      InventoryUtils.togglePair(inv, 29, 38, plugin.getItemStates().getRestoreHealthItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 30 || slot == 39) {
      plugin.getConfig().set(Settings.RESTORE_PLAYER_HUNGER.getValue(), !plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HUNGER.getValue()));
      plugin.saveConfig();
      InventoryUtils.togglePair(inv, 30, 39, plugin.getItemStates().getRestoreHungerItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 32 || slot == 41) {
      plugin.getConfig().set(Settings.ALLOW_SELF_HUG.getValue(), !plugin.getConfig().getBoolean(Settings.ALLOW_SELF_HUG.getValue()));
      plugin.saveConfig();
      InventoryUtils.togglePair(inv, 32, 41, plugin.getItemStates().getSelfHugsItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 33 || slot == 42) {
      plugin.getConfig().set(Settings.ALLOW_SHIFT_HUG.getValue(), !plugin.getConfig().getBoolean(Settings.ALLOW_SHIFT_HUG.getValue()));
      plugin.saveConfig();
      InventoryUtils.togglePair(inv, 33, 42, plugin.getItemStates().getShiftHugsItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }
}
