package me.nothixal.hugs.guis.stats;

import com.cryptomorin.xseries.XMaterial;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.Interaction;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class YourStatsGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public YourStatsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    String invName = Messages.YOUR_STATS_GUI_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    // "&7Achievements Unlocked: &e<Current>/<Total> &8(percentage)"

    String name = plugin.getLangFile().getString("menus.your_stats_menu.top_item.title");
    List<String> lore = plugin.getLangFile().getStringList("menus.your_stats_menu.top_item.lore");
    List<String> updateLore = new ArrayList<>();

    for (String s : lore) {
      updateLore.add(s
          .replace("%self_given%", formatValue(plugin.getPlayerDataManager().getSelfHugs(player.getUniqueId())))
          .replace("%normal_given%", formatValue(plugin.getPlayerDataManager().getHugsGiven(player.getUniqueId())))
          .replace("%normal_received%", formatValue(plugin.getPlayerDataManager().getHugsReceived(player.getUniqueId())))
          .replace("%mass_given%", formatValue(plugin.getPlayerDataManager().getMassHugsGiven(player.getUniqueId())))
          .replace("%mass_received%", formatValue(plugin.getPlayerDataManager().getMassHugsReceived(player.getUniqueId())))
          .replace("%unique_hugs%", formatValue(plugin.getPlayerDataManager().getUniqueHugsGiven(player.getUniqueId())))
      );
    }

    inventory.setItem(4, itemManager.createSkullItem(player.getUniqueId(), name, updateLore));

    Date firstHugGiven = new Date(plugin.getPlayerDataManager().getFirstHugGivenTimestamp(player.getUniqueId()));
    SimpleDateFormat format = new SimpleDateFormat("MMM d, yyyy");
//    SimpleDateFormat format = new SimpleDateFormat("MMM d, yyyy - hh:mm aaa");

    if (plugin.getPlayerDataManager().getFirstHugGivenTimestamp(player.getUniqueId()) == 0L) {
      String displayName = plugin.getLangFile().getString("common_items.no_first_hug.title");
      List<String> originalLore = plugin.getLangFile().getStringList("common_items.no_first_hug.lore");

      inventory.setItem(20, itemManager.createItem(XMaterial.OAK_SAPLING, displayName, originalLore));
    } else {
      String playerName = Bukkit.getOfflinePlayer(UUID.fromString(plugin.getPlayerDataManager().getFirstHugGivenTo(player.getUniqueId()))).getName();
      String displayName = plugin.getLangFile().getString("common_items.first_hug.title");
      List<String> originalLore = plugin.getLangFile().getStringList("common_items.first_hug.lore");
      List<String> updatedLore = new ArrayList<>();

      for (String s : originalLore) {
        updatedLore.add(s
            .replace("%date%", format.format(firstHugGiven))
            .replace("%target%", playerName));
      }

      inventory.setItem(20, itemManager.createItem(XMaterial.OAK_SAPLING, displayName, updatedLore));
    }

    // This item will become a "Recent Hugs" list.
    List<Interaction> recentHugs = plugin.getPlayerDataManager().getRecentHugs(player.getUniqueId());
//    Collections.reverse(recentHugs);
    List<String> updatedLore = new ArrayList<>();

    for (Interaction interaction : recentHugs) {
      updatedLore.add(interaction.toString());
    }

    if (updatedLore.isEmpty()) {
      updatedLore.add("&7None");
    }

    inventory.setItem(29, itemManager.createItem(XMaterial.SWEET_BERRIES, "&eRecent Hugs", updatedLore));

    DecimalFormat numberFormat = new DecimalFormat("###,###");

    String displayName = plugin.getLangFile().getString(Messages.NORMAL_HUGS_GIVEN.getValue());
    inventory.setItem(22, itemManager.createItem(XMaterial.POPPY, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getHugsGiven(player.getUniqueId())))));

    displayName = plugin.getLangFile().getString(Messages.MASS_HUGS_GIVEN.getValue());
    inventory.setItem(23, itemManager.createItem(XMaterial.ROSE_BUSH, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getMassHugsGiven(player.getUniqueId())))));

    displayName = plugin.getLangFile().getString(Messages.SELF_HUGS_GIVEN.getValue());
    inventory.setItem(24, itemManager.createItem(XMaterial.ARMOR_STAND, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getSelfHugs(player.getUniqueId())))));

    displayName = plugin.getLangFile().getString(Messages.NORMAL_HUGS_RECEIVED.getValue());
    inventory.setItem(31, itemManager.createItem(XMaterial.RABBIT_HIDE, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getHugsReceived(player.getUniqueId())))));

    displayName = plugin.getLangFile().getString(Messages.MASS_HUGS_RECEIVED.getValue());
    inventory.setItem(32, itemManager.createItem(XMaterial.LEATHER, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getMassHugsReceived(player.getUniqueId())))));

    displayName = plugin.getLangFile().getString(Messages.UNIQUE_HUGS_GIVEN.getValue());
    inventory.setItem(33, itemManager.createItem(XMaterial.SUNFLOWER, displayName,
        Collections.singletonList("&7" + numberFormat.format(plugin.getPlayerDataManager().getUniqueHugsGiven(player.getUniqueId())))));

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

//    if (dataManager.getDeveloperHugsGiven(player.getUniqueId()) > 0) {
//      inventory.setItem(2, itemManager.getEros(player.getUniqueId()));
//    }

    plugin.getGUIManager().getYourStatsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getYourStatsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getYourStatsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new HelpGUI(plugin).openGUI(player);
    }
  }

  public String formatValue(float value) {
    String[] arr = {"", "k", "M", "B", "T", "P", "E"};
    int index = 0;
    while ((value / 1000) >= 1) {
      value = value / 1000;
      index++;
    }
    DecimalFormat decimalFormat;

    if (index == 1) {
      decimalFormat = new DecimalFormat("#.##");
    } else {
      decimalFormat = new DecimalFormat("#.#");
    }

    return String.format("%s%s", decimalFormat.format(value), arr[index]);
  }

}
