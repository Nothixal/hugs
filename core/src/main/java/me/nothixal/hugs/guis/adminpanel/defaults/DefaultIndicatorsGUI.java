package me.nothixal.hugs.guis.adminpanel.defaults;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.io.IOException;
import java.util.Arrays;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class DefaultIndicatorsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public DefaultIndicatorsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6,
        ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Defaults > Indicators"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.YELLOW_STAINED_GLASS_PANE, " "));

    inventory.setItem(2, itemManager.createSkullItem(itemManager.getEros(),
        Arrays.asList("&7&o\"Don't forget! These settings", "&7&oonly apply to new players!\"")));

    inventory.setItem(4, itemManager.createItemNoAttrib(XMaterial.FLOWER_BANNER_PATTERN, "&eIndicator Defaults"));

    //inventory.setItem(4, itemManager.createItem(Material.FLOWER_BANNER_PATTERN, "&eYour Indicators"));

    boolean chat = plugin.getFileManager().getDefaultsYMLData().getBoolean("defaults.indicators.chat");
    boolean titles = plugin.getFileManager().getDefaultsYMLData().getBoolean("defaults.indicators.titles");
    boolean bossbar = plugin.getFileManager().getDefaultsYMLData().getBoolean("defaults.indicators.bossbar");
    boolean actionbar = plugin.getFileManager().getDefaultsYMLData().getBoolean("defaults.indicators.actionbar");
    boolean toasts = plugin.getFileManager().getDefaultsYMLData().getBoolean("defaults.indicators.toasts");

    if (chat) {
      inventory.setItem(20, plugin.getItemStates().CHAT_INDICATOR_ENABLED_ICON);
      inventory.setItem(29, plugin.getItemStates().CHAT_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(20, plugin.getItemStates().CHAT_INDICATOR_DISABLED_ICON);
      inventory.setItem(29, plugin.getItemStates().CHAT_INDICATOR_DISABLED_BUTTON);
    }

    if (titles) {
      inventory.setItem(21, plugin.getItemStates().TITLES_INDICATOR_ENABLED_ICON);
      inventory.setItem(30, plugin.getItemStates().TITLES_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(21, plugin.getItemStates().TITLES_INDICATOR_DISABLED_ICON);
      inventory.setItem(30, plugin.getItemStates().TITLES_INDICATOR_DISABLED_BUTTON);
    }

    if (actionbar) {
      inventory.setItem(22, plugin.getItemStates().ACTIONBAR_INDICATOR_ENABLED_ICON);
      inventory.setItem(31, plugin.getItemStates().ACTIONBAR_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(22, plugin.getItemStates().ACTIONBAR_INDICATOR_DISABLED_ICON);
      inventory.setItem(31, plugin.getItemStates().ACTIONBAR_INDICATOR_DISABLED_BUTTON);
    }

    if (bossbar) {
      inventory.setItem(23, plugin.getItemStates().BOSSBAR_INDICATOR_ENABLED_ICON);
      inventory.setItem(32, plugin.getItemStates().BOSSBAR_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(23, plugin.getItemStates().BOSSBAR_INDICATOR_DISABLED_ICON);
      inventory.setItem(32, plugin.getItemStates().BOSSBAR_INDICATOR_DISABLED_BUTTON);
    }

    if (toasts) {
      inventory.setItem(24, plugin.getItemStates().TOAST_INDICATOR_ENABLED_ICON);
      inventory.setItem(33, plugin.getItemStates().TOAST_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(24, plugin.getItemStates().TOAST_INDICATOR_DISABLED_ICON);
      inventory.setItem(33, plugin.getItemStates().TOAST_INDICATOR_DISABLED_BUTTON);
    }

    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose"));

    plugin.getGUIManager().getDefaultsSettingsIndicatorsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getDefaultsSettingsIndicatorsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getDefaultsSettingsIndicatorsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new DefaultsGUI(plugin).openGUI(player);
      return;
    }

    FileConfiguration config = plugin.getFileManager().getDefaultsYMLData();

    // Chat Indicator Check
    if (slot == 20 || slot == 29) {
      config.set("defaults.indicators.chat", !config.getBoolean("defaults.indicators.chat"));
      InventoryUtils.togglePair(inv, 20, 29, plugin.getItemStates().getChatIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Titles Indicator Check
    if (slot == 21 || slot == 30) {
      config.set("defaults.indicators.titles", !config.getBoolean("defaults.indicators.titles"));
      InventoryUtils.togglePair(inv, 21, 30, plugin.getItemStates().getTitlesIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Actionbar Indicator Check
    if (slot == 22 || slot == 31) {
      config.set("defaults.indicators.actionbar", !config.getBoolean("defaults.indicators.actionbar"));
      InventoryUtils.togglePair(inv, 22, 31, plugin.getItemStates().getActionbarIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Bossbar Indicator Check
    if (slot == 23 || slot == 32) {
      config.set("defaults.indicators.bossbar", !config.getBoolean("defaults.indicators.bossbar"));
      InventoryUtils.togglePair(inv, 23, 32, plugin.getItemStates().getBossbarIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Toast Indicator Check
    if (slot == 24 || slot == 33) {
      config.set("defaults.indicators.toasts", !config.getBoolean("defaults.indicators.toasts"));
      InventoryUtils.togglePair(inv, 24, 33, plugin.getItemStates().getToastIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }
  }

  @EventHandler
  public void onInventoryClose(InventoryCloseEvent event) {
    Inventory inv = event.getInventory();
    Player player = (Player) event.getPlayer();

    if (inv != plugin.getGUIManager().getDefaultsSettingsIndicatorsGUIs().get(player.getUniqueId())) {
      return;
    }

    try {
      plugin.getFileManager().getDefaultsYMLData().save(plugin.getFileManager().getDefaultsYMLFile());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 2);
    }
  }

}
