package me.nothixal.hugs.guis.settings.player;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class YourSoundsGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public YourSoundsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    String invName = Messages.YOUR_SOUNDS_GUI_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    String displayName = plugin.getLangFile().getString("menus.your_sounds_menu.top_item.title");
    inventory.setItem(4, itemManager.createItem(XMaterial.BELL, displayName));

    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      inventory.setItem(20, plugin.getItemStates().SOUNDS_MENU_ENABLED_ICON);
      inventory.setItem(29, plugin.getItemStates().SOUNDS_MENU_ENABLED_BUTTON);
    } else {
      inventory.setItem(20, plugin.getItemStates().SOUNDS_MENU_DISABLED_ICON);
      inventory.setItem(29, plugin.getItemStates().SOUNDS_MENU_DISABLED_BUTTON);
    }

    if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
      inventory.setItem(22, plugin.getItemStates().SOUNDS_COMMAND_ENABLED_ICON);
      inventory.setItem(31, plugin.getItemStates().SOUNDS_COMMAND_ENABLED_BUTTON);
    } else {
      inventory.setItem(22, plugin.getItemStates().SOUNDS_COMMAND_DISABLED_ICON);
      inventory.setItem(31, plugin.getItemStates().SOUNDS_COMMAND_DISABLED_BUTTON);
    }

    if (plugin.getPlayerDataManager().wantsHugSounds(player.getUniqueId())) {
      inventory.setItem(24, plugin.getItemStates().HUG_SOUNDS_ENABLED_ICON);
      inventory.setItem(33, plugin.getItemStates().HUG_SOUNDS_ENABLED_BUTTON);
    } else {
      inventory.setItem(24, plugin.getItemStates().HUG_SOUNDS_DISABLED_ICON);
      inventory.setItem(33, plugin.getItemStates().HUG_SOUNDS_DISABLED_BUTTON);
    }

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    plugin.getGUIManager().getYourSoundsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getYourSoundsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getYourSoundsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new YourPreferencesGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 20 || slot == 29) {
      plugin.getPlayerDataManager().setWantsMenuSounds(player.getUniqueId(), !plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId()));
      InventoryUtils.togglePair(inv, 20, 29, plugin.getItemStates().getSoundMenuSettingsItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 22 || slot == 31) {
      plugin.getPlayerDataManager().setWantsCommandSounds(player.getUniqueId(), !plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId()));
      InventoryUtils.togglePair(inv, 22, 31, plugin.getItemStates().getSoundCommandSettingsItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 24 || slot == 33) {
      plugin.getPlayerDataManager().setWantsHugSounds(player.getUniqueId(), !plugin.getPlayerDataManager().wantsHugSounds(player.getUniqueId()));
      InventoryUtils.togglePair(inv, 24, 33, plugin.getItemStates().getHugSoundSettingsItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }

}
