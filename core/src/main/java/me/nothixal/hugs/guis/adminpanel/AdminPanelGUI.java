package me.nothixal.hugs.guis.adminpanel;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class AdminPanelGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public AdminPanelGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    String invName = Messages.ADMIN_PANEL_GUI_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createHeaderSeparator(inventory, itemManager.createItem(XMaterial.RED_STAINED_GLASS_PANE, " "));
    InventoryUtils.createGenericHeader(inventory, itemManager);

    String displayName = plugin.getLangFile().getString("menus.admin_panel_menu.all_settings.title");
    List<String> lore = plugin.getLangFile().getStringList("menus.admin_panel_menu.all_settings.lore");

    inventory.setItem(28, itemManager.createItem(XMaterial.HONEYCOMB, displayName, lore));

    displayName = plugin.getLangFile().getString("menus.admin_panel_menu.modify_player.title");
    lore = plugin.getLangFile().getStringList("menus.admin_panel_menu.modify_player.lore");
    inventory.setItem(30, itemManager.createItem(XMaterial.PLAYER_HEAD, displayName, lore));

    displayName = plugin.getLangFile().getString("menus.admin_panel_menu.select_language.title");
    lore = plugin.getLangFile().getStringList("menus.admin_panel_menu.select_language.lore");
    inventory.setItem(32, itemManager.createNMSSkullItem(PluginConstants.SELECT_LANGUAGE_TEXTURE, displayName, lore));

    displayName = plugin.getLangFile().getString("menus.admin_panel_menu.experimental_settings.title");
    lore = plugin.getLangFile().getStringList("menus.admin_panel_menu.experimental_settings.lore");
    inventory.setItem(34, itemManager.createItem(XMaterial.BREWING_STAND, displayName, lore));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(45, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(46, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    plugin.getGUIManager().getAdminPanelGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getAdminPanelGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getAdminPanelGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 45) {
      player.closeInventory();
      return;
    }

    if (slot == 46) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 28) {
      new GeneralSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 32) {
      new SelectLanguageGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 34) {
      new ExperimentalSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }
}
