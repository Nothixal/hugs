package me.nothixal.hugs.guis;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.util.Collections;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.guis.adminpanel.AdminPanelGUI;
import me.nothixal.hugs.guis.settings.player.YourPreferencesGUI;
import me.nothixal.hugs.guis.stats.ErosStatsGUI;
import me.nothixal.hugs.guis.stats.GlobalStatsGUI;
import me.nothixal.hugs.guis.stats.SilviaStatsGUI;
import me.nothixal.hugs.guis.stats.YourStatsGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.BookUtils;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class HelpGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public HelpGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    String invName = Messages.HELP_GUI_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createHeaderSeparator(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));
    InventoryUtils.createGenericHeader(inventory, itemManager);

    String displayName = plugin.getLangFile().getString("menus.help_menu.leaderboards.title");
    String lore = plugin.getLangFile().getString("menus.help_menu.leaderboards.lore");

    inventory.setItem(20, itemManager.createItem(XMaterial.ELYTRA, displayName, Collections.singletonList(lore)));

    displayName = plugin.getLangFile().getString("menus.help_menu.global_stats.title");
    lore = plugin.getLangFile().getString("menus.help_menu.global_stats.lore");

    inventory.setItem(21, itemManager.createItem(XMaterial.SPRUCE_SIGN, displayName, Collections.singletonList(lore)));

    displayName = plugin.getLangFile().getString("menus.help_menu.your_stats.title");
    lore = plugin.getLangFile().getString("menus.help_menu.your_stats.lore");

    inventory.setItem(22, itemManager.createSkullItem(player.getUniqueId(), displayName, Collections.singletonList(lore)));

    displayName = "&a???";
    lore = plugin.getLangFile().getString("common_items.coming_soon");
    inventory.setItem(23, itemManager.createItem(XMaterial.NAME_TAG, displayName, Collections.singletonList(lore)));

    displayName = plugin.getLangFile().getString("menus.help_menu.achievements.title");
    lore = plugin.getLangFile().getString("menus.help_menu.achievements.lore");

    inventory.setItem(24, itemManager.createItem(XMaterial.DIAMOND, displayName, Collections.singletonList(lore)));

    displayName = plugin.getLangFile().getString("menus.help_menu.plugin_archives.title");
    lore = plugin.getLangFile().getString("menus.help_menu.plugin_archives.lore");

    inventory.setItem(30, itemManager.createItem(XMaterial.BOOKSHELF, displayName, Collections.singletonList(lore)));
    inventory.setItem(31, itemManager.createItem(XMaterial.LIGHT_BLUE_STAINED_GLASS_PANE, " "));

    displayName = plugin.getLangFile().getString("menus.help_menu.commands.title");
    lore = plugin.getLangFile().getString("menus.help_menu.commands.lore");

    inventory.setItem(32, itemManager.createItem(XMaterial.SWEET_BERRIES, displayName, Collections.singletonList(lore)));

    displayName = plugin.getLangFile().getString("menus.help_menu.your_preferences.title");
    lore = plugin.getLangFile().getString("menus.help_menu.your_preferences.lore");

    inventory.setItem(39, itemManager.createItem(XMaterial.COMPARATOR, displayName, Collections.singletonList(lore)));

    displayName = plugin.getLangFile().getString("menus.help_menu.admin_panel.title");
    lore = plugin.getLangFile().getString("menus.help_menu.admin_panel.lore");

    inventory.setItem(41, itemManager.createItem(XMaterial.ANVIL, displayName, Collections.singletonList(lore)));

    displayName = plugin.getLangFile().getString("menus.help_menu.donations.title");
    lore = plugin.getLangFile().getString("menus.help_menu.donations.lore");

    inventory.setItem(49, itemManager.createItem(XMaterial.RABBIT_STEW, displayName, Collections.singletonList(lore)));

    plugin.getGUIManager().getHelpGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getHelpGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack clickedItem = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getHelpGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (clickedItem == null) {
      return;
    }

//    // Close Button
//    if (slot == 49) {
//      player.closeInventory();
//      return;
//    }

    if (slot == 20) {
      if (!player.hasPermission(Permissions.LEADERBOARD.getPermissionNode())) {
        InventoryUtils.showError(inv, slot, clickedItem, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
        return;
      }

      new LeaderboardGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 21) {
      new GlobalStatsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 22) {
      if (!player.hasPermission(Permissions.STATS.getPermissionNode())) {
        InventoryUtils.showError(inv, slot, clickedItem, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
        return;
      }

      new YourStatsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 23) {
//      player.sendMessage("Coming Soon");
      return;
    }

    if (slot == 24) {
//      new YourLanguageGUI(plugin).openGUI(player);
//      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 30) {
      if (!player.hasPermission(Permissions.INFO.getPermissionNode())) {
        InventoryUtils.showError(inv, slot, clickedItem, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
        return;
      }

      new PluginArchivesGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_LOOM_SELECT_PATTERN);
      return;
    }

    if (slot == 31) {
//      player.sendMessage("Coming Soon");
      return;
    }

    if (slot == 32) {
      new CommandsGUI(plugin).openGUI(player, 0);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 39) {
      if (!player.hasPermission(Permissions.PREFERENCES.getPermissionNode())) {
        InventoryUtils.showError(inv, slot, clickedItem, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
        return;
      }

      new YourPreferencesGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 41) {
      if (!player.hasPermission(Permissions.SETTINGS.getPermissionNode())) {
        InventoryUtils.showError(inv, slot, clickedItem, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
        return;
      }

      new AdminPanelGUI(plugin).openGUI(player);
//      if (player.hasPermission(Permissions.SETTINGS.getPermissionNode())) {
//        new PluginSettingsGUI(plugin).openGUI(player);
//      } else {
//        InventoryUtils.showError(inv, slot, clickedItem, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
//      }
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 49) {
      BookUtils.openDonateBook(plugin, player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    //TODO: Uncomment when GUI is completed.
//    if (slot == 51) {
//      if (player.hasPermission(Permissions.MENU_PLUGIN_SETTINGS.getPermissionNode())) {
//        new PluginLexiconGUI(plugin).openGUI(player);
//      } else {
//        InventoryUtils.showError(inv, slot, clickedItem, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
//      }
//
//    }

    if (slot == 6) {
      new ErosStatsGUI(plugin).openGUI(player);
      playSound(player, XSound.ENTITY_PARROT_AMBIENT);
      return;
    }

    if (slot == 7) {
      new SilviaStatsGUI(plugin).openGUI(player);
      playSound(player, XSound.ENTITY_PARROT_AMBIENT);
//      return;
    }

  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }
}
