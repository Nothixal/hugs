package me.nothixal.hugs.guis.adminpanel;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Languages;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.PlayerUtils;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

public class SelectLanguageGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private final ItemManager itemManager;
  private final NamespacedKey key;

  // Language Map
  // Sort order defined in another file.
  private Map<String, Integer> languages = new HashMap<>();
  private Map<String, Integer> nonstandardLanguages = new HashMap<>();

  private Map<String, Languages> test = new HashMap<>();
  private List<String> test2 = new ArrayList<>();

  public SelectLanguageGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.key = new NamespacedKey(plugin, "language");
  }

  public void openGUI(Player player) {
    String invName = Messages.SELECT_LANGUAGE_GUI_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

//    inventory.setItem(31, itemManager.createItem(XMaterial.SCAFFOLDING, "&aWork In Progress"));

    File languageDir = new File(plugin.getFileManager().getLocaleDirectory());
    File nonstandardLocalesDir = new File(plugin.getFileManager().getLocaleDirectory() + File.separator + "nonstandard");

    loadLanguages(languageDir);
    loadLanguages(nonstandardLocalesDir);

//    String selectedLanguage = plugin.getPlayerDataManager().getPreferredLanguage(player.getUniqueId());

    // TODO: Finish this.
//    for (String s : languages.keySet()) {
//      inventory.addItem(itemManager.createNMSSkullItem("", "&8(&f" + s + "&8)"));
//    }

    for (String fileName : test2) {
      ItemStack item = itemManager.createItem(XMaterial.MAP, "&8(&f" + fileName + "&8)");
      ItemMeta meta = item.getItemMeta();
      PersistentDataContainer pdc = meta.getPersistentDataContainer();

      pdc.set(key, PersistentDataType.STRING, fileName);
      item.setItemMeta(meta);

      inventory.addItem(item);
    }


    String displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));
    inventory.setItem(51, itemManager.createItem(XMaterial.WRITABLE_BOOK, "&aHelp Us Translate"));
    inventory.setItem(52, itemManager.createItem(XMaterial.SLIME_BALL, "&aRefresh Languages",
        Arrays.asList(
            "&7This will check for new language files.",
            "",
            "&eClick to refresh.")));

    plugin.getGUIManager().getTemporaryGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getTemporaryGUIs().get(player.getUniqueId()));
  }

  private void loadLanguages(File directory, Map<String, Integer> map) {
    File[] files = directory.listFiles();

    if (files == null) {
      return;
    }

    for (File file : files) {
      if (file.isDirectory()) {
        continue;
      }

      if (!file.getName().endsWith(".yml")) {
        continue;
      }

      String fileName = file.getName().replace(".yml", "");
      String[] split = fileName.split("_");

//      System.out.println(file.getName());
//      System.out.println(fileName);
//      System.out.println(split[split.length - 1]);

//      test.put(file.getName(), Languages);
      test2.add(file.getName());

      map.put(file.getName(), map.size() + 1);
    }
  }

  private void loadLanguages(File directory) {
    File[] files = directory.listFiles();

    if (files == null) {
      return;
    }

    for (File file : files) {
      if (file.isDirectory()) {
        continue;
      }

      if (!file.getName().endsWith(".yml")) {
        continue;
      }

      String fileName = file.getName().replace(".yml", "");
      String[] split = fileName.split("_");

//      System.out.println(file.getName());
//      System.out.println(fileName);
//      System.out.println(split[split.length - 1]);

//      test.put(file.getName(), Languages);
      test2.add(file.getName());
    }
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getTemporaryGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    // Go Back
    if (slot == 48) {
      new AdminPanelGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 52) {
      return;
    }

    // Language Selection Logic
    //TODO: Make this method save language selection changes to the main config.
    ItemMeta itemMeta = item.getItemMeta();
    PersistentDataContainer container = itemMeta.getPersistentDataContainer();
    if (container.has(key, PersistentDataType.STRING)) {
      String language = container.get(key, PersistentDataType.STRING);
      plugin.getLocaleManager().load(language);
      player.sendMessage(ChatUtils.colorChat(Messages.LANGUAGE_CHANGED.getLangValue().replace("%language%", language)));
      test2.clear();
      openGUI(player);
      PlayerUtils.playSound(player, XSound.UI_BUTTON_CLICK);
    }
  }
}
