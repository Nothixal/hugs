package me.nothixal.hugs.guis.settings.player;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class YourVisualsGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public YourVisualsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    String invName = Messages.YOUR_VISUALS_GUI_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    String displayName = plugin.getLangFile().getString("menus.your_visuals_menu.top_item.title", "&eVisual Preferences");
    inventory.setItem(4, itemManager.createItem(XMaterial.PRISMARINE_CRYSTALS, displayName));

    inventory.setItem(22, plugin.getItemStates().getParticleSettingIcon(player.getUniqueId()));
    inventory.setItem(31, plugin.getItemStates().getParticleSettingButton(player.getUniqueId()));

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    plugin.getGUIManager().getYourVisualsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getYourVisualsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getYourVisualsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new YourPreferencesGUI(plugin).openGUI(player);
      return;
    }

    // Particles Button
    if (slot == 22 || slot == 31) {
      if (event.isLeftClick()) {
        plugin.getPlayerDataManager().increaseParticleQuality(player.getUniqueId());
        InventoryUtils.togglePair(inv, 22, 31, plugin.getItemStates().getParticleSettingsItems());
      }

      if (event.isRightClick()) {
        plugin.getPlayerDataManager().decreaseParticleQuality(player.getUniqueId());
        InventoryUtils.togglePairReverse(inv, 22, 31, plugin.getItemStates().getParticleSettingsItems());
      }

      playSound(player, XSound.UI_BUTTON_CLICK, 2);
      return;
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }

  private void playSound(Player player, XSound sound, int pitch) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, pitch);
    }
  }

}
