package me.nothixal.hugs.guis.adminpanel.defaults;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.util.Collections;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.guis.adminpanel.AdminPanelGUI;
import me.nothixal.hugs.guis.adminpanel.AdvancedSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.ExperimentalSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.GeneralSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.TechnicalSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.overrides.OverridesGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class DefaultsGUI implements Listener {

  private final HugsPlugin plugin;
  private final Inventory inventory;
  private final ItemManager itemManager;

  public DefaultsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6,
        ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Defaults"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createActiveHeader(inventory, 3,
        itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "),
        itemManager.createItem(XMaterial.LIGHT_BLUE_STAINED_GLASS_PANE, " "));

    inventory.setItem(0, itemManager.createItem(XMaterial.CHEST, "&aGeneral Settings"));
    inventory.setItem(1, itemManager.createItem(XMaterial.ENDER_CHEST, "&aAdvanced Settings"));
    inventory.setItem(2, itemManager.createItem(XMaterial.ANVIL, "&aTechnical Settings"));
    inventory.setItem(3, itemManager.createItem(XMaterial.KNOWLEDGE_BOOK, "&aDefaults"));
    inventory.setItem(4, itemManager.createItem(XMaterial.COMPARATOR, "&aOverrides"));
    inventory.setItem(5, itemManager.createItem(XMaterial.BREWING_STAND, "&aExperimental Settings"));

    inventory.setItem(30, itemManager.createItem(XMaterial.PRISMARINE_CRYSTALS, "&eVisuals"));
    inventory.setItem(31, itemManager.createItem(XMaterial.BELL, "&eSounds"));
    inventory.setItem(32, itemManager.createItem(XMaterial.PAINTING, "&eText Indicators"));

    inventory.setItem(39, itemManager.createItem(XMaterial.ORANGE_DYE, "&eVisual Defaults", Collections.singletonList("&7Click to edit.")));
    inventory.setItem(40, itemManager.createItem(XMaterial.ORANGE_DYE, "&eSound Defaults", Collections.singletonList("&7Click to edit.")));
    inventory.setItem(41, itemManager.createItem(XMaterial.ORANGE_DYE, "&eTextual Defaults", Collections.singletonList("&7Click to edit.")));


    String displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(45, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(46, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    plugin.getGUIManager().getDefaultsSettingsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getDefaultsSettingsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getDefaultsSettingsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 45) {
      player.closeInventory();
      return;
    }

    if (slot == 46) {
      new AdminPanelGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 0) {
      new GeneralSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 1) {
      new AdvancedSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 2) {
      new TechnicalSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 3) {
      new DefaultsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 4) {
      new OverridesGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 5) {
      new ExperimentalSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 30 || slot == 39) {
      new DefaultVisualsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 31 || slot == 40) {
      new DefaultSoundsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 32 || slot == 41) {
      new DefaultIndicatorsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }
}
