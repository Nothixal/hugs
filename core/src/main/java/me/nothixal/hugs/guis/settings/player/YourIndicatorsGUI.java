package me.nothixal.hugs.guis.settings.player;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class YourIndicatorsGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public YourIndicatorsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    String invName = Messages.YOUR_INDICATORS_GUI_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    String displayName = plugin.getLangFile().getString("menus.your_indicators_menu.top_item.title");
    inventory.setItem(4, itemManager.createItem(XMaterial.PAINTING, displayName));
    //inventory.setItem(4, itemManager.createItem(Material.FLOWER_BANNER_PATTERN, "&eYour Indicators"));

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.CHAT)) {
      inventory.setItem(20, plugin.getItemStates().CHAT_INDICATOR_ENABLED_ICON);
      inventory.setItem(29, plugin.getItemStates().CHAT_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(20, plugin.getItemStates().CHAT_INDICATOR_DISABLED_ICON);
      inventory.setItem(29, plugin.getItemStates().CHAT_INDICATOR_DISABLED_BUTTON);
    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TITLES)) {
      inventory.setItem(21, plugin.getItemStates().TITLES_INDICATOR_ENABLED_ICON);
      inventory.setItem(30, plugin.getItemStates().TITLES_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(21, plugin.getItemStates().TITLES_INDICATOR_DISABLED_ICON);
      inventory.setItem(30, plugin.getItemStates().TITLES_INDICATOR_DISABLED_BUTTON);
    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.ACTIONBAR)) {
      inventory.setItem(22, plugin.getItemStates().ACTIONBAR_INDICATOR_ENABLED_ICON);
      inventory.setItem(31, plugin.getItemStates().ACTIONBAR_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(22, plugin.getItemStates().ACTIONBAR_INDICATOR_DISABLED_ICON);
      inventory.setItem(31, plugin.getItemStates().ACTIONBAR_INDICATOR_DISABLED_BUTTON);
    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.BOSSBAR)) {
      inventory.setItem(23, plugin.getItemStates().BOSSBAR_INDICATOR_ENABLED_ICON);
      inventory.setItem(32, plugin.getItemStates().BOSSBAR_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(23, plugin.getItemStates().BOSSBAR_INDICATOR_DISABLED_ICON);
      inventory.setItem(32, plugin.getItemStates().BOSSBAR_INDICATOR_DISABLED_BUTTON);
    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TOASTS)) {
      inventory.setItem(24, plugin.getItemStates().TOAST_INDICATOR_ENABLED_ICON);
      inventory.setItem(33, plugin.getItemStates().TOAST_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(24, plugin.getItemStates().TOAST_INDICATOR_DISABLED_ICON);
      inventory.setItem(33, plugin.getItemStates().TOAST_INDICATOR_DISABLED_BUTTON);
    }

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    plugin.getGUIManager().getYourIndicatorGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getYourIndicatorGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    int slot = event.getSlot();
    Player player = (Player) event.getWhoClicked();

    if (inv != plugin.getGUIManager().getYourIndicatorGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new YourPreferencesGUI(plugin).openGUI(player);
      return;
    }

    // Chat Indicator Check
    if (slot == 20 || slot == 29) {
      plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), IndicatorType.CHAT, !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.CHAT));
      InventoryUtils.togglePair(inv, 20, 29, plugin.getItemStates().getChatIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Titles Indicator Check
    if (slot == 21 || slot == 30) {
      plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), IndicatorType.TITLES, !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TITLES));
      InventoryUtils.togglePair(inv, 21, 30, plugin.getItemStates().getTitlesIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Actionbar Indicator Check
    if (slot == 22 || slot == 31) {
      plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), IndicatorType.ACTIONBAR, !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.ACTIONBAR));
      InventoryUtils.togglePair(inv, 22, 31, plugin.getItemStates().getActionbarIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Bossbar Indicator Check
    if (slot == 23 || slot == 32) {
      plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), IndicatorType.BOSSBAR, !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.BOSSBAR));
      InventoryUtils.togglePair(inv, 23, 32, plugin.getItemStates().getBossbarIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Toast Indicator Check
    if (slot == 24 || slot == 33) {
      plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), IndicatorType.TOASTS, !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TOASTS));
      InventoryUtils.togglePair(inv, 24, 33, plugin.getItemStates().getToastIndicatorItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 2);
    }
  }
  
}
