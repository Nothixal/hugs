package me.nothixal.hugs.guis.adminpanel.overrides;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.io.IOException;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.preferences.ParticleQuality;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class OverrideVisualsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  private FileConfiguration config;

  public OverrideVisualsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6,
        ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Overrides > Visuals"));
    this.config = plugin.getFileManager().getOverridesYMLData();
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.RED_STAINED_GLASS_PANE, " "));

    inventory.setItem(4, itemManager.createItemNoAttrib(XMaterial.PRISMARINE_CRYSTALS, "&eVisual Overrides"));

    ParticleQuality quality = ParticleQuality.valueOf(config.getString("overrides.particle_quality", "low").toUpperCase());

    inventory.setItem(22, getParticleSettingIcon(quality));
    inventory.setItem(31, getParticleSettingButton(quality));

    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose"));

    plugin.getGUIManager().getOverridesSettingsVisualsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getOverridesSettingsVisualsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getOverridesSettingsVisualsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new OverridesGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 22 || slot == 31) {
      if (event.isLeftClick()) {
        ParticleQuality quality = ParticleQuality.valueOf(config.getString("overrides.particle_quality").toUpperCase());
        config.set("overrides.particle_quality", quality.getNext().name().toLowerCase());
        InventoryUtils.togglePair(inv, 22, 31, plugin.getItemStates().getParticleSettingsItems());
      }

      if (event.isRightClick()) {
        ParticleQuality quality = ParticleQuality.valueOf(config.getString("overrides.particle_quality").toUpperCase());
        config.set("overrides.particle_quality", quality.getPrevious().name().toLowerCase());
        InventoryUtils.togglePairReverse(inv, 22, 31, plugin.getItemStates().getParticleSettingsItems());
      }

      playSound(player, XSound.UI_BUTTON_CLICK, 2);
      return;
    }
  }

  @EventHandler
  public void onInventoryClose(InventoryCloseEvent event) {
    Inventory inv = event.getInventory();
    Player player = (Player) event.getPlayer();

    if (inv != plugin.getGUIManager().getOverridesSettingsVisualsGUIs().get(player.getUniqueId())) {
      return;
    }

    try {
      plugin.getFileManager().getOverridesYMLData().save(plugin.getFileManager().getOverridesYMLFile());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private ItemStack getParticleSettingIcon(ParticleQuality quality) {
    switch (quality) {
      case NONE:
        return plugin.getItemStates().PARTICLES_OFF_ICON;
      case LOW:
        return plugin.getItemStates().PARTICLES_LOW_ICON;
      case MEDIUM:
        return plugin.getItemStates().PARTICLES_MEDIUM_ICON;
      case HIGH:
        return plugin.getItemStates().PARTICLES_HIGH_ICON;
      case EXTREME:
        return plugin.getItemStates().PARTICLES_EXTREME_ICON;
      default:
        return itemManager.createItem(XMaterial.BARRIER, "&cERROR!");
    }
  }

  private ItemStack getParticleSettingButton(ParticleQuality quality) {
    switch (quality) {
      case NONE:
        return plugin.getItemStates().PARTICLES_OFF_BUTTON;
      case LOW:
        return plugin.getItemStates().PARTICLES_LOW_BUTTON;
      case MEDIUM:
        return plugin.getItemStates().PARTICLES_MEDIUM_BUTTON;
      case HIGH:
        return plugin.getItemStates().PARTICLES_HIGH_BUTTON;
      case EXTREME:
        return plugin.getItemStates().PARTICLES_EXTREME_BUTTON;
      default:
        return itemManager.createItem(XMaterial.BARRIER, "&cERROR!");
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }

  private void playSound(Player player, XSound sound, int pitch) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, pitch);
    }
  }

}
