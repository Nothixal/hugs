package me.nothixal.hugs.guis.stats;

import com.cryptomorin.xseries.XMaterial;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class SilviaStatsGUI implements Listener {

  private final HugsPlugin plugin;
  private final ItemManager itemManager;

  public SilviaStatsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    String invName = Messages.SILVIA_STATS_GUI_TITLE.getLangValue();
    Inventory inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(invName));

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    inventory.setItem(4, itemManager.getSilvia());

    String displayName = plugin.getLangFile().getString(Messages.NORMAL_HUGS_GIVEN.getValue());
    inventory.setItem(21, itemManager.createItem(XMaterial.POPPY, displayName,
        Collections.singletonList("&721")));

    displayName = plugin.getLangFile().getString(Messages.MASS_HUGS_GIVEN.getValue());
    inventory.setItem(22, itemManager.createItem(XMaterial.ROSE_BUSH, displayName,
        Collections.singletonList("&710")));

    displayName = plugin.getLangFile().getString(Messages.SELF_HUGS_GIVEN.getValue());
    inventory.setItem(23, itemManager.createItem(XMaterial.ARMOR_STAND, displayName,
        Collections.singletonList("&753")));

    displayName = plugin.getLangFile().getString(Messages.NORMAL_HUGS_RECEIVED.getValue());
    inventory.setItem(30, itemManager.createItem(XMaterial.RABBIT_HIDE, displayName,
        Collections.singletonList("&739")));

    displayName = plugin.getLangFile().getString(Messages.MASS_HUGS_RECEIVED.getValue());
    inventory.setItem(31, itemManager.createItem(XMaterial.LEATHER, displayName,
        Collections.singletonList("&773")));


    LocalDate myDateObj = LocalDate.of(2020, 1, 1);
    DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM d, yyyy");

    displayName = plugin.getLangFile().getString("common_items.first_hug.title");
    List<String> originalLore = plugin.getLangFile().getStringList("common_items.first_hug.lore");
    List<String> updatedLore = new ArrayList<>();

    for (String s : originalLore) {
      updatedLore.add(s
          .replace("%date%", format.format(myDateObj))
          .replace("%target%", "Eros"));
    }

    inventory.setItem(32, itemManager.createItem(XMaterial.OAK_SAPLING, displayName, updatedLore));

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    plugin.getGUIManager().getSilviaStatsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getSilviaStatsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getSilviaStatsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new HelpGUI(plugin).openGUI(player);
    }
  }

}
