package me.nothixal.hugs.guis.adminpanel.defaults;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.io.IOException;
import java.util.Arrays;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class DefaultSoundsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public DefaultSoundsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6,
        ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Defaults > Sounds"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.YELLOW_STAINED_GLASS_PANE, " "));

    inventory.setItem(2, itemManager.createSkullItem(itemManager.getEros(),
        Arrays.asList("&7&o\"Don't forget! These settings", "&7&oonly apply to new players!\"")));

    inventory.setItem(4, itemManager.createItemNoAttrib(XMaterial.BELL, "&eSounds Defaults"));

    boolean wantsMenuSounds = plugin.getFileManager().getDefaultsYMLData().getBoolean("defaults.sounds.menu");
    boolean wantsCommandSounds = plugin.getFileManager().getDefaultsYMLData().getBoolean("defaults.sounds.commands");
    boolean wantsHugSounds = plugin.getFileManager().getDefaultsYMLData().getBoolean("defaults.sounds.hugs");

    if (wantsMenuSounds) {
      inventory.setItem(20, plugin.getItemStates().SOUNDS_MENU_ENABLED_ICON);
      inventory.setItem(29, plugin.getItemStates().SOUNDS_MENU_ENABLED_BUTTON);
    } else {
      inventory.setItem(20, plugin.getItemStates().SOUNDS_MENU_DISABLED_ICON);
      inventory.setItem(29, plugin.getItemStates().SOUNDS_MENU_DISABLED_BUTTON);
    }

    if (wantsCommandSounds) {
      inventory.setItem(22, plugin.getItemStates().SOUNDS_COMMAND_ENABLED_ICON);
      inventory.setItem(31, plugin.getItemStates().SOUNDS_COMMAND_ENABLED_BUTTON);
    } else {
      inventory.setItem(22, plugin.getItemStates().SOUNDS_COMMAND_DISABLED_ICON);
      inventory.setItem(31, plugin.getItemStates().SOUNDS_COMMAND_DISABLED_BUTTON);
    }

    if (wantsHugSounds) {
      inventory.setItem(24, plugin.getItemStates().HUG_SOUNDS_ENABLED_ICON);
      inventory.setItem(33, plugin.getItemStates().HUG_SOUNDS_ENABLED_BUTTON);
    } else {
      inventory.setItem(24, plugin.getItemStates().HUG_SOUNDS_DISABLED_ICON);
      inventory.setItem(33, plugin.getItemStates().HUG_SOUNDS_DISABLED_BUTTON);
    }

    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose"));

    plugin.getGUIManager().getDefaultsSettingsSoundsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getDefaultsSettingsSoundsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getDefaultsSettingsSoundsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (slot == 48) {
      new DefaultsGUI(plugin).openGUI(player);
      return;
    }

    FileConfiguration config = plugin.getFileManager().getDefaultsYMLData();

    // Menu Sounds
    if (slot == 20 || slot == 29) {
      config.set("defaults.sounds.menu", !config.getBoolean("defaults.sounds.menu"));
      InventoryUtils.togglePair(inv, 20, 29, plugin.getItemStates().getSoundMenuSettingsItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Command Sounds
    if (slot == 22 || slot == 31) {
      config.set("defaults.sounds.commands", !config.getBoolean("defaults.sounds.commands"));
      InventoryUtils.togglePair(inv, 22, 31, plugin.getItemStates().getSoundCommandSettingsItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    // Hug Sounds
    if (slot == 24 || slot == 33) {
      config.set("defaults.sounds.hugs", !config.getBoolean("defaults.sounds.hugs"));
      InventoryUtils.togglePair(inv, 24, 33, plugin.getItemStates().getHugSoundSettingsItems());
      player.updateInventory();

      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }
  }

  @EventHandler
  public void onInventoryClose(InventoryCloseEvent event) {
    Inventory inv = event.getInventory();
    Player player = (Player) event.getPlayer();

    if (inv != plugin.getGUIManager().getDefaultsSettingsSoundsGUIs().get(player.getUniqueId())) {
      return;
    }

    try {
      plugin.getFileManager().getDefaultsYMLData().save(plugin.getFileManager().getDefaultsYMLFile());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }

}
