package me.nothixal.hugs.guis.adminpanel.overrides;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.util.Collections;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.guis.adminpanel.AdminPanelGUI;
import me.nothixal.hugs.guis.adminpanel.AdvancedSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.ExperimentalSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.GeneralSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.TechnicalSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.defaults.DefaultsGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class OverridesGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public OverridesGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6,
        ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Overrides"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createActiveHeader(inventory, 4,
        itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "),
        itemManager.createItem(XMaterial.LIGHT_BLUE_STAINED_GLASS_PANE, " "));

    inventory.setItem(0, itemManager.createItem(XMaterial.CHEST, "&aGeneral Settings"));
    inventory.setItem(1, itemManager.createItem(XMaterial.ENDER_CHEST, "&aAdvanced Settings"));
    inventory.setItem(2, itemManager.createItem(XMaterial.ANVIL, "&aTechnical Settings"));
    inventory.setItem(3, itemManager.createItem(XMaterial.KNOWLEDGE_BOOK, "&aDefaults"));
    inventory.setItem(4, itemManager.createItem(XMaterial.COMPARATOR, "&aOverrides"));
    inventory.setItem(5, itemManager.createItem(XMaterial.BREWING_STAND, "&aExperimental Settings"));

    if (plugin.getConfig().getBoolean(Settings.OVERRIDES.getValue())) {
      inventory.setItem(28, plugin.getItemStates().OVERRIDE_ENABLED_ICON);
      inventory.setItem(37, plugin.getItemStates().OVERRIDE_ENABLED_BUTTON);
    } else {
      inventory.setItem(28, plugin.getItemStates().OVERRIDE_DISABLED_ICON);
      inventory.setItem(37, plugin.getItemStates().OVERRIDE_DISABLED_BUTTON);
    }

    inventory.setItem(30, itemManager.createItem(XMaterial.PRISMARINE_CRYSTALS, "&eVisuals"));
    inventory.setItem(31, itemManager.createItem(XMaterial.BELL, "&eSounds"));
    inventory.setItem(32, itemManager.createItem(XMaterial.PAINTING, "&eText Indicators"));

    inventory.setItem(39, itemManager.createItem(XMaterial.RED_DYE, "&eVisual Overrides", Collections.singletonList("&7Click to edit.")));
    inventory.setItem(40, itemManager.createItem(XMaterial.RED_DYE, "&eSound Overrides", Collections.singletonList("&7Click to edit.")));
    inventory.setItem(41, itemManager.createItem(XMaterial.RED_DYE, "&eTextual Overrides", Collections.singletonList("&7Click to edit.")));


    String displayName = plugin.getLangFile().getString(Messages.CLOSE_MENU.getValue());
    inventory.setItem(45, itemManager.createItem(XMaterial.STRUCTURE_VOID, displayName));

    displayName = plugin.getLangFile().getString(Messages.GO_BACK.getValue());
    inventory.setItem(46, itemManager.createItem(XMaterial.SPECTRAL_ARROW, displayName));

    plugin.getGUIManager().getOverridesSettingsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getOverridesSettingsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getOverridesSettingsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 45) {
      player.closeInventory();
      return;
    }

    if (slot == 46) {
      new AdminPanelGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 0) {
      new GeneralSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 1) {
      new AdvancedSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
      return;
    }

    if (slot == 2) {
      new TechnicalSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 3) {
      new DefaultsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 4) {
      new OverridesGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 5) {
      new ExperimentalSettingsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 28 || slot == 37) {
      plugin.getConfig().set(Settings.OVERRIDES.getValue(), !plugin.getConfig().getBoolean(Settings.OVERRIDES.getValue()));
      InventoryUtils.togglePair(inv, 28, 37, plugin.getItemStates().getOverrideItems());
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 30 || slot == 39) {
      new OverrideVisualsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 31 || slot == 40) {
      new OverrideSoundsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }

    if (slot == 32 || slot == 41) {
      new OverrideIndicatorsGUI(plugin).openGUI(player);
      playSound(player, XSound.UI_BUTTON_CLICK);
    }
  }

  @EventHandler
  public void onInventoryClose(InventoryCloseEvent event) {
    Inventory inv = event.getInventory();
    Player player = (Player) event.getPlayer();

    if (inv != plugin.getGUIManager().getOverridesSettingsGUIs().get(player.getUniqueId())) {
      return;
    }

    plugin.saveConfig();
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsMenuSounds(player.getUniqueId())) {
      player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
    }
  }

}
