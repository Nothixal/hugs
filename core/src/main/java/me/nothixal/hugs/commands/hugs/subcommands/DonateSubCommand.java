package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DonateSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public DonateSubCommand(HugsPlugin plugin) {
    super("donate", Permissions.USE.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;
    int length = args.length;

    String tertiaryColor = PluginConstants.TERTIARY_COLOR_HEX;
    String prefix = Messages.PREFIX.getLangValue();
    String altPrefix = Messages.ALT_PREFIX.getLangValue();

    if (length == 0) {
      player.sendMessage(ChatUtils.colorChat(altPrefix + tertiaryColor + " Donate to the project!"));
      player.sendMessage(ChatUtils.colorChat(
          prefix + " &7If you enjoy using this plugin, consider donating."
          + "\n"
          + prefix + " &7My work is free, but I strive to put my best effort into all of my projects."
          + "\n"
          + prefix + " &bPaypal: " + PluginConstants.PAYPAL_DONATE_LINK
          + "\n"
          + prefix + " &aCashapp: " + PluginConstants.CASHAPP_DONATE_LINK
      ));
      return true;
    }

    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return new ArrayList<>();
  }
}
