package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.guis.NewTestGUI;
import me.nothixal.hugs.guis.TestGUI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class DebugSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public DebugSubCommand(HugsPlugin plugin) {
    super("debug", Permissions.ADMIN.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;
    int length = args.length;

//    Inventory inventory = Bukkit.createInventory(null, 9 * 6, "Hugs Item Debug");
//    inventory.addItem(plugin.getItemStates().HUGGABILITY_ALL_ICON);
//    inventory.addItem(plugin.getItemStates().HUGGABILITY_REALISTIC_ICON);
//    inventory.addItem(plugin.getItemStates().HUGGABILITY_OFF_ICON);
//    inventory.addItem(plugin.getItemStates().PARTICLES_OFF_ICON);
//    inventory.addItem(plugin.getItemStates().PARTICLES_LOW_ICON);
//    inventory.addItem(plugin.getItemStates().PARTICLES_MEDIUM_ICON);
//    inventory.addItem(plugin.getItemStates().PARTICLES_HIGH_ICON);
//    inventory.addItem(plugin.getItemStates().PARTICLES_EXTREME_ICON);
//    inventory.addItem(plugin.getItemStates().SOUNDS_MENU_ENABLED_ICON);
//    inventory.addItem(plugin.getItemStates().SOUNDS_COMMAND_ENABLED_ICON);
//
//    inventory.addItem(plugin.getItemStates().HUGGABILITY_ALL_BUTTON);
//    inventory.addItem(plugin.getItemStates().HUGGABILITY_REALISTIC_BUTTON);
//    inventory.addItem(plugin.getItemStates().HUGGABILITY_OFF_BUTTON);
//    inventory.addItem(plugin.getItemStates().PARTICLES_OFF_BUTTON);
//    inventory.addItem(plugin.getItemStates().PARTICLES_LOW_BUTTON);
//    inventory.addItem(plugin.getItemStates().PARTICLES_MEDIUM_BUTTON);
//    inventory.addItem(plugin.getItemStates().PARTICLES_HIGH_BUTTON);
//    inventory.addItem(plugin.getItemStates().PARTICLES_EXTREME_BUTTON);
//    inventory.addItem(plugin.getItemStates().SOUNDS_MENU_ENABLED_BUTTON);
//    inventory.addItem(plugin.getItemStates().SOUNDS_COMMAND_ENABLED_BUTTON);
//
//    player.openInventory(inventory);

//    TestGUI testGUI = new TestGUI();
//    Inventory inventory1 = Bukkit.createInventory(null, 9, "Test GUI!");
//    plugin.getNewGUIManager().registerHandledInventories(inventory1, testGUI);
//    player.openInventory(inventory1);


    plugin.getNewGUIManager().openGUI(new NewTestGUI(), player);
    return true;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return new ArrayList<>();
  }
}
