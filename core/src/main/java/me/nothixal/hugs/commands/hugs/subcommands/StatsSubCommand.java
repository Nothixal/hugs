package me.nothixal.hugs.commands.hugs.subcommands;

import com.cryptomorin.xseries.XSound;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.configuration.Cooldowns;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Errors;
import me.nothixal.hugs.enums.configuration.Messages.Lists;
import me.nothixal.hugs.guis.stats.ErosStatsGUI;
import me.nothixal.hugs.guis.stats.PlayerStatsGUI;
import me.nothixal.hugs.guis.stats.SilviaStatsGUI;
import me.nothixal.hugs.guis.stats.YourStatsGUI;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.utils.PlayerUtils;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.cooldowns.CooldownOLD;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StatsSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public StatsSubCommand(HugsPlugin plugin) {
    super("stats", Permissions.STATS.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;

    int length = args.length;

    if (length == 0) {
      if (plugin.getPlayerDataManager().prefersMenus(player.getUniqueId())) {
        new YourStatsGUI(plugin).openGUI(player);
        return true;
      }

      if (CooldownOLD.hasCooldown(player.getUniqueId(), Cooldowns.HUGS_STATS_COMMAND_COOLDOWN.getCooldownName())) {
        return true;
      }

      boolean hasGivenHugsBefore = plugin.getPlayerDataManager().hasGivenHugsBefore(player.getUniqueId());
      boolean hasReceivedHugsBefore = plugin.getPlayerDataManager().hasReceivedHugsBefore(player.getUniqueId());

      if (!hasGivenHugsBefore && !hasReceivedHugsBefore) {
        player.sendMessage(ChatUtils.colorChat(Errors.PLAYER_DATA_NOT_FOUND.getLangValue().replace("%target%", player.getName())));
        playErrorSound(player);
        return true;
      }

      sendPlayerStatistics(player, player);

      new CooldownOLD(player.getUniqueId(), Cooldowns.HUGS_STATS_COMMAND_COOLDOWN.getCooldownName(), Cooldowns.HUGS_STATS_COMMAND_COOLDOWN.getDuration()).start();
      return true;
    }

    if (length > 1) {
      player.sendMessage(ChatUtils.colorChat(Errors.TOO_MANY_ARGUMENTS.getLangValue()));
      playErrorSound(player);
      return true;
    }

    if (args[0].length() > 16) {
      player.sendMessage(ChatUtils.colorChat(Errors.INVALID_PLAYER.getLangValue()));
      playErrorSound(player);
      return true;
    }

    if (args[0].equalsIgnoreCase("Hugs:Eros")) {
      new ErosStatsGUI(plugin).openGUI(player);
      return true;
    }

    if (args[0].equalsIgnoreCase("Hugs:Silvia")) {
      new SilviaStatsGUI(plugin).openGUI(player);
      return true;
    }

    Bukkit.getScheduler().runTask(this.plugin, () -> {
      @SuppressWarnings("deprecation")
      OfflinePlayer receiver = Bukkit.getServer().getOfflinePlayer(args[0]);

      if (plugin.getPlayerDataManager().prefersMenus(player.getUniqueId())) {
        new PlayerStatsGUI(plugin).openGUI(player, receiver);
        return;
      }

      boolean hasGivenHugsBefore = plugin.getPlayerDataManager().hasGivenHugsBefore(receiver.getUniqueId());
      boolean hasReceivedHugsBefore = plugin.getPlayerDataManager().hasReceivedHugsBefore(receiver.getUniqueId());

      if (!hasGivenHugsBefore && !hasReceivedHugsBefore) {
        player.sendMessage(ChatUtils.colorChat(Errors.PLAYER_DATA_NOT_FOUND.getLangValue().replace("%target%", receiver.getName())));
        playErrorSound(player);
        return;
      }

      sendPlayerStatistics(player, receiver);
      plugin.getVerboseManager().logVerbose(player.getName() + " retrieved " + receiver.getName() + "'s player data.");
    });
    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return null;
  }

  private void sendPlayerStatistics(Player sender, OfflinePlayer receiver) {
    List<String> playerProfileData;

    if (sender == receiver) {
      playerProfileData = Lists.YOUR_PROFILE.getLangValue();
    } else {
      playerProfileData = Lists.PLAYER_PROFILE.getLangValue();
    }

    PlayerDataManager manager = plugin.getPlayerDataManager();

    int totalSelfHugsGiven = manager.getSelfHugs(receiver.getUniqueId());
    int totalHugsGiven = manager.getHugsGiven(receiver.getUniqueId());
    int totalHugsReceived = manager.getHugsReceived(receiver.getUniqueId());
    int totalMassHugsGiven = manager.getMassHugsGiven(receiver.getUniqueId());
    int totalMassHugsReceived = manager.getMassHugsReceived(receiver.getUniqueId());
    int totalUniqueHugsGiven = manager.getUniqueHugsGiven(receiver.getUniqueId());

    for (int i = 0; i < playerProfileData.size(); i++) {
      if (playerProfileData.get(i).contains("%target%")) {
        playerProfileData.set(i, playerProfileData.get(i).replace("%target%", receiver.getName()));
      }

      if (playerProfileData.get(i).contains("%total_self_hugs_given%")) {
//        if (totalSelfHugsGiven == 0) {
//          continue;
//        }

        playerProfileData.set(i, playerProfileData.get(i).replace("%total_self_hugs_given%", Integer.toString(totalSelfHugsGiven)));
      }

      if (playerProfileData.get(i).contains("%total_hugs_given%") || playerProfileData.get(i).contains("%total_hugs_received%")) {
//        if ((totalHugsGiven == 0 && totalHugsReceived == 0)) {
//          continue;
//        }

        playerProfileData.set(i, playerProfileData.get(i).replace("%total_hugs_given%", Integer.toString(totalHugsGiven)));
        playerProfileData.set(i, playerProfileData.get(i).replace("%total_hugs_received%", Integer.toString(totalHugsReceived)));
      }

      if (playerProfileData.get(i).contains("%total_mass_hugs_given%") || playerProfileData.get(i).contains("%total_mass_hugs_received%")) {
//        if (totalMassHugsGiven == 0 && totalMassHugsReceived == 0) {
//          continue;
//        }

        playerProfileData.set(i, playerProfileData.get(i).replace("%total_mass_hugs_given%", Integer.toString(totalMassHugsGiven)));
        playerProfileData.set(i, playerProfileData.get(i).replace("%total_mass_hugs_received%", Integer.toString(totalMassHugsReceived)));
      }

      if (playerProfileData.get(i).contains("%total_unique_hugs%")) {
        playerProfileData.set(i, playerProfileData.get(i).replace("%total_unique_hugs%", Integer.toString(totalUniqueHugsGiven)));
      }

      sender.sendMessage(ChatUtils.colorChat(playerProfileData.get(i)));
    }
  }

  private void playErrorSound(Player player) {
    if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
      playSound(player, XSound.ENTITY_VILLAGER_NO);
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
      PlayerUtils.playSound(player, sound);
    }
  }
}
