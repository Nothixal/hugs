package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.preferences.HuggableState;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.guis.settings.player.YourPreferencesGUI;
import me.nothixal.hugs.utils.Indicator;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.text.TextUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

public class PreferencesSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public PreferencesSubCommand(HugsPlugin plugin) {
    super("preferences", Permissions.PREFERENCES.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;
    int length = args.length;

    if (length == 0) {
      if (plugin.getPlayerDataManager().prefersMenus(player.getUniqueId())) {
        new YourPreferencesGUI(plugin).openGUI(player);
      } else {
        sendPreferences(player);
      }
      return true;
    }

    if (length == 1) {
      if (args[0].equalsIgnoreCase("chat")) {
        plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), IndicatorType.CHAT, !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.CHAT));

        if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.CHAT)) {
          player.sendMessage(ChatUtils.colorChat(Messages.CHAT_ENABLED.getLangValue()));
        } else {
          player.sendMessage(ChatUtils.colorChat(Messages.CHAT_DISABLED.getLangValue()));
        }
      }

      if (args[0].equalsIgnoreCase("actionbar")) {
        plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), IndicatorType.ACTIONBAR, !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.ACTIONBAR));

        if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.ACTIONBAR)) {
          player.sendMessage(ChatUtils.colorChat(Messages.ACTIONBAR_ENABLED.getLangValue()));
        } else {
          player.sendMessage(ChatUtils.colorChat(Messages.ACTIONBAR_DISABLED.getLangValue()));
        }
      }

      if (args[0].equalsIgnoreCase("titles")) {
        plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), IndicatorType.TITLES, !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TITLES));

        if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TITLES)) {
          player.sendMessage(ChatUtils.colorChat(Messages.TITLES_ENABLED.getLangValue()));
        } else {
          player.sendMessage(ChatUtils.colorChat(Messages.TITLES_DISABLED.getLangValue()));
        }
      }

      if (args[0].equalsIgnoreCase("bossbar")) {
        plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), IndicatorType.BOSSBAR, !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.BOSSBAR));

        if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.BOSSBAR)) {
          player.sendMessage(ChatUtils.colorChat(Messages.BOSSBAR_ENABLED.getLangValue()));
        } else {
          player.sendMessage(ChatUtils.colorChat(Messages.BOSSBAR_DISABLED.getLangValue()));
        }
      }

      if (args[0].equalsIgnoreCase("toast")) {
        plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), IndicatorType.TOASTS, !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TOASTS));

        if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TOASTS)) {
          player.sendMessage(ChatUtils.colorChat(Messages.TOASTS_ENABLED.getLangValue()));
        } else {
          player.sendMessage(ChatUtils.colorChat(Messages.TOASTS_DISABLED.getLangValue()));
        }
      }

      if (args[0].equalsIgnoreCase("huggability")) {
        plugin.getPlayerDataManager().setHuggability(player.getUniqueId(), plugin.getPlayerDataManager().getHuggability(player.getUniqueId()).getNext());

        player.sendMessage(ChatUtils.colorChat("&7Your huggability is now: " + plugin.getPlayerDataManager().getHuggability(player.getUniqueId()).getValue()));
//
//          if (dataManager.wantsHugs(player.getUniqueId())) {
//            player.sendMessage(ChatUtils.colorChat(Messages.HUG_STATUS_HUGGABLE.getLangValue()));
//          } else {
//            player.sendMessage(ChatUtils.colorChat(Messages.HUG_STATUS_NOT_HUGGABLE.getLangValue()));
//          }
      }

      if (args[0].equalsIgnoreCase("menus")) {
        plugin.getPlayerDataManager().setPrefersMenus(player.getUniqueId(), !plugin.getPlayerDataManager().prefersMenus(player.getUniqueId()));

        if (plugin.getPlayerDataManager().prefersMenus(player.getUniqueId())) {
          player.sendMessage(ChatUtils.colorChat(Messages.MENUS_ENABLED.getLangValue()));
        } else {
          player.sendMessage(ChatUtils.colorChat(Messages.MENUS_DISABLED.getLangValue()));
        }
      }

      //TODO: Implement functionality
      if (args[0].equalsIgnoreCase("reset")) {

      }

//      if (length == 2) {
//
//      }
    }

    return false;
  }

  //TODO: Make this section translatable.
  private void sendPreferences(Player player) {
    String primaryColor = PluginConstants.DEFAULT_PREFIX_COLOR_HEX;
    String tertiaryColor = PluginConstants.TERTIARY_COLOR_HEX;
    String prefix = Messages.PREFIX.getLangValue();
    String altPrefix = Messages.ALT_PREFIX.getLangValue();

    HuggableState huggability = plugin.getPlayerDataManager().getHuggability(player.getUniqueId());
    String particleQuality = TextUtils.capitalizeFirstLetter(plugin.getPlayerDataManager().getParticleQuality(player.getUniqueId()).getValue());
    String particleQualityColor = plugin.getPlayerDataManager().getParticleQuality(player.getUniqueId()).getColor();
    boolean sound = plugin.getPlayerDataManager().wantsHugSounds(player.getUniqueId());

    player.sendMessage(ChatUtils.colorChat(altPrefix + tertiaryColor + " Your Preferences"));
    player.sendMessage(ChatUtils.colorChat(prefix + " &7Huggability: &a" + TextUtils.capitalizeFirstLetter(huggability.getValue())));
    player.sendMessage(ChatUtils.colorChat(prefix + " &7Particle Quality: " + particleQualityColor + particleQuality));
    player.sendMessage(ChatUtils.colorChat(prefix + " &7Sounds: " + (sound ? "&atrue" : "&cfalse")));

    List<Indicator> test = plugin.getPlayerDataManager().getIndicators(player.getUniqueId());

    StringBuilder builder = new StringBuilder();

    for (int i = 0; i < test.size(); i++) {
      if (test.get(i).isEnabled()) {
        builder.append("&a").append(TextUtils.capitalizeFirstLetter(test.get(i).getType().getValue()));
      } else {
        builder.append("&c").append(TextUtils.capitalizeFirstLetter(test.get(i).getType().getValue()));
      }

      if (i != test.size() - 1) {
        builder.append("&7, ");
      }
    }

    player.sendMessage(ChatUtils.colorChat(prefix + " &7Indicators: " + builder));
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    if (args.length == 1) {
      List<String> commands = Arrays.asList("chat", "actionbar", "titles", "bossbar", "toasts", "huggability", "menus");
      List<String> completions = new ArrayList<>();

      StringUtil.copyPartialMatches(args[0], commands, completions);
      Collections.sort(completions);
      return completions;
    }

    if (args.length >=3) {
      return new ArrayList<>();
    }

    return null;
//    return new ArrayList<>();
  }
}
