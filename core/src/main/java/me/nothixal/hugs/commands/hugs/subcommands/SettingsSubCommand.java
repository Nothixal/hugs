package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.guis.adminpanel.AdminPanelGUI;
import me.nothixal.hugs.guis.adminpanel.AdvancedSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.ExperimentalSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.GeneralSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.TechnicalSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.defaults.DefaultsGUI;
import me.nothixal.hugs.guis.adminpanel.overrides.OverridesGUI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

public class SettingsSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public SettingsSubCommand(HugsPlugin plugin) {
    super("settings", Permissions.SETTINGS.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;
    int length = args.length;

    if (length == 0) {
      new AdminPanelGUI(plugin).openGUI(player);
      return true;
    }

    if (length == 1) {
      if (args[0].equalsIgnoreCase("general")) {
        new GeneralSettingsGUI(plugin).openGUI(player);
        return true;
      }

      if (args[0].equalsIgnoreCase("advanced")) {
        new AdvancedSettingsGUI(plugin).openGUI(player);
        return true;
      }

      if (args[0].equalsIgnoreCase("technical")) {
        new TechnicalSettingsGUI(plugin).openGUI(player);
        return true;
      }

      if (args[0].equalsIgnoreCase("defaults")) {
        new DefaultsGUI(plugin).openGUI(player);
        return true;
      }

      if (args[0].equalsIgnoreCase("overrides")) {
        new OverridesGUI(plugin).openGUI(player);
        return true;
      }

      if (args[0].equalsIgnoreCase("experimental")) {
        new ExperimentalSettingsGUI(plugin).openGUI(player);
        return true;
      }
    }

    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    if (args.length == 1) {
      List<String> commands = Arrays.asList("general", "advanced", "technical", "defaults", "overrides", "experimental");
      List<String> completions = new ArrayList<>();

      StringUtil.copyPartialMatches(args[0], commands, completions);
      Collections.sort(completions);
      return completions;
    }

    if (args.length >=2) {
      return new ArrayList<>();
    }

    return null;
  }
}
