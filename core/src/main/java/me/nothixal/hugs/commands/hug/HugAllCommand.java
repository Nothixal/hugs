package me.nothixal.hugs.commands.hug;

import com.cryptomorin.xseries.XSound;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Errors;
import me.nothixal.hugs.managers.HugManager;
import me.nothixal.hugs.utils.PlayerUtils;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class HugAllCommand implements TabExecutor {

  private final HugsPlugin plugin;
  private final HugManager hugManager;

  public HugAllCommand(HugsPlugin plugin) {
    this.plugin = plugin;
    this.hugManager = plugin.getHugManager();
  }

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;
    int length = args.length;

    if (length > 0) {
      player.sendMessage(ChatUtils.colorChat(Errors.TOO_MANY_ARGUMENTS.getLangValue()));
      playErrorSound(player);
      return true;
    }

    if (!player.hasPermission(Permissions.HUG_ALL.getPermissionNode())) {
      sendNoPermissionMessage(player, Permissions.HUG_ALL);
      playErrorSound(player);
      return true;
    }

    if (Bukkit.getServer().getOnlinePlayers().size() < 3) {
      player.sendMessage(ChatUtils.colorChat(Errors.NOT_ENOUGH_PLAYERS.getLangValue()));
      playErrorSound(player);
      return true;
    }

    hugManager.giveMassHug(player);
    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
    return null;
  }

  private void sendNoPermissionMessage(Player player, Permissions permission) {
    player.sendMessage(ChatUtils.colorChat(
        Errors.NO_PERMISSION.getLangValue()).replace("%permission%", permission.getPermissionNode()));
  }

  private void playErrorSound(Player player) {
    playSound(player, XSound.ENTITY_VILLAGER_NO);
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
      PlayerUtils.playSound(player, sound);
    }
  }
}
