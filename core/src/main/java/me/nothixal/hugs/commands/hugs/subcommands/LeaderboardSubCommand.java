package me.nothixal.hugs.commands.hugs.subcommands;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.guis.LeaderboardGUI;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

public class LeaderboardSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public LeaderboardSubCommand(HugsPlugin plugin) {
    super("leaderboard", Permissions.LEADERBOARD.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sendTextLeaderboard(sender);
      return true;
    }

    Player player = (Player) sender;
    int length = args.length;

    if (!plugin.getPlayerDataManager().prefersMenus(player.getUniqueId())) {
      if (length == 0) {
        sendTextLeaderboard(player, "normal_given");
        return true;
      }


      if (length == 1) {
        if (args[0].equalsIgnoreCase("normal_given")) {
          sendTextLeaderboard(player, "normal_given");
          return true;
        }

        if (args[0].equalsIgnoreCase("normal_received")) {
          sendTextLeaderboard(player, "normal_received");
          return true;
        }

        if (args[0].equalsIgnoreCase("mass_given")) {
          sendTextLeaderboard(player, "mass_given");
          return true;
        }

        if (args[0].equalsIgnoreCase("mass_received")) {
          sendTextLeaderboard(player, "mass_received");
          return true;
        }

        if (args[0].equalsIgnoreCase("self_hugs")) {
          sendTextLeaderboard(player, "self_hugs");
          return true;
        }
      }

      return true;
    }

    //player.sendMessage(ChatUtils.colorChat("&3&lHugs &8&l>> &7Retrieving leaderboard. This could take a while."));
    new LeaderboardGUI(plugin).openGUI(player);
    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    if (args.length == 1) {
      List<String> commands = Arrays.asList("normal_given", "normal_received", "mass_given", "mass_received", "self_hugs");
      List<String> completions = new ArrayList<>();

      StringUtil.copyPartialMatches(args[0], commands, completions);
      Collections.sort(completions);
      return completions;
    }

    if (args.length >=2) {
      return new ArrayList<>();
    }

    return null;
  }

  private void sendTextLeaderboard(CommandSender sender) {
    String prefix = Messages.PREFIX.getLangValue();

    sender.sendMessage(ChatUtils.colorChat(prefix + " &dLeaderboard Data Here"));
  }

  private void sendTextLeaderboard(Player player, String sortingMethod) {
    String primaryColor = PluginConstants.DEFAULT_PREFIX_COLOR_HEX;
    String tertiaryColor = PluginConstants.TERTIARY_COLOR_HEX;
    String prefix = Messages.PREFIX.getLangValue();

    String header = prefix + tertiaryColor + " Leaderboard (%sorting_by%)";

    player.sendMessage(ChatUtils.colorChat(header.replace("%sorting_by%", sortingMethod)));

    File playerDataDirectory = new File(plugin.getFileManager().getPlayerDataDirectory());

    File[] files = playerDataDirectory.listFiles();

    if (files == null) {
      return;
    }

    Map<UUID, Integer> unSortedMap = new HashMap<>();

    for (File file : files) {
      if (file.isDirectory()) {
        continue;
      }

      if (!file.getName().endsWith(".yml")) {
        continue;
      }

      UUID uuid = UUID.fromString(file.getName().replace(".yml", ""));

      switch (sortingMethod) {
        case "normal_given":
          unSortedMap.put(uuid, plugin.getPlayerDataManager().getHugsGiven(uuid));
          break;
        case "normal_received":
          unSortedMap.put(uuid, plugin.getPlayerDataManager().getHugsReceived(uuid));
          break;
        case "mass_given":
          unSortedMap.put(uuid, plugin.getPlayerDataManager().getMassHugsGiven(uuid));
          break;
        case "mass_received":
          unSortedMap.put(uuid, plugin.getPlayerDataManager().getMassHugsReceived(uuid));
          break;
        case "self_hugs":
          unSortedMap.put(uuid, plugin.getPlayerDataManager().getSelfHugs(uuid));
          break;
        default:
          unSortedMap.put(uuid, plugin.getPlayerDataManager().getHugsGiven(uuid));
          break;
      }
    }

    //LinkedHashMap preserve the ordering of elements in which they are inserted
    LinkedHashMap<UUID, Integer> sortedMap = new LinkedHashMap<>();

    unSortedMap.entrySet()
        .stream()
        .limit(5)
        .sorted(Entry.<UUID, Integer>comparingByValue().reversed())
        .forEachOrdered(uuidIntegerEntry -> {
          if (uuidIntegerEntry.getValue() > 0) {
            sortedMap.put(uuidIntegerEntry.getKey(), uuidIntegerEntry.getValue());
          }
        });

    int rank = 1;

    if (sortedMap.isEmpty()) {
      player.sendMessage(ChatUtils.colorChat(Messages.PREFIX.getLangValue() + " &cNo one has given any hugs! :("));
      return;
    }

    for (Entry<UUID, Integer> entry : sortedMap.entrySet()) {
      String message = "&a" + rank++ + ". " + Bukkit.getOfflinePlayer(entry.getKey()).getName() + " - ";
      String total = "&e" + entry.getValue();

      player.sendMessage(ChatUtils.colorChat(message + total));
    }

//    if (sortedMap.size() < 5) {
//      player.sendMessage(ChatUtils.colorChat("&cEmpty Space"));
//    }

  }
}
