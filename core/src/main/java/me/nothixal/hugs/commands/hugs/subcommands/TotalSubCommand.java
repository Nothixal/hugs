package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.configuration.Cooldowns;
import me.nothixal.hugs.enums.configuration.Messages.Lists;
import me.nothixal.hugs.utils.cooldowns.CooldownOLD;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TotalSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public TotalSubCommand(HugsPlugin plugin) {
    super("total", "hugs.total", true);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      getTotal(sender);
      return true;
    }

    Player player = (Player) sender;

    if (CooldownOLD.hasCooldown(player.getUniqueId(), Cooldowns.HUGS_TOTAL_COMMAND_COOLDOWN.getCooldownName())) {
      return true;
    }

    getTotal(player);

    new CooldownOLD(player.getUniqueId(), Cooldowns.HUGS_TOTAL_COMMAND_COOLDOWN.getCooldownName(), Cooldowns.HUGS_TOTAL_COMMAND_COOLDOWN.getDuration()).start();

    plugin.getVerboseManager().logVerbose(player.getName() + " looked at the total amount of hugs given.");

    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return new ArrayList<>();
  }

  private void getTotal(CommandSender sender) {
    plugin.reloadData();

    int selfHugs = plugin.getDataFile().getInt("total_self_hugs_given");
    int hugs = plugin.getDataFile().getInt("total_hugs_given");
    int massHugs = plugin.getDataFile().getInt("total_mass_hugs_given");

    for (String s : Lists.TOTAL.getLangValue()) {
      s = s.replace("%self_hugs_total%", "" + selfHugs);
      s = s.replace("%normal_hugs_total%", "" + hugs);
      s = s.replace("%mass_hugs_total%", "" + massHugs);
      sender.sendMessage(ChatUtils.colorChat(s));
    }
  }
}
