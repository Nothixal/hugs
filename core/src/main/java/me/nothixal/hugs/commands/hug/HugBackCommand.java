package me.nothixal.hugs.commands.hug;

import com.cryptomorin.xseries.XSound;
import java.util.List;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Errors;
import me.nothixal.hugs.managers.HugManager;
import me.nothixal.hugs.utils.PlayerUtils;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HugBackCommand implements TabExecutor {

  private final HugsPlugin plugin;
  private final HugManager hugManager;

  public HugBackCommand(HugsPlugin plugin) {
    this.plugin = plugin;
    this.hugManager = plugin.getHugManager();
  }

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;
    Player receiver;

    int length = args.length;

    String id = plugin.getPlayerDataManager().getLastHugReceivedFrom(player.getUniqueId());

    if (id.equals("") || id.equals("None")) {
      player.sendMessage(ChatUtils.colorChat(Errors.NO_RECENT_HUG_GIVEN.getLangValue()));
      playErrorSound(player);
      return true;
    }

    UUID uuid = UUID.fromString(plugin.getPlayerDataManager().getLastHugReceivedFrom(player.getUniqueId()));

    if (uuid == null) {
      player.sendMessage(ChatUtils.colorChat(Errors.NO_RECENT_HUG_GIVEN.getLangValue()));
      playErrorSound(player);
      return true;
    }

    receiver = Bukkit.getPlayer(uuid);

    if (length == 0) {
      if (receiver == null) {
        player.sendMessage(ChatUtils.colorChat(Errors.PLAYER_NOT_FOUND.getLangValue().replace("%target%", Bukkit.getOfflinePlayer(uuid).getName())));
        playErrorSound(player);
        return true;
      }

      hugManager.giveNormalHug(player, receiver);
      return true;
    }

    return false;
  }

  @Nullable
  @Override
  public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label,
      @NotNull String[] args) {
    return null;
  }

  private void playErrorSound(Player player) {
    playSound(player, XSound.ENTITY_VILLAGER_NO);
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
      PlayerUtils.playSound(player, sound);
    }
  }
}
