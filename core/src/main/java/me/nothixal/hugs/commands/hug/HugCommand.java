package me.nothixal.hugs.commands.hug;

import com.cryptomorin.xseries.XSound;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Cooldowns;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Errors;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.enums.configuration.Usages;
import me.nothixal.hugs.managers.HugManager;
import me.nothixal.hugs.utils.PlayerUtils;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.cooldowns.CooldownOLD;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;
import org.jetbrains.annotations.Nullable;

public class HugCommand implements TabExecutor {

  private final List<String> everyone = Arrays.asList("@a", "*");
  private final List<String> random = Collections.singletonList("@r");

  private final HugsPlugin plugin;
  private final HugManager hugManager;

  public HugCommand(HugsPlugin plugin) {
    this.plugin = plugin;
    this.hugManager = plugin.getHugManager();
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;
    Player receiver;
    int length = args.length;

    if (!player.hasPermission(Permissions.HUG.getPermissionNode())) {
      sendNoPermissionMessage(player, Permissions.HUG);
      return true;
    }

    // Proper Usage Message
    if (length == 0) {
      String hugCmdCooldown = Cooldowns.HUG_COMMAND_COOLDOWN.getCooldownName();

      // Prevent the user from spamming the command with no arguments. For whatever reason that might be.
      if (CooldownOLD.hasCooldown(player.getUniqueId(), hugCmdCooldown)) {
        return true;
      }

      player.sendMessage(ChatUtils.colorChat(Usages.HUG.getLangValue()));
      new CooldownOLD(player, hugCmdCooldown, Cooldowns.HUG_COMMAND_COOLDOWN.getDuration()).start();
      return true;
    }

    // TODO: Re-evaluate whether or not this is necessary.
    if (length > 1) {
      player.sendMessage(ChatUtils.colorChat(Errors.TOO_MANY_ARGUMENTS.getLangValue()));
      playErrorSound(player);
      return true;
    }

    // Check to see if the player is trying to give a mass hug.
    if (everyone.contains(args[0].toLowerCase())) {
      if (!player.hasPermission(Permissions.HUG_ALL.getPermissionNode())) {
        sendNoPermissionMessage(player, Permissions.HUG_ALL);
        playErrorSound(player);
        return true;
      }

      //TODO Un-Comment when ready to release.
      if (Bukkit.getServer().getOnlinePlayers().size() < 3) {
        player.sendMessage(ChatUtils.colorChat(Errors.NOT_ENOUGH_PLAYERS.getLangValue()));
        playErrorSound(player);
        return true;
      }

      hugManager.giveMassHug(player);
      plugin.getVerboseManager().logVerbose(player.getName() + " gave everyone a hug! :O");
      return true;
    }

    // Check to see if the player is trying to give a random hug.
    if (random.contains(args[0].toLowerCase())) {
      if (!player.hasPermission(Permissions.HUG_RANDOM.getPermissionNode())) {
        sendNoPermissionMessage(player, Permissions.HUG_RANDOM);
        return true;
      }

      List<Player> players = new ArrayList<>(Bukkit.getServer().getOnlinePlayers());

      players.remove(player);
      players.removeIf(p -> !plugin.getPlayerDataManager().wantsHugs(p.getUniqueId()));

      if (players.size() == 0) {
        player.sendMessage(ChatUtils.colorChat(Errors.NO_RANDOM_PLAYERS.getLangValue()));
        playErrorSound(player);
        return true;
      }

      hugManager.giveNormalHug(player, players.get(new Random().nextInt(players.size())));
      return true;
    }

    // Check to see if the target is real.
    if (args[0].length() > 16) {
      player.sendMessage(ChatUtils.colorChat(Errors.INVALID_PLAYER.getLangValue()));
      playErrorSound(player);
      return true;
    }

    // Check to see if the server owner wanted to force exact usernames when using the command.
    if (plugin.getConfig().getBoolean(Settings.FORCE_EXACT_NAMES.getValue())) {
      receiver = Bukkit.getServer().getPlayerExact(args[0]);
    } else {
      receiver = Bukkit.getServer().getPlayer(args[0]);
    }

    // Check to see if the target is online.
    if (receiver == null) {
      player.sendMessage(ChatUtils.colorChat(Errors.PLAYER_NOT_FOUND.getLangValue().replace("%target%", args[0])));
      playErrorSound(player);
      return true;
    }

    // Check to see if realistic hugs are being forced.
    if (plugin.getConfig().getBoolean(Settings.REALISTIC_HUGS.getValue())) {
      player.sendMessage(ChatUtils.colorChat("&c&lERROR: &7You can only give hugs by shift clicking players!"));
      return true;
    }

    // Check to see if the player is trying to give a self hug.
    if (player == receiver) {
      if (!plugin.getConfig().getBoolean(Settings.ALLOW_SELF_HUG.getValue())) {
        player.sendMessage(ChatUtils.colorChat(Errors.NO_SELF_HUG.getLangValue()));
        playErrorSound(player);
        return true;
      }

      if (!player.hasPermission(Permissions.HUG_SELF.getPermissionNode())) {
        sendNoPermissionMessage(player, Permissions.HUG_SELF);
        return true;
      }

      hugManager.giveSelfHug(player);
      plugin.getVerboseManager().logVerbose(player.getName() + " hugged themself.");
      return true;
    }

    // Give a normal hug.
    hugManager.giveNormalHug(player, receiver);
    plugin.getVerboseManager().logVerbose(player.getName() + " gave " + receiver.getName() + " a hug. :3");
    return true;
  }

  private void playErrorSound(Player player) {
    playSound(player, XSound.ENTITY_VILLAGER_NO);
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
      PlayerUtils.playSound(player, sound);
    }
  }

  private void sendNoPermissionMessage(Player player, Permissions permission) {
    player.sendMessage(ChatUtils.colorChat(
        Errors.NO_PERMISSION.getLangValue()).replace("%permission%", permission.getPermissionNode()));
  }

  @Nullable
  @Override
  public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
    if (args.length == 1) {
      List<String> commands = new ArrayList<>();
      List<String> completions = new ArrayList<>();

      commands.add("*");
      commands.add("@a");
      commands.add("@r");

      for (Player player : Bukkit.getOnlinePlayers()) {
        commands.add(player.getName());
      }

      StringUtil.copyPartialMatches(args[0], commands, completions);
      Collections.sort(completions);
      return completions;
    }

    return null;
  }
}