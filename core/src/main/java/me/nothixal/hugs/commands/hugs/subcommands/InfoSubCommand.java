package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.guis.PluginArchivesGUI;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.chat.ClickableMessage;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InfoSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public InfoSubCommand(HugsPlugin plugin) {
    super("info", Permissions.INFO.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;
    int length = args.length;

    if (!plugin.getPlayerDataManager().prefersMenus(player.getUniqueId())) {
      sendPluginInformation(player);
      return true;
    }

    if (length == 0) {
      new PluginArchivesGUI(plugin).openGUI(player);
      return true;
    }

    return false;
  }

  private void sendPluginInformation(Player player) {
    String primaryColor = PluginConstants.DEFAULT_PREFIX_COLOR_HEX;
    String tertiaryColor = PluginConstants.TERTIARY_COLOR_HEX;
    String prefix = Messages.PREFIX.getLangValue();
    String altPrefix = Messages.ALT_PREFIX.getLangValue();

    player.sendMessage(ChatUtils.colorChat(altPrefix + tertiaryColor + " Plugin Information"));
    player.sendMessage(ChatUtils.colorChat(prefix + " &7Server Version: &a" + plugin.getVersionChecker().getServerVersion().getDisplayValue()));
    player.sendMessage(ChatUtils.colorChat(prefix + " &7Plugin Version: &a" + plugin.getDescription().getVersion()));

//    BaseComponent[] test = new ComponentBuilder()
//        .append(TextComponent.fromLegacyText(ChatUtils.colorChat(prefix)))
//        .append(" &a[WEBSITE]")
//        .create();
//
//    player.spigot().sendMessage(test);

    ClickableMessage clickableMessage = new ClickableMessage();
    clickableMessage.addLegacy(prefix);
    clickableMessage.add(" ");
    clickableMessage.add("&a[WEBSITE]");
    clickableMessage.hover(
        "&7While visiting, consider giving",
        "&7the plugin a &e✰✰✰✰✰ &7rating!",
        "",
        "&eClick to visit!"
    );
    clickableMessage.link(clickableMessage.getCurrent(), PluginConstants.PLUGIN_WEBSITE_LINK);

    clickableMessage.add(" ");
    clickableMessage.add("&3[WIKI]");
    clickableMessage.hover("&eClick to visit!");
    clickableMessage.link(clickableMessage.getCurrent(), PluginConstants.PLUGIN_WIKI_LINK);

    clickableMessage.add(" ");
    clickableMessage.add("&c[SOURCE CODE]");
    clickableMessage.hover("&eClick to view!");
    clickableMessage.link(clickableMessage.getCurrent(), PluginConstants.PLUGIN_SOURCE_CODE_LINK);

    clickableMessage.add(" ");
    clickableMessage.add("&e[BUG TRACKER]");
    clickableMessage.hover("&eClick to view!");
    clickableMessage.link(clickableMessage.getCurrent(), PluginConstants.PLUGIN_BUG_TRACKER);

    clickableMessage.add(" ");
    clickableMessage.add("&9[DISCORD]");
    clickableMessage.hover("&eClick to join!");
    clickableMessage.link(clickableMessage.getCurrent(), PluginConstants.DISCORD_INVITE_LINK);

    clickableMessage.sendToPlayer(player);

  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return new ArrayList<>();
  }

}
