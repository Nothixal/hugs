package me.nothixal.hugs.commands.hugs.subcommands;

import com.cryptomorin.xseries.XSound;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Errors;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.guis.ConfirmationGUI;
import me.nothixal.hugs.utils.PlayerUtils;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class PurgeSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public PurgeSubCommand(HugsPlugin plugin) {
    super("purge", Permissions.PURGE.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command.");
      return true;
    }

    Player player = (Player) sender;

    int length = args.length;

    if (length == 0) {
      player.sendMessage(ChatUtils.colorChat(Errors.TOO_FEW_ARGUMENTS.getLangValue()));

      playErrorSound(player);
      return true;
    }

    if (length > 1) {
      player.sendMessage(ChatUtils.colorChat(Errors.TOO_MANY_ARGUMENTS.getLangValue()));
      playErrorSound(player);
      return true;
    }

    OfflinePlayer receiver = Bukkit.getServer().getOfflinePlayer(args[0]);

    // TODO: Rewrite this section.
    if (plugin.getConfig().getBoolean(Settings.CONFIRMATION_ENABLED.getValue())) {
      if (everyoneWasMentioned(args[0])) {
        if (!player.hasPermission(Permissions.PURGE_ALL.getPermissionNode())) {
          player.sendMessage(ChatUtils.colorChat(Errors.NO_PERMISSION.getLangValue().replace("%permission%", Permissions.PURGE_ALL.getPermissionNode())));
          playErrorSound(player);
          return true;
        }

        if (plugin.getPlayerDataManager().prefersMenus(player.getUniqueId())) {
          plugin.getHugsCommand().getConfirming().put(player, receiver.getName());
          new ConfirmationGUI(plugin).openGUI(player);
          return true;
        }

        player.sendMessage(ChatUtils.colorChat(Messages.CONFIRMATION_ALL_MESSAGE.getLangValue()));

        plugin.getHugsCommand().getTyping2().put(player.getUniqueId(), receiver.getUniqueId());
        return true;
      }

      if (isRealPlayer(args[0])) {
        player.sendMessage(ChatUtils.colorChat(Errors.INVALID_PLAYER.getLangValue()));
        playErrorSound(player);
        return true;
      }

      if (plugin.getPlayerDataManager().prefersMenus(player.getUniqueId())) {
        plugin.getHugsCommand().getConfirming2().put(player.getUniqueId(), receiver.getUniqueId());
        new ConfirmationGUI(plugin).openGUI(player);
        return true;
      }

      player.sendMessage(ChatUtils.colorChat(Messages.CONFIRMATION_MESSAGE.getLangValue().replace("%player%", args[0])));

      // TODO: Replace with Conversations API
      plugin.getHugsCommand().getTyping2().put(player.getUniqueId(), receiver.getUniqueId());
      return true;
    }

    if (isRealPlayer(args[0])) {
      player.sendMessage(ChatUtils.colorChat(Errors.INVALID_PLAYER.getLangValue()));
      playErrorSound(player);
      return true;
    }

    if (everyoneWasMentioned(args[0])) {
      plugin.getPlayerDataManager().purgeDatabase();
      player.sendMessage(ChatUtils.colorChat(Messages.PURGED_ALL_DATA.getLangValue()));
      return true;
    }

    new BukkitRunnable() {

      @Override
      public void run() {
        plugin.getPlayerDataManager().purgeData(receiver.getUniqueId());
        player.sendMessage(ChatUtils.colorChat(Messages.PURGED_PLAYER_DATA.getLangValue().replace("%target%", receiver.getName())));
      }

    }.runTaskLater(plugin, 1);

    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return null;
  }

  private boolean isRealPlayer(String playerName) {
    return playerName.length() > 16;
  }

  private boolean everyoneWasMentioned(String word) {
    return word.equalsIgnoreCase("*") || word.equalsIgnoreCase("all") || word.equalsIgnoreCase("everyone");
  }

  private void playErrorSound(Player player) {
    if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
      playSound(player, XSound.ENTITY_VILLAGER_NO);
    }
  }

  private void playSound(Player player, XSound sound) {
    if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
      PlayerUtils.playSound(player, sound);
    }
  }
}
