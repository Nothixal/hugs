package me.nothixal.hugs.enums.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import org.bukkit.plugin.java.JavaPlugin;

public enum Messages {
  PREFIX("plugin_placeholders.prefix", "&8[&3Hugs&8]"),
  ALT_PREFIX("plugin_placeholders.alt_prefix", "&3&lHugs &8&l>>"),
  ERROR_PREFIX("plugin_placeholders.error_prefix", "&8[&cHugs&8]"),
  HEADER("plugin_placeholders.header", "&8&l<&8&m&l-------&8&l<&3+&8&l>&m&l--------&8&l<<&b&l Hugs &8&l>>&m&l--------&8&l<&3+&8&l>&m&l-------&8&l>"),
  FOOTER("plugin_placeholders.footer", "&8&l<&8&m&l--------&8&l<&3+&8&l>&m&l---------&8&l<<&3+&8&l>>&m&l---------&8&l<&3+&8&l>&m&l--------&8&l>"),

  // Notification Messages
  CHAT_ENABLED("indicators.chat_messages.enabled", "%prefix% &aYou will now receive chat indicators."),
  CHAT_DISABLED("indicators.chat_messages.disabled", "%prefix% &cYou will no longer receive chat indicators."),
  MESSAGE_TO_SENDER("indicators.chat_messages.message_to_sender", "%prefix% &3You gave &7&l%target% &3a hug!"),
  MESSAGE_TO_RECEIVER("indicators.chat_messages.message_to_receiver", "%prefix% &3You were hugged by &7&l%sender%&3!"),
  SELF_HUG("indicators.chat_messages.self_hug", "%prefix% &7You hugged yourself."),

  MENUS_ENABLED("messages.menus.enabled", "%prefix% &aMenus enabled."),
  MENUS_DISABLED("messages.menus.disabled", "%prefix% &cMenus disabled."),

  LANGUAGE_CHANGED("messages.language_changed", "%prefix% &7Set language to &a%language%."),

  TITLES_ENABLED("indicators.title_messages.enabled", "%prefix% &aYou will now receive title indicators."),
  TITLES_DISABLED("indicators.title_messages.disabled", "%prefix% &cYou will no longer receive title indicators."),
  TITLE_SELF_HUG("indicators.title_messages.self_hug.title", "&3You"),
  SUBTITLE_SELF_HUG("indicators.title_messages.self_hug.subtitle", "&7hugged yourself."),
  TITLE_NORMAL_HUG("indicators.title_messages.normal_hug.title", "&3%sender%"),
  SUBTITLE_NORMAL_HUG("indicators.title_messages.normal_hug.subtitle", "&7hugged you."),
  TITLE_MASS_HUG("indicators.title_messages.mass_hug.title", "&3%sender%"),
  SUBTITLE_MASS_HUG("indicators.title_messages.mass_hug.subtitle", "&7hugged everyone!"),

  ACTIONBAR_ENABLED("indicators.actionbar_messages.enabled", "%prefix% &aYou will now receive actionbar indicators."),
  ACTIONBAR_DISABLED("indicators.actionbar_messages.disabled", "%prefix% &cYou will no longer receive actionbar indicators."),
  ACTIONBAR_SELF_HUG("indicators.actionbar_messages.self_hug", "&7You gave yourself a warm hug."),
  ACTIONBAR_NORMAL_HUG("indicators.actionbar_messages.normal_hug", "&e%player% &7gave you a warm hug. Awww &c<3"),
  ACTIONBAR_MASS_HUG("indicators.actionbar_messages.mass_hug", "&e%player% &7gave everyone a hug!"),

  BOSSBAR_ENABLED("indicators.bossbar_messages.enabled", "%prefix% &aYou will now receive bossbar indicators."),
  BOSSBAR_DISABLED("indicators.bossbar_messages.disabled", "%prefix% &cYou will no longer receive bossbar indicators."),
  BOSSBAR_SELF_HUG("indicators.bossbar_messages.self_hug", "&7You gave yourself a warm hug."),
  BOSSBAR_NORMAL_HUG("indicators.bossbar_messages.normal_hug", "&e%player% &7gave you a warm hug. Awww &c<3"),
  BOSSBAR_MASS_HUG("indicators.bossbar_messages.mass_hug", "&e%player% &7gave everyone a hug!"),

  TOASTS_ENABLED("indicators.toast_messages.enabled", "%prefix% &aYou will now receive toast indicators."),
  TOASTS_DISABLED("indicators.toast_messages.disabled", "%prefix% &cYou will no longer receive toast indicators."),
  TOAST_SELF_HUG("indicators.toast_messages.self_hug", "&7You gave yourself a warm hug."),
  TOAST_NORMAL_HUG("indicators.toast_messages.normal_hug", "&e%player% &7gave you a warm hug. Awww &c<3"),
  TOAST_MASS_HUG("indicators.toast_messages.mass_hug", "&e%player% &7gave everyone a hug!"),

  // General Messages
  HUGGABILITY_ALL("messages.huggability.all_hugs", "%prefix% &aYou are now huggable!"),
  HUGGABILITY_NONE("messages.huggability.no_hugs", "%prefix% &cYou are no longer huggable!"),
  HUGGABILITY_REALISTIC("messages.huggability.realistic_hugs", "%prefix% &aYou are now only huggable when close to other players."),

  BROADCAST_SELF_HUG("messages.broadcasts.self_hugs", "%prefix% &7&l%sender% &3hugged themself."),
  BROADCAST_NORMAL_HUG("messages.broadcasts.normal_hugs", "%prefix% &7&l%sender% &3hugged &7&l%target%"),
  BROADCAST_MASS_HUG("messages.broadcasts.mass_hugs", "%prefix% &7&l%sender% &3gave everyone a hug!"),

  CONFIRMATION_ALL_MESSAGE("messages.confirmations.confirmation_all_message", "%prefix% &7Are you sure? Doing this will result in all player data being deleted! \nType \"yes\" to continue or type \"no\" to cancel."),
  CONFIRMATION_MESSAGE("messages.confirmations.confirmation_message", "%prefix% &7Are you sure? Doing this will result in %player%'s data being deleted! \nType \"yes\" to continue or type \"no\" to cancel."),

  CANCELLED_PLAYER_DATA_PURGE("messages.confirmations.cancelled_purge", "%prefix% &7Canceled data purge."),
  CANCELLED_FULL_DATA_PURGE("messages.confirmations.canceled_purge", "%prefix% &7Canceled data purge."),
  PURGED_PLAYER_DATA("messages.confirmations.player_data_purged", "%prefix% &3%target%'s &7data was &cdeleted&7!"),
  PURGED_ALL_DATA("messages.confirmations.all_data_purged", "%prefix% &7Everyone's data was &cdeleted&7!"),

  CONFIRMATION_GUI_NAME("gui_names.confirmation", "&3&lHugs &8&l>> &8Confirmation Page"),
  LEADERBOARD_GUI_NAME("gui_names.leaderboard", "&3&lHugs &8&l>> &8Leaderboard"),

  // Experimental Messages
  PASSIVE_MODE("messages.experimental.passive_mode", "%prefix% &3Passive mode is enabled. You cannot harm other players!"),

  // GUI Titles
  HELP_GUI_TITLE("menu_titles.help_menu", "&3&lHugs &8&l>> &8Help Menu"),
  LEADERBOARD_GUI_TITLE("menu_titles.leaderboard_menu", "&3&lHugs &8&l>> &8Leaderboards"),
  COMMANDS_GUI_TITLE("menu_titles.commands_menu", "&3&lHugs &8&l>> &8Commands"),
  PLUGIN_ARCHIVES_TITLE("menu_titles.plugin_archives_menu", "&3&lHugs &8&l>> &8The Archives"),

  YOUR_STATS_GUI_TITLE("menu_titles.your_stats_menu", "&3&lHugs &8&l>> &8Your Stats"),
  PLAYER_STATS_GUI_TITLE("menu_titles.player_stats_menu", "&3&lHugs &8&l>> &8Player Stats"),
  GLOBAL_STATS_GUI_TITLE("menu_titles.global_stats_menu", "&3&lHugs &8&l>> &8Global Stats"),

  EROS_STATS_GUI_TITLE("menu_titles.eros_stats_menu", "&3&lHugs &8&l>> &8Eros"),
  SILVIA_STATS_GUI_TITLE("menu_titles.silvia_stats_menu", "&3&lHugs &8&l>> &8Silvia"),

  YOUR_PREFERENCES_GUI_TITLE("menu_titles.your_preferences_menu", "&3&lHugs &8&l>> &8Your Preferences"),
  YOUR_VISUALS_GUI_TITLE("menu_titles.your_visuals_menu", "&3&lHugs &8&l>> &8Your Visuals"),
  YOUR_SOUNDS_GUI_TITLE("menu_titles.your_sounds_menu", "&3&lHugs &8&l>> &8Your Sounds"),
  YOUR_INDICATORS_GUI_TITLE("menu_titles.your_indicators_menu", "&3&lHugs &8&l>> &8Your Indicators"),

  ADMIN_PANEL_GUI_TITLE("menu_titles.admin_panel", "&3&lHugs &8&l>> &8Admin Panel"),
  SELECT_LANGUAGE_GUI_TITLE("menu_titles.select_language", "&3&lHugs &8&l>> &8Select Language"),

  // GUI Items
  CLOSE_MENU("common_items.close_menu", "&cClose Menu"),
  GO_BACK("common_items.go_back", "&eGo Back"),
  NEXT_PAGE("common_items.next_page", "&aNext Page"),
  PREVIOUS_PAGE("common_items.previous_page", "&aPrevious Page"),
  SWITCH_FILTER("common_items.switch_filter", "&eClick to switch filter!"),
  TOGGLE("common_items.toggle", "&eClick to toggle."),
  EMPTY_SPACE("common_items.empty_space", "&cEmpty Space"),
  COMING_SOON("common_items.coming_soon", "&7Coming Soon"),

  NORMAL_HUGS_GIVEN("common_items.normal_hugs_given", "&eGiven"),
  NORMAL_HUGS_RECEIVED("common_items.normal_hugs_received", "&eReceived"),
  MASS_HUGS_GIVEN("common_items.mass_hugs_given", "&eMass Given"),
  MASS_HUGS_RECEIVED("common_items.mass_hugs_received", "&eMass Received"),
  SELF_HUGS_GIVEN("common_items.self_hugs", "&eSelf Hugs"),
  UNIQUE_HUGS_GIVEN("common_items.unique_hugs", "&eUnique Hugs"),

  ;

  private static HugsPlugin plugin = JavaPlugin.getPlugin(HugsPlugin.class);
  private final String value;
  private final String defaultValue;

  Messages(String value, String defaultValue) {
    this.value = value;
    this.defaultValue = defaultValue;
  }

  public String getLangValue() {
    String output = plugin.getLangFile().getString(value, defaultValue);

    if (plugin.getLangFile().getConfigurationSection("plugin_placeholders") == null) {
      return output;
    }

    for (String placeholder : plugin.getLangFile().getConfigurationSection("plugin_placeholders").getKeys(false)) {
      output = output.replace("%" + placeholder + "%", plugin.getLangFile().getString("plugin_placeholders." + placeholder));
    }

    return output;
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return value;
  }

  public enum Lists {
    RELOAD("messages.reload_message", Arrays.asList(
        "%header%",
        "%prefix% &b&lConfiguration Reloaded",
        "%footer%"
    )),
    TOTAL("data.total_hugs_message", Arrays.asList(
        "#03A9F4&lHugs &8&l>> #67cbf8Server Totals",
        "%prefix% &3%self_hugs_total% &7self hugs have been given!",
        "%prefix% &3%normal_hugs_total% &7hugs have been given!",
        "%prefix% &3%mass_hugs_total% &7mass hugs have been given!"
    )),
    YOUR_PROFILE("data.your_profile", Arrays.asList(
        "#03A9F4&lHugs &8&l>> #67cbf8Viewing your profile.",
        "&7Self Given: &b%total_self_hugs_given%",
        "&7Hugs Given: &e%total_hugs_given%",
        "&7Hugs Received: &e%total_hugs_received%",
        "&7Mass Given: &6%total_mass_hugs_given%",
        "&7Mass Received: &6%total_mass_hugs_received%",
        "&7Unique Given: &d%total_unique_hugs%"
    )),
    PLAYER_PROFILE("data.player_profile", Arrays.asList(
        "#03A9F4&lHugs &8&l>> #67cbf8Viewing %target%'s profile.",
        "&7Self Given: &b%total_self_hugs_given%",
        "&7Hugs Given: &e%total_hugs_given%",
        "&7Hugs Received: &e%total_hugs_received%",
        "&7Mass Given: &6%total_mass_hugs_given%",
        "&7Mass Received: &6%total_mass_hugs_received%",
        "&7Unique Given: &d%total_unique_hugs%"
    )),
    ;

    private final String path;
    private final List<String> defaultValue;

    Lists(String path, List<String> defaultValue) {
      this.path = path;
      this.defaultValue = defaultValue;
    }

    public List<String> getLangValue() {
      List<String> modified = new ArrayList<>();

      for (String message : plugin.getLangFile().getStringList(path)) {
        for (String placeholder : plugin.getLangFile().getConfigurationSection("plugin_placeholders").getKeys(false)) {
          message = message.replace("%" + placeholder + "%", plugin.getLangFile().getString("plugin_placeholders." + placeholder));
        }

        modified.add(message);
      }

      return modified;
    }

    public String getValue() {
      return path;
    }

    public List<String> getDefaultValue() {
      return defaultValue;
    }

    @Override
    public String toString() {
      return path;
    }

  }

}
