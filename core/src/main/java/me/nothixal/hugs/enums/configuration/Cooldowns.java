package me.nothixal.hugs.enums.configuration;

import me.nothixal.hugs.HugsPlugin;
import org.bukkit.plugin.java.JavaPlugin;

public enum Cooldowns {

  SELF_HUG_COOLDOWN("settings.cooldowns.self_hugs", 5),
  NORMAL_HUG_COOLDOWN("settings.cooldowns.normal_hugs", 10),
  MASS_HUG_COOLDOWN("settings.cooldowns.mass_hugs", 30),

  HUG_COMMAND_COOLDOWN("hugCommandCooldown", 7),
  HUGS_STATS_COMMAND_COOLDOWN("hugsStatsCommandCooldown", 7),
  HUGS_TOTAL_COMMAND_COOLDOWN("hugsTotalCommandCooldown", 7),
  ;

  private static HugsPlugin plugin = JavaPlugin.getPlugin(HugsPlugin.class);
  private final String cooldownName;
  private final int duration;

  Cooldowns(String cooldownName, int durationInSeconds) {
    this.cooldownName = cooldownName;
    this.duration = durationInSeconds;
  }

  public String getCooldownName() {
    return cooldownName;
  }

  public int getDuration() {
    return plugin.getConfig().getInt(cooldownName, duration);
  }
}
