package me.nothixal.hugs.enums.configuration;

import me.nothixal.hugs.HugsPlugin;
import org.bukkit.plugin.java.JavaPlugin;

public enum Usages {

  HUG("usages.hug", "&3&lProper Usage &8&l>> &b/hug <player>"),
  HUGS_INFO("usages.info", "&3&lProper Usage &8&l>> &b/hugs info <player>"),
  HUGS_TOTAL("usages.total", "&3&lProper Usage &8&l>> &b/hugs total"),
  HUGS_PURGE("usages.purge", "&3&lProper Usage &8&l>> &b/hugs admin purge <player>");

  private static HugsPlugin plugin = JavaPlugin.getPlugin(HugsPlugin.class);
  private final String value;
  private final String def;

  Usages(String value, String def) {
    this.value = value;
    this.def = def;
  }

  public String getLangValue() {
    return plugin.getLangFile().getString(value, def).replace("%prefix%", Messages.PREFIX.getLangValue());
  }

  @Override
  public String toString() {
    return value;
  }
}
