package me.nothixal.hugs.utils;

import java.util.Calendar;
import java.util.Date;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.holidays.Holiday;
import me.nothixal.hugs.enums.holidays.HolidayOverride;

public class HolidayChecker {

  private final HugsPlugin plugin;
  private final Holiday holiday = Holiday.NONE;

  public HolidayChecker(HugsPlugin plugin) {
    this.plugin = plugin;
    checkForHoliday();
  }

  public void checkForHoliday() {
    Holiday holiday = (Holiday) checkHolidays();

    switch (holiday) {
      case NEW_YEARS:
        plugin.setHoliday(Holiday.NEW_YEARS);
        plugin.setHolidayOverride(HolidayOverride.NEW_YEARS);
        break;
      case NATIONAL_HUG_DAY:
        plugin.setHoliday(Holiday.NATIONAL_HUG_DAY);
        plugin.setHolidayOverride(HolidayOverride.NATIONAL_HUG_DAY);
        break;
      case VALENTINES_DAY:
        plugin.setHoliday(Holiday.VALENTINES_DAY);
        plugin.setHolidayOverride(HolidayOverride.VALENTINES_DAY);
        break;
      case INDEPENDENCE_DAY:
        plugin.setHoliday(Holiday.INDEPENDENCE_DAY);
        plugin.setHolidayOverride(HolidayOverride.INDEPENDENCE_DAY);
        break;
      case HALLOWEEN:
        plugin.setHoliday(Holiday.HALLOWEEN);
        plugin.setHolidayOverride(HolidayOverride.HALLOWEEN);
        break;
      case VETERANS_DAY:
        plugin.setHoliday(Holiday.VETERANS_DAY);
        plugin.setHolidayOverride(HolidayOverride.VETERANS_DAY);
        break;
      case CHRISTMAS:
        plugin.setHoliday(Holiday.CHRISTMAS);
        plugin.setHolidayOverride(HolidayOverride.CHRISTMAS);
        break;
      case BOXING_DAY:
        plugin.setHoliday(Holiday.BOXING_DAY);
        plugin.setHolidayOverride(HolidayOverride.BOXING_DAY);
        break;
      case NONE:
        plugin.setHolidayOverride(HolidayOverride.NONE);
        break;
    }
  }

  private Enum<Holiday> checkHolidays() {
    Calendar today = Calendar.getInstance();
    Date date1 = new Date();

    today.setTime(date1);
    today.set(Calendar.HOUR_OF_DAY, 0);
    today.set(Calendar.MINUTE, 0);
    today.set(Calendar.SECOND, 0);
    today.set(Calendar.MILLISECOND, 0);

    Calendar holiday = Calendar.getInstance();
    Date date2 = new Date();

    holiday.setTime(date2);
    holiday.set(Calendar.HOUR_OF_DAY, 0);
    holiday.set(Calendar.MINUTE, 0);
    holiday.set(Calendar.SECOND, 0);
    holiday.set(Calendar.MILLISECOND, 0);

    holiday.set(Calendar.MONTH, 0);
    holiday.set(Calendar.DAY_OF_MONTH, 1);

    if (isSameDay(today, holiday)) {
      return Holiday.NEW_YEARS;
    }

    holiday.set(Calendar.MONTH, 0);
    holiday.set(Calendar.DAY_OF_MONTH, 21);

    if (isSameDay(today, holiday)) {
      return Holiday.NATIONAL_HUG_DAY;
    }

    holiday.set(Calendar.MONTH, 1);
    holiday.set(Calendar.DAY_OF_MONTH, 14);

    if (isSameDay(today, holiday)) {
      return Holiday.VALENTINES_DAY;
    }

    holiday.set(Calendar.MONTH, 6);
    holiday.set(Calendar.DAY_OF_MONTH, 4);

    if (isSameDay(today, holiday)) {
      return Holiday.INDEPENDENCE_DAY;
    }

    holiday.set(Calendar.MONTH, 9);
    holiday.set(Calendar.DAY_OF_MONTH, 31);

    if (isSameDay(today, holiday)) {
      return Holiday.HALLOWEEN;
    }

    holiday.set(Calendar.MONTH, 10);
    holiday.set(Calendar.DAY_OF_MONTH, 11);

    if (isSameDay(today, holiday)) {
      return Holiday.VETERANS_DAY;
    }

    holiday.set(Calendar.MONTH, 11);
    holiday.set(Calendar.DAY_OF_MONTH, 25);

    if (isSameDay(today, holiday)) {
      return Holiday.CHRISTMAS;
    }

    holiday.set(Calendar.MONTH, 11);
    holiday.set(Calendar.DAY_OF_MONTH, 26);

    if (isSameDay(today, holiday)) {
      return Holiday.BOXING_DAY;
    }

    return Holiday.NONE;
  }

  private boolean isSameDay(Calendar c1, Calendar c2) {
    return (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR) &&
        c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH) &&
        c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH));
  }

  private boolean isHolidayToday() {
    return holiday != Holiday.NONE;
  }

  public Holiday getHoliday() {
    return holiday;
  }
}
