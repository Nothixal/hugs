package me.nothixal.hugs.utils.cooldowns;

import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Cooldowns;
import me.nothixal.hugs.enums.types.HugType;
import me.nothixal.hugs.utils.RelativeTime;
import org.bukkit.entity.Player;

public class CooldownManager {

  private final HugsPlugin plugin;

  public CooldownManager(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  public void addCooldown(UUID sender, UUID receiver, HugType type, long duration, long start) {
    plugin.getPlayerDataManager().addCooldown(sender, receiver, type, duration, start);
  }

  public void addCooldown(UUID sender, UUID receiver, HugType type, long duration) {
    addCooldown(sender, receiver, type, duration, System.currentTimeMillis());
  }

  public void addCooldown(Player sender, Player receiver, HugType type, Cooldowns cooldown) {
    addCooldown(sender.getUniqueId(), receiver.getUniqueId(), type, cooldown.getDuration() * 1000L);
  }

  public void removeCooldown(UUID sender, UUID receiver) {
    plugin.getPlayerDataManager().removeCooldown(sender, receiver);
  }

  public Cooldown getCooldown(UUID sender, UUID receiver) {
    return plugin.getPlayerDataManager().getCooldown(sender, receiver);
  }

  public boolean isOnCooldown(UUID sender, UUID receiver) {
    Cooldown cooldown = getCooldown(sender, receiver);

    if (cooldown == null) {
      return false;
    }

//    if (!cooldowns.contains(cooldown)) {
//      return false;
//    }

    if (getTimeRemaining(sender, receiver) > 0L) {
      return true;
    } else {
      removeCooldown(sender, receiver);
      return false;
    }
  }

  public String getTimeLeft(Player sender, Player receiver) {
    return getTimeLeft(sender.getUniqueId(), receiver.getUniqueId());
  }

  public String getTimeLeft(UUID sender, UUID receiver) {
    return RelativeTime.toRelative(getTimeRemaining(sender, receiver), 2).toString();
  }

  public long getTimeRemaining(UUID sender, UUID receiver) {
    Cooldown cooldown = getCooldown(sender, receiver);

    if (cooldown == null) {
      return 0;
    }

//    System.out.println(cooldown.getSender() + "::" + cooldown.getReceiver());
//    System.out.println("Now:      " + System.currentTimeMillis());
//    System.out.println("Start:    " + cooldown.getStart());
//    System.out.println("Duration: " + cooldown.getDuration());
//
//    long temp = cooldown.getDuration() - (System.currentTimeMillis() - cooldown.getStart());
//    System.out.println("Mafs: " + temp);

//    return cooldown.getDuration() - (System.currentTimeMillis() - cooldown.getStart());
    return cooldown.getDuration();
  }

}
