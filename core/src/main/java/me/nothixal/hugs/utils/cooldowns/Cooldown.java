package me.nothixal.hugs.utils.cooldowns;

import java.util.UUID;
import me.nothixal.hugs.enums.configuration.Cooldowns;
import me.nothixal.hugs.enums.types.HugType;

public class Cooldown {

  private final UUID sender;
  private final UUID receiver;
  private final HugType type;
  private final long duration;
  private final long start;

  public Cooldown(UUID sender, UUID receiver, HugType type, long duration, long start) {
    this.sender = sender;
    this.receiver = receiver;
    this.type = type;
    this.duration = duration;
    this.start = start;
  }

  public Cooldown(UUID sender, UUID receiver, HugType type, long duration) {
    this.sender = sender;
    this.receiver = receiver;
    this.type = type;
    this.duration = duration;
    this.start = System.currentTimeMillis();
  }

  public Cooldown(UUID sender, UUID receiver, long duration) {
    this.sender = sender;
    this.receiver = receiver;
    this.duration = duration * 1000L;
    this.type = HugType.NORMAL;
    this.start = System.currentTimeMillis();
  }

  public Cooldown(UUID sender, UUID receiver, Cooldowns cooldown) {
    this.sender = sender;
    this.receiver = receiver;
    this.duration = cooldown.getDuration() * 1000L;
    this.type = HugType.NORMAL;
    this.start = System.currentTimeMillis();
  }

  public UUID getSender() {
    return sender;
  }

  public UUID getReceiver() {
    return receiver;
  }

  public long getDuration() {
    return duration;
  }

  public long getStart() {
    return start;
  }

  public HugType getType() {
    return type;
  }
}
