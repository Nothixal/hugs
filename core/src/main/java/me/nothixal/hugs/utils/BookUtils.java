package me.nothixal.hugs.utils;

import me.nothixal.hugs.HugsPlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class BookUtils {

  private static final String PRIMARY_COLOR = PluginConstants.DEFAULT_PREFIX_COLOR_HEX;

  public static void showWebsiteBook(HugsPlugin plugin, Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    ComponentBuilder website = new ComponentBuilder("[VISIT SITE]").color(ChatColor.of(PRIMARY_COLOR)).bold(true)
        .event(new ClickEvent(Action.OPEN_URL, PluginConstants.PLUGIN_WEBSITE_LINK))
        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click me to visit the site!").color(ChatColor.AQUA).create()));

    BaseComponent[] websiteBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(PRIMARY_COLOR)).bold(true).append("").reset()
        .append("\n")
        .append("        ").append("Spigot Site")
        .append("\n\n")
        .append("Consider giving the plugin a 5-star rating.")
        .append("\n\n\n")
        .append("     ").append(website.create()).append("").reset()
        .create();

    bookMeta.spigot().addPage(websiteBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Website Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }

  public static void showSourceCodeBook(HugsPlugin plugin, Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    ComponentBuilder website = new ComponentBuilder("[VISIT SITE]").color(ChatColor.of(PRIMARY_COLOR)).bold(true)
        .event(new ClickEvent(Action.OPEN_URL, PluginConstants.PLUGIN_SOURCE_CODE_LINK))
        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click me to visit the site!").color(ChatColor.AQUA).create()));

    BaseComponent[] sourceCodeBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(PRIMARY_COLOR)).bold(true).append("").reset()
        .append("\n")
        .append("      ").append("Source Code")
        .append("\n\n")
        .append("Consider contributing to the development!")
        .append("\n\n\n")
        .append("    ").append(website.create()).append("").reset()
        .create();

    bookMeta.spigot().addPage(sourceCodeBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Source Code Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }

  public static void showWikiBook(HugsPlugin plugin, Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    ComponentBuilder website = new ComponentBuilder("[VISIT SITE]").color(ChatColor.of(PRIMARY_COLOR)).bold(true)
        .event(new ClickEvent(Action.OPEN_URL, PluginConstants.PLUGIN_WIKI_LINK))
        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click me to visit the site!").color(ChatColor.AQUA).create()));

    BaseComponent[] wikiBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(PRIMARY_COLOR)).bold(true).append("").reset()
        .append("\n")
        .append("            ").append("Wiki")
        .append("\n\n")
        .append("Need help with configuration?")
        .append("\n\n")
        .append("    ").append(website.create()).append("").reset()
        .create();

    bookMeta.spigot().addPage(wikiBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Wiki Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }

  public static void showBugTrackerBook(HugsPlugin plugin, Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    ComponentBuilder website = new ComponentBuilder("[VISIT SITE]").color(ChatColor.of(PRIMARY_COLOR)).bold(true)
        .event(new ClickEvent(Action.OPEN_URL, PluginConstants.PLUGIN_BUG_TRACKER))
        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click me to visit the site!").color(ChatColor.AQUA).create()));

    BaseComponent[] bugTrackerBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(PRIMARY_COLOR)).bold(true).append("").reset()
        .append("\n")
        .append("       ").append("Bug Tracker")
        .append("\n\n")
        .append("Found a bug or need to report an issue?")
        .append("\n\n")
        .append("    ").append(website.create()).append("").reset()
        .create();

    bookMeta.spigot().addPage(bugTrackerBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Bug Tracker Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }

  public static void showDiscordBook(HugsPlugin plugin, Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    ComponentBuilder discord = new ComponentBuilder("[JOIN COMMUNITY]").color(ChatColor.of(PRIMARY_COLOR)).bold(true)
        .event(new ClickEvent(Action.OPEN_URL, PluginConstants.DISCORD_INVITE_LINK))
        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to join the community!").color(ChatColor.AQUA).create()));

    BaseComponent[] discordBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(PRIMARY_COLOR)).bold(true).append("").reset()
        .append("\n")
        .append("          ").append("Discord")
        .append("\n\n\n\n")
        .append("  ").append(discord.create()).append("").reset()
        .create();

    bookMeta.spigot().addPage(discordBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Discord Invite Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }

  public static void openDonateBook(HugsPlugin plugin, Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    String primaryColor = PluginConstants.DEFAULT_PREFIX_COLOR_HEX;

    BaseComponent[] page1 = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .append("\n")
        .append("        ").append("Donations")
        .append("\n\n")
        .append("If you enjoy using this plugin, please consider donating.")
        .append("\n\n")
        .append("My work is free, but I strive to put my best effort into all my works.")
        .append("\n\n")
        .append("Donation methods are on the next page.")
//        .append("   ").append(website.create()).append("").reset()
        .create();

    BaseComponent[] page2 = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .append("\n")
        .append("        ").append("Donations")
        .append("\n\n")
        .append(PluginConstants.PAYPAL_BASE_COMPONENT)
        .append("\n\n")
        .append(PluginConstants.CASHAPP_BASE_COMPONENT)
        .append("\n\n")
//        .append("   ").append(website.create()).append("").reset()
        .create();

    if (bookMeta == null) {
      player.sendMessage("An error occurred creating the book.");
      return;
    }

    bookMeta.spigot().addPage(page1);
    bookMeta.spigot().addPage(page2);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Donation Book");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }
}
