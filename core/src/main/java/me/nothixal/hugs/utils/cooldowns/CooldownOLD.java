package me.nothixal.hugs.utils.cooldowns;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.utils.RelativeTime;
import org.bukkit.entity.Player;

public class CooldownOLD {

  private static final Map<String, CooldownOLD> cooldowns = new HashMap<>();

  private final UUID uuid;
  private final String cooldownName;
  private final long cooldownDuration;
  private long cooldownStart;

  public CooldownOLD(UUID uuid, String cooldownName, long cooldownDuration) {
    this.uuid = uuid;
    this.cooldownName = cooldownName;
    this.cooldownDuration = cooldownDuration * 1000L;
  }

  public CooldownOLD(Player player, String cooldownName, long cooldownDuration) {
    this(player.getUniqueId(), cooldownName, cooldownDuration);
  }

  public void start() {
    this.cooldownStart = System.currentTimeMillis();
    cooldowns.put(this.uuid + this.cooldownName, this);
  }

  private static void stop(UUID uuid, String cooldownName) {
    CooldownOLD.cooldowns.remove(uuid + cooldownName);
  }

  public static boolean hasCooldown(UUID uuid, String cooldownName) {
    if (!cooldowns.containsKey(uuid + cooldownName)) return false;

    if (getTimeRemaining(uuid, cooldownName) > 0L) {
      return true;
    } else {
      stop(uuid, cooldownName);
      return false;
    }
  }

  public static boolean hasCooldown(Player player, String cooldownName) {
    return hasCooldown(player.getUniqueId(), cooldownName);
  }

  private static CooldownOLD getCooldown(UUID uuid, String cooldownName) {
    return cooldowns.get(uuid + cooldownName);
  }

  public static String getTimeLeft(Player player, String cooldownName) {
    return getTimeLeft(player.getUniqueId(), cooldownName);
  }

  public static String getTimeLeft(UUID uuid, String cooldownName) {
    return RelativeTime.toRelative(getTimeRemaining(uuid, cooldownName), 2).toString();
  }

  public static long getTimeRemaining(UUID uuid, String cooldownName) {
    CooldownOLD cooldown = getCooldown(uuid, cooldownName);
    return cooldown.cooldownDuration - (System.currentTimeMillis() - cooldown.cooldownStart);
  }

}
