package me.nothixal.hugs.utils.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.holidays.HolidayOverride;
import me.nothixal.hugs.utils.text.TextUtils;
import org.bukkit.plugin.java.JavaPlugin;

public final class LogUtils {

  private static HugsPlugin plugin = JavaPlugin.getPlugin(HugsPlugin.class);
  private static final String consolePrefix = "&7[&3Hugs&7]&r ";
  private static final String consoleWarningPrefix = "&7[&eHugs&7]&r ";
  private static final String consoleErrorPrefix = "&7[&cHugs&7]&r ";

  private LogUtils(HugsPlugin plugin) {
    LogUtils.plugin = plugin;
  }

  private static Logger getLogger() {
    return plugin.getLogger().getParent();
  }

  public static void logInfo(String msg) {
    getLogger().info(TextUtils.translateANSIColorCodes(getConsolePrefix() + msg));
  }

  public static void logInfoNoPrefix(String msg) {
    getLogger().info(TextUtils.translateANSIColorCodes(msg));
  }

  public static void logWarning(String msg) {
    getLogger().warning(TextUtils.translateANSIColorCodes(getConsoleWarningPrefix() + msg));
  }

  public static void logWarningNoPrefix(String msg) {
    getLogger().warning(TextUtils.translateANSIColorCodes(msg));
  }

  public static void logError(String msg) {
    getLogger().severe(TextUtils.translateANSIColorCodes(getConsoleErrorPrefix() + msg));
  }

  public static void logErrorNoPrefix(String msg) {
    getLogger().severe(TextUtils.translateANSIColorCodes(msg));
  }

  public static void logToFile(String message) {
    final DateFormat fileDateFormat = new SimpleDateFormat("yyyy EEE, MMMMM dd");
    final Date d = new Date();
    final String date = fileDateFormat.format(d);
    final DateFormat dateFormat = new SimpleDateFormat("hh:mm a");

    String newMessage;
    newMessage = "[" + dateFormat.format(d) + "] " + message;

    try {
      final File file = new File(plugin.getDataFolder().getAbsolutePath() + File.separator + "logs", date + ".txt");
      final File parent = file.getParentFile();
      if (!parent.exists()) {
        parent.mkdirs();
      }
      final FileWriter fw = new FileWriter(file, true);
      final PrintWriter pw = new PrintWriter(fw);
      pw.println(newMessage);
      pw.flush();
      pw.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static String getConsolePrefix() {
    HolidayOverride override = plugin.getHolidayOverride();
    String pluginPrefix = "";

    if (override == HolidayOverride.NONE) {
      pluginPrefix = HolidayOverride.NONE.getConsolePrefix();
    } else if (override == HolidayOverride.NEW_YEARS) {
      pluginPrefix = HolidayOverride.NEW_YEARS.getConsolePrefix();
    } else if (override == HolidayOverride.NATIONAL_HUG_DAY) {
      pluginPrefix = HolidayOverride.NATIONAL_HUG_DAY.getConsolePrefix();
    } else if (override == HolidayOverride.VALENTINES_DAY) {
      pluginPrefix = HolidayOverride.VALENTINES_DAY.getConsolePrefix();
    } else if (override == HolidayOverride.INDEPENDENCE_DAY) {
      pluginPrefix = HolidayOverride.INDEPENDENCE_DAY.getConsolePrefix();
    } else if (override == HolidayOverride.HALLOWEEN) {
      pluginPrefix = HolidayOverride.HALLOWEEN.getConsolePrefix();
    } else if (override == HolidayOverride.VETERANS_DAY) {
      pluginPrefix = HolidayOverride.VETERANS_DAY.getConsolePrefix();
    } else if (override == HolidayOverride.CHRISTMAS) {
      pluginPrefix = HolidayOverride.CHRISTMAS.getConsolePrefix();
    } else if (override == HolidayOverride.BOXING_DAY) {
      pluginPrefix = HolidayOverride.BOXING_DAY.getConsolePrefix();
    }

    return pluginPrefix;
  }

  private static String getConsoleWarningPrefix() {
    return consoleWarningPrefix;
  }

  private static String getConsoleErrorPrefix() {
    return consoleErrorPrefix;
  }
}