package me.nothixal.hugs.utils;

import java.util.Date;
import java.util.UUID;
import me.nothixal.hugs.enums.types.HugType;
import org.bukkit.Bukkit;

public class Interaction {

  private UUID sender;
  private UUID receiver;

  private Date date;

  private HugType hugType;

  public Interaction(UUID sender, UUID receiver, HugType hugType) {
    this.sender = sender;
    this.receiver = receiver;
    this.hugType = hugType;
    this.date = new Date();
  }

  public Interaction(UUID sender, UUID receiver, HugType hugType, long timestamp) {
    this.sender = sender;
    this.receiver = receiver;
    this.hugType = hugType;
    this.date = new Date(timestamp);
  }

  public UUID getSender() {
    return sender;
  }

  public UUID getReceiver() {
    return receiver;
  }

  public HugType getHugType() {
    return hugType;
  }

  public Date getDate() {
    return date;
  }

  public String toString() {
    return "&7- " + PluginConstants.TERTIARY_COLOR_HEX + Bukkit.getOfflinePlayer(receiver).getName() + " &8(&f" + RelativeTime.toRelativePast(date, new Date(), 1) + "&8)";
  }
}
