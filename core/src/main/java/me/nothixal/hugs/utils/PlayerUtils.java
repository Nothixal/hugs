package me.nothixal.hugs.utils;

import com.cryptomorin.xseries.XMaterial;
import com.cryptomorin.xseries.XSound;
import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.enums.preferences.ParticleQuality;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.logger.LogUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.attribute.Attribute;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerUtils {
  
  private static final HugsPlugin PLUGIN = JavaPlugin.getPlugin(HugsPlugin.class);

  public static void restoreHealth(Player player) {
    player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
  }

  public static void restoreHunger(Player player) {
    player.setFoodLevel(20);
  }

  public static void sendSelfHugChatMessage(Player player) {
    String messageToSender = Messages.SELF_HUG.getLangValue();
    player.sendMessage(ChatUtils.colorChat(messageToSender));
  }

  public static void sendChatMessageToSender(Player player, Player receiver) {
    String messageToSender = Messages.MESSAGE_TO_SENDER.getLangValue().replace("%target%", receiver.getName());
    player.sendMessage(ChatUtils.colorChat(messageToSender));
  }

  public static void sendChatMessageToReceiver(Player player, Player receiver) {
    String messageToReceiver = Messages.MESSAGE_TO_RECEIVER.getLangValue().replace("%sender%", player.getName());
    receiver.sendMessage(ChatUtils.colorChat(messageToReceiver));
  }

  public static void broadcastSelfHug(Player player, Player receiver) {
    String massHugsBroadcast = Messages.BROADCAST_SELF_HUG.getLangValue().replace("%sender%", player.getName());
    massHugsBroadcast = massHugsBroadcast.replace("%target%", receiver.getName());
    Bukkit.broadcastMessage(ChatUtils.colorChat(massHugsBroadcast));
  }

  public static void broadcastHug(Player player, Player receiver) {
    String hugsBroadcast = Messages.BROADCAST_NORMAL_HUG.getLangValue().replace("%sender%", player.getName());
    hugsBroadcast = hugsBroadcast.replace("%target%", receiver.getName());
    Bukkit.broadcastMessage(ChatUtils.colorChat(hugsBroadcast));
  }

  public static void broadcastMassHug(Player player) {
    String massHugsBroadcast = Messages.BROADCAST_MASS_HUG.getLangValue().replace("%sender%", player.getName());
    Bukkit.broadcastMessage(ChatUtils.colorChat(massHugsBroadcast));
  }

  private static void sendTitle(Player player, String hugsTitle, String hugsSubTitle) {
    int fadeIn = PLUGIN.getConfig().getInt(Settings.TITLE_FADE_IN.getValue());
    int stay = PLUGIN.getConfig().getInt(Settings.TITLE_STAY.getValue());
    int fadeOut = PLUGIN.getConfig().getInt(Settings.TITLE_FADE_OUT.getValue());

    player.sendTitle(ChatUtils.colorChat(hugsTitle), null, fadeIn * 20, stay * 20, fadeOut * 20);
    player.sendTitle(null, ChatUtils.colorChat(hugsSubTitle), fadeIn * 20, stay * 20, fadeOut * 20);
  }

  public static void sendSelfHugTitle(Player player) {
    String hugsTitle = Messages.TITLE_SELF_HUG.getLangValue().replace("%sender%", player.getName());
    String hugsSubTitle = Messages.SUBTITLE_SELF_HUG.getLangValue().replace("%sender%", player.getName());
    sendTitle(player, hugsTitle, hugsSubTitle);
  }

  public static void sendNormalHugTitle(Player player, Player receiver) {
    String hugsTitle = Messages.TITLE_NORMAL_HUG.getLangValue().replace("%sender%", player.getName());
    String hugsSubTitle = Messages.SUBTITLE_NORMAL_HUG.getLangValue().replace("%sender%", player.getName());
    sendTitle(receiver, hugsTitle, hugsSubTitle);
  }

  public static void sendMassHugTitle(Player player, Player receiver) {
    String hugsTitle = Messages.TITLE_MASS_HUG.getLangValue().replace("%sender%", player.getName());
    String hugsSubTitle = Messages.SUBTITLE_MASS_HUG.getLangValue().replace("%sender%", player.getName());
    sendTitle(receiver, hugsTitle, hugsSubTitle);
  }

  public static void sendSelfHugActionBar(Player player) {
    String message = Messages.ACTIONBAR_SELF_HUG.getLangValue();
    PLUGIN.getChatManager().sendActionBar(player, ChatUtils.colorChat(message));
  }

  public static void sendActionBar(Player player, Player receiver) {
    String message = Messages.ACTIONBAR_NORMAL_HUG.getLangValue().replace("%player%", player.getName());
    PLUGIN.getChatManager().sendActionBar(receiver, ChatUtils.colorChat(message));
  }

  public static void sendMassActionBar(Player player, Player receiver) {
    String message = Messages.ACTIONBAR_MASS_HUG.getLangValue().replace("%player%", player.getName());
    PLUGIN.getChatManager().sendActionBar(receiver, ChatUtils.colorChat(message));
  }

  public static void sendSelfHugBossbar(Player player) {
    String message = Messages.BOSSBAR_SELF_HUG.getLangValue();
    createBossbar(player, message);
  }

  public static void sendBossbar(Player player, Player receiver) {
    String message = Messages.BOSSBAR_NORMAL_HUG.getLangValue().replace("%player%", player.getName());
    createBossbar(receiver, message);
  }

  public static void sendMassHugBossbar(Player player, Player receiver) {
    String message = Messages.BOSSBAR_MASS_HUG.getLangValue().replace("%player%", player.getName());
    createBossbar(receiver, message);
  }

  public static void createBossbar(Player player, String message) {
    BossBar bossBar = Bukkit.createBossBar(ChatUtils.colorChat(message), BarColor.BLUE, BarStyle.SOLID);
    bossBar.setProgress(0.0D);

    bossBar.addPlayer(player);

    new BukkitRunnable() {
      int counter = 0;

      @Override
      public void run() {
        if (counter < 60) {
          counter++;
          bossBar.setProgress(counter / 60D);
        } else {
          bossBar.removePlayer(player);
          cancel();
        }
      }
    }.runTaskTimerAsynchronously(PLUGIN, 0, 1);
  }

  public static void sendSelfHugToast(Player player) {
    ItemStack item = new ItemStack(Material.PLAYER_HEAD);
    SkullMeta meta = (SkullMeta) item.getItemMeta();
    meta.setOwningPlayer(player);

    item.setItemMeta(meta);

    PLUGIN.getAdvancementManager()
        .sendToast(item, player.getUniqueId(), ChatUtils.colorChat(Messages.TOAST_SELF_HUG.getLangValue()), "Self Hug");
  }

  public static void sendToast(Player player, Player receiver) {
    ItemStack item = new ItemStack(Material.PLAYER_HEAD);
    SkullMeta meta = (SkullMeta) item.getItemMeta();
    meta.setOwningPlayer(player);

    item.setItemMeta(meta);

    String message = Messages.TOAST_NORMAL_HUG.getLangValue().replace("%player%", player.getName());

    PLUGIN.getAdvancementManager()
        .sendToast(item, receiver.getUniqueId(), ChatUtils.colorChat(message), "404 Description not found.");
  }

  public static void sendMassToast(Player player, Player receiver) {
    ItemStack item = new ItemStack(Material.PLAYER_HEAD);
    SkullMeta meta = (SkullMeta) item.getItemMeta();
    meta.setOwningPlayer(player);

    item.setItemMeta(meta);

    String message = Messages.TOAST_MASS_HUG.getLangValue().replace("%player%", player.getName());

    PLUGIN.getAdvancementManager()
        .sendToast(item, receiver.getUniqueId(), ChatUtils.colorChat(message), "404 Description not found.");
  }

  public static void sendParticles(Player receiver) {
    String particleEffect = PLUGIN.getConfig().getString(Settings.PARTICLE_EFFECT.getValue(), "HEART").toUpperCase();

    Particle particle = Particle.valueOf(particleEffect);

    if (particle == null) {
      LogUtils.logWarning(
          "&eWARNING: &rThe particle effect defined in the config.yml (" + particleEffect + ") is invalid!");

      LogUtils.logWarning("Go to &a"
          + "https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Particle.html"
          + " &rfor a list of valid particle effects.");

      return;
    }

    ParticleQuality quality = PLUGIN.getPlayerDataManager().getParticleQuality(receiver.getUniqueId());

    spawnParticles(receiver, particle, quality, PLUGIN.getConfig().getBoolean(Settings.GLOBAL_PARTICLES.getValue()));
  }

  private static void spawnParticles(Player player, Particle particle, ParticleQuality quality, boolean global) {
    Location location = player.getLocation().add(0.0D, 1.0D, 0.0D);

    // spawnParticle(<PARTICLE>, <LOCATION>, <AMOUNT OF PARTICLES>, <X OFFSET>, <Y OFFSET>, <Z OFFSET>);
    if (global) {
      switch (quality) {
        case NONE:
          return;
        case LOW:
          player.getWorld().spawnParticle(particle, location, 3, 0.4D, 0.5D, 0.4D);
          break;
        case MEDIUM:
          player.getWorld().spawnParticle(particle, location, 9, 0.4D, 0.5D, 0.4D);
          break;
        case HIGH:
          player.getWorld().spawnParticle(particle, location, 15, 0.4D, 0.5D, 0.4D);
          break;
        case EXTREME:
          player.getWorld().spawnParticle(particle, location, 25, 0.4D, 0.5D, 0.4D);
          break;
        default:
          player.getWorld().spawnParticle(particle, location, 1, 0.4D, 0.5D, 0.4D);
      }
    } else {
      switch (quality) {
        case NONE:
          return;
        case LOW:
          player.spawnParticle(particle, location, 3, 0.4D, 0.5D, 0.4D);
          break;
        case MEDIUM:
          player.spawnParticle(particle, location, 9, 0.4D, 0.5D, 0.4D);
          break;
        case HIGH:
          player.spawnParticle(particle, location, 15, 0.4D, 0.5D, 0.4D);
          break;
        case EXTREME:
          player.spawnParticle(particle, location, 25, 0.4D, 0.5D, 0.4D);
          break;
        default:
          player.spawnParticle(particle, location, 1, 0.4D, 0.5D, 0.4D);
      }
    }
  }

  public static void playSound(Player player, XSound sound) {
    player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, 1);
  }

  public static void playSound(Player player, XSound sound, int pitch) {
    player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, 1, pitch);
  }

  public static void playSound(Player player, XSound sound, int volume, int pitch) {
    player.playSound(player.getLocation(), sound.parseSound(), SoundCategory.MASTER, volume, pitch);
  }

  public static void playSound(Player player, XSound sound, SoundCategory category, int volume, int pitch) {
    player.playSound(player.getLocation(), sound.parseSound(), category, volume, pitch);
  }

  public static void playSound(Player receiver) {
    String soundEffect = PLUGIN.getConfig().getString(Settings.SOUND_EFFECT.getValue(), "ENTITY_CAT_PURR")
        .toUpperCase();
    int volume = PLUGIN.getConfig().getInt(Settings.SOUND_VOLUME.getValue());
    int pitch = PLUGIN.getConfig().getInt(Settings.SOUND_PITCH.getValue());

    boolean validVolume = true;
    if (volume < 0 || volume > 10) {
      LogUtils.logWarning("The volume is the config.yml is invalid!");
      LogUtils.logWarning("Volume was set to the max value.");
      validVolume = false;
    }

    boolean validPitch = true;
    if (pitch < 0 || pitch > 16) {
      LogUtils.logWarning("The pitch is the config.yml is invalid!");
      LogUtils.logWarning("Pitch was set to the default value.");
      validPitch = false;
    }

    float realVolume = getRealVolume(volume, validVolume);
    float realPitch = getRealPitch(pitch, validPitch);

    try {
      if (PLUGIN.getConfig().getBoolean(Settings.GLOBAL_SOUND.getValue())) {
        receiver.getWorld().playSound(receiver.getLocation(), Sound.valueOf(soundEffect), realVolume, realPitch);
      } else {
        receiver.playSound(receiver.getLocation(), Sound.valueOf(soundEffect), realVolume, realPitch);
      }
    } catch (IllegalArgumentException ex) {
      LogUtils.logWarning("&eWARNING: &rThe sound effect defined in the config.yml (" + soundEffect + ") is invalid!");

      LogUtils.logWarning("Go to &a"
          + "https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Sound.html"
          + " &rfor a list of valid sound effects.");
    }
  }

  private static float getVolume(float configVolume) {
    if (configVolume > 10F) {
      configVolume = 10F;
    } else if (configVolume < 1F) {
      configVolume = 1F;
    }
    return configVolume;
  }

  private static float getRealVolume(float configVolume, boolean isValid) {
    final float defaultVolume = isValid ? 0.1F : 1.0F;
    float volume = getVolume(configVolume);
    if (volume < 1F) {
      return defaultVolume * volume;
    } else {
      float ratio = ((volume - 1) / 9) * (0.8F - defaultVolume);
      return ratio + defaultVolume;
    }
  }

  private static float getPitch(float configPitch) {
    if (configPitch > 16F) {
      configPitch = 16F;
    } else if (configPitch < 1F) {
      configPitch = 1F;
    }
    return configPitch;
  }

  private static float getRealPitch(float configPitch, boolean isValid) {
    final float defaultPitch = isValid ? 0.5F : 2.0F;
    float pitch = getPitch(configPitch);
    if (pitch < 1F) {
      return defaultPitch * pitch;
    } else {
      float ratio = ((pitch - 1) / 9) * (1.4F - defaultPitch);
      return ratio + defaultPitch;
    }
  }

  public static void checkDeveloper(Player player, Player receiver) {
    List<String> uuid = new ArrayList<>();
    uuid.add("03687ca030cf4c3c8c1d3b2dd75c3ff0");
    uuid.add("03687ca0-30cf-4c3c-8c1d-3b2dd75c3ff0");

    // If the receiver is not the developer, return.
    if (!uuid.contains(receiver.getUniqueId().toString())) {
      return;
    }

    // If the player has toast notifications enabled, send the developer toast.
    if (PLUGIN.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TOASTS)) {
      PLUGIN.getAdvancementManager()
          .sendChallengeToast(PLUGIN.getItemManager().createItem(XMaterial.CAKE), player.getUniqueId(),
              "&eYou hugged the developer of the plugin!", "Congratulations!");
      //sendToast(player, "&eYou hugged the developer of the plugin!");
    } else {
      player.sendMessage(ChatUtils.colorChat("&6You hugged the developer of the plugin! :O"));
    }

    PLUGIN.getVerboseManager().logVerbose(player.getName() + " hugged the developer of the plugin! :O");

    if (!PLUGIN.getConfig().getBoolean(Settings.DEVELOPER_REWARD.getValue())) {
      return;
    }

    if (player.getInventory().firstEmpty() == -1) {
      player.sendMessage(ChatUtils.colorChat(
          "&c&lATTENTION: &7You hugged the developer and your inventory is full! &7Make space for a reward. :D"));
    } else {
      player.sendMessage(ChatUtils.colorChat("&6Pst. Here's a cookie. Don't tell anyone!"));
      player.getInventory().addItem(new ItemStack(Material.COOKIE, 1));
    }

    PLUGIN.getVerboseManager().logVerbose(player.getName() + " was given a cookie because they hugged the developer!");
  }
}
