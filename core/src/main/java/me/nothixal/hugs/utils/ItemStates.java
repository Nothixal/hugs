package me.nothixal.hugs.utils;

import com.cryptomorin.xseries.XMaterial;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.preferences.HuggableState;
import me.nothixal.hugs.enums.preferences.ParticleQuality;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.inventory.ButtonSet;
import me.nothixal.hugs.utils.inventory.IconSet;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

public class ItemStates {

  private final HugsPlugin plugin;

  // Huggable Toggle (OLD)
  public ItemStack HUGGABLE_ENABLED_ICON;
  public ItemStack HUGGABLE_DISABLED_ICON;

  public ItemStack HUGGABLE_ENABLED_BUTTON;
  public ItemStack HUGGABLE_DISABLED_BUTTON;

  public ItemPair huggableItems;

  // New Huggable Settings
  public ItemStack HUGGABILITY_OFF_ICON;
  public ItemStack HUGGABILITY_OFF_BUTTON;

  public ItemStack HUGGABILITY_REALISTIC_ICON;
  public ItemStack HUGGABILITY_REALISTIC_BUTTON;

  public ItemStack HUGGABILITY_ALL_ICON;
  public ItemStack HUGGABILITY_ALL_BUTTON;

  // New Particle Settings
  public ItemStack PARTICLES_OFF_ICON;
  public ItemStack PARTICLES_OFF_BUTTON;

  public ItemStack PARTICLES_LOW_ICON;
  public ItemStack PARTICLES_LOW_BUTTON;

  public ItemStack PARTICLES_MEDIUM_ICON;
  public ItemStack PARTICLES_MEDIUM_BUTTON;

  public ItemStack PARTICLES_HIGH_ICON;
  public ItemStack PARTICLES_HIGH_BUTTON;

  public ItemStack PARTICLES_EXTREME_ICON;
  public ItemStack PARTICLES_EXTREME_BUTTON;

  public ItemPair particleSettingItems;

  public ItemStack HUG_SOUNDS_ENABLED_ICON;
  public ItemStack HUG_SOUNDS_DISABLED_ICON;

  public ItemStack HUG_SOUNDS_ENABLED_BUTTON;
  public ItemStack HUG_SOUNDS_DISABLED_BUTTON;

  public ItemPair hugSoundSettingsItems;

  // New Sound Settings
  public ItemStack SOUNDS_MENU_ENABLED_ICON;
  public ItemStack SOUNDS_MENU_ENABLED_BUTTON;

  public ItemStack SOUNDS_MENU_DISABLED_ICON;
  public ItemStack SOUNDS_MENU_DISABLED_BUTTON;

  public ItemStack SOUNDS_COMMAND_ENABLED_ICON;
  public ItemStack SOUNDS_COMMAND_ENABLED_BUTTON;

  public ItemStack SOUNDS_COMMAND_DISABLED_ICON;
  public ItemStack SOUNDS_COMMAND_DISABLED_BUTTON;

  public ItemPair soundMenuSettingsItems;
  public ItemPair soundCommandSettingsItems;

  // Your Indicators
  public ItemStack CHAT_INDICATOR_ENABLED_ICON;
  public ItemStack CHAT_INDICATOR_DISABLED_ICON;

  public ItemStack CHAT_INDICATOR_ENABLED_BUTTON;
  public ItemStack CHAT_INDICATOR_DISABLED_BUTTON;

  public ItemPair chatIndicatorItems;

  public ItemStack ACTIONBAR_INDICATOR_ENABLED_ICON;
  public ItemStack ACTIONBAR_INDICATOR_DISABLED_ICON;

  public ItemStack ACTIONBAR_INDICATOR_ENABLED_BUTTON;
  public ItemStack ACTIONBAR_INDICATOR_DISABLED_BUTTON;

  public ItemPair actionbarIndicatorItems;

  public ItemStack BOSSBAR_INDICATOR_ENABLED_ICON;
  public ItemStack BOSSBAR_INDICATOR_DISABLED_ICON;

  public ItemStack BOSSBAR_INDICATOR_ENABLED_BUTTON;
  public ItemStack BOSSBAR_INDICATOR_DISABLED_BUTTON;

  public ItemPair bossbarIndicatorItems;

  public ItemStack TITLES_INDICATOR_ENABLED_ICON;
  public ItemStack TITLES_INDICATOR_DISABLED_ICON;

  public ItemStack TITLES_INDICATOR_ENABLED_BUTTON;
  public ItemStack TITLES_INDICATOR_DISABLED_BUTTON;

  public ItemPair titlesIndicatorItems;

  public ItemStack TOAST_INDICATOR_ENABLED_ICON;
  public ItemStack TOAST_INDICATOR_DISABLED_ICON;

  public ItemStack TOAST_INDICATOR_ENABLED_BUTTON;
  public ItemStack TOAST_INDICATOR_DISABLED_BUTTON;

  public ItemPair toastIndicatorItems;

  // Plugin Settings
  public ItemStack TEMPORARY_INVINCIBILITY_ENABLED_ICON;
  public ItemStack TEMPORARY_INVINCIBILITY_DISABLED_ICON;

  public ItemStack TEMPORARY_INVINCIBILITY_ENABLED_BUTTON;
  public ItemStack TEMPORARY_INVINCIBILITY_DISABLED_BUTTON;

  public ItemPair temporaryInvincibilityItems;

  public ItemStack PASSIVE_MODE_ENABLED_ICON;
  public ItemStack PASSIVE_MODE_DISABLED_ICON;

  public ItemStack PASSIVE_MODE_ENABLED_BUTTON;
  public ItemStack PASSIVE_MODE_DISABLED_BUTTON;

  public ItemPair passiveModeItems;

  public ItemStack REALISTIC_HUGS_ENABLED_ICON;
  public ItemStack REALISTIC_HUGS_DISABLED_ICON;

  public ItemStack REALISTIC_HUGS_ENABLED_BUTTON;
  public ItemStack REALISTIC_HUGS_DISABLED_BUTTON;

  public ItemPair realisticHugsItems;

  public ItemStack EXACT_NAMES_ENABLED_ICON;
  public ItemStack EXACT_NAMES_DISABLED_ICON;

  public ItemStack EXACT_NAMES_ENABLED_BUTTON;
  public ItemStack EXACT_NAMES_DISABLED_BUTTON;

  public ItemPair exactNamesItems;

  public ItemStack CONFIRMATIONS_ENABLED_ICON;
  public ItemStack CONFIRMATIONS_DISABLED_ICON;

  public ItemStack CONFIRMATIONS_ENABLED_BUTTON;
  public ItemStack CONFIRMATIONS_DISABLED_BUTTON;

  public ItemPair confirmationItems;

  public ItemStack RESTORE_HEALTH_ENABLED_ICON;
  public ItemStack RESTORE_HEALTH_DISABLED_ICON;

  public ItemStack RESTORE_HEALTH_ENABLED_BUTTON;
  public ItemStack RESTORE_HEALTH_DISABLED_BUTTON;

  public ItemPair restoreHealthItems;

  public ItemStack RESTORE_HUNGER_ENABLED_ICON;
  public ItemStack RESTORE_HUNGER_DISABLED_ICON;

  public ItemStack RESTORE_HUNGER_ENABLED_BUTTON;
  public ItemStack RESTORE_HUNGER_DISABLED_BUTTON;

  public ItemPair restoreHungerItems;

  public ItemStack SELF_HUGS_ENABLED_ICON;
  public ItemStack SELF_HUGS_DISABLED_ICON;

  public ItemStack SELF_HUGS_ENABLED_BUTTON;
  public ItemStack SELF_HUGS_DISABLED_BUTTON;

  public ItemPair selfHugsItems;

  public ItemStack SHIFT_HUGS_ENABLED_ICON;
  public ItemStack SHIFT_HUGS_DISABLED_ICON;

  public ItemStack SHIFT_HUGS_ENABLED_BUTTON;
  public ItemStack SHIFT_HUGS_DISABLED_BUTTON;

  public ItemPair shiftHugsItems;

  // - Backend Settings
  public ItemStack CHECK_FOR_UPDATES_ENABLED_ICON;
  public ItemStack CHECK_FOR_UPDATES_DISABLED_ICON;

  public ItemStack CHECK_FOR_UPDATES_ENABLED_BUTTON;
  public ItemStack CHECK_FOR_UPDATES_DISABLED_BUTTON;

  public ItemPair checkForUpdatesItems;

  public ItemStack DEVELOPER_REWARD_ENABLED_ICON;
  public ItemStack DEVELOPER_REWARD_DISABLED_ICON;

  public ItemStack DEVELOPER_REWARD_ENABLED_BUTTON;
  public ItemStack DEVELOPER_REWARD_DISABLED_BUTTON;

  public ItemPair developerRewardItems;

  public ItemStack OVERRIDE_ENABLED_ICON;
  public ItemStack OVERRIDE_ENABLED_BUTTON;

  public ItemStack OVERRIDE_DISABLED_ICON;
  public ItemStack OVERRIDE_DISABLED_BUTTON;

  public ItemPair overrideItems;

  private final ItemManager itemManager;
  private final String CLICK_TO_ENABLE = "&7Click to enable!";
  private final String CLICK_TO_DISABLE = "&7Click to disable!";
  private final String CLICK_TO_TOGGLE = "&eClick to toggle.";
  private final String PADDING = "   ";

  public ItemStates(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    registerItems();
    registerSounds();
    registerParticles();
    registerIndicators();

    registerGeneralSettings();
    registerAdvancedSettings();
    registerTechnicalSettings();
    registerDefaultSettings();
    registerOverrideSettings();
    registerExperimentalSettings();
  }

  private void registerGeneralSettings() {
    String[] restoreHealthDesc = new String[]{
        "&7Set the receiver's health back",
        "&7to 100% when receiving a hug."
    };

    String[] restoreHungerDesc = new String[]{
        "&7Set the receiver's hunger back",
        "&7to 100% when receiving a hug."
    };

    String[] selfHugsDesc = new String[]{
        "&7Allow players to hug themselves."
    };

    String[] shiftHugsDesc = new String[]{
        "&7Allow players to give shift hugs."
    };

    // TODO: Change this to an Instant Health Potion.
    RESTORE_HEALTH_ENABLED_ICON = itemManager.createItemNoAttrib(XMaterial.POTION, "&aRestore Health",
        Arrays.asList(restoreHealthDesc));
    RESTORE_HEALTH_DISABLED_ICON = itemManager.createItemNoAttrib(XMaterial.GLASS_BOTTLE, "&cRestore Health",
        Arrays.asList(restoreHealthDesc));

    RESTORE_HEALTH_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aRestore Health",
        Collections.singletonList(CLICK_TO_DISABLE));
    RESTORE_HEALTH_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cRestore Health",
        Collections.singletonList(CLICK_TO_ENABLE));

    restoreHealthItems = new ItemPair(
        new IconSet(RESTORE_HEALTH_ENABLED_ICON, RESTORE_HEALTH_DISABLED_ICON),
        new ButtonSet(RESTORE_HEALTH_ENABLED_BUTTON, RESTORE_HEALTH_DISABLED_BUTTON)
    );

    RESTORE_HUNGER_ENABLED_ICON = itemManager.createItem(XMaterial.RABBIT_STEW, "&aRestore Hunger",
        Arrays.asList(restoreHungerDesc));
    RESTORE_HUNGER_DISABLED_ICON = itemManager.createItem(XMaterial.BOWL, "&cRestore Hunger",
        Arrays.asList(restoreHungerDesc));

    RESTORE_HUNGER_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aRestore Hunger",
        Collections.singletonList(CLICK_TO_DISABLE));
    RESTORE_HUNGER_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cRestore Hunger",
        Collections.singletonList(CLICK_TO_ENABLE));

    restoreHungerItems = new ItemPair(
        new IconSet(RESTORE_HUNGER_ENABLED_ICON, RESTORE_HUNGER_DISABLED_ICON),
        new ButtonSet(RESTORE_HUNGER_ENABLED_BUTTON, RESTORE_HUNGER_DISABLED_BUTTON)
    );

    SELF_HUGS_ENABLED_ICON = itemManager.createItem(XMaterial.ARMOR_STAND, "&aSelf Hugs",
        Arrays.asList(selfHugsDesc));
    SELF_HUGS_DISABLED_ICON = itemManager.createItem(XMaterial.ARMOR_STAND, "&cSelf Hugs",
        Arrays.asList(selfHugsDesc));

    SELF_HUGS_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aSelf Hugs",
        Collections.singletonList(CLICK_TO_DISABLE));
    SELF_HUGS_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cSelf Hugs",
        Collections.singletonList(CLICK_TO_ENABLE));

    selfHugsItems = new ItemPair(
        new IconSet(SELF_HUGS_ENABLED_ICON, SELF_HUGS_DISABLED_ICON),
        new ButtonSet(SELF_HUGS_ENABLED_BUTTON, SELF_HUGS_DISABLED_BUTTON)
    );

    SHIFT_HUGS_ENABLED_ICON = itemManager.createItem(XMaterial.SEA_PICKLE, "&aShift Hugs",
        Arrays.asList(shiftHugsDesc));
    SHIFT_HUGS_DISABLED_ICON = itemManager.createItem(XMaterial.SEA_PICKLE, "&cShift Hugs",
        Arrays.asList(shiftHugsDesc));

    SHIFT_HUGS_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aShift Hugs",
        Collections.singletonList(CLICK_TO_DISABLE));
    SHIFT_HUGS_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cShift Hugs",
        Collections.singletonList(CLICK_TO_ENABLE));

    shiftHugsItems = new ItemPair(
        new IconSet(SHIFT_HUGS_ENABLED_ICON, SHIFT_HUGS_DISABLED_ICON),
        new ButtonSet(SHIFT_HUGS_ENABLED_BUTTON, SHIFT_HUGS_DISABLED_BUTTON)
    );
  }

  private void registerAdvancedSettings() {
    String[] exactNamesDesc = new String[]{
        "&7Force players to type the exact name",
        "&7of the person they are trying to hug."
    };

    String[] confirmationsDesc = new String[]{
        "&7Ask for confirmation when executing",
        "&7commands that would benefit from it."
    };

    EXACT_NAMES_ENABLED_ICON = itemManager.createItem(XMaterial.NAME_TAG, "&aForce Exact Names",
        Arrays.asList(exactNamesDesc));
    EXACT_NAMES_DISABLED_ICON = itemManager.createItem(XMaterial.NAME_TAG, "&cForce Exact Names",
        Arrays.asList(exactNamesDesc));

    EXACT_NAMES_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aForce Exact Names",
        Collections.singletonList(CLICK_TO_DISABLE));
    EXACT_NAMES_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cForce Exact Names",
        Collections.singletonList(CLICK_TO_ENABLE));

    exactNamesItems = new ItemPair(
        new IconSet(EXACT_NAMES_ENABLED_ICON, EXACT_NAMES_DISABLED_ICON),
        new ButtonSet(EXACT_NAMES_ENABLED_BUTTON, EXACT_NAMES_DISABLED_BUTTON)
    );

    CONFIRMATIONS_ENABLED_ICON = itemManager.createItem(XMaterial.ENDER_EYE, "&aConfirmations",
        Arrays.asList(confirmationsDesc));
    CONFIRMATIONS_DISABLED_ICON = itemManager.createItem(XMaterial.ENDER_EYE, "&cConfirmations",
        Arrays.asList(confirmationsDesc));

    CONFIRMATIONS_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aConfirmations",
        Collections.singletonList(CLICK_TO_DISABLE));
    CONFIRMATIONS_DISABLED_BUTTON = itemManager.createItem(Material.GRAY_DYE, "&cConfirmations",
        Collections.singletonList(CLICK_TO_ENABLE));

    confirmationItems = new ItemPair(
        new IconSet(CONFIRMATIONS_ENABLED_ICON, CONFIRMATIONS_DISABLED_ICON),
        new ButtonSet(CONFIRMATIONS_ENABLED_BUTTON, CONFIRMATIONS_DISABLED_BUTTON)
    );
  }

  private void registerTechnicalSettings() {
    String[] checkForUpdatesDesc = new String[]{
        "&7Allow the plugin to check",
        "&7for updates on startup."
    };

    CHECK_FOR_UPDATES_ENABLED_ICON = itemManager.createItem(XMaterial.ENCHANTING_TABLE, "&aCheck For Updates",
        Arrays.asList(checkForUpdatesDesc));
    CHECK_FOR_UPDATES_DISABLED_ICON = itemManager.createItem(XMaterial.ENCHANTING_TABLE, "&cCheck For Updates",
        Arrays.asList(checkForUpdatesDesc));

    CHECK_FOR_UPDATES_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aCheck For Updates",
        Collections.singletonList(CLICK_TO_DISABLE));
    CHECK_FOR_UPDATES_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cCheck For Updates",
        Collections.singletonList(CLICK_TO_ENABLE));

    checkForUpdatesItems = new ItemPair(
        new IconSet(Arrays.asList(CHECK_FOR_UPDATES_ENABLED_ICON, CHECK_FOR_UPDATES_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(CHECK_FOR_UPDATES_ENABLED_BUTTON, CHECK_FOR_UPDATES_DISABLED_BUTTON)));

    DEVELOPER_REWARD_ENABLED_ICON = itemManager.createNMSSkullItem(PluginConstants.DEVELOPER_REWARD_TEXTURE,
        "&aDeveloper Reward");
    DEVELOPER_REWARD_DISABLED_ICON = itemManager.createNMSSkullItem(PluginConstants.DEVELOPER_REWARD_TEXTURE,
        "&cDeveloper Reward");

    DEVELOPER_REWARD_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aDeveloper Reward",
        Collections.singletonList(CLICK_TO_DISABLE));
    DEVELOPER_REWARD_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cDeveloper Reward",
        Collections.singletonList(CLICK_TO_ENABLE));

    developerRewardItems = new ItemPair(
        new IconSet(DEVELOPER_REWARD_ENABLED_ICON, DEVELOPER_REWARD_DISABLED_ICON),
        new ButtonSet(DEVELOPER_REWARD_ENABLED_BUTTON, DEVELOPER_REWARD_DISABLED_BUTTON)
    );
  }

  private void registerDefaultSettings() {

  }

  private void registerOverrideSettings() {
    OVERRIDE_ENABLED_ICON = itemManager.createItem(XMaterial.PARROT_SPAWN_EGG, "&aOverrides");
    OVERRIDE_DISABLED_ICON = itemManager.createItem(XMaterial.ENDERMAN_SPAWN_EGG, "&cOverrides");

    OVERRIDE_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aOverrides",
        Collections.singletonList(CLICK_TO_DISABLE));
    OVERRIDE_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cOverrides",
        Collections.singletonList(CLICK_TO_ENABLE));

    overrideItems = new ItemPair(
        new IconSet(OVERRIDE_ENABLED_ICON, OVERRIDE_DISABLED_ICON),
        new ButtonSet(OVERRIDE_ENABLED_BUTTON, OVERRIDE_DISABLED_BUTTON)
    );
  }

  private void registerExperimentalSettings() {
    String[] tempInvincibilityDesc = new String[]{
        "&7Allow players to receive temporary",
        "&7invincibility when hugged."
    };

    String[] passiveModeDesc = new String[]{
        "&7Prevent players from harming",
        "&7each other."
    };

    String[] realisticHugsDesc = new String[]{
        "&7Force players to give shift hugs",
        "&7instead of using commands."
    };

    TEMPORARY_INVINCIBILITY_ENABLED_ICON = itemManager.createItem(XMaterial.BEDROCK, "&aTemporary Invincibility",
        Arrays.asList(tempInvincibilityDesc));
    TEMPORARY_INVINCIBILITY_DISABLED_ICON = itemManager.createItem(XMaterial.BEDROCK, "&cTemporary Invincibility",
        Arrays.asList(tempInvincibilityDesc));

    TEMPORARY_INVINCIBILITY_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aTemporary Invincibility",
        Collections.singletonList(CLICK_TO_DISABLE));
    TEMPORARY_INVINCIBILITY_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cTemporary Invincibility",
        Collections.singletonList(CLICK_TO_ENABLE));

    temporaryInvincibilityItems = new ItemPair(
        new IconSet(TEMPORARY_INVINCIBILITY_ENABLED_ICON, TEMPORARY_INVINCIBILITY_DISABLED_ICON),
        new ButtonSet(TEMPORARY_INVINCIBILITY_ENABLED_BUTTON, TEMPORARY_INVINCIBILITY_DISABLED_BUTTON)
    );

    PASSIVE_MODE_ENABLED_ICON = itemManager.createItem(XMaterial.POPPY, "&aPassive Mode",
        Arrays.asList(passiveModeDesc));
    PASSIVE_MODE_DISABLED_ICON = itemManager.createItem(XMaterial.WITHER_ROSE, "&cPassive Mode",
        Arrays.asList(passiveModeDesc));

    PASSIVE_MODE_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aPassive Mode",
        Collections.singletonList(CLICK_TO_DISABLE));
    PASSIVE_MODE_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cPassive Mode",
        Collections.singletonList(CLICK_TO_ENABLE));

    passiveModeItems = new ItemPair(
        new IconSet(PASSIVE_MODE_ENABLED_ICON, PASSIVE_MODE_DISABLED_ICON),
        new ButtonSet(PASSIVE_MODE_ENABLED_BUTTON, PASSIVE_MODE_DISABLED_BUTTON)
    );

    REALISTIC_HUGS_ENABLED_ICON = itemManager.createItem(XMaterial.LEAD, "&aRealistic Hugs",
        Arrays.asList(realisticHugsDesc));
    REALISTIC_HUGS_DISABLED_ICON = itemManager.createItem(XMaterial.LEAD, "&cRealistic Hugs",
        Arrays.asList(realisticHugsDesc));

    REALISTIC_HUGS_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aRealistic Hugs",
        Collections.singletonList(CLICK_TO_DISABLE));
    REALISTIC_HUGS_DISABLED_BUTTON = itemManager.createItem(Material.GRAY_DYE, "&cRealistic Hugs",
        Collections.singletonList(CLICK_TO_ENABLE));

    realisticHugsItems = new ItemPair(
        new IconSet(REALISTIC_HUGS_ENABLED_ICON, REALISTIC_HUGS_DISABLED_ICON),
        new ButtonSet(REALISTIC_HUGS_ENABLED_BUTTON, REALISTIC_HUGS_DISABLED_BUTTON)
    );
  }

  public void registerItems() {
    //Your Settings
    String[] huggableDesc = new String[]{
        "&7Toggle whether or not you want",
        "&7to be hugged by random people."
    };

    HUGGABLE_ENABLED_ICON = itemManager.createItem(XMaterial.PARROT_SPAWN_EGG, "&aHuggable",
        Arrays.asList(huggableDesc));
    HUGGABLE_DISABLED_ICON = itemManager.createItem(XMaterial.GHAST_SPAWN_EGG, "&cHuggable",
        Arrays.asList(huggableDesc));

    HUGGABLE_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aHuggable",
        Collections.singletonList(CLICK_TO_DISABLE));
    HUGGABLE_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cHuggable",
        Collections.singletonList(CLICK_TO_ENABLE));

//    huggableItems = new ItemPair(
//        new IconSet(Arrays.asList(HUGGABLE_ENABLED_ICON, HUGGABLE_DISABLED_ICON)),
//        new ButtonSet(Arrays.asList(HUGGABLE_ENABLED_BUTTON, HUGGABLE_DISABLED_BUTTON)));


    String path = "menus.your_preferences_menu.huggability";
    String iconPath = "menus.your_preferences_menu.huggability_icon";

    String iconTitle = plugin.getLangFile().getString(iconPath + ".title");
    List<String> iconLore = plugin.getLangFile().getStringList(iconPath + ".lore");

    String buttonTitle = plugin.getLangFile().getString(path + ".title");
    List<String> buttonLore = buildLore(path, HuggableState.NONE.ordinal());

    HUGGABILITY_OFF_ICON = itemManager.createItem(XMaterial.GHAST_SPAWN_EGG, iconTitle, iconLore);
    HUGGABILITY_OFF_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, buttonTitle, buttonLore);

    buttonLore = buildLore(path, HuggableState.REALISTIC.ordinal());

    HUGGABILITY_REALISTIC_ICON = itemManager.createItem(XMaterial.FOX_SPAWN_EGG, iconTitle, iconLore);
    HUGGABILITY_REALISTIC_BUTTON = itemManager.createItem(XMaterial.PINK_DYE, buttonTitle, buttonLore);

    buttonLore = buildLore(path, HuggableState.ALL.ordinal());

    HUGGABILITY_ALL_ICON = itemManager.createItem(XMaterial.PARROT_SPAWN_EGG, iconTitle, iconLore);
    HUGGABILITY_ALL_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, buttonTitle, buttonLore);

    huggableItems = new ItemPair(
        new IconSet(HUGGABILITY_ALL_ICON, HUGGABILITY_REALISTIC_ICON, HUGGABILITY_OFF_ICON),
        new ButtonSet(HUGGABILITY_ALL_BUTTON, HUGGABILITY_REALISTIC_BUTTON, HUGGABILITY_OFF_BUTTON)
    );
  }

  private void registerSounds() {
    String hugSoundsIconTitle = plugin.getLangFile().getString("menus.your_sounds_menu.hug_sounds_enabled_icon.title");
    List<String> hugSoundsIconButton = plugin.getLangFile()
        .getStringList("menus.your_sounds_menu.hug_sounds_enabled_icon.lore");
    String hugSoundsButtonTitle = plugin.getLangFile()
        .getString("menus.your_sounds_menu.hug_sounds_enabled_button.title");
    List<String> hugSoundsButtonLore = plugin.getLangFile()
        .getStringList("menus.your_sounds_menu.hug_sounds_enabled_button.lore");

    HUG_SOUNDS_ENABLED_ICON = itemManager.createItem(XMaterial.RABBIT_HIDE, hugSoundsIconTitle, hugSoundsIconButton);
    HUG_SOUNDS_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, hugSoundsButtonTitle, hugSoundsButtonLore);

    hugSoundsIconTitle = plugin.getLangFile().getString("menus.your_sounds_menu.hug_sounds_disabled_icon.title");
    hugSoundsIconButton = plugin.getLangFile().getStringList("menus.your_sounds_menu.hug_sounds_disabled_icon.lore");
    hugSoundsButtonTitle = plugin.getLangFile().getString("menus.your_sounds_menu.hug_sounds_disabled_button.title");
    hugSoundsButtonLore = plugin.getLangFile().getStringList("menus.your_sounds_menu.hug_sounds_disabled_button.lore");

    HUG_SOUNDS_DISABLED_ICON = itemManager.createItem(XMaterial.RABBIT_HIDE, hugSoundsIconTitle, hugSoundsIconButton);
    HUG_SOUNDS_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, hugSoundsButtonTitle, hugSoundsButtonLore);

    hugSoundSettingsItems = new ItemPair(
        new IconSet(Arrays.asList(HUG_SOUNDS_ENABLED_ICON, HUG_SOUNDS_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(HUG_SOUNDS_ENABLED_BUTTON, HUG_SOUNDS_DISABLED_BUTTON)));

    String menuSoundsIconTitle = plugin.getLangFile()
        .getString("menus.your_sounds_menu.menu_sounds_enabled_icon.title");
    List<String> menuSoundsIconButton = plugin.getLangFile()
        .getStringList("menus.your_sounds_menu.menu_sounds_enabled_icon.lore");
    String menuSoundsButtonTitle = plugin.getLangFile()
        .getString("menus.your_sounds_menu.menu_sounds_enabled_button.title");
    List<String> menuSoundsButtonLore = plugin.getLangFile()
        .getStringList("menus.your_sounds_menu.menu_sounds_enabled_button.lore");

    SOUNDS_MENU_ENABLED_ICON = itemManager.createItem(XMaterial.ITEM_FRAME, menuSoundsIconTitle, menuSoundsIconButton);
    SOUNDS_MENU_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, menuSoundsButtonTitle,
        menuSoundsButtonLore);

    menuSoundsIconTitle = plugin.getLangFile().getString("menus.your_sounds_menu.menu_sounds_disabled_icon.title");
    menuSoundsIconButton = plugin.getLangFile().getStringList("menus.your_sounds_menu.menu_sounds_disabled_icon.lore");
    menuSoundsButtonTitle = plugin.getLangFile().getString("menus.your_sounds_menu.menu_sounds_disabled_button.title");
    menuSoundsButtonLore = plugin.getLangFile()
        .getStringList("menus.your_sounds_menu.menu_sounds_disabled_button.lore");

    SOUNDS_MENU_DISABLED_ICON = itemManager.createItem(XMaterial.ITEM_FRAME, menuSoundsIconTitle, menuSoundsIconButton);
    SOUNDS_MENU_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, menuSoundsButtonTitle,
        menuSoundsButtonLore);

    String commandSoundsIconTitle = plugin.getLangFile()
        .getString("menus.your_sounds_menu.command_sounds_enabled_icon.title");
    List<String> commandSoundsIconButton = plugin.getLangFile()
        .getStringList("menus.your_sounds_menu.command_sounds_enabled_icon.lore");
    String commandSoundsButtonTitle = plugin.getLangFile()
        .getString("menus.your_sounds_menu.command_sounds_enabled_button.title");
    List<String> commandSoundsButtonLore = plugin.getLangFile()
        .getStringList("menus.your_sounds_menu.command_sounds_enabled_button.lore");

    SOUNDS_COMMAND_ENABLED_ICON = itemManager.createItem(XMaterial.PAPER, commandSoundsIconTitle,
        commandSoundsIconButton);
    SOUNDS_COMMAND_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, commandSoundsButtonTitle,
        commandSoundsButtonLore);

    commandSoundsIconTitle = plugin.getLangFile()
        .getString("menus.your_sounds_menu.command_sounds_disabled_icon.title");
    commandSoundsIconButton = plugin.getLangFile()
        .getStringList("menus.your_sounds_menu.command_sounds_disabled_icon.lore");
    commandSoundsButtonTitle = plugin.getLangFile()
        .getString("menus.your_sounds_menu.command_sounds_disabled_button.title");
    commandSoundsButtonLore = plugin.getLangFile()
        .getStringList("menus.your_sounds_menu.command_sounds_disabled_button.lore");

    SOUNDS_COMMAND_DISABLED_ICON = itemManager.createItem(XMaterial.PAPER, commandSoundsIconTitle,
        commandSoundsIconButton);
    SOUNDS_COMMAND_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, commandSoundsButtonTitle,
        commandSoundsButtonLore);

    soundMenuSettingsItems = new ItemPair(
        new IconSet(Arrays.asList(SOUNDS_MENU_ENABLED_ICON, SOUNDS_MENU_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(SOUNDS_MENU_ENABLED_BUTTON, SOUNDS_MENU_DISABLED_BUTTON)));

    soundCommandSettingsItems = new ItemPair(
        new IconSet(Arrays.asList(SOUNDS_COMMAND_ENABLED_ICON, SOUNDS_COMMAND_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(SOUNDS_COMMAND_ENABLED_BUTTON, SOUNDS_COMMAND_DISABLED_BUTTON)));
  }

  private void registerParticles() {
    String path = "menus.your_visuals_menu.particle_quality";
    String iconTitle = plugin.getLangFile().getString("menus.your_visuals_menu.particle_quality_icon.title")
        .replace("%color_prefix%", ParticleQuality.NONE.getColor());
    List<String> iconLore = plugin.getLangFile().getStringList("menus.your_visuals_menu.particle_quality_icon.lore");

    String buttonTitle = plugin.getLangFile().getString("menus.your_visuals_menu.particle_quality.title");
    List<String> buttonLore = buildLore(path, ParticleQuality.NONE.ordinal());

    PARTICLES_OFF_ICON = itemManager.createItem(XMaterial.DEAD_BUSH, iconTitle, iconLore);
    PARTICLES_OFF_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, buttonTitle, buttonLore);

    iconTitle = plugin.getLangFile().getString("menus.your_visuals_menu.particle_quality_icon.title")
        .replace("%color_prefix%", ParticleQuality.LOW.getColor());
    iconLore = plugin.getLangFile().getStringList("menus.your_visuals_menu.particle_quality_icon.lore");
    buttonLore = buildLore(path, ParticleQuality.LOW.ordinal());

    PARTICLES_LOW_ICON = itemManager.createItem(XMaterial.DANDELION, iconTitle, iconLore);
    PARTICLES_LOW_BUTTON = itemManager.createItem(XMaterial.YELLOW_DYE, buttonTitle, buttonLore);

    iconTitle = plugin.getLangFile().getString("menus.your_visuals_menu.particle_quality_icon.title")
        .replace("%color_prefix%", ParticleQuality.MEDIUM.getColor());
    iconLore = plugin.getLangFile().getStringList("menus.your_visuals_menu.particle_quality_icon.lore");
    buttonLore = buildLore(path, ParticleQuality.MEDIUM.ordinal());

    PARTICLES_MEDIUM_ICON = itemManager.createItem(XMaterial.AZURE_BLUET, iconTitle, iconLore);
    PARTICLES_MEDIUM_BUTTON = itemManager.createItem(XMaterial.GREEN_DYE, buttonTitle, buttonLore);

    iconTitle = plugin.getLangFile().getString("menus.your_visuals_menu.particle_quality_icon.title")
        .replace("%color_prefix%", ParticleQuality.HIGH.getColor());
    iconLore = plugin.getLangFile().getStringList("menus.your_visuals_menu.particle_quality_icon.lore");
    buttonLore = buildLore(path, ParticleQuality.HIGH.ordinal());

    PARTICLES_HIGH_ICON = itemManager.createItem(XMaterial.BLUE_ORCHID, iconTitle, iconLore);
    PARTICLES_HIGH_BUTTON = itemManager.createItem(XMaterial.CYAN_DYE, buttonTitle, buttonLore);

    iconTitle = plugin.getLangFile().getString("menus.your_visuals_menu.particle_quality_icon.title")
        .replace("%color_prefix%", ParticleQuality.EXTREME.getColor());
    iconLore = plugin.getLangFile().getStringList("menus.your_visuals_menu.particle_quality_icon.lore");
    buttonLore = buildLore(path, ParticleQuality.EXTREME.ordinal());

    PARTICLES_EXTREME_ICON = itemManager.createItem(XMaterial.LILAC, iconTitle, iconLore);
    PARTICLES_EXTREME_BUTTON = itemManager.createItem(XMaterial.PURPLE_DYE, buttonTitle, buttonLore);

    particleSettingItems = new ItemPair(
        new IconSet(Arrays.asList(PARTICLES_OFF_ICON, PARTICLES_LOW_ICON, PARTICLES_MEDIUM_ICON, PARTICLES_HIGH_ICON,
            PARTICLES_EXTREME_ICON)),
        new ButtonSet(
            Arrays.asList(PARTICLES_OFF_BUTTON, PARTICLES_LOW_BUTTON, PARTICLES_MEDIUM_BUTTON, PARTICLES_HIGH_BUTTON,
                PARTICLES_EXTREME_BUTTON)));
  }

  private void registerIndicators() {
    String chatIconEnabled = plugin.getLangFile().getString("menus.your_indicators_menu.chat_icon.title_enabled");
    String chatIconDisabled = plugin.getLangFile().getString("menus.your_indicators_menu.chat_icon.title_disabled");
    List<String> chatIndicatorLore = plugin.getLangFile().getStringList("menus.your_indicators_menu.chat_icon.lore");

    String chatButtonEnabled = plugin.getLangFile().getString("menus.your_indicators_menu.chat_button.title_enabled");
    String chatButtonDisabled = plugin.getLangFile().getString("menus.your_indicators_menu.chat_button.title_disabled");
    List<String> chatButtonLoreEnabled = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.chat_button.lore_enabled");
    List<String> chatButtonLoreDisabled = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.chat_button.lore_disabled");

    CHAT_INDICATOR_ENABLED_ICON = itemManager.createItemNoAttrib(XMaterial.FLOWER_BANNER_PATTERN, chatIconEnabled,
        chatIndicatorLore);
    CHAT_INDICATOR_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, chatButtonEnabled,
        chatButtonLoreEnabled);

    CHAT_INDICATOR_DISABLED_ICON = itemManager.createItemNoAttrib(XMaterial.FLOWER_BANNER_PATTERN, chatIconDisabled,
        chatIndicatorLore);
    CHAT_INDICATOR_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, chatButtonDisabled,
        chatButtonLoreDisabled);

    chatIndicatorItems = new ItemPair(
        new IconSet(Arrays.asList(CHAT_INDICATOR_ENABLED_ICON, CHAT_INDICATOR_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(CHAT_INDICATOR_ENABLED_BUTTON, CHAT_INDICATOR_DISABLED_BUTTON)));

    String actionbarIconEnabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.actionbar_icon.title_enabled");
    String actionbarIconDisabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.actionbar_icon.title_disabled");
    List<String> actionbarIndicatorLore = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.actionbar_icon.lore");

    String actionbarButtonEnabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.actionbar_button.title_enabled");
    String actionbarButtonDisabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.actionbar_button.title_disabled");
    List<String> actionbarButtonLoreEnabled = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.actionbar_button.lore_enabled");
    List<String> actionbarButtonLoreDisabled = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.actionbar_button.lore_disabled");

    ACTIONBAR_INDICATOR_ENABLED_ICON = itemManager.createItem(XMaterial.CAMPFIRE, actionbarIconEnabled,
        actionbarIndicatorLore);
    ACTIONBAR_INDICATOR_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, actionbarButtonEnabled,
        actionbarButtonLoreEnabled);

    ACTIONBAR_INDICATOR_DISABLED_ICON = itemManager.createItem(XMaterial.CAMPFIRE, actionbarIconDisabled,
        actionbarIndicatorLore);
    ACTIONBAR_INDICATOR_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, actionbarButtonDisabled,
        actionbarButtonLoreDisabled);

    actionbarIndicatorItems = new ItemPair(
        new IconSet(Arrays.asList(ACTIONBAR_INDICATOR_ENABLED_ICON, ACTIONBAR_INDICATOR_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(ACTIONBAR_INDICATOR_ENABLED_BUTTON, ACTIONBAR_INDICATOR_DISABLED_BUTTON)));

    String bossbarIconEnabled = plugin.getLangFile().getString("menus.your_indicators_menu.bossbar_icon.title_enabled");
    String bossbarIconDisabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.bossbar_icon.title_disabled");
    List<String> bossbarIndicatorLore = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.bossbar_icon.lore");

    String bossbarButtonEnabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.bossbar_button.title_enabled");
    String bossbarButtonDisabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.bossbar_button.title_disabled");
    List<String> bossbarButtonLoreEnabled = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.bossbar_button.lore_enabled");
    List<String> bossbarButtonLoreDisabled = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.bossbar_button.lore_disabled");

    BOSSBAR_INDICATOR_ENABLED_ICON = itemManager.createNMSSkullItem(PluginConstants.BOSSBAR_TEXTURE_VALUE,
        bossbarIconEnabled, bossbarIndicatorLore);
    BOSSBAR_INDICATOR_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, bossbarButtonEnabled,
        bossbarButtonLoreEnabled);

    BOSSBAR_INDICATOR_DISABLED_ICON = itemManager.createItem(XMaterial.WITHER_SKELETON_SKULL, bossbarIconDisabled,
        bossbarIndicatorLore);
    BOSSBAR_INDICATOR_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, bossbarButtonDisabled,
        bossbarButtonLoreDisabled);

    bossbarIndicatorItems = new ItemPair(
        new IconSet(Arrays.asList(BOSSBAR_INDICATOR_ENABLED_ICON, BOSSBAR_INDICATOR_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(BOSSBAR_INDICATOR_ENABLED_BUTTON, BOSSBAR_INDICATOR_DISABLED_BUTTON)));

    String titlesIconEnabled = plugin.getLangFile().getString("menus.your_indicators_menu.titles_icon.title_enabled");
    String titlesIconDisabled = plugin.getLangFile().getString("menus.your_indicators_menu.titles_icon.title_disabled");
    List<String> titlesIndicatorLore = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.titles_icon.lore");

    String titlesButtonEnabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.titles_button.title_enabled");
    String titlesButtonDisabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.titles_button.title_disabled");
    List<String> titlesButtonLoreEnabled = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.titles_button.lore_enabled");
    List<String> titlesButtonLoreDisabled = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.titles_button.lore_disabled");

    TITLES_INDICATOR_ENABLED_ICON = itemManager.createItem(XMaterial.ITEM_FRAME, titlesIconEnabled,
        titlesIndicatorLore);
    TITLES_INDICATOR_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, titlesButtonEnabled,
        titlesButtonLoreEnabled);

    TITLES_INDICATOR_DISABLED_ICON = itemManager.createItem(XMaterial.ITEM_FRAME, titlesIconDisabled,
        titlesIndicatorLore);
    TITLES_INDICATOR_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, titlesButtonDisabled,
        titlesButtonLoreDisabled);

    titlesIndicatorItems = new ItemPair(
        new IconSet(Arrays.asList(TITLES_INDICATOR_ENABLED_ICON, TITLES_INDICATOR_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(TITLES_INDICATOR_ENABLED_BUTTON, TITLES_INDICATOR_DISABLED_BUTTON)));

    String toastsIconEnabled = plugin.getLangFile().getString("menus.your_indicators_menu.toasts_icon.title_enabled");
    String toastsIconDisabled = plugin.getLangFile().getString("menus.your_indicators_menu.toasts_icon.title_disabled");
    List<String> toastsIndicatorLore = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.toasts_icon.lore");

    String toastsButtonEnabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.toasts_button.title_enabled");
    String toastsButtonDisabled = plugin.getLangFile()
        .getString("menus.your_indicators_menu.toasts_button.title_disabled");
    List<String> toastsButtonLoreEnabled = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.toasts_button.lore_enabled");
    List<String> toastsButtonLoreDisabled = plugin.getLangFile()
        .getStringList("menus.your_indicators_menu.toasts_button.lore_disabled");

    TOAST_INDICATOR_ENABLED_ICON = itemManager.createItem(XMaterial.BREAD, toastsIconEnabled, toastsIndicatorLore);
    TOAST_INDICATOR_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, toastsButtonEnabled,
        toastsButtonLoreEnabled);

    TOAST_INDICATOR_DISABLED_ICON = itemManager.createItem(XMaterial.BREAD, toastsIconDisabled, toastsIndicatorLore);
    TOAST_INDICATOR_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, toastsButtonDisabled,
        toastsButtonLoreDisabled);

    toastIndicatorItems = new ItemPair(
        new IconSet(Arrays.asList(TOAST_INDICATOR_ENABLED_ICON, TOAST_INDICATOR_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(TOAST_INDICATOR_ENABLED_BUTTON, TOAST_INDICATOR_DISABLED_BUTTON)));
  }

  public ItemPair getHuggableItems() {
    return huggableItems;
  }

  public ItemPair getParticleSettingItems() {
    return particleSettingItems;
  }

  public ItemStack getParticleSettingIcon(UUID uuid) {
    ParticleQuality value = plugin.getPlayerDataManager().getParticleQuality(uuid);

    switch (value) {
      case NONE:
        return PARTICLES_OFF_ICON;
      case LOW:
        return PARTICLES_LOW_ICON;
      case MEDIUM:
        return PARTICLES_MEDIUM_ICON;
      case HIGH:
        return PARTICLES_HIGH_ICON;
      case EXTREME:
        return PARTICLES_EXTREME_ICON;
      default:
        return itemManager.createItem(XMaterial.BARRIER, "&cERROR!");
    }
  }

  public ItemStack getParticleSettingButton(UUID uuid) {
    ParticleQuality value = plugin.getPlayerDataManager().getParticleQuality(uuid);

    switch (value) {
      case NONE:
        return PARTICLES_OFF_BUTTON;
      case LOW:
        return PARTICLES_LOW_BUTTON;
      case MEDIUM:
        return PARTICLES_MEDIUM_BUTTON;
      case HIGH:
        return PARTICLES_HIGH_BUTTON;
      case EXTREME:
        return PARTICLES_EXTREME_BUTTON;
      default:
        return itemManager.createItem(XMaterial.BARRIER, "&cERROR!");
    }
  }

  public ItemStack getParticleSettingsButton(UUID uuid) {
    ParticleQuality value = plugin.getPlayerDataManager().getParticleQuality(uuid);

    String path = "particle_quality";
    List<String> lore = buildLore(path, value.ordinal());

    return null;
  }


  private List<String> buildLore(String path, int selectedState) {
    FileConfiguration config = plugin.getLangFile();

    // Array size safety check.
    int totalStates = config.getConfigurationSection(path + ".states").getKeys(false).size();
    if (selectedState >= totalStates) {
      selectedState = totalStates - 1;
    } else if (selectedState < 0) {
      selectedState = 0;
    }

    List<String> lore = config.getStringList(path + ".lore");
    List<String> stateNames = new ArrayList<>(config.getConfigurationSection(path + ".states").getKeys(false));
    List<String> states = buildList(path, selectedState);

    // Replace the description placeholder using the selected state.
    int i = 0;
    for (String s : lore) {
      if (s.equalsIgnoreCase("%" + config.getConfigurationSection(path).getName() + "_description" + "%")) {
//        lore.set(i, config.getString(path + ".states." + stateNames.get(selectedState) + ".description"));

        lore.remove(i);

        lore.addAll(i, config.getStringList(path + ".states." + stateNames.get(selectedState) + ".description"));

//        lore.addAll(i, List.of(ChatPaginator.wordWrap(
//            config.getString(path + ".states." + stateNames.get(selectedState) + ".description"), 40)));
//
//        lore.set(i, Arrays.toString(ChatPaginator.wordWrap(
//            config.getString(path + ".states." + stateNames.get(selectedState) + ".description"), 32)));
//        ChatPaginator.wordWrap("", 32);
        break;
      }
      i++;
    }

    // Replace the states placeholder with the list of states.
    i = 0;
    for (int j = 0; j < lore.size(); j++) {
      if (lore.get(j).equalsIgnoreCase("%" + config.getConfigurationSection(path).getName() + "_states" + "%")) {
        lore.remove(j);
        i = j;
        break;
      }
    }

    for (String s : states) {
      lore.add(i, s);
      i++;
    }

    return lore;
  }


  private List<String> buildList(String inputPath, int selectedState) {
    List<String> lore = new ArrayList<>();
    FileConfiguration config = plugin.getLangFile();

    int i = 0;

    for (String state : config.getConfigurationSection(inputPath + ".states").getKeys(false)) {
      String selected = config.getString(inputPath + ".indicator.selected_format");
      String unselected = config.getString(inputPath + ".indicator.unselected_format");

      if (i == selectedState) {
        lore.add(selected
            .replace("%color_prefix%", config.getString(inputPath + ".states." + state + ".color_prefix"))
            .replace("%display_name%", config.getString(inputPath + ".states." + state + ".display_name"))
        );
      } else {
        lore.add(unselected
            .replace("%color_prefix%", config.getString(inputPath + ".states." + state + ".color_prefix"))
            .replace("%display_name%", config.getString(inputPath + ".states." + state + ".display_name"))
        );
      }

      i++;
    }

    return lore;
  }

  public ItemPair getHugSoundSettingsItems() {
    return hugSoundSettingsItems;
  }

  public ItemPair getSoundMenuSettingsItems() {
    return soundMenuSettingsItems;
  }

  public ItemPair getSoundCommandSettingsItems() {
    return soundCommandSettingsItems;
  }

  public ItemPair getChatIndicatorItems() {
    return chatIndicatorItems;
  }

  public ItemPair getActionbarIndicatorItems() {
    return actionbarIndicatorItems;
  }

  public ItemPair getBossbarIndicatorItems() {
    return bossbarIndicatorItems;
  }

  public ItemPair getTitlesIndicatorItems() {
    return titlesIndicatorItems;
  }

  public ItemPair getToastIndicatorItems() {
    return toastIndicatorItems;
  }

  public ItemPair getCheckForUpdatesItems() {
    return checkForUpdatesItems;
  }

  public ItemPair getParticleSettingsItems() {
    return particleSettingItems;
  }

  public ItemPair getPassiveModeItems() {
    return passiveModeItems;
  }

  public ItemPair getRealisticHugsItems() {
    return realisticHugsItems;
  }

  public ItemPair getExactNamesItems() {
    return exactNamesItems;
  }

  public ItemPair getRestoreHealthItems() {
    return restoreHealthItems;
  }

  public ItemPair getRestoreHungerItems() {
    return restoreHungerItems;
  }

  public ItemPair getTemporaryInvincibilityItems() {
    return temporaryInvincibilityItems;
  }

  public ItemPair getSelfHugsItems() {
    return selfHugsItems;
  }

  public ItemPair getDeveloperRewardItems() {
    return developerRewardItems;
  }

  public ItemPair getConfirmationItems() {
    return confirmationItems;
  }

  public ItemPair getShiftHugsItems() {
    return shiftHugsItems;
  }

  public ItemPair getOverrideItems() {
    return overrideItems;
  }
}
