package me.nothixal.hugs.utils;

import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.ServerVersion;
import me.nothixal.hugs.managers.advancements.Advancement_v1_14_R1;
import me.nothixal.hugs.managers.advancements.Advancement_v1_15_R1;
import me.nothixal.hugs.managers.advancements.Advancement_v1_16_R1;
import me.nothixal.hugs.managers.advancements.Advancement_v1_16_R2;
import me.nothixal.hugs.managers.advancements.Advancement_v1_16_R3;
import me.nothixal.hugs.managers.advancements.Advancement_v1_17_R1;
import me.nothixal.hugs.managers.advancements.Advancement_v1_18_R1;
import me.nothixal.hugs.managers.advancements.Advancement_v1_18_R2;
import me.nothixal.hugs.managers.advancements.Advancement_v1_19_R1;
import me.nothixal.hugs.managers.advancements.Advancement_v1_19_R2;
import me.nothixal.hugs.managers.advancements.Advancement_v1_19_R3;
import me.nothixal.hugs.managers.advancements.Advancement_v1_20_R1;
import me.nothixal.hugs.managers.advancements.Advancement_v1_20_R2;
import me.nothixal.hugs.managers.advancements.Advancement_v1_20_R3;
import me.nothixal.hugs.managers.chat.Chat_v1_14_R1;
import me.nothixal.hugs.managers.chat.Chat_v1_15_R1;
import me.nothixal.hugs.managers.chat.Chat_v1_16_R1;
import me.nothixal.hugs.managers.chat.Chat_v1_16_R2;
import me.nothixal.hugs.managers.chat.Chat_v1_16_R3;
import me.nothixal.hugs.managers.chat.Chat_v1_17_R1;
import me.nothixal.hugs.managers.chat.Chat_v1_18_R1;
import me.nothixal.hugs.managers.chat.Chat_v1_18_R2;
import me.nothixal.hugs.managers.chat.Chat_v1_19_R1;
import me.nothixal.hugs.managers.chat.Chat_v1_19_R2;
import me.nothixal.hugs.managers.chat.Chat_v1_19_R3;
import me.nothixal.hugs.managers.chat.Chat_v1_20_R1;
import me.nothixal.hugs.managers.chat.Chat_v1_20_R2;
import me.nothixal.hugs.managers.chat.Chat_v1_20_R3;
import me.nothixal.hugs.managers.items.Item_v1_14_R1;
import me.nothixal.hugs.managers.items.Item_v1_15_R1;
import me.nothixal.hugs.managers.items.Item_v1_16_R1;
import me.nothixal.hugs.managers.items.Item_v1_16_R2;
import me.nothixal.hugs.managers.items.Item_v1_16_R3;
import me.nothixal.hugs.managers.items.Item_v1_17_R1;
import me.nothixal.hugs.managers.items.Item_v1_18_R1;
import me.nothixal.hugs.managers.items.Item_v1_18_R2;
import me.nothixal.hugs.managers.items.Item_v1_19_R1;
import me.nothixal.hugs.managers.items.Item_v1_19_R2;
import me.nothixal.hugs.managers.items.Item_v1_19_R3;
import me.nothixal.hugs.managers.items.Item_v1_20_R1;
import me.nothixal.hugs.managers.items.Item_v1_20_R2;
import me.nothixal.hugs.managers.items.Item_v1_20_R3;
import org.bukkit.Bukkit;

public class VersionChecker {

  private final HugsPlugin plugin;
  private ServerVersion serverVersion = ServerVersion.NOT_SUPPORTED;

  public VersionChecker(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  /**
  * Check the version of the server the plugin is running on.
  * */
  public void checkServerVersion() {
    String version;

    try {
      version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
//      System.out.println(version);
    } catch (ArrayIndexOutOfBoundsException versionNotSupported) {
      version = "UNKNOWN";
    }

    for (ServerVersion ver : ServerVersion.values()) {
      if (ver.getValue().contains(version)) {
        this.serverVersion = ver;
        return;
      }
    }

//    System.out.println(serverVersion);
//    System.out.println(serverVersion.getValue());
//    System.out.println(serverVersion.getDisplayValue());
//    System.out.println(serverVersion.isSupported());
  }

  /**
  * Register the necessary classes depending on what server version was detected.
  * */
  public void registerClasses() {
    switch (serverVersion) {
      case VERSION1_20R3:
        plugin.setItemManager(new Item_v1_20_R3());
        plugin.setChatManager(new Chat_v1_20_R3());
        plugin.setAdvancementManager(new Advancement_v1_20_R3());
        break;
      case VERSION1_20R2:
        plugin.setItemManager(new Item_v1_20_R2());
        plugin.setChatManager(new Chat_v1_20_R2());
        plugin.setAdvancementManager(new Advancement_v1_20_R2());
        break;
      case VERSION1_20R1:
        plugin.setItemManager(new Item_v1_20_R1());
        plugin.setChatManager(new Chat_v1_20_R1());
        plugin.setAdvancementManager(new Advancement_v1_20_R1());
        break;
      case VERSION1_19R3:
        plugin.setItemManager(new Item_v1_19_R3());
        plugin.setChatManager(new Chat_v1_19_R3());
        plugin.setAdvancementManager(new Advancement_v1_19_R3());
        break;
      case VERSION1_19R2:
        plugin.setItemManager(new Item_v1_19_R2());
        plugin.setChatManager(new Chat_v1_19_R2());
        plugin.setAdvancementManager(new Advancement_v1_19_R2());
        break;
      case VERSION1_19R1:
        plugin.setItemManager(new Item_v1_19_R1());
        plugin.setChatManager(new Chat_v1_19_R1());
        plugin.setAdvancementManager(new Advancement_v1_19_R1());
        break;
      case VERSION1_18R2:
        plugin.setItemManager(new Item_v1_18_R2());
        plugin.setChatManager(new Chat_v1_18_R2());
        plugin.setAdvancementManager(new Advancement_v1_18_R2());
        break;
      case VERSION1_18R1:
        plugin.setItemManager(new Item_v1_18_R1());
        plugin.setChatManager(new Chat_v1_18_R1());
        plugin.setAdvancementManager(new Advancement_v1_18_R1());
        break;
      case VERSION1_17R1:
        plugin.setItemManager(new Item_v1_17_R1());
        plugin.setChatManager(new Chat_v1_17_R1());
        plugin.setAdvancementManager(new Advancement_v1_17_R1());
        break;
      case VERSION1_16R3:
        plugin.setItemManager(new Item_v1_16_R3());
        plugin.setChatManager(new Chat_v1_16_R3());
        plugin.setAdvancementManager(new Advancement_v1_16_R3());
        break;
      case VERSION1_16R2:
        plugin.setItemManager(new Item_v1_16_R2());
        plugin.setChatManager(new Chat_v1_16_R2());
        plugin.setAdvancementManager(new Advancement_v1_16_R2());
        break;
      case VERSION1_16R1:
        plugin.setItemManager(new Item_v1_16_R1());
        plugin.setChatManager(new Chat_v1_16_R1());
        plugin.setAdvancementManager(new Advancement_v1_16_R1());
        break;
      case VERSION1_15R1:
        plugin.setItemManager(new Item_v1_15_R1());
        plugin.setChatManager(new Chat_v1_15_R1());
        plugin.setAdvancementManager(new Advancement_v1_15_R1());
        break;
      case VERSION1_14R1:
        plugin.setItemManager(new Item_v1_14_R1());
        plugin.setChatManager(new Chat_v1_14_R1());
        plugin.setAdvancementManager(new Advancement_v1_14_R1());
        break;
    }
  }

  public boolean isSupportedVersion() {
    return serverVersion.isSupported();
  }

  public ServerVersion getServerVersion() {
    return serverVersion;
  }
}
