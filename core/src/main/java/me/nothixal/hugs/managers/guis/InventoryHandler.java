package me.nothixal.hugs.managers.guis;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;

public interface InventoryHandler {

  void onOpen(InventoryOpenEvent event);

  void onClick(InventoryClickEvent event);

  void onClose(InventoryCloseEvent event);

}
