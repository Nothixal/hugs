package me.nothixal.hugs.managers.data.types.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.preferences.HuggableState;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.enums.preferences.ParticleQuality;
import me.nothixal.hugs.enums.types.DatabaseType;
import me.nothixal.hugs.enums.types.HugType;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.utils.Indicator;
import me.nothixal.hugs.utils.Interaction;
import me.nothixal.hugs.utils.cooldowns.Cooldown;

public class MySQLPlayerDataManager implements PlayerDataManager {

  private final HugsPlugin plugin;
  private final MySQL mySQL;

  private final String PLAYER_EXISTS = "SELECT uuid FROM player_data WHERE uuid=? LIMIT 1";
  private final String CREATE_PLAYER = "";
  private final String DELETE_PLAYER = "DELETE FROM users WHERE uuid=?";

  private final Map<UUID, MySQLPlayerData> dataCache = new HashMap<>();

  public MySQLPlayerDataManager(HugsPlugin plugin) {
    this.plugin = plugin;
    mySQL = new MySQL(plugin);
  }

  @Override
  public boolean playerExists(UUID uuid) {
    String query = "SELECT * FROM users WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.executeQuery();

      ResultSet resultSet = stmt.getResultSet();

      if (resultSet.next()) {
        return true;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return false;
  }

  @Override
  public void createPlayer(UUID uuid) {
    getPlayerData(uuid).createPlayer();
  }

  @Override
  public void deletePlayer(UUID uuid) {
    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(DELETE_PLAYER)
    ) {
      statement.setString(1, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void reloadPlayer(UUID uuid) {

  }

  @Override
  public void setDefaults(UUID uuid) {

  }

  @Override
  public void loadPlayer(UUID uuid) {
    MySQLPlayerData playerData = new MySQLPlayerData(mySQL, uuid);

    dataCache.put(uuid, playerData);
  }

  @Override
  public void loadDatabase() {
    mySQL.load();
  }

  @Override
  public void reloadDatabase() {

  }

  @Override
  public void saveDatabase() {

  }

  @Override
  public void purgeDatabase() {

  }

  @Override
  public void loadData(UUID uuid) {

  }

  @Override
  public void saveData(UUID uuid) {

  }

  @Override
  public void reloadData(UUID uuid) {

  }

  @Override
  public void purgeData(UUID uuid) {

  }

  @Override
  public void addHugs(UUID sender, UUID receiver, HugType type, long timestamp) {
    String query = "";
//    String query = "INSERT INTO hugs (giver, receiver, type, timestamp) VALUES (?, ?, ?, ?);";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, sender.toString());

      if (receiver == null) {
        statement.setString(2, null);
      } else {
        statement.setString(2, receiver.toString());
      }

      statement.setString(3, type.name().toLowerCase());
      statement.setLong(4, timestamp);
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public int getSelfHugs(UUID uuid) {
    String query = "SELECT COUNT(*) AS self_hugs FROM interactions WHERE type='self' AND giver=?";
//    String query = "SELECT self_hugs FROM player_data WHERE uuid=?";
    int result = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        result = results.getInt("self_hugs");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setSelfHugs(UUID uuid, int amount) {
    String query = "INSERT INTO interactions (giver, type, timestamp) VALUES (?, ?, ?);";
    String q = "INSERT INTO hugs_interactions (interaction_id, receiver) VALUES (?, ?);";
//    String query = "UPDATE player_data SET self_hugs=? WHERE uuid=?";

    long timestamp = System.currentTimeMillis();
    int id = -1;
    //TODO: Verify this is accurate.
    String s = "SELECT LAST_INSERT_ID();";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query);
        PreparedStatement statement1 = connection.prepareStatement(s);
        PreparedStatement statement2 = connection.prepareStatement(q);
    ) {
      statement.setString(1, uuid.toString());
      statement.setString(2, HugType.SELF.name().toLowerCase());
      statement.setLong(3, timestamp);
      statement.executeUpdate();

      statement1.execute();
      ResultSet results = statement1.getResultSet();
      while (results.next()) {
        id = results.getInt(1);
      }

      statement2.setInt(1, id);
      statement2.setString(2, uuid.toString());
      statement2.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void incrementSelfHugs(UUID uuid) {
    setSelfHugs(uuid, getSelfHugs(uuid) + 1);
  }

  @Override
  public void incrementSelfHugs(UUID uuid, int amount) {
    setSelfHugs(uuid, getSelfHugs(uuid) + amount);
  }

  @Override
  public int getHugsGiven(UUID uuid) {
    String query = "SELECT COUNT(*) AS normal_hugs_given FROM interactions WHERE type='normal' AND giver=?";
//    String query = "SELECT normal_hugs_given FROM player_data WHERE uuid=?";
    int result = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        result = results.getInt("normal_hugs_given");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setHugsGiven(UUID uuid, int amount) {
    String query = "";
//    String query = "INSERT INTO hugs (giver) VALUES (?);";
//    String query = "UPDATE player_data SET normal_hugs_given=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setInt(1, amount);
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void incrementHugsGiven(UUID uuid) {
    setHugsGiven(uuid, getHugsGiven(uuid) + 1);
  }

  @Override
  public void incrementHugsGiven(UUID uuid, int amount) {
    setHugsGiven(uuid, getHugsGiven(uuid) + amount);
  }

  @Override
  public int getHugsReceived(UUID uuid) {
    String query =
        "SELECT interactions.interaction_id, giver, receiver, type, timestamp FROM interactions "
            + "INNER JOIN hugs_interactions hi on interactions.interaction_id = hi.interaction_id "
            + "WHERE type='normal' AND receiver=?;";
//        "SELECT COUNT(*) AS normal_hugs_received FROM interactions WHERE type='normal' AND receiver=?";
    int result = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();
      result = results.getFetchSize();

//      while (results.next()) {
//        result = results.getFetchSize();
//      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setHugsReceived(UUID uuid, int amount) {
    String query = "";
//    String query = "UPDATE player_data SET normal_hugs_recieved=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setInt(1, amount);
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void incrementHugsReceived(UUID uuid) {
    setHugsReceived(uuid, getHugsReceived(uuid) + 1);
  }

  @Override
  public void incrementHugsReceived(UUID uuid, int amount) {
    setHugsReceived(uuid, getHugsReceived(uuid) + amount);
  }

  @Override
  public int getMassHugsGiven(UUID uuid) {
    String query = "SELECT COUNT(*) AS mass_hugs_given FROM interactions WHERE type='mass' AND giver=?";
    int result = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        result = results.getInt("mass_hugs_given");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setMassHugsGiven(UUID uuid, int amount) {
    String query = "";
//    String query = "UPDATE player_data SET mass_hugs_given=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setInt(1, amount);
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void incrementMassHugsGiven(UUID uuid) {
    setMassHugsGiven(uuid, getMassHugsGiven(uuid) + 1);
  }

  @Override
  public void incrementMassHugsGiven(UUID uuid, int amount) {
    setMassHugsGiven(uuid, getMassHugsGiven(uuid) + amount);
  }

  @Override
  public int getMassHugsReceived(UUID uuid) {
    String query =
        "SELECT interactions.interaction_id, giver, receiver, type, timestamp FROM interactions "
            + "INNER JOIN hugs_interactions hi on interactions.interaction_id = hi.interaction_id "
            + "WHERE type='mass' AND receiver=?;";
//        "SELECT COUNT(*) AS mass_hugs_received FROM interactions WHERE type='mass' AND receiver=?";
    int result = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();
      result = results.getFetchSize();

//      while (results.next()) {
//        result = results.getInt("mass_hugs_received");
//      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setMassHugsReceived(UUID uuid, int amount) {
    String query = "";
//    String query = "UPDATE player_data SET mass_hugs_received=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setInt(1, amount);
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void incrementMassHugsReceived(UUID uuid) {
    setMassHugsReceived(uuid, getMassHugsReceived(uuid) + 1);
  }

  @Override
  public void incrementMassHugsReceived(UUID uuid, int amount) {
    setMassHugsReceived(uuid, getMassHugsReceived(uuid) + amount);
  }

  @Override
  public int getTotalSelfHugsGiven() {
    String query = "SELECT COUNT(*) AS total_self_hugs FROM interactions WHERE type='self'";
    int value = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        value = resultSet.getInt("total_self_hugs");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public int getTotalHugsGiven() {
    String query = "SELECT COUNT(*) AS total_normal_hugs FROM interactions WHERE type='normal'";
    int value = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        value = resultSet.getInt("total_normal_hugs");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public int getTotalMassHugsGiven() {
    String query = "SELECT COUNT(*) AS total_mass_hugs FROM interactions WHERE type='mass'";
    int value = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        value = resultSet.getInt("total_mass_hugs");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public HuggableState getHuggability(UUID uuid) {
    String query = "SELECT huggability FROM preferences WHERE uuid=?";
    HuggableState value = HuggableState.NONE;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, uuid.toString());
      statement.execute();

      ResultSet resultSet = statement.getResultSet();

      while (resultSet.next()) {
        value = HuggableState.valueOf(resultSet.getString("huggability").toUpperCase());
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public void setHuggability(UUID uuid, HuggableState state) {
    String query = "UPDATE preferences SET huggability=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, state.name().toLowerCase());
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public boolean wantsHugs(UUID uuid) {
    HuggableState state = getHuggability(uuid);
    return state != HuggableState.NONE;
  }

  @Override
  public boolean wantsRealisticHugs(UUID uuid) {
    HuggableState state = getHuggability(uuid);
    return state != HuggableState.REALISTIC;
  }

  @Override
  public boolean wantsParticles(UUID uuid) {
    ParticleQuality quality = getParticleQuality(uuid);
    return quality != ParticleQuality.NONE;
  }

  @Override
  public void setWantsParticles(UUID uuid, boolean bool) {
    String query = "";
//    String query = "UPDATE preferences SET wants_particles=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, bool);
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public String getParticleEffect(UUID uuid) {
    return "HEART";
  }

  @Override
  public void setParticleEffect(UUID uuid, String value) {
    // Not yet implemented.
  }

  @Override
  public ParticleQuality getParticleQuality(UUID uuid) {
    String query = "SELECT particle_quality FROM preferences_visuals WHERE uuid=?";
    ParticleQuality value = ParticleQuality.NONE;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, uuid.toString());
      statement.execute();

      ResultSet resultSet = statement.getResultSet();

      while (resultSet.next()) {
        value = ParticleQuality.valueOf(resultSet.getString("particle_quality").toUpperCase());
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public void setParticleQuality(UUID uuid, ParticleQuality state) {
    String query = "UPDATE preferences_visuals SET particle_quality=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, state.name().toLowerCase());
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void increaseParticleQuality(UUID uuid) {
    setParticleQuality(uuid, ParticleQuality.valueOf(getParticleQuality(uuid).getNext().name()));
  }

  @Override
  public void decreaseParticleQuality(UUID uuid) {
    setParticleQuality(uuid, ParticleQuality.valueOf(getParticleQuality(uuid).getPrevious().name()));
  }

  @Override
  public boolean wantsHugSounds(UUID uuid) {
    String query = "SELECT hug_sounds FROM preferences_sounds WHERE uuid=?";
    boolean result = false;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        result = results.getBoolean("hug_sounds");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setWantsHugSounds(UUID uuid, boolean bool) {
    String query = "UPDATE preferences_sounds SET hug_sounds=? WHERE uuid=?";
//    String query = "UPDATE preferences_sound SET hug_sounds=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, bool);
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public String getSoundEffect(UUID uuid) {
    return "ENTITY_CAT_PURR";
  }

  @Override
  public boolean wantsMenuSounds(UUID uuid) {
    String query = "SELECT menu_sounds FROM preferences_sounds WHERE uuid=?";
    boolean result = false;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        result = results.getBoolean("menu_sounds");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setWantsMenuSounds(UUID uuid, boolean bool) {
    String query = "UPDATE preferences_sounds SET menu_sounds=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, bool);
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public boolean wantsCommandSounds(UUID uuid) {
    String query = "SELECT command_sounds FROM preferences_sounds WHERE uuid=?";
    boolean result = false;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        result = results.getBoolean("command_sounds");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setWantsCommandSounds(UUID uuid, boolean bool) {
    String query = "UPDATE preferences_sounds SET command_sounds=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, bool);
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public boolean hasGivenHugsBefore(UUID uuid) {
    return getHugsGiven(uuid) != 0 || getMassHugsGiven(uuid) != 0;
  }

  @Override
  public boolean hasReceivedHugsBefore(UUID uuid) {
    return getHugsReceived(uuid) != 0 || getMassHugsReceived(uuid) != 0;
  }

  @Override
  public List<Indicator> getIndicators(UUID uuid) {
    List<Indicator> indicators = new ArrayList<>();
    indicators.add(new Indicator(1, IndicatorType.CHAT, getIndicator(uuid, IndicatorType.CHAT)));
    indicators.add(new Indicator(2, IndicatorType.TITLES, getIndicator(uuid, IndicatorType.TITLES)));
    indicators.add(new Indicator(3, IndicatorType.ACTIONBAR, getIndicator(uuid, IndicatorType.ACTIONBAR)));
    indicators.add(new Indicator(4, IndicatorType.BOSSBAR, getIndicator(uuid, IndicatorType.BOSSBAR)));
    indicators.add(new Indicator(5, IndicatorType.TOASTS, getIndicator(uuid, IndicatorType.TOASTS)));
    return indicators;
  }

  @Override
  public boolean getIndicator(UUID uuid, IndicatorType type) {
    String query = "SELECT " + type.getValue() + " FROM preferences_indicators WHERE uuid=?";
    boolean result = false;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        result = results.getBoolean(type.getValue());
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setIndicator(UUID uuid, IndicatorType type, boolean enabled) {
    String query = "UPDATE preferences_indicators SET " + type.getValue() + "=? WHERE uuid=?";

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, enabled);
      statement.setString(2, uuid.toString());
      statement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public DatabaseType getDatabaseType() {
    return DatabaseType.MYSQL;
  }

  @Override
  public long getFirstHugGivenTimestamp(UUID uuid) {
    String query =
        "SELECT giver, receiver, timestamp "
            + "FROM hugs_interactions JOIN hugs.interactions i on i.interaction_id = hugs_interactions.interaction_id "
            + "WHERE receiver=? "
            + "ORDER BY timestamp "
            + "ASC LIMIT 1;";
    long result = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        result = results.getLong("timestamp");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setFirstHugGivenTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public String getFirstHugGivenTo(UUID uuid) {
    return "None";
  }

  @Override
  public void setFirstHugGivenTo(UUID sender, UUID receiver) {

  }

  @Override
  public long getFirstHugReceivedTimestamp(UUID uuid) {
    String query =
        "SELECT giver, receiver, timestamp "
            + "FROM hugs_interactions JOIN hugs.interactions i on i.interaction_id = hugs_interactions.interaction_id "
            + "WHERE giver=? "
            + "ORDER BY timestamp "
            + "ASC LIMIT 1;";
    long result = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        result = results.getLong("timestamp");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public void setFirstHugReceivedTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public String getLastHugGivenTo(UUID uuid) {
    return "None";
  }

  @Override
  public void setLastHugGivenTo(UUID sender, UUID receiver) {

  }

  @Override
  public long getLastHugGivenTimestamp(UUID uuid) {
    return 0;
  }

  @Override
  public void setLastHugGivenTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public String getLastHugReceivedFrom(UUID uuid) {
    return "None";
  }

  @Override
  public void setLastHugReceivedFrom(UUID sender, UUID receiver) {

  }

  @Override
  public long getLastHugReceivedTimestamp(UUID uuid) {
    return 0;
  }

  @Override
  public void setLastHugReceivedTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public int getUniqueHugsGiven(UUID uuid) {
    String query = "SELECT COUNT(DISTINCT receiver) AS unique_hugs FROM interactions "
        + "INNER JOIN hugs_interactions hi on interactions.interaction_id = hi.interaction_id "
        + "WHERE giver=? AND type='normal';";
//    String query = "SELECT COUNT(*) AS unique_hugs FROM interactions WHERE giver=?";
    int value = 0;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, uuid.toString());
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        value = resultSet.getInt("unique_hugs");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public List<String> getUniqueHugsGivenAsList(UUID uuid) {
    return null;
  }

  @Override
  public void setUniqueHugsGiven(UUID sender, UUID receiver, int amount) {

  }

  @Override
  public int getUniqueHugsReceived(UUID uuid) {
    return 0;
  }

  @Override
  public List<String> getUniqueHugsReceivedAsList(UUID uuid) {
    return null;
  }

  @Override
  public void setUniqueHugsReceived(UUID sender, UUID receiver, int amount) {

  }

  @Override
  public void addUniqueHugGiven(UUID player, UUID receiver) {

  }

  @Override
  public List<Interaction> getRecentHugs(UUID player) {
    List<Interaction> interactions = new ArrayList<>();

    String query = "SELECT giver, receiver, type, timestamp FROM interactions "
        + "LEFT JOIN hugs_interactions hi on interactions.interaction_id = hi.interaction_id "
        + "WHERE giver=? /*AND type='normal'*/ "
        + "ORDER BY timestamp "
        + "DESC LIMIT 5;";
//    String query = "SELECT * FROM interactions WHERE giver=? ORDER BY timestamp DESC LIMIT 5";

    UUID sender;
    UUID receiver = null;
    HugType type;
    long timestamp;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, player.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        sender = UUID.fromString(results.getString("giver"));

        if (results.getString("receiver") != null) {
          receiver = UUID.fromString(results.getString("receiver"));
        }

        type = HugType.valueOf(results.getString("type").toUpperCase());
        timestamp = results.getLong("timestamp");

        interactions.add(new Interaction(sender, receiver, type, timestamp));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return interactions;
  }

  @Override
  public List<Interaction> getRecentHugs(UUID player, boolean allStats) {
    List<Interaction> interactions = new ArrayList<>();

    String query;

    if (allStats) {
      query =
          "SELECT giver, receiver, type, timestamp FROM interactions "
          + "LEFT JOIN hugs_interactions hi on interactions.interaction_id = hi.interaction_id "
          + "WHERE giver=? /*AND type='normal'*/ "
          + "ORDER BY timestamp "
          + "DESC LIMIT 5;";
    } else {
      query =
          "SELECT giver, receiver, type, timestamp FROM interactions "
          + "LEFT JOIN hugs_interactions hi on interactions.interaction_id = hi.interaction_id "
          + "WHERE giver=? AND type='normal' "
          + "ORDER BY timestamp "
          + "DESC LIMIT 5;";
    }
//    String query = "SELECT * FROM interactions WHERE giver=? ORDER BY timestamp DESC LIMIT 5";

    UUID sender;
    UUID receiver = null;
    HugType type;
    long timestamp;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, player.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        sender = UUID.fromString(results.getString("giver"));

        if (results.getString("receiver") != null) {
          receiver = UUID.fromString(results.getString("receiver"));
        }

        type = HugType.valueOf(results.getString("type").toUpperCase());
        timestamp = results.getLong("timestamp");

        interactions.add(new Interaction(sender, receiver, type, timestamp));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return interactions;
  }

  @Override
  public void addRecentHugGiven(UUID sender, UUID receiver, HugType type, long timestamp) {

  }

  @Override
  public void setPrefersMenus(UUID uuid, boolean value) {

  }

  @Override
  public List<Cooldown> getCooldowns(UUID player) {
//    List<Interaction> recentHugs = getRecentHugs(player);

    return null;
  }

  @Override
  public Cooldown getCooldown(UUID player, UUID target) {
    return null;
  }

  @Override
  public void addCooldown(UUID player, UUID target, HugType type, long duration, long start) {

  }

  @Override
  public void removeCooldown(UUID player, UUID target) {

  }

  @Override
  public boolean prefersMenus(UUID uuid) {
    String query = "SELECT prefers_menus FROM preferences WHERE uuid=?";
    boolean result = false;

    try (Connection connection = mySQL.getSQLConnection();
        PreparedStatement stmt = connection.prepareStatement(query)
    ) {
      stmt.setString(1, uuid.toString());
      stmt.execute();

      ResultSet results = stmt.getResultSet();

      while (results.next()) {
        result = results.getBoolean("prefers_menus");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public String getFirstHugReceivedFrom(UUID uuid) {
    return null;
  }

  @Override
  public void setFirstHugReceivedFrom(UUID sender, UUID receiver) {

  }

  private MySQLPlayerData getPlayerData(UUID uuid) {
    return dataCache.computeIfAbsent(uuid, k -> new MySQLPlayerData(mySQL, k));
  }
}