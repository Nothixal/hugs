package me.nothixal.hugs.managers.data;

import java.util.List;
import java.util.UUID;
import me.nothixal.hugs.enums.preferences.HuggableState;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.enums.preferences.ParticleQuality;
import me.nothixal.hugs.enums.types.DatabaseType;
import me.nothixal.hugs.enums.types.HugType;
import me.nothixal.hugs.utils.Indicator;
import me.nothixal.hugs.utils.Interaction;
import me.nothixal.hugs.utils.cooldowns.Cooldown;

public interface PlayerDataManager {

  /**
   * Returns whether the player exists in the database.
   * */
  boolean playerExists(UUID uuid);

  /**
   * Create a database entry for the player.
   * */
  void createPlayer(UUID uuid);

  /**
   * Remove the database entry for the player.
   * */
  void deletePlayer(UUID uuid);

  /*
   * Load player's from the database into cache.
   * */
  void loadPlayer(UUID uuid);

  /*
  * Reload a player's data from the database.
  * */
  void reloadPlayer(UUID uuid);

  void setDefaults(UUID uuid);

  void loadDatabase();

  void reloadDatabase();

  void saveDatabase();

  void purgeDatabase();

  void loadData(UUID uuid);

  void saveData(UUID uuid);

  void reloadData(UUID uuid);

  void purgeData(UUID uuid);

  //
  // New way to store hugs. TEST
  //

  void addHugs(UUID sender, UUID receiver, HugType type, long timestamp);

  //
  // Self Hugs
  //

  int getSelfHugs(UUID uuid);

  void setSelfHugs(UUID uuid, int value);

  void incrementSelfHugs(UUID uuid);

  void incrementSelfHugs(UUID uuid, int amount);

  //
  // Normal Hugs
  //

  /**
  * Get the amount of normal hugs a player has given.
  * */
  int getHugsGiven(UUID uuid);

  /**
   * Set the amount of normal hugs a player has given.
   * */
  void setHugsGiven(UUID uuid, int value);

  /**
   * Increment the amount of normal hugs a player has given by 1.
   * */
  void incrementHugsGiven(UUID uuid);

  /**
   * Increment the amount of normal hugs a player has given.
   * @param amount The amount to increment by.
   * */
  void incrementHugsGiven(UUID uuid, int amount);

  /**
   * Get the amount of normal hugs a player has received.
   * */
  int getHugsReceived(UUID uuid);

  /**
   * Set the amount of normal hugs a player has received.
   * */
  void setHugsReceived(UUID uuid, int value);

  /**
   * Increment the amount of normal hugs a player has received by 1.
   * */
  void incrementHugsReceived(UUID uuid);

  /**
   * Increment the amount of normal hugs a player has received.
   * @param amount The amount to increment by.
   * */
  void incrementHugsReceived(UUID uuid, int amount);

  //
  // Mass Hugs
  //

  /**
   * Get the amount of mass hugs a player has given.
   * */
  int getMassHugsGiven(UUID uuid);

  /**
   * Set the amount of mass hugs a player has given.
   * */
  void setMassHugsGiven(UUID uuid, int value);

  /**
   * Increment the amount of mass hugs a player has given by 1.
   * */
  void incrementMassHugsGiven(UUID uuid);

  /**
   * Increment the amount of mass hugs a player has given.
   * @param amount The amount to increment by.
   * */
  void incrementMassHugsGiven(UUID uuid, int amount);

  /**
   * Get the amount of mass hugs a player has received.
   * */
  int getMassHugsReceived(UUID uuid);

  /**
   * Set the amount of mass hugs a player has received.
   * */
  void setMassHugsReceived(UUID uuid, int value);

  /**
   * Increment the amount of mass hugs a player has received by 1.
   * */
  void incrementMassHugsReceived(UUID uuid);

  /**
   * Increment the amount of mass hugs a player has received.
   * @param amount The amount to increment by.
   * */
  void incrementMassHugsReceived(UUID uuid, int amount);





  /**
   * Get the total amount of self hugs given by all players.
   * */
  int getTotalSelfHugsGiven();

  /**
   * Get the total amount of hugs given by all players.
   * */
  int getTotalHugsGiven();

  /**
   * Get the total amount of hugs received by all players.
   * */
  int getTotalMassHugsGiven();


  //
  // New Huggability Status settings.
  //
  HuggableState getHuggability(UUID uuid);

  void setHuggability(UUID uuid, HuggableState state);

  /**
   * Check to see if player wants to accept hugs from anyone.
   * */
  boolean wantsHugs(UUID uuid);

  /**
   * Check to see if player wants to accept hugs from anyone in proximity.
   * */
  boolean wantsRealisticHugs(UUID uuid);

  //
  // Old particle settings.
  //
  @Deprecated
  boolean wantsParticles(UUID uuid);

  @Deprecated
  void setWantsParticles(UUID uuid, boolean bool);

  /**
   * Check to see if player wants to receive particle effects when getting hugged.
   * */
  String getParticleEffect(UUID uuid);

  void setParticleEffect(UUID uuid, String value);


  // Particle Quality Settings
  ParticleQuality getParticleQuality(UUID uuid);

  void setParticleQuality(UUID uuid, ParticleQuality state);

  void increaseParticleQuality(UUID uuid);

  void decreaseParticleQuality(UUID uuid);

  /**
   * Check to see if player wants to hear sounds when getting hugged.
   * */
  boolean wantsHugSounds(UUID uuid);

  void setWantsHugSounds(UUID uuid, boolean bool);



  String getSoundEffect(UUID uuid);

  boolean wantsMenuSounds(UUID uuid);

  void setWantsMenuSounds(UUID uuid, boolean bool);

  boolean wantsCommandSounds(UUID uuid);

  void setWantsCommandSounds(UUID uuid, boolean bool);


  /**
   * Check to see if the player has given any form of hugs before.
   * */
  boolean hasGivenHugsBefore(UUID uuid);

  /**
   * Check to see if the player has received any form of hugs before.
   * */
  boolean hasReceivedHugsBefore(UUID uuid);

  List<Indicator> getIndicators(UUID uuid);

  boolean getIndicator(UUID uuid, IndicatorType type);

  void setIndicator(UUID uuid, IndicatorType type, boolean enabled);

  DatabaseType getDatabaseType();

  //
  // First Hug Sections
  //
  String getFirstHugGivenTo(UUID uuid);

  void setFirstHugGivenTo(UUID sender, UUID receiver);

  long getFirstHugGivenTimestamp(UUID uuid);

  void setFirstHugGivenTimestamp(UUID uuid, long timestamp);


  String getFirstHugReceivedFrom(UUID uuid);

  void setFirstHugReceivedFrom(UUID sender, UUID receiver);

  long getFirstHugReceivedTimestamp(UUID uuid);

  void setFirstHugReceivedTimestamp(UUID uuid, long timestamp);

  //
  // Last Hug Sections
  //

  String getLastHugGivenTo(UUID uuid);

  void setLastHugGivenTo(UUID sender, UUID receiver);

  long getLastHugGivenTimestamp(UUID uuid);

  void setLastHugGivenTimestamp(UUID uuid, long timestamp);


  String getLastHugReceivedFrom(UUID uuid);

  void setLastHugReceivedFrom(UUID sender, UUID receiver);

  long getLastHugReceivedTimestamp(UUID uuid);

  void setLastHugReceivedTimestamp(UUID uuid, long timestamp);

  //
  // Unique Hugs Section
  //
  int getUniqueHugsGiven(UUID uuid);

  List<String> getUniqueHugsGivenAsList(UUID uuid);

  void setUniqueHugsGiven(UUID sender, UUID receiver, int amount);

  int getUniqueHugsReceived(UUID uuid);

  List<String> getUniqueHugsReceivedAsList(UUID uuid);

  void setUniqueHugsReceived(UUID sender, UUID receiver, int amount);

  void addUniqueHugGiven(UUID player, UUID receiver);

  //
  // Recent Hugs Section
  //
  List<Interaction> getRecentHugs(UUID player);

  List<Interaction> getRecentHugs(UUID player, boolean allStats);

  void addRecentHugGiven(UUID sender, UUID receiver, HugType type, long timestamp);

  boolean prefersMenus(UUID uuid);

  void setPrefersMenus(UUID uuid, boolean value);

  //
  // Cooldowns
  //
  List<Cooldown> getCooldowns(UUID player);

  Cooldown getCooldown(UUID player, UUID target);

  void addCooldown(UUID player, UUID target, HugType type, long duration, long start);

  void removeCooldown(UUID player, UUID target);
}