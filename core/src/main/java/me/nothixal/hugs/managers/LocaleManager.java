package me.nothixal.hugs.managers;

import java.io.File;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.utils.config.ConfigFile;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class LocaleManager {

  private final HugsPlugin plugin;

  private ConfigFile locale;

  public LocaleManager(HugsPlugin plugin) {
    this.plugin = plugin;
    setup();
  }

  private void setup() {
    File file = new File(plugin.getFileManager().getLocaleDirectory(), "locale_en.yml");
    this.locale = new ConfigFile(file, YamlConfiguration.loadConfiguration(file));
  }

  public void load(String language) {
    File file = new File(plugin.getFileManager().getLocaleDirectory(), language);
    this.locale = new ConfigFile(file, YamlConfiguration.loadConfiguration(file));
  }

  public void loadFromFileExtension(String language) {
    File file = new File(plugin.getFileManager().getLocaleDirectory(), "locale_" + language + ".yml");
    this.locale = new ConfigFile(file, YamlConfiguration.loadConfiguration(file));
  }

  public File getLocaleFile() {
    return locale.getConfigFile();
  }

  public FileConfiguration getLangFile() {
    return locale.getConfig();
  }
}
