package me.nothixal.hugs.managers;

import com.cryptomorin.xseries.XSound;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Cooldowns;
import me.nothixal.hugs.enums.configuration.Errors;
import me.nothixal.hugs.enums.configuration.Overrides;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.enums.types.HugType;
import me.nothixal.hugs.utils.PlayerUtils;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.cooldowns.CooldownManager;
import me.nothixal.hugs.utils.cooldowns.CooldownOLD;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class HugManager {

  private final HugsPlugin plugin;

  private Map<Player, Boolean> invinciblePlayers = new HashMap<>();

  public HugManager(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  public void giveSelfHug(Player player) {
    CooldownManager cooldownManager = plugin.getCooldownManager();
    String cooldownError = Errors.COOLDOWN_MESSAGE.getLangValue();

    if (cooldownManager.isOnCooldown(player.getUniqueId(), player.getUniqueId())) {
      player.sendMessage(ChatUtils.colorChat(cooldownError.replace("%cooldown%",
          cooldownManager.getTimeLeft(player, player))));

      if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
        PlayerUtils.playSound(player, XSound.ENTITY_VILLAGER_NO);
      }
      return;
    }

    // Check to see if the receiver is huggable.
    if (!plugin.getPlayerDataManager().wantsHugs(player.getUniqueId())) {
      player.sendMessage(ChatUtils.colorChat(Errors.SELF_NOT_HUGGABLE.getLangValue()));

      if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
        PlayerUtils.playSound(player, XSound.ENTITY_VILLAGER_NO);
      }
      return;
    }

    // Player Indicator Preferences
    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.CHAT)) {
      PlayerUtils.sendSelfHugChatMessage(player);
    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.ACTIONBAR)) {
      PlayerUtils.sendSelfHugActionBar(player);
    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.BOSSBAR)) {
      PlayerUtils.sendSelfHugBossbar(player);
    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TITLES)) {
      PlayerUtils.sendSelfHugTitle(player);
    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.TOASTS)) {
      PlayerUtils.sendSelfHugToast(player);
    }

    if (plugin.getPlayerDataManager().wantsHugSounds(player.getUniqueId())) {
      PlayerUtils.playSound(player);
    }

    PlayerUtils.sendParticles(player);

    // Broadcasts
    if (plugin.getConfig().getBoolean(Settings.BROADCAST_SELF_HUGS.getValue())) {
      PlayerUtils.broadcastSelfHug(player, player);
    }

    // Generic Hug Stuff
    doHugExtras(player);

    plugin.getPlayerDataManager().setLastHugGivenTo(player.getUniqueId(), player.getUniqueId());
    plugin.getPlayerDataManager().setLastHugGivenTimestamp(player.getUniqueId(), System.currentTimeMillis());

    plugin.getPlayerDataManager().addRecentHugGiven(player.getUniqueId(), player.getUniqueId(), HugType.SELF, System.currentTimeMillis());
    plugin.getPlayerDataManager().incrementSelfHugs(player.getUniqueId());

    if (!player.isOp() && !player.hasPermission(Permissions.BYPASS_SELF.getPermissionNode())) {
//      cooldownManager.addCooldown(player, player, plugin.getConfig().getLong(Settings.SELF_HUG_COOLDOWN.getValue(), 15));
      cooldownManager.addCooldown(player, player, HugType.SELF, Cooldowns.SELF_HUG_COOLDOWN);
    }

    plugin.getDataFile().set("total_self_hugs_given", plugin.getDataFile().getInt("total_self_hugs_given") + 1);
    plugin.saveData();
  }

  public void giveNormalHug(Player player, Player receiver) {
    // Cooldown Check
    CooldownManager cooldownManager = plugin.getCooldownManager();
    String normalCooldown = Cooldowns.NORMAL_HUG_COOLDOWN.getCooldownName();
    String cooldownError = Errors.COOLDOWN_MESSAGE.getLangValue();

    if (cooldownManager.isOnCooldown(player.getUniqueId(), receiver.getUniqueId())) {
      player.sendMessage(ChatUtils.colorChat(cooldownError.replace("%cooldown%",
          cooldownManager.getTimeLeft(player, receiver))));

      if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
        PlayerUtils.playSound(player, XSound.ENTITY_VILLAGER_NO);
      }
      return;
    }

    // TODO: Verify this works properly and or is even needed for this check.
    // Check to see if the receiver wants realistic hugs.
    if (plugin.getPlayerDataManager().wantsRealisticHugs(receiver.getUniqueId())) {
      player.sendMessage(ChatUtils.colorChat("&cThis player wants to be hugged in person!"));
      return;
    }

    // Check to see if the receiver is huggable.
    if (!plugin.getPlayerDataManager().wantsHugs(receiver.getUniqueId())) {
      player.sendMessage(
          ChatUtils.colorChat(Errors.PLAYER_NOT_HUGGABLE.getLangValue().replace("%target%", receiver.getName())));

      if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
        PlayerUtils.playSound(player, XSound.ENTITY_VILLAGER_NO);
      }
      return;
    }

    //sendChatMessageToSender(player, receiver);

    if (plugin.getConfig().getBoolean(Settings.OVERRIDES.getValue())) {
      sendOverriddenHug(player, receiver);
      return;
    }

    // Player Indicator Preferences
    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), IndicatorType.CHAT)) {
      PlayerUtils.sendChatMessageToSender(player, receiver);
    }

    if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), IndicatorType.CHAT)) {
      PlayerUtils.sendChatMessageToReceiver(player, receiver);
    }

    if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), IndicatorType.ACTIONBAR)) {
      PlayerUtils.sendActionBar(player, receiver);
    }

    if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), IndicatorType.BOSSBAR)) {
      PlayerUtils.sendBossbar(player, receiver);
    }

    if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), IndicatorType.TITLES)) {
      PlayerUtils.sendNormalHugTitle(player, receiver);
    }

    if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), IndicatorType.TOASTS)) {
      PlayerUtils.sendToast(player, receiver);
    }

    if (plugin.getPlayerDataManager().wantsHugSounds(receiver.getUniqueId())) {
      PlayerUtils.playSound(receiver);
    }

    PlayerUtils.sendParticles(receiver);

    // Broadcasts
    if (plugin.getConfig().getBoolean(Settings.BROADCAST_NORMAL_HUGS.getValue())) {
      PlayerUtils.broadcastHug(player, receiver);
    }

    doHugExtras(receiver);

    if (plugin.getConfig().getBoolean(Settings.DEVELOPER_REWARD.getValue())) {
      PlayerUtils.checkDeveloper(player, receiver);
    }

    // Check for first hug given.
    if (plugin.getPlayerDataManager().getFirstHugGivenTo(player.getUniqueId()) == null ||
        plugin.getPlayerDataManager().getFirstHugGivenTo(player.getUniqueId()).equals("None")) {
      plugin.getPlayerDataManager().setFirstHugGivenTo(player.getUniqueId(), receiver.getUniqueId());
      plugin.getPlayerDataManager().setFirstHugGivenTimestamp(player.getUniqueId(), System.currentTimeMillis());
    }

    // Check for last hug given.
//    if (dataManager.getLastHugGivenTo(player.getUniqueId()) == null ||
//        dataManager.getLastHugGivenTo(player.getUniqueId()).equals("None")) {
    plugin.getPlayerDataManager().setLastHugGivenTo(player.getUniqueId(), receiver.getUniqueId());
    plugin.getPlayerDataManager().setLastHugGivenTimestamp(player.getUniqueId(), System.currentTimeMillis());
//    }

    // Check if hug is unique.
    if (!plugin.getPlayerDataManager().getUniqueHugsGivenAsList(player.getUniqueId()).contains(receiver.getUniqueId().toString())) {
      plugin.getPlayerDataManager().addUniqueHugGiven(player.getUniqueId(), receiver.getUniqueId());
    }

    plugin.getPlayerDataManager().incrementHugsGiven(player.getUniqueId());
    plugin.getPlayerDataManager().incrementHugsReceived(receiver.getUniqueId());

    plugin.getPlayerDataManager().addRecentHugGiven(player.getUniqueId(), receiver.getUniqueId(), HugType.NORMAL, System.currentTimeMillis());

//    dataManager.addHugs(player.getUniqueId(), receiver.getUniqueId(), HugType.NORMAL, System.currentTimeMillis());

    if (!player.isOp() && !player.hasPermission(Permissions.BYPASS_NORMAL.getPermissionNode())) {
//      cooldownManager.addCooldown(player, receiver, plugin.getConfig().getLong(Settings.NORMAL_HUG_COOLDOWN.getValue(), 15));
      cooldownManager.addCooldown(player, receiver, HugType.NORMAL, Cooldowns.NORMAL_HUG_COOLDOWN);
//      giveCooldown(player, normalCooldown, plugin.getConfig().getLong(Settings.NORMAL_HUG_COOLDOWN.getValue()));
    }

    plugin.getDataFile().set("total_hugs_given", plugin.getDataFile().getInt("total_hugs_given") + 1);
    plugin.saveData();
  }

  public void giveMassHug(Player player) {
    CooldownManager cooldownManager = plugin.getCooldownManager();
    String cooldownError = Errors.COOLDOWN_MESSAGE.getLangValue();

    if (cooldownManager.isOnCooldown(player.getUniqueId(), player.getUniqueId())) {
      player.sendMessage(ChatUtils.colorChat(cooldownError.replace("%cooldown%",
          cooldownManager.getTimeLeft(player, player))));

      if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
        PlayerUtils.playSound(player, XSound.ENTITY_VILLAGER_NO);
      }
      return;
    }

    List<Player> onlinePlayers = new ArrayList<>(Bukkit.getOnlinePlayers());
    onlinePlayers.remove(player);

    for (Player receiver : onlinePlayers) {
      // Player Indicator Settings
      if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), IndicatorType.CHAT)) {
        PlayerUtils.sendChatMessageToReceiver(player, receiver);
      }

      if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), IndicatorType.TITLES)) {
        PlayerUtils.sendMassHugTitle(player, receiver);
      }

      if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), IndicatorType.ACTIONBAR)) {
        PlayerUtils.sendMassActionBar(player, receiver);
      }

      if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), IndicatorType.BOSSBAR)) {
        PlayerUtils.sendMassHugBossbar(player, receiver);
      }

      if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), IndicatorType.TOASTS)) {
        PlayerUtils.sendMassToast(player, receiver);
      }

      if (plugin.getPlayerDataManager().wantsHugSounds(receiver.getUniqueId())) {
        PlayerUtils.playSound(player);
      }

      PlayerUtils.sendParticles(receiver);

      // Generic Hug Stuff
      if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HEALTH.getValue())) {
        PlayerUtils.restoreHealth(receiver);
      }

      if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HUNGER.getValue())) {
        PlayerUtils.restoreHunger(receiver);
      }

//      if (plugin.getConfig().getBoolean(Settings.DEVELOPER_REWARD.getValue())) {
//        PlayerUtils.checkDeveloper(player, receiver);
//      }

      plugin.getPlayerDataManager().incrementMassHugsReceived(receiver.getUniqueId());
    }

    // Broadcasts
    if (plugin.getConfig().getBoolean(Settings.BROADCAST_MASS_HUGS.getValue())) {
      PlayerUtils.broadcastMassHug(player);
    }

    plugin.getPlayerDataManager().incrementMassHugsGiven(player.getUniqueId());

    if (!player.isOp() && !player.hasPermission(Permissions.BYPASS_MASS.getPermissionNode())) {
//      cooldownManager.addCooldown(player, player, plugin.getConfig().getLong(Settings.MASS_HUG_COOLDOWN.getValue(), 15));
      cooldownManager.addCooldown(player, player, HugType.MASS, Cooldowns.MASS_HUG_COOLDOWN);
    }

    plugin.getDataFile().set("total_mass_hugs_given", plugin.getDataFile().getInt("total_mass_hugs_given") + 1);
    plugin.saveData();
    //HugEvent hugEvent = new HugEvent(player, player, HugType.MASS);
    //Bukkit.getPluginManager().callEvent(hugEvent);
  }

  public void sendOverriddenHug(Player player, Player receiver) {
    FileConfiguration overridesConfig = plugin.getFileManager().getOverridesYMLData();
    CooldownManager cooldownManager = plugin.getCooldownManager();
    String cooldownError = Errors.COOLDOWN_MESSAGE.getLangValue();

//    System.out.println("Chat Messages:       " + overridesConfig.getBoolean(Overrides.CHAT_MESSAGES_OVERRIDDEN.getPath()));
//    System.out.println("Title Message:       " + overridesConfig.getBoolean(Overrides.TITLE_MESSAGES_OVERRIDDEN.getPath()));
//    System.out.println("Actionbar Messages:  " + overridesConfig.getBoolean(Overrides.ACTIONBAR_MESSAGES_OVERRIDDEN.getPath()));
//    System.out.println("Bossbar Messages:    " + overridesConfig.getBoolean(Overrides.BOSSBAR_MESSAGES_OVERRIDDEN.getPath()));
//    System.out.println("Toast Messages:      " + overridesConfig.getBoolean(Overrides.TOAST_MESSAGES_OVERRIDDEN.getPath()));
//    System.out.println("Particles:           " + overridesConfig.getBoolean(Overrides.PARTICLES_OVERRIDDEN.getPath()));
//    System.out.println("Sounds:              " + overridesConfig.getBoolean(Overrides.SOUNDS_OVERRIDDEN.getPath()));

    if (cooldownManager.isOnCooldown(player.getUniqueId(), receiver.getUniqueId())) {
      player.sendMessage(ChatUtils.colorChat(cooldownError.replace("%cooldown%",
          cooldownManager.getTimeLeft(player, receiver))));

      if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
        PlayerUtils.playSound(player, XSound.ENTITY_VILLAGER_NO);
      }
      return;
    }

    // Overridden Player Indicators
    if (overridesConfig.getBoolean(Overrides.CHAT_MESSAGES_OVERRIDDEN.getPath())) {
      PlayerUtils.sendChatMessageToSender(player, receiver);
      PlayerUtils.sendChatMessageToReceiver(player, receiver);
    }

    if (overridesConfig.getBoolean(Overrides.TITLE_MESSAGES_OVERRIDDEN.getPath())) {
      PlayerUtils.sendNormalHugTitle(player, receiver);
    }

    if (overridesConfig.getBoolean(Overrides.ACTIONBAR_MESSAGES_OVERRIDDEN.getPath())) {
      PlayerUtils.sendActionBar(player, receiver);
    }

    if (overridesConfig.getBoolean(Overrides.BOSSBAR_MESSAGES_OVERRIDDEN.getPath())) {
      PlayerUtils.sendBossbar(player, receiver);
    }

    if (overridesConfig.getBoolean(Overrides.TOAST_MESSAGES_OVERRIDDEN.getPath())) {
      PlayerUtils.sendToast(player, receiver);
    }

    // Overridden Player Particles
    if (overridesConfig.getBoolean(Overrides.PARTICLES_OVERRIDDEN.getPath())) {
      PlayerUtils.sendParticles(receiver);
    }

    if (overridesConfig.getBoolean(Overrides.HUG_SOUNDS_OVERRIDDEN.getPath())) {
      PlayerUtils.playSound(receiver);
    }

    // Broadcasts
    if (plugin.getConfig().getBoolean(Settings.BROADCAST_NORMAL_HUGS.getValue())) {
      PlayerUtils.broadcastHug(player, receiver);
    }

    // Generic Hug Stuff
    doHugExtras(receiver);

    if (plugin.getConfig().getBoolean(Settings.DEVELOPER_REWARD.getValue())) {
      PlayerUtils.checkDeveloper(player, receiver);
    }

    plugin.getPlayerDataManager().incrementHugsGiven(player.getUniqueId());
    plugin.getPlayerDataManager().incrementHugsReceived(receiver.getUniqueId());

    if (!player.isOp() && !player.hasPermission(Permissions.BYPASS_NORMAL.getPermissionNode())) {
//      cooldownManager.addCooldown(player, receiver, plugin.getConfig().getLong(Settings.NORMAL_HUG_COOLDOWN.getValue(), 15));
      cooldownManager.addCooldown(player, receiver, HugType.NORMAL, Cooldowns.NORMAL_HUG_COOLDOWN);
//      giveCooldown(player, normalCooldown, plugin.getConfig().getLong(Settings.NORMAL_HUG_COOLDOWN.getValue()));
    }

    plugin.getDataFile().set("total_hugs_given", plugin.getDataFile().getInt("total_hugs_given") + 1);
    plugin.saveData();
  }

//  public void giveShiftHug(Player player, Player receiver) {
//    // Cooldown Check
//    CooldownManager cooldownManager = plugin.getCooldownManager();
//    String shiftHugCooldown = Cooldowns.NORMAL_HUG_COOLDOWN.getCooldownName();
//    String cooldownError = Errors.COOLDOWN_MESSAGE.getLangValue();
//
//    if (cooldownManager.isOnCooldown(player.getUniqueId(), receiver.getUniqueId())) {
//      player.sendMessage(ChatUtils.colorChat(cooldownError.replace("%cooldown%",
//          cooldownManager.getTimeLeft(plugin, player, receiver))));
//
//      if (plugin.getPlayerDataManager().wantsCommandSounds(player.getUniqueId())) {
//        PlayerUtils.playSound(player, XSound.ENTITY_VILLAGER_NO);
//      }
//      return;
//    }
//
//    // Check to see if the receiver is huggable.
//    if (!dataManager.wantsHugs(receiver.getUniqueId()) && !dataManager.wantsRealisticHugs(receiver.getUniqueId())) {
//      player.sendMessage(
//          ChatUtils.colorChat(Errors.PLAYER_NOT_HUGGABLE.getLangValue().replace("%target%", receiver.getName())));
//
//      if (dataManager.wantsCommandSounds(player.getUniqueId())) {
//        PlayerUtils.playSound(player, XSound.ENTITY_VILLAGER_NO);
//      }
//      return;
//    }
//
//    //sendChatMessageToSender(player, receiver);
//
//    if (plugin.getConfig().getBoolean(Settings.OVERRIDES.getValue())) {
//      sendOverriddenHug(player, receiver);
//      return;
//    }
//
//    // Player Indicator Preferences
//    if (dataManager.getIndicator(player.getUniqueId(), IndicatorType.CHAT)) {
//      PlayerUtils.sendChatMessageToSender(player, receiver);
//    }
//
//    if (dataManager.getIndicator(receiver.getUniqueId(), IndicatorType.CHAT)) {
//      PlayerUtils.sendChatMessageToReceiver(player, receiver);
//    }
//
//    if (dataManager.getIndicator(receiver.getUniqueId(), IndicatorType.ACTIONBAR)) {
//      PlayerUtils.sendActionBar(player, receiver);
//    }
//
//    if (dataManager.getIndicator(receiver.getUniqueId(), IndicatorType.BOSSBAR)) {
//      PlayerUtils.sendBossbar(player, receiver);
//    }
//
//    if (dataManager.getIndicator(receiver.getUniqueId(), IndicatorType.TITLES)) {
//      PlayerUtils.sendNormalHugTitle(player, receiver);
//    }
//
//    if (dataManager.getIndicator(receiver.getUniqueId(), IndicatorType.TOASTS)) {
//      PlayerUtils.sendToast(player, receiver);
//    }
//
//    if (dataManager.wantsHugSounds(receiver.getUniqueId())) {
//      PlayerUtils.playSound(receiver);
//    }
//
//    PlayerUtils.sendParticles(receiver);
//
//    // Broadcasts
//    if (plugin.getConfig().getBoolean(Settings.BROADCAST_NORMAL_HUGS.getValue())) {
//      PlayerUtils.broadcastHug(player, receiver);
//    }
//
//    // Player Indicator Preferences
//    doHugExtras(receiver);
//
//    if (plugin.getConfig().getBoolean(Settings.DEVELOPER_REWARD.getValue())) {
//      PlayerUtils.checkDeveloper(player, receiver);
//    }
//
//    // Check for first hug given.
//    if (dataManager.getFirstHugGivenTo(player.getUniqueId()) == null ||
//        dataManager.getFirstHugGivenTo(player.getUniqueId()).equals("None")) {
//      dataManager.setFirstHugGivenTo(player.getUniqueId(), receiver.getUniqueId());
//      dataManager.setFirstHugGivenTimestamp(player.getUniqueId(), System.currentTimeMillis());
//    }
//
//    // Check for first hug received.
//
//    // Check if hug is unique.
//    if (!dataManager.getUniqueHugsGivenAsList(player.getUniqueId()).contains(receiver.getUniqueId().toString())) {
//      dataManager.addUniqueHugGiven(player.getUniqueId(), receiver.getUniqueId());
//    }
//
//    dataManager.incrementHugsGiven(player.getUniqueId());
//    dataManager.incrementHugsReceived(receiver.getUniqueId());
//
//    if (!player.isOp() && !player.hasPermission(Permissions.BYPASS_NORMAL.getPermissionNode())) {
//      cooldownManager.addCooldown(player, receiver, plugin.getConfig().getLong(Settings.NORMAL_HUG_COOLDOWN.getValue(), 15));
////      giveCooldown(player, shiftHugCooldown, plugin.getConfig().getLong(Settings.NORMAL_HUG_COOLDOWN.getValue()));
//    }
//
//    plugin.getDataFile().set("total_hugs_given", plugin.getDataFile().getInt("total_hugs_given") + 1);
//    plugin.saveData();
//  }

  private void doHugExtras(Player player) {
    if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HEALTH.getValue())) {
      PlayerUtils.restoreHealth(player);
    }

    if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HUNGER.getValue())) {
      PlayerUtils.restoreHunger(player);
    }

    if (plugin.getConfig().getBoolean(Settings.TEMPORARY_INVINCIBILITY_ENABLED.getValue())) {
      invinciblePlayers.put(player, false);
    }
  }

  private void giveCooldown(Player player, String cooldownName, long duration) {
    new CooldownOLD(player, cooldownName, duration).start();
  }

  public Map<Player, Boolean> getInvinciblePlayers() {
    return invinciblePlayers;
  }

  public void setInvinciblePlayers(Map<Player, Boolean> invinciblePlayers) {
    this.invinciblePlayers = invinciblePlayers;
  }
}
