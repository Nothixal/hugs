package me.nothixal.hugs.managers.data.types;

import java.util.List;
import java.util.UUID;
import me.nothixal.hugs.enums.preferences.HuggableState;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.enums.preferences.ParticleQuality;
import me.nothixal.hugs.enums.types.DatabaseType;
import me.nothixal.hugs.enums.types.HugType;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.utils.Indicator;
import me.nothixal.hugs.utils.Interaction;
import me.nothixal.hugs.utils.cooldowns.Cooldown;

public class H2PlayerDataManager implements PlayerDataManager {

  @Override
  public boolean playerExists(UUID uuid) {
    return false;
  }

  @Override
  public void createPlayer(UUID uuid) {

  }

  @Override
  public void deletePlayer(UUID uuid) {

  }

  @Override
  public void reloadPlayer(UUID uuid) {

  }

  @Override
  public void setDefaults(UUID uuid) {

  }

  @Override
  public void loadPlayer(UUID uuid) {

  }

  @Override
  public void loadDatabase() {

  }

  @Override
  public void reloadDatabase() {

  }

  @Override
  public void saveDatabase() {

  }

  @Override
  public void purgeDatabase() {

  }

  @Override
  public void loadData(UUID uuid) {

  }

  @Override
  public void saveData(UUID uuid) {

  }

  @Override
  public void reloadData(UUID uuid) {

  }

  @Override
  public void purgeData(UUID uuid) {

  }

  @Override
  public void addHugs(UUID sender, UUID receiver, HugType type, long timestamp) {

  }

  @Override
  public int getSelfHugs(UUID uuid) {
    return 0;
  }

  @Override
  public void setSelfHugs(UUID uuid, int value) {

  }

  @Override
  public void incrementSelfHugs(UUID uuid) {

  }

  @Override
  public void incrementSelfHugs(UUID uuid, int amount) {

  }

  @Override
  public int getHugsGiven(UUID uuid) {
    return 0;
  }

  @Override
  public void setHugsGiven(UUID uuid, int amount) {

  }

  @Override
  public void incrementHugsGiven(UUID uuid) {

  }

  @Override
  public void incrementHugsGiven(UUID uuid, int amount) {

  }

  @Override
  public int getHugsReceived(UUID uuid) {
    return 0;
  }

  @Override
  public void setHugsReceived(UUID uuid, int amount) {

  }

  @Override
  public void incrementHugsReceived(UUID uuid) {

  }

  @Override
  public void incrementHugsReceived(UUID uuid, int amount) {

  }

  @Override
  public int getMassHugsGiven(UUID uuid) {
    return 0;
  }

  @Override
  public void setMassHugsGiven(UUID uuid, int amount) {

  }

  @Override
  public void incrementMassHugsGiven(UUID uuid) {

  }

  @Override
  public void incrementMassHugsGiven(UUID uuid, int amount) {

  }

  @Override
  public int getMassHugsReceived(UUID uuid) {
    return 0;
  }

  @Override
  public void setMassHugsReceived(UUID uuid, int value) {

  }

  @Override
  public void incrementMassHugsReceived(UUID uuid) {

  }

  @Override
  public void incrementMassHugsReceived(UUID uuid, int amount) {

  }

  @Override
  public int getTotalSelfHugsGiven() {
    return 0;
  }

  @Override
  public int getTotalHugsGiven() {
    return 0;
  }

  @Override
  public int getTotalMassHugsGiven() {
    return 0;
  }

  @Override
  public HuggableState getHuggability(UUID uuid) {
    return null;
  }

  @Override
  public void setHuggability(UUID uuid, HuggableState state) {

  }

  @Override
  public boolean wantsHugs(UUID uuid) {
    return false;
  }

  @Override
  public boolean wantsRealisticHugs(UUID uuid) {
    return false;
  }

  @Override
  public boolean wantsParticles(UUID uuid) {
    return false;
  }

  @Override
  public void setWantsParticles(UUID uuid, boolean bool) {

  }

  @Override
  public String getParticleEffect(UUID uuid) {
    return null;
  }

  @Override
  public void setParticleEffect(UUID uuid, String value) {

  }

  @Override
  public ParticleQuality getParticleQuality(UUID uuid) {
    return null;
  }

  @Override
  public void setParticleQuality(UUID uuid, ParticleQuality state) {

  }

  @Override
  public void increaseParticleQuality(UUID uuid) {

  }

  @Override
  public void decreaseParticleQuality(UUID uuid) {

  }

  @Override
  public boolean wantsHugSounds(UUID uuid) {
    return false;
  }

  @Override
  public void setWantsHugSounds(UUID uuid, boolean bool) {

  }

  @Override
  public String getSoundEffect(UUID uuid) {
    return null;
  }

  @Override
  public boolean wantsMenuSounds(UUID uuid) {
    return false;
  }

  @Override
  public void setWantsMenuSounds(UUID uuid, boolean bool) {

  }

  @Override
  public boolean wantsCommandSounds(UUID uuid) {
    return false;
  }

  @Override
  public void setWantsCommandSounds(UUID uuid, boolean bool) {

  }

  @Override
  public boolean hasGivenHugsBefore(UUID uuid) {
    return false;
  }

  @Override
  public boolean hasReceivedHugsBefore(UUID uuid) {
    return false;
  }

  @Override
  public List<Indicator> getIndicators(UUID uuid) {
    return null;
  }

  @Override
  public boolean getIndicator(UUID uuid, IndicatorType type) {
    return false;
  }

  @Override
  public void setIndicator(UUID uuid, IndicatorType type, boolean enabled) {

  }

  @Override
  public DatabaseType getDatabaseType() {
    return DatabaseType.H2;
  }

  @Override
  public long getFirstHugGivenTimestamp(UUID uuid) {
    return 0;
  }

  @Override
  public void setFirstHugGivenTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public String getFirstHugGivenTo(UUID uuid) {
    return null;
  }

  @Override
  public void setFirstHugGivenTo(UUID sender, UUID receiver) {

  }

  @Override
  public long getFirstHugReceivedTimestamp(UUID uuid) {
    return 0;
  }

  @Override
  public void setFirstHugReceivedTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public String getLastHugGivenTo(UUID uuid) {
    return null;
  }

  @Override
  public void setLastHugGivenTo(UUID sender, UUID receiver) {

  }

  @Override
  public long getLastHugGivenTimestamp(UUID uuid) {
    return 0;
  }

  @Override
  public void setLastHugGivenTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public String getLastHugReceivedFrom(UUID uuid) {
    return null;
  }

  @Override
  public void setLastHugReceivedFrom(UUID sender, UUID receiver) {

  }

  @Override
  public long getLastHugReceivedTimestamp(UUID uuid) {
    return 0;
  }

  @Override
  public void setLastHugReceivedTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public int getUniqueHugsGiven(UUID uuid) {
    return 0;
  }

  @Override
  public List<String> getUniqueHugsGivenAsList(UUID uuid) {
    return null;
  }

  @Override
  public void setUniqueHugsGiven(UUID sender, UUID receiver, int amount) {

  }

  @Override
  public int getUniqueHugsReceived(UUID uuid) {
    return 0;
  }

  @Override
  public List<String> getUniqueHugsReceivedAsList(UUID uuid) {
    return null;
  }

  @Override
  public void setUniqueHugsReceived(UUID sender, UUID receiver, int amount) {

  }

  @Override
  public void addUniqueHugGiven(UUID player, UUID receiver) {

  }

  @Override
  public List<Interaction> getRecentHugs(UUID player) {
    return null;
  }

  @Override
  public List<Interaction> getRecentHugs(UUID player, boolean allStats) {
    return null;
  }

  @Override
  public void addRecentHugGiven(UUID sender, UUID receiver, HugType type, long timestamp) {

  }

  @Override
  public void setPrefersMenus(UUID uuid, boolean value) {

  }

  @Override
  public List<Cooldown> getCooldowns(UUID player) {
    return null;
  }

  @Override
  public Cooldown getCooldown(UUID player, UUID target) {
    return null;
  }

  @Override
  public void addCooldown(UUID player, UUID target, HugType type, long duration, long start) {

  }

  @Override
  public void removeCooldown(UUID player, UUID target) {

  }

  @Override
  public boolean prefersMenus(UUID uuid) {
    return false;
  }

  @Override
  public String getFirstHugReceivedFrom(UUID uuid) {
    return null;
  }

  @Override
  public void setFirstHugReceivedFrom(UUID sender, UUID receiver) {

  }
}
