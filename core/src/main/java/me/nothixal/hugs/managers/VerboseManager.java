package me.nothixal.hugs.managers;

import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.ServerVersion;
import me.nothixal.hugs.enums.Type;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.utils.GrammarFix;
import me.nothixal.hugs.utils.logger.LogUtils;
import org.bukkit.Particle;
import org.bukkit.Sound;

public class VerboseManager {

  private final HugsPlugin plugin;

  public VerboseManager(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  public void logVerbose(String message, Enum<Type> type) {
    if (plugin.getConfig().getBoolean(Settings.CONSOLE_VERBOSE_ENABLED.getValue())) {
      if (type == Type.INFO) {
        LogUtils.logInfo(message);
      }

      if (type == Type.WARNING) {
        LogUtils.logWarning(message);
      }

      if (type == Type.ERROR) {
        LogUtils.logError(message);
      }
    }

    if (plugin.getConfig().getBoolean(Settings.FILE_LOGGING_ENABLED.getValue())) {
      LogUtils.logToFile(message);
    }
  }

  public void logVerbose(String message) {
    logVerbose(message, Type.INFO);
  }

  public void logVerboseInfo(String message) {
    logVerbose(message, Type.INFO);
  }

  public void logVerboseWarning(String message) {
    logVerbose(message, Type.WARNING);
  }

  public void logVerboseError(String message) {
    logVerbose(message, Type.ERROR);
  }

  /*
  * -={ TEST }=-
  * -=[ TEST ]=-
  * -=( TEST )=-
  * -=< TEST >=-
  * -=/ TEST \=-
  * -=\ TEST /=-
  * -=| TEST |=-
  * -=+ TEST +=-
  * */

  /*
   * ------------------
   * Light verbose
   * ------------------
   * Restore Health
   * Restore Hunger
   * Chat Messages
   * Titles
   * Particles
   * Sounds
   * Cooldowns
   * Verbose types
   */

  public void sendLightConfigSettings() {
    LogUtils.logInfo("-=( Plugin Configuration )=-");
//    LogUtils.logInfo("-=( Configuration &8(&7Basic Settings&8)&r )=-");

    String allowSelfHug = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.ALLOW_SELF_HUG.getValue()));
    String allowShiftHug = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.ALLOW_SHIFT_HUG.getValue()));
    String restoreHealth = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HEALTH.getValue()));
    String restoreHunger = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HUNGER.getValue()));

    LogUtils.logInfo("Basic Features:");
    LogUtils.logInfo("  Allow Self Hugs: " + allowSelfHug + " | " + "Allow Shift Hugs: " + allowShiftHug);
    LogUtils.logInfo("  Restore Health: " + restoreHealth + " | " + "Restore Hunger: " + restoreHunger);

    String consoleVerboseEnabled = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.CONSOLE_VERBOSE_ENABLED.getValue()));
    String fileVerboseEnabled = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.FILE_LOGGING_ENABLED.getValue()));
    LogUtils.logInfo("Verbose:");
    LogUtils.logInfo("  Console verbose: " + consoleVerboseEnabled + " | " + "File verbose: " + fileVerboseEnabled);

    String passiveModeEnabled= checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.PASSIVE_MODE_ENABLED.getValue()));
    String tempInvincibilityEnabled = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.TEMPORARY_INVINCIBILITY_ENABLED.getValue()));
    String realisticHugs = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.REALISTIC_HUGS.getValue()));
    LogUtils.logInfo("Experimental Features:");
    LogUtils.logInfo("  Passive Mode: " + passiveModeEnabled);
    LogUtils.logInfo("  Temporary Invincibility: " + tempInvincibilityEnabled);
    LogUtils.logInfo("  Realistic Hugs: " + realisticHugs);
    LogUtils.logInfo(" ");
  }

  /*
   * ------------------
   * Full verbose
   * ------------------
   * Check For Updates
   * Lang File
   * Force Exact Names
   * Self Hugging
   * Shift Hugging
   * Restore Health
   * Restore Hunger
   * Chat Messages
   * Broadcasts
   * Titles
   * Particles
   * Sounds
   * Cooldowns
   * Verbose Types
   * Temporary Invincibility
   * Passive Mode
   */

  public void sendFullConfigSettings() {
    LogUtils.logInfo("-=( Plugin Configuration )=-");
//    LogUtils.logInfo("-=( Configuration )=- &8(&7All Settings&8)");

    String checkForUpdates = checkTrueOrFalse(plugin.getFileManager().getUpdaterData().getBoolean(Settings.CHECK_FOR_UPDATES.getValue()));
    String forceExactNames = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.FORCE_EXACT_NAMES.getValue()));
    String allowSelfHug = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.ALLOW_SELF_HUG.getValue()));
    String allowShiftHug = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.ALLOW_SHIFT_HUG.getValue()));
    String restoreHealth = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HEALTH.getValue()));
    String restoreHunger = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HUNGER.getValue()));
//    String tes = String.valueOf(plugin.getConfig().getBoolean(Settings.CHECK_FOR_UPDATES.getPermissionNode()));

//    TableBuilder tb = new TableBuilder();
//    tb.addRow(LogUtils.getConsolePrefix() + "7777777", "7777777", "7777777");
//    tb.addRow(LogUtils.getConsolePrefix() + "666666", "666666", "666666");
//    tb.addRow(LogUtils.getConsolePrefix() + "55555", "55555", "55555");
//    tb.addRow(LogUtils.getConsolePrefix() + "4444", "4444", "4444");
//    tb.addRow(LogUtils.getConsolePrefix() + "333", "333", "333");
//    tb.addRow(LogUtils.getConsolePrefix() + "22", "22", "22");
//    tb.addRow(LogUtils.getConsolePrefix() + "1", "1", "1");
//    tb.addRow(LogUtils.getConsolePrefix() + "Color Test", "Test", "Test");
//    tb.addRow(LogUtils.getConsolePrefix() + "Color Test", tes, "Test");
//    tb.addRow(LogUtils.getConsolePrefix() + "Check12521515125331", "Check", "Check 123");
//    tb.addRow(LogUtils.getConsolePrefix() + "Check For Updates", "Language File", "Force Exact Names");
//    tb.addRow(LogUtils.getConsolePrefix() + checkForUpdates, "Lang File", forceExactNames);
//    tb.addRow(LogUtils.getConsolePrefix() + "Allow Self Hug", "Allow Shift Hug");
//    tb.addRow(LogUtils.getConsolePrefix() + allowSelfHug, allowShiftHug);
//    LogUtils.logInfo("\n" + tb.toString());

    LogUtils.logInfo("Basic Features:");
    LogUtils.logInfo("  Check For Updates: " + checkForUpdates + " | " + "Force Exact Names: " + forceExactNames);
    LogUtils.logInfo("  Allow Self Hugs: " + allowSelfHug + " | " + "Allow Shift Hugs: " + allowShiftHug);
    LogUtils.logInfo("  Restore Health: " + restoreHealth + " | " + "Restore Hunger: " + restoreHunger);

    int titleFadeIn = plugin.getConfig().getInt(Settings.TITLE_FADE_IN.getValue());
    int titleStay = plugin.getConfig().getInt(Settings.TITLE_STAY.getValue());
    int titleFadeOut = plugin.getConfig().getInt(Settings.TITLE_FADE_OUT.getValue());
    String titleFadeInGrammarFix = GrammarFix.getPlural("second", plugin.getConfig().getInt(Settings.TITLE_FADE_IN.getValue()) == 1);
    String titleStayGrammarFix = GrammarFix.getPlural("second", plugin.getConfig().getInt(Settings.TITLE_STAY.getValue()) == 1);
    String titleFadeOutGrammarFix = GrammarFix.getPlural("second", plugin.getConfig().getInt(Settings.TITLE_FADE_OUT.getValue()) == 1);
    LogUtils.logInfo("Titles: ");
    LogUtils.logInfo("  Fade In: &e" + titleFadeIn + " " + titleFadeInGrammarFix);
    LogUtils.logInfo("  Stay: &e" + titleStay + " " + titleStayGrammarFix);
    LogUtils.logInfo("  Fade-Out: &e" + titleFadeOut + " " + titleFadeOutGrammarFix);


    String globalParticles = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.GLOBAL_PARTICLES.getValue()));
    String particleEffect = plugin.getConfig().getString(Settings.PARTICLE_EFFECT.getValue());
    LogUtils.logInfo("Particles: ");
    LogUtils.logInfo("  Global: " + globalParticles);
    LogUtils.logInfo("  Effect: &d" + particleEffect);

    if (!isValidParticle(particleEffect)) {
      LogUtils.logWarning("    &eWARNING: &rThe particle effect provided is invalid!");
      LogUtils.logWarning("    Valid particle effects can be found at:");
      LogUtils.logWarning("    &3https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Particle.html");
    }

    String globalSounds = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.GLOBAL_SOUND.getValue()));
    String soundEffect = plugin.getConfig().getString(Settings.SOUND_EFFECT.getValue());
    LogUtils.logInfo("Sounds: ");
    LogUtils.logInfo("  Global: " + globalSounds);
    LogUtils.logInfo("  Effect: &d" + soundEffect);

    if (!isValidSound(soundEffect)) {
      LogUtils.logWarning("    &eWARNING: &rThe sound effect provided is invalid!");
      LogUtils.logWarning("    Valid sound effects can be found at:");
      LogUtils.logWarning("    &3https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Sound.html");
    }

    LogUtils.logInfo("  Volume: " + plugin.getConfig().getInt(Settings.SOUND_VOLUME.getValue()) + " dB");
    LogUtils.logInfo("  Pitch: " + plugin.getConfig().getInt(Settings.SOUND_PITCH.getValue()) + " Hz");


    int selfCooldown = plugin.getConfig().getInt(Settings.SELF_HUG_COOLDOWN.getValue());
    int normalCooldown = plugin.getConfig().getInt(Settings.NORMAL_HUG_COOLDOWN.getValue());
    int massCooldown = plugin.getConfig().getInt(Settings.MASS_HUG_COOLDOWN.getValue());
    String selfCooldownGrammarFix = GrammarFix.getPlural("second",plugin.getConfig().getInt(Settings.SELF_HUG_COOLDOWN.getValue()) == 1);
    String normalCooldownGrammarFix = GrammarFix.getPlural("second",plugin.getConfig().getInt(Settings.NORMAL_HUG_COOLDOWN.getValue()) == 1);
    String massCooldownGrammarFix = GrammarFix.getPlural("second",plugin.getConfig().getInt(Settings.MASS_HUG_COOLDOWN.getValue()) == 1);
    String exactTime = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.USE_EXACT_TIME.getValue()));
    LogUtils.logInfo("Cooldowns:");
    LogUtils.logInfo("  Self Hugs: &e" + selfCooldown + " " + selfCooldownGrammarFix);
    LogUtils.logInfo("  Normal Hugs: &e" + normalCooldown + " " + normalCooldownGrammarFix);
    LogUtils.logInfo("  Mass Hugs: &e" + massCooldown + " " + massCooldownGrammarFix);
    LogUtils.logInfo("  Use Exact Time: " + exactTime);

    String consoleVerboseEnabled = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.CONSOLE_VERBOSE_ENABLED.getValue()));
    String fileVerboseEnabled = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.FILE_LOGGING_ENABLED.getValue()));
    LogUtils.logInfo("Verbose:");
    LogUtils.logInfo("  Console verbose: " + consoleVerboseEnabled + " | " + "File verbose: " + fileVerboseEnabled);

    String passiveModeEnabled= checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.PASSIVE_MODE_ENABLED.getValue()));
    String tempInvincibilityEnabled = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.TEMPORARY_INVINCIBILITY_ENABLED.getValue()));
    String realisticHugs = checkTrueOrFalse(plugin.getConfig().getBoolean(Settings.REALISTIC_HUGS.getValue()));
    LogUtils.logInfo("Experimental Features:");
    LogUtils.logInfo("  Passive Mode: " + passiveModeEnabled);
    LogUtils.logInfo("  Temporary Invincibility: " + tempInvincibilityEnabled);
    LogUtils.logInfo("  Realistic Hugs: " + realisticHugs);
    LogUtils.logInfo(" ");
  }

  public void sendPluginFailure() {
    List<String> versionsToSort = new ArrayList<>();

    for (ServerVersion ver : ServerVersion.values()) {
      if (ver.isSupported()) {
        versionsToSort.add(ver.getDisplayValue());
      }
    }

    String compatVersions = getListAsSortedString(versionsToSort);

    String spacing = String.format("%-3s", "");
    String header = "= - = - = - = - = - = - = - = - = - = - = - =";
//    String header = String.join("-", " = ", " = ").repeat(11).trim();

    LogUtils.logInfoNoPrefix(spacing + header);
    LogUtils.logInfoNoPrefix(spacing + "&cERROR: Initial Setup Failed");
    LogUtils.logInfoNoPrefix(spacing + "Your server version is &l&oNOT&r compatible with this plugin.");
    LogUtils.logInfoNoPrefix(spacing + "Update your server so that you can keep giving hugs!");
    LogUtils.logInfoNoPrefix(spacing + "Compatible Versions: " + compatVersions);
    LogUtils.logInfoNoPrefix(spacing + header);
    LogUtils.logInfoNoPrefix(" ");
  }

  private String getListAsSortedString(List<String> list) {
    StringBuilder stringBuilder = new StringBuilder();
    // Looping through the list.
    for (int i = 0; i < list.size(); i++) {
      //append the value into the builder
      stringBuilder.append("&a" + list.get(i));

      // If the value is not the last element of the list then append a comma(,).
      if (i != list.size() - 1) {
        stringBuilder.append("&7, ");
      }
    }

    return stringBuilder.toString();
  }

  private boolean isValidParticle(String particle) {
    try {
      Particle.valueOf(particle);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }

  private boolean isValidSound(String sound) {
    try {
      Sound.valueOf(sound);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }

  private String checkTrueOrFalse(boolean bool) {
    if (bool) {
      return "&atrue&r";
    } else {
      return "&cfalse&r";
    }
  }

  private String checkEnabledOrDisabled(boolean bool) {
    if (bool) {
      return "&aEnabled&r";
    } else {
      return "&cDisabled&r";
    }
  }

}
