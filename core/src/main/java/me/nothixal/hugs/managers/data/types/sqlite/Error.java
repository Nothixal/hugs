package me.nothixal.hugs.managers.data.types.sqlite;

import java.util.logging.Level;
import me.nothixal.hugs.HugsPlugin;

public class Error {

  public static void execute(HugsPlugin plugin, Exception ex) {
    plugin.getLogger().log(Level.SEVERE, "Couldn't execute MySQL statement: ", ex);
  }

  public static void close(HugsPlugin plugin, Exception ex) {
    plugin.getLogger().log(Level.SEVERE, "Failed to close MySQL connection: ", ex);
  }
}
