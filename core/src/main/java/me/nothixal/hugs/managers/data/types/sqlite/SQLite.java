package me.nothixal.hugs.managers.data.types.sqlite;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import me.nothixal.hugs.managers.data.SQLDatabase;

public class SQLite extends SQLDatabase {

  private String dbname;

  public SQLite() {
    dbname = plugin.getConfig().getString("SQLite.Filename", "hugs-sqlite");
  }

  public String createHugsDataTable =
      "CREATE TABLE IF NOT EXISTS hugs_data (" +
          "`uuid` varchar(36) NOT NULL," +
          "`total_self_hugs` int(11) NOT NULL," +
          "`total_normal_hugs` int(11) NOT NULL," +
          "`total_mass_hugs` int(11) NOT NULL," +
          "PRIMARY KEY (`uuid`)" +
          ");";

  public String createHugsPlayerdataTable =
      "CREATE TABLE IF NOT EXISTS hugs_playerdata (" +
          "`uuid` varchar(36) NOT NULL," +
          "`self_hugs` int(11) NOT NULL," +
          "`normal_hugs_given` int(11) NOT NULL," +
          "`normal_hugs_received` int(11) NOT NULL," +
          "`mass_hugs_given` int(11) NOT NULL," +
          "`mass_hugs_received` int(11) NOT NULL," +
          "`unique_hugs` int(11) NOT NULL," +
          "PRIMARY KEY (`uuid`)" +
          ");";
  public String createHugsPreferencesTable =
      "CREATE TABLE IF NOT EXISTS hugs_preferences (" +
          "`uuid` varchar(36) NOT NULL," +
          "`huggability` varchar(10) NOT NULL," +
          "`particle_quality` varchar(10) NOT NULL," +
          "`wants_hugs` boolean NOT NULL," +
          "`wants_realistic_hugs` boolean NOT NULL," +
          "`wants_particles` boolean NOT NULL," +
          "`prefers_menus` boolean NOT NULL," +
          "PRIMARY KEY (`uuid`)" +
          ");";
  public String createHugsPreferencesSoundsTable =
      "CREATE TABLE IF NOT EXISTS hugs_preferences_sounds (" +
          "`uuid` varchar(36) NOT NULL," +
          "`hug_sounds` boolean NOT NULL," +
          "`menu_sounds` boolean NOT NULL," +
          "`command_sounds` boolean NOT NULL," +
          "PRIMARY KEY (`uuid`)" +
          ");";
  public String createHugsPreferencesIndicatorsTable =
      "CREATE TABLE IF NOT EXISTS hugs_preferences_indicators (" +
          "`uuid` varchar(36) NOT NULL," +
          "`chat` boolean NOT NULL," +
          "`titles` boolean NOT NULL," +
          "`actionbar` boolean NOT NULL," +
          "`bossbar` boolean NOT NULL," +
          "`toasts` boolean NOT NULL," +
          "PRIMARY KEY (`uuid`)" +
          ");";

  // SQL creation stuff, You can leave the blow stuff untouched.
  public Connection getSQLConnection() {
    File dataFolder = new File(plugin.getFileManager().getDataDirectory(), dbname + ".db");
    if (!dataFolder.exists()) {
      try {
        dataFolder.createNewFile();
      } catch (IOException e) {
        plugin.getLogger().log(Level.SEVERE, "File write error: " + dbname + ".db");
      }
    }

    try {
      if (connection != null && !connection.isClosed()) {
        return connection;
      }

      Class.forName("org.sqlite.JDBC");
      connection = DriverManager.getConnection("jdbc:sqlite:" + dataFolder);
      return connection;
    } catch (SQLException ex) {
      plugin.getLogger().log(Level.SEVERE, "SQLite exception on initialize", ex);
    } catch (ClassNotFoundException ex) {
      plugin.getLogger().log(Level.SEVERE, "You need the SQLite JBDC library. Google it. Put it in /lib folder.");
    }

    return null;
  }

  public void load() {
    connection = getSQLConnection();
    try {
      Statement s = connection.createStatement();
      s.executeUpdate(createHugsDataTable);
      s.execute(createHugsPlayerdataTable);
      s.execute(createHugsPreferencesTable);
      s.execute(createHugsPreferencesSoundsTable);
      s.execute(createHugsPreferencesIndicatorsTable);
      s.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    initialize();
  }
}
