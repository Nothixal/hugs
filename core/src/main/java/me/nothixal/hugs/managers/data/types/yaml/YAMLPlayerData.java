package me.nothixal.hugs.managers.data.types.yaml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.utils.logger.LogUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class YAMLPlayerData {

  private final UUID uuid;
  private final File playerFile;
  private final FileConfiguration playerConfig;
  private boolean hasBeenModified = false;

  public YAMLPlayerData(HugsPlugin plugin, UUID uuid) {
    this.uuid = uuid;
    this.playerFile = new File(plugin.getFileManager().getPlayerDataDirectory(), uuid + ".yml");
    this.playerConfig = YamlConfiguration.loadConfiguration(playerFile);
    prerequisiteCheck();
  }

  private void prerequisiteCheck() {
    if (playerFileExists()) {
//      System.out.println("Player files exists! Exiting method.");
      return;
    }

//    System.out.println("Check 1");

//    // This is to prevent unnecessary files from being created.
//    // This helps prevent the leaderboard GUI from fucking up.
//    if (Bukkit.getOfflinePlayer(uuid).hasPlayedBefore()) {
//      return;
//    }

//    System.out.println("Check 2");

    createPlayerFile();
    createFileStructure();
  }

  private void createFileStructure() {
    //
    // General Information Section
    //
    if (playerConfig.getConfigurationSection("uuid") == null) {
      playerConfig.set("uuid", uuid.toString());
    }

    if (playerConfig.getConfigurationSection("name") == null) {
      playerConfig.set("name", Bukkit.getOfflinePlayer(uuid).getName());
    }

    //
    // User Settings Section
    //
    if (playerConfig.getConfigurationSection("settings") == null) {
      playerConfig.createSection("settings");
    }

    if (playerConfig.getConfigurationSection("settings.language") == null) {
      playerConfig.set("settings.language", "en");
    }

    if (playerConfig.getConfigurationSection("settings.huggability") == null) {
      playerConfig.set("settings.huggability", "all");
    }

    if (playerConfig.getConfigurationSection("settings.prefers_menus") == null) {
      playerConfig.set("settings.prefers_menus", true);
    }

    // Particles Section
    if (playerConfig.getConfigurationSection("settings.particles") == null) {
      playerConfig.createSection("settings.particles");
    }

    if (playerConfig.getConfigurationSection("settings.particles.effect") == null) {
      playerConfig.set("settings.particles.effect", "HEART");
    }

    if (playerConfig.getConfigurationSection("settings.particles.quality") == null) {
      playerConfig.set("settings.particles.quality", "low");
    }

    // Sounds Section
    if (playerConfig.getConfigurationSection("settings.sounds") == null) {
      playerConfig.createSection("settings.sounds");
    }

    if (playerConfig.getConfigurationSection("settings.sounds.effect") == null) {
      playerConfig.set("settings.sounds.effect", "ENTITY_CAT_PURR");
    }

    if (playerConfig.getConfigurationSection("settings.sounds.hugs") == null) {
      playerConfig.set("settings.sounds.hugs", true);
    }

    if (playerConfig.getConfigurationSection("settings.sounds.menu") == null) {
      playerConfig.set("settings.sounds.menu", true);
    }

    if (playerConfig.getConfigurationSection("settings.sounds.commands") == null) {
      playerConfig.set("settings.sounds.commands", true);
    }

    // Indicators Section
    if (playerConfig.getConfigurationSection("settings.indicators") == null) {
      playerConfig.createSection("settings.indicators");
    }

    if (playerConfig.getConfigurationSection("settings.indicators.chat") == null) {
      playerConfig.set("settings.indicators.chat", true);
    }

    if (playerConfig.getConfigurationSection("settings.indicators.actionbar") == null) {
      playerConfig.set("settings.indicators.actionbar", true);
    }

    if (playerConfig.getConfigurationSection("settings.indicators.bossbar") == null) {
      playerConfig.set("settings.indicators.bossbar", true);
    }

    if (playerConfig.getConfigurationSection("settings.indicators.titles") == null) {
      playerConfig.set("settings.indicators.titles", true);
    }

    if (playerConfig.getConfigurationSection("settings.indicators.toasts") == null) {
      playerConfig.set("settings.indicators.toasts", true);
    }

    //
    // User Data Section
    //
    if (playerConfig.getConfigurationSection("data") == null) {
      playerConfig.createSection("data");
    }

    if (playerConfig.getConfigurationSection("data.self_hugs") == null) {
      playerConfig.set("data.self_hugs", 0);
    }

    if (playerConfig.getConfigurationSection("data.normal_hugs") == null) {
      playerConfig.createSection("data.normal_hugs");
    }

    if (playerConfig.getConfigurationSection("data.normal_hugs.given") == null) {
      playerConfig.set("data.normal_hugs.given", 0);
    }

    if (playerConfig.getConfigurationSection("data.normal_hugs.received") == null) {
      playerConfig.set("data.normal_hugs.received", 0);
    }

    if (playerConfig.getConfigurationSection("data.mass_hugs") == null) {
      playerConfig.createSection("data.mass_hugs");
    }

    if (playerConfig.getConfigurationSection("data.mass_hugs.given") == null) {
      playerConfig.set("data.mass_hugs.given", 0);
    }

    if (playerConfig.getConfigurationSection("data.mass_hugs.received") == null) {
      playerConfig.set("data.mass_hugs.received", 0);
    }

    //
    // First Hug Section
    //
    if (playerConfig.getConfigurationSection("data.first_hug") == null) {
      playerConfig.createSection("data.first_hug");
    }

    // First Hug Given
    if (playerConfig.getConfigurationSection("data.first_hug.given") == null) {
      playerConfig.createSection("data.first_hug.given");
    }

    if (playerConfig.getConfigurationSection("data.first_hug.given.to") == null) {
      playerConfig.set("data.first_hug.given.to", "None");
    }

    if (playerConfig.getConfigurationSection("data.first_hug.given.timestamp") == null) {
      playerConfig.set("data.first_hug.given.timestamp", 0);
    }

    // First Hug Received
    if (playerConfig.getConfigurationSection("data.first_hug.received") == null) {
      playerConfig.createSection("data.first_hug.received");
    }

    if (playerConfig.getConfigurationSection("data.first_hug.received.from") == null) {
      playerConfig.set("data.first_hug.received.from", "None");
    }

    if (playerConfig.getConfigurationSection("data.first_hug.received.timestamp") == null) {
      playerConfig.set("data.first_hug.received.timestamp", 0);
    }

    //
    // Last Hug Section
    //
    if (playerConfig.getConfigurationSection("data.last_hug") == null) {
      playerConfig.createSection("data.last_hug");
    }

    // Last Hug Given
    if (playerConfig.getConfigurationSection("data.last_hug.given") == null) {
      playerConfig.createSection("data.last_hug.given");
    }

    if (playerConfig.getConfigurationSection("data.last_hug.given.to") == null) {
      playerConfig.set("data.last_hug.given.to", "None");
    }

    if (playerConfig.getConfigurationSection("data.last_hug.given.timestamp") == null) {
      playerConfig.set("data.last_hug.given.timestamp", 0);
    }

    // Last Hug Received
    if (playerConfig.getConfigurationSection("data.last_hug.received") == null) {
      playerConfig.createSection("data.last_hug.received");
    }

    if (playerConfig.getConfigurationSection("data.last_hug.received.from") == null) {
      playerConfig.set("data.last_hug.received.from", "None");
    }

    if (playerConfig.getConfigurationSection("data.last_hug.received.timestamp") == null) {
      playerConfig.set("data.last_hug.received.timestamp", 0);
    }

    // Unique Hug Section
    if (playerConfig.getConfigurationSection("data.unique_hugs") == null) {
      playerConfig.createSection("data.unique_hugs");
    }

    if (playerConfig.getConfigurationSection("data.unique_hugs.given") == null) {
      playerConfig.set("data.unique_hugs.given", new ArrayList<>());
    }

    if (playerConfig.getConfigurationSection("data.unique_hugs.received") == null) {
      playerConfig.set("data.unique_hugs.received", new ArrayList<>());
    }

    // Recent Hug Section
    if (playerConfig.getConfigurationSection("data.recent_hugs") == null) {
      playerConfig.createSection("data.recent_hugs");
    }

    if (playerConfig.getConfigurationSection("data.recent_hugs.given") == null) {
      playerConfig.set("data.recent_hugs.given", new ArrayList<>());
    }

    if (playerConfig.getConfigurationSection("data.recent_hugs.received") == null) {
      playerConfig.set("data.recent_hugs.received", new ArrayList<>());
    }

    // Cooldowns Section
    if (playerConfig.getConfigurationSection("data.cooldowns") == null) {
      playerConfig.set("data.cooldowns", new ArrayList<>());
    }

    savePlayerFile();
  }

  public void saveDataFile() {
    prerequisiteCheck();

    try {
      playerConfig.save(playerFile);
      hasBeenModified = false;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void savePlayerFile() {
    try {
      playerConfig.save(playerFile);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public boolean playerFileExists() {
    return playerFile.exists();
  }

  private void createPlayerFile() {
    boolean fileCreated = false;

    if (!playerFileExists()) {
      try {
        fileCreated = playerFile.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if (!fileCreated) {
      LogUtils.logError("An error occurred while creating " + Bukkit.getOfflinePlayer(uuid).getName() + "'s data file.");
    }
  }

  public boolean deletePlayerFile() {
    return playerFile.delete();
  }

  public File getPlayerFile() {
    return playerFile;
  }

  public FileConfiguration getPlayerConfig() {
    return playerConfig;
  }

  // hasBeenModified getter and setter.
  public boolean hasBeenModified() {
    return hasBeenModified;
  }

  public void setHasBeenModified(boolean hasBeenModified) {
    this.hasBeenModified = hasBeenModified;
  }
}
