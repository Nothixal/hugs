package me.nothixal.hugs.managers.data.types.local;

import java.io.IOException;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.managers.data.LocalSettingsManager;

public class YAMLLocalSettingsManager implements LocalSettingsManager {

  private final HugsPlugin plugin;

  public YAMLLocalSettingsManager(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @Override
  public boolean canCheckForUpdates() {
    return plugin.getFileManager().getUpdaterData().getBoolean(Settings.CHECK_FOR_UPDATES.getValue());
  }

  @Override
  public void setCanCheckForUpdates(boolean value) {
    plugin.getFileManager().getUpdaterData().set(Settings.CHECK_FOR_UPDATES.getValue(), value);
    try {
      plugin.getFileManager().getUpdaterData().save(plugin.getFileManager().getUpdaterFile());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public boolean passiveModeActive() {
    return false;
  }
}
