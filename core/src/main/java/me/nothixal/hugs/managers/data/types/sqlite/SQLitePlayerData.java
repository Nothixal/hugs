package me.nothixal.hugs.managers.data.types.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import me.nothixal.hugs.enums.preferences.HuggableState;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.enums.preferences.ParticleQuality;

public class SQLitePlayerData {

  private SQLite sqLite;
  private UUID uuid;
  private int selfHugs = 0;
  private int normalHugsGiven = 0;
  private int normalHugsReceived = 0;
  private int massHugsGiven = 0;
  private int massHugsReceived = 0;

  private boolean wantsHugs;
  private boolean wantsRealisticHugs;
  private boolean wantsParticles;
  private boolean prefersMenus;

  private HuggableState huggability;
  private ParticleQuality particleQuality;


  private boolean wantsChatIndicators;
  private boolean wantsTitleIndicators;
  private boolean wantsActionbarIndicators;
  private boolean wantsBossbarIndicators;
  private boolean wantsToastIndicators;

  private boolean wantsHugSounds;
  private boolean wantsMenuSounds;
  private boolean wantsCommandSounds;


  private boolean hasBeenModified = false;

  public SQLitePlayerData(SQLite sqLite, UUID uuid) {
    this.sqLite = sqLite;
    this.uuid = uuid;
  }

  // TODO: Set the values after creating the data!
  //       Errors occur because the values in this class aren't being updated.
  public void createPlayer() {
    String playerdata = "INSERT INTO hugs_playerdata VALUES (?, ?, ?, ?, ?, ?, ?)";
    String preferences = "INSERT INTO hugs_preferences VALUES (?, ?, ?, ?, ?, ?, ?)";
    String indicators = "INSERT INTO hugs_preferences_indicators VALUES (?, ?, ?, ?, ?, ?)";
    String sounds = "INSERT INTO hugs_preferences_sounds VALUES (?, ?, ?, ?)";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement playerdataQuery = connection.prepareStatement(playerdata);
        PreparedStatement preferencesQuery = connection.prepareStatement(preferences);
        PreparedStatement indicatorsQuery = connection.prepareStatement(indicators);
        PreparedStatement soundsQuery = connection.prepareStatement(sounds);

    ) {
      playerdataQuery.setString(1, uuid.toString());
      playerdataQuery.setInt(2, 0);
      playerdataQuery.setInt(3, 0);
      playerdataQuery.setInt(4, 0);
      playerdataQuery.setInt(5, 0);
      playerdataQuery.setInt(6, 0);
      playerdataQuery.setInt(7, 0);
      playerdataQuery.execute();

      preferencesQuery.setString(1, uuid.toString());
      preferencesQuery.setString(2, HuggableState.ALL.getValue());
      preferencesQuery.setString(3, ParticleQuality.LOW.getValue());
      preferencesQuery.setBoolean(4, true);
      preferencesQuery.setBoolean(5, false);
      preferencesQuery.setBoolean(6, true);
      preferencesQuery.setBoolean(7, true);
      preferencesQuery.execute();

      indicatorsQuery.setString(1, uuid.toString());
      indicatorsQuery.setBoolean(2, true);
      indicatorsQuery.setBoolean(3, true);
      indicatorsQuery.setBoolean(4, true);
      indicatorsQuery.setBoolean(5, true);
      indicatorsQuery.setBoolean(6, true);
      indicatorsQuery.execute();

      soundsQuery.setString(1, uuid.toString());
      soundsQuery.setBoolean(2, true);
      soundsQuery.setBoolean(3, true);
      soundsQuery.setBoolean(4, true);
      soundsQuery.execute();

    } catch (SQLException e) {
      e.printStackTrace();
    }

    loadPlayer();
  }

  public void loadPlayer() {
    String query = "SELECT * FROM hugs_playerdata WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, uuid.toString());
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        selfHugs = resultSet.getInt("self_hugs");
        normalHugsGiven = resultSet.getInt("normal_hugs_given");
        normalHugsReceived = resultSet.getInt("normal_hugs_received");
        massHugsGiven = resultSet.getInt("mass_hugs_given");
        massHugsReceived = resultSet.getInt("mass_hugs_received");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "SELECT * FROM hugs_preferences WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, uuid.toString());
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        String huggable = resultSet.getString("huggability");
        huggability = HuggableState.valueOf(huggable.toUpperCase());

        String particles = resultSet.getString("particle_quality");
        particleQuality = ParticleQuality.valueOf(particles.toUpperCase());

        wantsParticles = resultSet.getBoolean("wants_particles");
        prefersMenus = resultSet.getBoolean("prefers_menus");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    // Indicators Section
    query = "SELECT * FROM hugs_preferences_indicators WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, uuid.toString());
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        wantsChatIndicators = resultSet.getBoolean("chat");
        wantsTitleIndicators = resultSet.getBoolean("titles");
        wantsActionbarIndicators = resultSet.getBoolean("actionbar");
        wantsBossbarIndicators = resultSet.getBoolean("bossbar");
        wantsToastIndicators = resultSet.getBoolean("toasts");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    // Sounds Section
    query = "SELECT * FROM hugs_preferences_sounds WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, uuid.toString());
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        wantsHugSounds = resultSet.getBoolean("hug_sounds");
        wantsMenuSounds = resultSet.getBoolean("menu_sounds");
        wantsCommandSounds = resultSet.getBoolean("command_sounds");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void saveData() {
    String query = "UPDATE hugs_playerdata SET self_hugs = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setInt(1, selfHugs);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_playerdata SET normal_hugs_given = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setInt(1, normalHugsGiven);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_playerdata SET normal_hugs_received = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setInt(1, normalHugsReceived);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_playerdata SET mass_hugs_given = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setInt(1, massHugsGiven);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_playerdata SET mass_hugs_received = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setInt(1, massHugsReceived);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences SET huggability = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, huggability.getValue());
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences SET wants_particles = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, wantsParticles);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences SET particle_quality = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, particleQuality.getValue());
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences_sounds SET hug_sounds = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, wantsHugSounds);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences_sounds SET menu_sounds = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, wantsMenuSounds);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences_sounds SET command_sounds = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, wantsCommandSounds);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences SET prefers_menus = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, prefersMenus);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    updateIndicators();
  }

  private void updateIndicators() {
    String query = "UPDATE hugs_preferences_indicators SET chat = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, wantsChatIndicators);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences_indicators SET titles = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, wantsTitleIndicators);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences_indicators SET actionbar = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, wantsActionbarIndicators);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences_indicators SET bossbar = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, wantsBossbarIndicators);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    query = "UPDATE hugs_preferences_indicators SET toasts = ? WHERE uuid=?";

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setBoolean(1, wantsToastIndicators);
      statement.setString(2, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public IndicatorType getIndicator(IndicatorType type) {
    switch (type) {
      case CHAT:
        return IndicatorType.CHAT;
      case TITLES:
        return IndicatorType.TITLES;
      case ACTIONBAR:
        return IndicatorType.ACTIONBAR;
      case BOSSBAR:
        return IndicatorType.BOSSBAR;
      case TOASTS:
        return IndicatorType.TOASTS;
    }

    return null;
  }

  public boolean wantsIndicator(IndicatorType type) {
    switch (type) {
      case CHAT:
        return wantsChatIndicators;
      case TITLES:
        return wantsTitleIndicators;
      case ACTIONBAR:
        return wantsActionbarIndicators;
      case BOSSBAR:
        return wantsBossbarIndicators;
      case TOASTS:
        return wantsToastIndicators;
    }

    return true;
  }

  public void setIndicator(IndicatorType type, boolean enabled) {
    switch (type) {
      case CHAT:
        wantsChatIndicators = enabled;
        break;
      case TITLES:
        wantsTitleIndicators = enabled;
        break;
      case ACTIONBAR:
        wantsActionbarIndicators = enabled;
        break;
      case BOSSBAR:
        wantsBossbarIndicators = enabled;
        break;
      case TOASTS:
        wantsToastIndicators = enabled;
        break;
    }
  }

  public boolean hasBeenModified() {
    return hasBeenModified;
  }

  public void setHasBeenModified(boolean hasBeenModified) {
    this.hasBeenModified = hasBeenModified;
  }

  public int getSelfHugs() {
    return selfHugs;
  }

  public void setSelfHugs(int selfHugs) {
    this.selfHugs = selfHugs;
  }

  public int getNormalHugsGiven() {
    return normalHugsGiven;
  }

  public void setNormalHugsGiven(int normalHugsGiven) {
    this.normalHugsGiven = normalHugsGiven;
  }

  public int getNormalHugsReceived() {
    return normalHugsReceived;
  }

  public void setNormalHugsReceived(int normalHugsReceived) {
    this.normalHugsReceived = normalHugsReceived;
  }

  public int getMassHugsGiven() {
    return massHugsGiven;
  }

  public void setMassHugsGiven(int massHugsGiven) {
    this.massHugsGiven = massHugsGiven;
  }

  public int getMassHugsReceived() {
    return massHugsReceived;
  }

  public void setMassHugsReceived(int massHugsReceived) {
    this.massHugsReceived = massHugsReceived;
  }

  public boolean isWantsHugs() {
    return wantsHugs;
  }

  public void setWantsHugs(boolean wantsHugs) {
    this.wantsHugs = wantsHugs;
  }

  public boolean isWantsRealisticHugs() {
    return wantsRealisticHugs;
  }

  public void setWantsRealisticHugs(boolean wantsRealisticHugs) {
    this.wantsRealisticHugs = wantsRealisticHugs;
  }

  public boolean wantsParticles() {
    return wantsParticles;
  }

  public void setWantsParticles(boolean wantsParticles) {
    this.wantsParticles = wantsParticles;
  }

  public boolean prefersMenus() {
    return prefersMenus;
  }

  public void setPrefersMenus(boolean prefersMenus) {
    this.prefersMenus = prefersMenus;
  }

  public HuggableState getHuggability() {
    return huggability;
  }

  public void setHuggability(HuggableState huggability) {
    this.huggability = huggability;
  }

  public ParticleQuality getParticleQuality() {
    return particleQuality;
  }

  public void setParticleQuality(ParticleQuality particleQuality) {
    this.particleQuality = particleQuality;
  }

  public boolean wantsHugSounds() {
    return wantsHugSounds;
  }

  public void setWantsHugSounds(boolean wantsHugSounds) {
    this.wantsHugSounds = wantsHugSounds;
  }

  public boolean wantsMenuSounds() {
    return wantsMenuSounds;
  }

  public void setWantsMenuSounds(boolean wantsMenuSounds) {
    this.wantsMenuSounds = wantsMenuSounds;
  }

  public boolean wantsCommandSounds() {
    return wantsCommandSounds;
  }

  public void setWantsCommandSounds(boolean wantsCommandSounds) {
    this.wantsCommandSounds = wantsCommandSounds;
  }
}
