package me.nothixal.hugs.managers.data.types.mysql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import me.nothixal.hugs.HugsPlugin;
import org.bukkit.configuration.file.FileConfiguration;

public class MySQL {

  private final HikariDataSource dataSource;

  private HugsPlugin plugin;
  private String hostname, username, password, database;
  private int port;

  public MySQL(HugsPlugin plugin) {
    this.plugin = plugin;
    this.dataSource = new HikariDataSource(createHikariConfig());
//    initializeValues();

    try {
      testConnection();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public String createHugsDatabase = "CREATE DATABASE IF NOT EXISTS hugs;";

  public String createHugsUsersTable =
      "CREATE TABLE IF NOT EXISTS users (" +
          "`uuid` varchar(36) NOT NULL," +
          "PRIMARY KEY (`uuid`)" +
          ");";

  public String createInteractionsTable =
      "CREATE TABLE IF NOT EXISTS interactions (" +
          "`interaction_id` INTEGER NOT NULL AUTO_INCREMENT UNIQUE," +
          "`giver`          VARCHAR(36) NOT NULL," +
          "`type`           ENUM('normal', 'self', 'mass') DEFAULT 'self'," +
          "`timestamp`      BIGINT NOT NULL DEFAULT (UNIX_TIMESTAMP())," +
          "FOREIGN KEY(`giver`) REFERENCES users (`uuid`)," +
          "PRIMARY KEY(`interaction_id`)" +
          ");";

  public String createHugsInteractionsTable =
      "CREATE TABLE IF NOT EXISTS hugs_interactions (" +
          "`interaction_id` INT         NOT NULL," +
          "`receiver`       VARCHAR(36) NOT NULL," +
          "FOREIGN KEY (`receiver`)       REFERENCES users (`uuid`)," +
          "FOREIGN KEY (`interaction_id`) REFERENCES interactions (`interaction_id`)" +
          ");";

  public String createHugsPreferencesTable =
      "CREATE TABLE IF NOT EXISTS preferences (" +
          "`uuid`             VARCHAR(36) NOT NULL," +
          "`huggability`      VARCHAR(10) NOT NULL," +
          "`prefers_menus`    BOOLEAN     NOT NULL," +
          "FOREIGN KEY (`uuid`) REFERENCES users (`uuid`)" +
          ");";

  public String createHugsPreferencesSoundsTable =
      "CREATE TABLE IF NOT EXISTS preferences_sounds (" +
          "`uuid`           VARCHAR(36) NOT NULL," +
          "`hug_sounds`     BOOLEAN     NOT NULL," +
          "`menu_sounds`    BOOLEAN     NOT NULL," +
          "`command_sounds` BOOLEAN     NOT NULL," +
          "FOREIGN KEY (`uuid`) REFERENCES preferences (`uuid`)" +
          ");";

  public String createHugsPreferencesVisualsTable =
      "CREATE TABLE IF NOT EXISTS preferences_visuals (" +
          "`uuid`             VARCHAR(36) NOT NULL," +
          "`particle_quality` VARCHAR(10) NOT NULL," +
          "`glowing_effect`   BOOLEAN     NOT NULL," +
          "FOREIGN KEY (`uuid`) REFERENCES preferences (`uuid`)" +
          ");";

  public String createHugsPreferencesIndicatorsTable =
      "CREATE TABLE IF NOT EXISTS preferences_indicators (" +
          "`uuid`      VARCHAR(36) NOT NULL," +
          "`chat`      BOOLEAN     NOT NULL," +
          "`titles`    BOOLEAN     NOT NULL," +
          "`actionbar` BOOLEAN     NOT NULL," +
          "`bossbar`   BOOLEAN     NOT NULL," +
          "`toasts`    BOOLEAN     NOT NULL," +
          "FOREIGN KEY (`uuid`) REFERENCES preferences (`uuid`)" +
          ");";

  private void initializeValues() {
    // Class.forName("com.mysql.jdbc.Driver");
    plugin.getFileManager().loadMySQLFile();
    FileConfiguration mysqlData = plugin.getFileManager().getMysqlFileData();

    hostname = mysqlData.getString("database.host");
    port = mysqlData.getInt("database.port");
    database = mysqlData.getString("database.database");
    username = mysqlData.getString("database.username");
    password = mysqlData.getString("database.password");

    dataSource.setJdbcUrl("jdbc:mysql://" + hostname + ":" + port + "/" + database);
    dataSource.setUsername(username);
    dataSource.setPassword(password);
    dataSource.addDataSourceProperty("cachePrepStmts", "true");
    dataSource.addDataSourceProperty("prepStmtCacheSize", "250");
    dataSource.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
  }

  private HikariConfig createHikariConfig() {
    HikariConfig config = new HikariConfig();

    plugin.getFileManager().loadMySQLFile();
    FileConfiguration mysqlData = plugin.getFileManager().getMysqlFileData();

    hostname = mysqlData.getString("database.host");
    port = mysqlData.getInt("database.port");
    database = mysqlData.getString("database.database");
    username = mysqlData.getString("database.username");
    password = mysqlData.getString("database.password");

    config.setJdbcUrl("jdbc:mysql://" + hostname + ":" + port + "/" + database);
    config.setUsername(username);
    config.setPassword(password);
    config.addDataSourceProperty("cachePrepStmts", "true");
    config.addDataSourceProperty("prepStmtCacheSize", "250");
    config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
    config.addDataSourceProperty("useServerPrepStmts", true);
    config.addDataSourceProperty("useLocalSessionState", true);
    config.addDataSourceProperty("rewriteBatchedStatements", true);
    config.addDataSourceProperty("cacheResultSetMetadata", true);
    config.addDataSourceProperty("cacheServerConfiguration", true);
    config.addDataSourceProperty("elideSetAutoCommits", true);
    config.addDataSourceProperty("maintainTimeStats", false);

    config.setMaximumPoolSize(20);
    config.setConnectionTimeout(1000 * 12);
    config.setLeakDetectionThreshold(1000 * 30);

    return config;
  }

  public void testConnection() throws SQLException {
    try (Connection conn = getSQLConnection()) {
      if (!conn.isValid(1000)) {
        throw new SQLException("Could not establish database connection.");
      } else {
        System.out.println(" ");
        System.out.println("Connection SUCCESSFUL!");
        System.out.println(" ");
      }
    }
  }

  public void closeConnection() {
    try {
      getSQLConnection().close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public Connection getSQLConnection() {
    try {
      return dataSource.getConnection();
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }

  public String getHostname() {
    return hostname;
  }

  public String getDatabase() {
    return database;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public int getPort() {
    return port;
  }

  public void load() {
    try (Connection connection = getSQLConnection();
        Statement s = connection.createStatement()) {
      s.execute(createHugsDatabase);
      s.execute(createHugsUsersTable);
      s.execute(createInteractionsTable);
      s.execute(createHugsInteractionsTable);
      s.execute(createHugsPreferencesTable);
      s.execute(createHugsPreferencesSoundsTable);
      s.execute(createHugsPreferencesVisualsTable);
      s.execute(createHugsPreferencesIndicatorsTable);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}