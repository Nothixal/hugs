package me.nothixal.hugs.managers.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.managers.data.types.sqlite.Error;

public abstract class SQLDatabase {

  public HugsPlugin plugin;
  public Connection connection;
  public String table = "hugs_data";

  public SQLDatabase() {}

  public SQLDatabase(HugsPlugin instance) {
    plugin = instance;
  }

  public abstract Connection getSQLConnection();

  public abstract void load();

  public void initialize() {
    connection = getSQLConnection();
    try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + table /*+ " WHERE uuid = ?"*/)) {
      ResultSet rs = ps.executeQuery();
      close(ps, rs);

    } catch (SQLException ex) {
      plugin.getLogger().log(Level.SEVERE, "Unable to retrieve connection", ex);
    }
  }

  public void close(PreparedStatement ps, ResultSet rs) {
    try {
      if (ps != null)  ps.close();
      if (rs != null)  rs.close();
    } catch (SQLException ex) {
      Error.close(plugin, ex);
    }
  }
}