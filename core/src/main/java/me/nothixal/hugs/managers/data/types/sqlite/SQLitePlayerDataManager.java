package me.nothixal.hugs.managers.data.types.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.preferences.HuggableState;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.enums.preferences.ParticleQuality;
import me.nothixal.hugs.enums.types.DatabaseType;
import me.nothixal.hugs.enums.types.HugType;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.utils.Indicator;
import me.nothixal.hugs.utils.Interaction;
import me.nothixal.hugs.utils.cooldowns.Cooldown;

public class SQLitePlayerDataManager implements PlayerDataManager {

  private final HugsPlugin plugin;
  private final SQLite sqLite;

  private final Map<UUID, SQLitePlayerData> dataCache = new HashMap<>();

  private final String PLAYER_EXISTS = "SELECT EXISTS(SELECT uuid FROM hugs_playerdata WHERE uuid=?)";
  private final String CREATE_PLAYER = "";
  private final String DELETE_PLAYER = "DELETE FROM hugs_playerdata WHERE uuid=?";

  public SQLitePlayerDataManager(HugsPlugin plugin) {
    this.plugin = plugin;
    sqLite = new SQLite();
  }

  @Override
  public boolean playerExists(UUID uuid) {
    boolean value = false;

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(PLAYER_EXISTS)
    ) {
      statement.setString(1, uuid.toString());
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        value = resultSet.getBoolean(1);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public void createPlayer(UUID uuid) {
    getPlayerData(uuid).createPlayer();
  }

  @Override
  public void deletePlayer(UUID uuid) {
    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(DELETE_PLAYER)
    ) {
      statement.setString(1, uuid.toString());
      statement.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void loadPlayer(UUID uuid) {
    SQLitePlayerData playerData = new SQLitePlayerData(sqLite, uuid);
    playerData.loadPlayer();

    dataCache.put(uuid, playerData);
  }

  @Override
  public void reloadPlayer(UUID uuid) {

  }

  @Override
  public void setDefaults(UUID uuid) {

  }

  @Override
  public void loadDatabase() {
    sqLite.load();
  }

  @Override
  public void reloadDatabase() {

  }

  @Override
  public void saveDatabase() {
    for (SQLitePlayerData playerData : dataCache.values()) {
      if (!playerData.hasBeenModified()) {
        continue;
      }

      playerData.saveData();
    }
  }

  @Override
  public void purgeDatabase() {

  }

  @Override
  public void loadData(UUID uuid) {

  }

  @Override
  public void saveData(UUID uuid) {
    getPlayerData(uuid).saveData();
  }

  @Override
  public void reloadData(UUID uuid) {

  }

  @Override
  public void purgeData(UUID uuid) {

  }

  @Override
  public void addHugs(UUID sender, UUID receiver, HugType type, long timestamp) {

  }

  @Override
  public int getSelfHugs(UUID uuid) {
    return getPlayerData(uuid).getSelfHugs();
  }

  @Override
  public void setSelfHugs(UUID uuid, int value) {
    getPlayerData(uuid).setSelfHugs(value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementSelfHugs(UUID uuid) {
    setSelfHugs(uuid, getSelfHugs(uuid) + 1);
  }

  @Override
  public void incrementSelfHugs(UUID uuid, int amount) {
    setSelfHugs(uuid, getSelfHugs(uuid) + amount);
  }

  @Override
  public int getHugsGiven(UUID uuid) {
    return getPlayerData(uuid).getNormalHugsGiven();
  }

  @Override
  public void setHugsGiven(UUID uuid, int value) {
    getPlayerData(uuid).setNormalHugsGiven(value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementHugsGiven(UUID uuid) {
    setHugsGiven(uuid, getHugsGiven(uuid) + 1);
  }

  @Override
  public void incrementHugsGiven(UUID uuid, int amount) {
    setHugsGiven(uuid, getHugsGiven(uuid) + amount);
  }

  @Override
  public int getHugsReceived(UUID uuid) {
    return getPlayerData(uuid).getNormalHugsGiven();
  }

  @Override
  public void setHugsReceived(UUID uuid, int value) {
    getPlayerData(uuid).setNormalHugsReceived(value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementHugsReceived(UUID uuid) {
    setHugsReceived(uuid, getHugsReceived(uuid) + 1);
  }

  @Override
  public void incrementHugsReceived(UUID uuid, int amount) {
    setHugsReceived(uuid, getHugsReceived(uuid) + amount);
  }

  @Override
  public int getMassHugsGiven(UUID uuid) {
    return getPlayerData(uuid).getMassHugsGiven();
  }

  @Override
  public void setMassHugsGiven(UUID uuid, int value) {
    getPlayerData(uuid).setMassHugsGiven(value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementMassHugsGiven(UUID uuid) {
    setMassHugsGiven(uuid, getMassHugsGiven(uuid) + 1);
  }

  @Override
  public void incrementMassHugsGiven(UUID uuid, int amount) {
    setMassHugsGiven(uuid, getMassHugsGiven(uuid) + amount);
  }

  @Override
  public int getMassHugsReceived(UUID uuid) {
    return getPlayerData(uuid).getMassHugsReceived();
  }

  @Override
  public void setMassHugsReceived(UUID uuid, int value) {
    getPlayerData(uuid).setMassHugsReceived(value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementMassHugsReceived(UUID uuid) {
    setMassHugsReceived(uuid, getMassHugsReceived(uuid) + 1);
  }

  @Override
  public void incrementMassHugsReceived(UUID uuid, int amount) {
    setMassHugsReceived(uuid, getMassHugsReceived(uuid) + amount);
  }

  @Override
  public int getTotalSelfHugsGiven() {
    String query = "SELECT * FROM hugs_data";
    int value = 0;

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        value = resultSet.getInt("total_self_hugs");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public int getTotalHugsGiven() {
    String query = "SELECT * FROM hugs_data";
    int value = 0;

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        value = resultSet.getInt("total_normal_hugs");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public int getTotalMassHugsGiven() {
    String query = "SELECT * FROM hugs_data";
    int value = 0;

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        value = resultSet.getInt("total_mass_hugs");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public HuggableState getHuggability(UUID uuid) {
    return getPlayerData(uuid).getHuggability();
  }

  @Override
  public void setHuggability(UUID uuid, HuggableState state) {
    getPlayerData(uuid).setHuggability(state);
  }

  @Override
  public boolean wantsHugs(UUID uuid) {
    HuggableState state = getHuggability(uuid);
    return state == HuggableState.ALL;
  }

  @Override
  public boolean wantsRealisticHugs(UUID uuid) {
    HuggableState state = getHuggability(uuid);
    return state == HuggableState.REALISTIC;
  }

  @Override
  public boolean wantsParticles(UUID uuid) {
    return getPlayerData(uuid).wantsParticles();
  }

  @Override
  public void setWantsParticles(UUID uuid, boolean bool) {
    getPlayerData(uuid).setWantsParticles(bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public String getParticleEffect(UUID uuid) {
    return "HEART";
  }

  @Override
  public void setParticleEffect(UUID uuid, String value) {

  }

  @Override
  public ParticleQuality getParticleQuality(UUID uuid) {
    return getPlayerData(uuid).getParticleQuality();
  }

  @Override
  public void setParticleQuality(UUID uuid, ParticleQuality state) {
    getPlayerData(uuid).setParticleQuality(state);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void increaseParticleQuality(UUID uuid) {
    setParticleQuality(uuid, ParticleQuality.valueOf(getParticleQuality(uuid).getNext().name()));
  }

  @Override
  public void decreaseParticleQuality(UUID uuid) {
    setParticleQuality(uuid, ParticleQuality.valueOf(getParticleQuality(uuid).getPrevious().name()));
  }

  @Override
  public boolean wantsHugSounds(UUID uuid) {
    return getPlayerData(uuid).wantsHugSounds();
  }

  @Override
  public void setWantsHugSounds(UUID uuid, boolean bool) {
    getPlayerData(uuid).setWantsHugSounds(bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public String getSoundEffect(UUID uuid) {
    return "ENTITY_CAT_PURR";
  }

  @Override
  public boolean wantsMenuSounds(UUID uuid) {
    return getPlayerData(uuid).wantsMenuSounds();
  }

  @Override
  public void setWantsMenuSounds(UUID uuid, boolean bool) {
    getPlayerData(uuid).setWantsMenuSounds(bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public boolean wantsCommandSounds(UUID uuid) {
    return getPlayerData(uuid).wantsCommandSounds();
  }

  @Override
  public void setWantsCommandSounds(UUID uuid, boolean bool) {
    getPlayerData(uuid).setWantsCommandSounds(bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public boolean hasGivenHugsBefore(UUID uuid) {
    return getHugsGiven(uuid) != 0 || getMassHugsGiven(uuid) != 0;
  }

  @Override
  public boolean hasReceivedHugsBefore(UUID uuid) {
    return getHugsReceived(uuid) != 0 || getMassHugsReceived(uuid) != 0;
  }

  @Override
  public List<Indicator> getIndicators(UUID uuid) {
    List<Indicator> indicators = new ArrayList<>();
    indicators.add(new Indicator(1, IndicatorType.CHAT, getIndicator(uuid, IndicatorType.CHAT)));
    indicators.add(new Indicator(2, IndicatorType.TITLES, getIndicator(uuid, IndicatorType.TITLES)));
    indicators.add(new Indicator(3, IndicatorType.ACTIONBAR, getIndicator(uuid, IndicatorType.ACTIONBAR)));
    indicators.add(new Indicator(4, IndicatorType.BOSSBAR, getIndicator(uuid, IndicatorType.BOSSBAR)));
    indicators.add(new Indicator(5, IndicatorType.TOASTS, getIndicator(uuid, IndicatorType.TOASTS)));
    return indicators;
  }

  @Override
  public boolean getIndicator(UUID uuid, IndicatorType type) {
    return getPlayerData(uuid).wantsIndicator(type);
  }

  @Override
  public void setIndicator(UUID uuid, IndicatorType type, boolean enabled) {
    getPlayerData(uuid).setIndicator(type, enabled);
  }

  @Override
  public DatabaseType getDatabaseType() {
    return DatabaseType.SQLITE;
  }

  @Override
  public String getFirstHugGivenTo(UUID uuid) {
    return "None";
  }

  @Override
  public void setFirstHugGivenTo(UUID sender, UUID receiver) {

  }

  @Override
  public long getFirstHugGivenTimestamp(UUID uuid) {
    return 0;
  }

  @Override
  public void setFirstHugGivenTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public String getFirstHugReceivedFrom(UUID uuid) {
    return "None";
  }

  @Override
  public void setFirstHugReceivedFrom(UUID sender, UUID receiver) {

  }

  @Override
  public long getFirstHugReceivedTimestamp(UUID uuid) {
    return 0;
  }

  @Override
  public void setFirstHugReceivedTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public String getLastHugGivenTo(UUID uuid) {
    return null;
  }

  @Override
  public void setLastHugGivenTo(UUID sender, UUID receiver) {

  }

  @Override
  public long getLastHugGivenTimestamp(UUID uuid) {
    return 0;
  }

  @Override
  public void setLastHugGivenTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public String getLastHugReceivedFrom(UUID uuid) {
    return null;
  }

  @Override
  public void setLastHugReceivedFrom(UUID sender, UUID receiver) {

  }

  @Override
  public long getLastHugReceivedTimestamp(UUID uuid) {
    return 0;
  }

  @Override
  public void setLastHugReceivedTimestamp(UUID uuid, long timestamp) {

  }

  @Override
  public int getUniqueHugsGiven(UUID uuid) {
    String query = "SELECT * FROM hugs_playerdata WHERE uuid=?";
    int value = 0;

    try (
        Connection connection = sqLite.getSQLConnection();
        PreparedStatement statement = connection.prepareStatement(query)
    ) {
      statement.setString(1, uuid.toString());
      ResultSet resultSet = statement.executeQuery();

      while (resultSet.next()) {
        value = resultSet.getInt("unique_hugs");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return value;
  }

  @Override
  public List<String> getUniqueHugsGivenAsList(UUID uuid) {
    return null;
  }

  @Override
  public void setUniqueHugsGiven(UUID sender, UUID receiver, int amount) {

  }

  @Override
  public int getUniqueHugsReceived(UUID uuid) {
    return 0;
  }

  @Override
  public List<String> getUniqueHugsReceivedAsList(UUID uuid) {
    return null;
  }

  @Override
  public void setUniqueHugsReceived(UUID sender, UUID receiver, int amount) {

  }

  @Override
  public void addUniqueHugGiven(UUID player, UUID receiver) {

  }

  @Override
  public List<Interaction> getRecentHugs(UUID player) {
    return null;
  }

  @Override
  public List<Interaction> getRecentHugs(UUID player, boolean allStats) {
    return null;
  }

  @Override
  public void addRecentHugGiven(UUID sender, UUID receiver, HugType type, long timestamp) {

  }

  @Override
  public boolean prefersMenus(UUID uuid) {
    return getPlayerData(uuid).prefersMenus();
  }

  @Override
  public void setPrefersMenus(UUID uuid, boolean value) {
    getPlayerData(uuid).setPrefersMenus(value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public List<Cooldown> getCooldowns(UUID player) {
    return null;
  }

  @Override
  public Cooldown getCooldown(UUID player, UUID target) {
    return null;
  }

  @Override
  public void addCooldown(UUID player, UUID target, HugType type, long duration, long start) {

  }

  @Override
  public void removeCooldown(UUID player, UUID target) {

  }

  private SQLitePlayerData getPlayerData(UUID uuid) {
    return dataCache.computeIfAbsent(uuid, k -> new SQLitePlayerData(sqLite, k));
  }
}
