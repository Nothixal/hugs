package me.nothixal.hugs.managers.files;

import com.google.common.base.Charsets;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.utils.logger.LogUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileManager {

  private final HugsPlugin plugin;

  private final File homeDirectory;
  private final String backupDirectory;
  private final String dataDirectory;
  private final String playerDataDirectory;
  private final String localeDirectory;
  private final String logsDirectory;

  // Necessary Files needed to load plugin.
  private File configYMLFile;
  private FileConfiguration configYMLData;

  private File dataYMLFile;
  public FileConfiguration dataYMLData;

  private File updaterYMLFile;
  public FileConfiguration updaterYMLData;

  private File startupYMLFile;
  public FileConfiguration startupYMLData;

  private File defaultsYMLFile;
  public FileConfiguration defaultsYMLData;

  // Files for extended plugin functionality.
  private File overridesYMLFile;
  public FileConfiguration overridesYMLData;

  private File metricsYMLFile;
  public FileConfiguration metricsYMLData;

  private File mysqlYMLFile;
  public FileConfiguration mysqlFileData;

  private File mariadbYMLFile;
  public FileConfiguration mariadbFileData;


  private Map<String, File> missingDirectories = new HashMap<>();
  private List<String> createdDirectories = new ArrayList<>();
  private List<String> uncreatedDirectories = new ArrayList<>();

  private Map<String, File> missingFiles = new HashMap<>();
  private List<String> createdFiles = new ArrayList<>();
  private List<String> uncreatedFiles = new ArrayList<>();

  private Map<File, FileConfiguration> loadedFiles = new HashMap<>();

  public FileManager(HugsPlugin plugin) {
    this.plugin = plugin;
    this.homeDirectory = plugin.getDataFolder();
    this.backupDirectory = homeDirectory + File.separator + "backups";
    this.dataDirectory = homeDirectory.getAbsolutePath() + File.separator + "data";
    this.playerDataDirectory = dataDirectory + File.separator + "playerdata";
    this.localeDirectory = homeDirectory + File.separator + "locale";
    this.logsDirectory = homeDirectory + File.separator + "logs";
  }

  public File getHomeDirectory() {
    return homeDirectory;
  }

  public String getDataDirectory() {
    return dataDirectory;
  }

  public String getPlayerDataDirectory() {
    return playerDataDirectory;
  }

  public String getLocaleDirectory() {
    return localeDirectory;
  }

  public String getLogsDirectory() {
    return logsDirectory;
  }

  public void checkForAndCreateFiles() {
    missingDirectories.clear();
    missingFiles.clear();

    if (!createDirectories()) {
      LogUtils.logError("An error occurred whilst creating the directories.");
      LogUtils.logError("Disabling Plugin");
      Bukkit.getPluginManager().disablePlugin(plugin);
      return;
    }

    if (!createFiles()) {
      LogUtils.logError("An error occurred whilst creating the files.");
      LogUtils.logError("Disabling Plugin");
      Bukkit.getPluginManager().disablePlugin(plugin);
      return;
    }

    loadFiles();
  }

  private void checkForMissingDirectories() {
    List<File> files = new ArrayList<>();
    files.add(homeDirectory);
    files.add(new File(backupDirectory));
    files.add(new File(dataDirectory));
    files.add(new File(playerDataDirectory));
    files.add(new File(localeDirectory));
    files.add(new File(logsDirectory));

    for (File file : files) {
      if (!directoryExists(file)) {
        missingDirectories.put(file.getName(), file);
      }
    }
  }

  private boolean createDirectories() {
    checkForMissingDirectories();

    if (missingDirectories.size() > 0) {
      for (Map.Entry<String, File> entry : missingDirectories.entrySet()) {
        if (entry.getValue().exists()) {
          continue;
        }

        if (createDirectory(entry.getValue())) {
          createdDirectories.add(entry.getKey());
        } else {
          uncreatedDirectories.add(entry.getKey());
        }
      }

      return true;
    }

    return true;
  }

  private void checkForMissingFiles() {
    this.configYMLFile = new File(homeDirectory, "config.yml");
    this.dataYMLFile = new File(dataDirectory, "data.yml");
    this.startupYMLFile = new File(homeDirectory, "startup.yml");
    this.updaterYMLFile = new File(homeDirectory, "updater.yml");

    this.overridesYMLFile = new File(homeDirectory, "overrides.yml");
    this.defaultsYMLFile = new File(homeDirectory, "defaults.yml");

    this.mysqlYMLFile = new File(dataDirectory, "mysql.yml");
    this.mariadbYMLFile = new File(dataDirectory, "mariadb.yml");

    if (!fileExists(configYMLFile)) {
      missingFiles.put("config.yml", configYMLFile);
      this.plugin.saveDefaultConfig();
    }

    if (!fileExists(dataYMLFile)) {
      missingFiles.put("data.yml", dataYMLFile);
      this.plugin.saveResource("data/data.yml", true);
      this.plugin.reloadData();
    }

    if (!fileExists(startupYMLFile)) {
      missingFiles.put("startup.yml", startupYMLFile);
      this.plugin.saveResource("startup.yml", false);
    }

    if (!fileExists(updaterYMLFile)) {
      missingFiles.put("updater.yml", updaterYMLFile);
      this.plugin.saveResource("updater.yml",false);
    }

    if (!fileExists(overridesYMLFile)) {
      missingFiles.put("overrides.yml", overridesYMLFile);
      this.plugin.saveResource("overrides.yml",false);
    }

    if (!fileExists(defaultsYMLFile)) {
      missingFiles.put("defaults.yml", defaultsYMLFile);
      this.plugin.saveResource("defaults.yml",false);
    }

    if (!fileExists(mysqlYMLFile)) {
      missingFiles.put("mysql.yml", mysqlYMLFile);
      this.plugin.saveResource("data/mysql.yml",false);
    }

    if (!fileExists(mariadbYMLFile)) {
      missingFiles.put("mariadb.yml", mariadbYMLFile);
      this.plugin.saveResource("data/mariadb.yml", false);
    }
  }

  private boolean createFiles() {
    checkForMissingFiles();

    if (missingFiles.size() > 0) {
      for (Map.Entry<String, File> entry : missingFiles.entrySet()) {
        if (entry.getValue().exists()) {
          continue;
        }

        if (createFile(entry.getValue())) {
          createdFiles.add(entry.getKey());
        } else {
          uncreatedFiles.add(entry.getKey());
        }
      }

      return true;
    }

    return true;
  }

  private void loadFiles() {
    configYMLData = YamlConfiguration.loadConfiguration(configYMLFile);
    loadedFiles.put(configYMLFile, configYMLData);

    dataYMLData = YamlConfiguration.loadConfiguration(dataYMLFile);
    loadedFiles.put(dataYMLFile, dataYMLData);

    startupYMLData = YamlConfiguration.loadConfiguration(startupYMLFile);
    loadedFiles.put(startupYMLFile, startupYMLData);

    updaterYMLData = YamlConfiguration.loadConfiguration(updaterYMLFile);
    loadedFiles.put(updaterYMLFile, updaterYMLData);

    overridesYMLData = YamlConfiguration.loadConfiguration(overridesYMLFile);
    loadedFiles.put(overridesYMLFile, overridesYMLData);

    defaultsYMLData = YamlConfiguration.loadConfiguration(defaultsYMLFile);
    loadedFiles.put(defaultsYMLFile, defaultsYMLData);

    mysqlFileData = YamlConfiguration.loadConfiguration(mysqlYMLFile);
    loadedFiles.put(mysqlYMLFile, mysqlFileData);
  }

  public void loadMySQLFile() {
    mysqlFileData = YamlConfiguration.loadConfiguration(mysqlYMLFile);
  }

  public void printFileStatus() {
    LogUtils.logInfo("-={ Files }=-");

    printMissingDirectories();
    printMissingFiles();
  }

  public void printMissingDirectories() {
    if (missingDirectories.size() > 0) {
      List<String> missingDirectoriesKeys = new ArrayList<>(missingDirectories.keySet());

      String unaccountedDirectories = getListAsSortedString(missingDirectoriesKeys, "&e");

      LogUtils.logInfo("Missing Directories: " + unaccountedDirectories);
      LogUtils.logInfo("Attempting Creation...");

      if (uncreatedDirectories.size() > 0) {
        boolean check = true;

        String uncreatedDirs = getListAsSortedString(uncreatedDirectories, "&c");

        LogUtils.logInfo("Missing Directories: " + uncreatedDirs);

        if (uncreatedDirs.contains(homeDirectory.getName())) {
          check = false;
        }

        if (!check) {
          LogUtils.logError(uncreatedDirs + " directories failed to generate. Disabling plugin.");
          return;
        }

        return;
      }

      LogUtils.logInfo("&aSUCCESS: &7All missing directories were created successfully.");
      LogUtils.logInfo(" ");
    } else {
      LogUtils.logInfo("Missing Directories: &aNone");
    }
  }

  public void printMissingFiles() {
    if (missingFiles.size() > 0) {
      List<String> missingFilesKeys = new ArrayList<>(missingFiles.keySet());

      String unaccountedFiles = getListAsSortedString(missingFilesKeys, "&e");

      LogUtils.logInfo("Missing Files: " + unaccountedFiles);
      LogUtils.logInfo("Attempting Creation...");

      if (uncreatedFiles.size() > 0) {
        boolean check = true;

        String test = getListAsSortedString(uncreatedFiles, "&c");

        LogUtils.logInfo("Missing Files: " + test);

        if (test.contains("config.yml")) {
          check = false;
        }

        if (!check) {
          LogUtils.logError(test + " files failed to generate. Disabling plugin.");
          return;
        }

        return;
      }

      LogUtils.logInfo("&aSUCCESS: &7All missing files were created successfully.");
      LogUtils.logInfo(" ");
    } else {
      LogUtils.logInfo("Missing Files: &aNone");
      LogUtils.logInfo(" ");
    }
  }

  private String getListAsSortedString(List<String> list, String colorCode) {
    StringBuilder stringBuilder = new StringBuilder();
    // Looping through the list.
    for (int i = 0; i < list.size(); i++) {
      //append the value into the builder
      stringBuilder.append(colorCode + list.get(i));

      // If the value is not the last element of the list then append a comma(,).
      if (i != list.size() - 1) {
        stringBuilder.append("&7, ");
      }
    }
    return stringBuilder.toString();
  }

  private boolean directoryExists(File file) {
    return file.exists();
  }

  private boolean fileExists(File file) {
    return directoryExists(file);
  }

  private boolean createFile(File file) {
    boolean fileCreated = file.exists();

    if (!fileCreated) {
      try {
        fileCreated = file.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if (!fileCreated) {
      LogUtils.logError("An error occurred while creating the player's data.");
      return false;
    }

    return true;
  }

  public boolean createDirectory(File file) {
    boolean isDirectoryCreated = directoryExists(file);

    if (!isDirectoryCreated) {
      isDirectoryCreated = file.mkdirs();
    }

    return isDirectoryCreated;
  }

  private boolean directoryCreated(File file) {
    boolean isDirectoryCreated = directoryExists(file);

    if (!isDirectoryCreated) {
      isDirectoryCreated = file.mkdirs();
    }

    return isDirectoryCreated;
  }

  public boolean deleteDirectory(File file) {
    if (!file.isDirectory()) {
      return false;
    }

    return file.delete();
  }

  public File getConfigFile() {
    return configYMLFile;
  }

  public FileConfiguration getConfig() {
    return loadedFiles.get(configYMLFile);
  }

  public void reloadConfig() {
    configYMLData = YamlConfiguration.loadConfiguration(configYMLFile);

    InputStream defConfigStream = plugin.getResource("config.yml");
    if (defConfigStream != null) {
      configYMLData.setDefaults(YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream, Charsets.UTF_8)));
    }

    loadedFiles.put(configYMLFile, configYMLData);
  }

  public File getDataFile() {
    return dataYMLFile;
  }

  public FileConfiguration getData() {
    return dataYMLData;
  }

  public void reloadData() {
    dataYMLData = YamlConfiguration.loadConfiguration(dataYMLFile);
  }

  public File getStartupFile() {
    return startupYMLFile;
  }

  public FileConfiguration getStartupData() {
    return startupYMLData;
  }

  public File getUpdaterFile() {
    return updaterYMLFile;
  }

  public FileConfiguration getUpdaterData() {
    return updaterYMLData;
  }

  public File getOverridesYMLFile() {
    return overridesYMLFile;
  }

  public FileConfiguration getOverridesYMLData() {
    return overridesYMLData;
  }

  public File getDefaultsYMLFile() {
    return defaultsYMLFile;
  }

  public FileConfiguration getDefaultsYMLData() {
    return defaultsYMLData;
  }

  public File getMysqlYMLFile() {
    return mysqlYMLFile;
  }

  public FileConfiguration getMysqlFileData() {
    return mysqlFileData;
  }

  public void reloadYMLFile(File file, FileConfiguration fileConfiguration, String pathToResource) {
    fileConfiguration = YamlConfiguration.loadConfiguration(file);

    InputStream inputStream = plugin.getResource(pathToResource);
    if (inputStream != null) {
      fileConfiguration.setDefaults(YamlConfiguration.loadConfiguration(new InputStreamReader(inputStream, Charsets.UTF_8)));
    }
  }
}
