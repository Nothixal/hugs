package me.nothixal.hugs.managers;

import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.types.DatabaseType;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.managers.data.types.H2PlayerDataManager;
import me.nothixal.hugs.managers.data.types.json.JSONPlayerDataManager;
import me.nothixal.hugs.managers.data.types.mariadb.MariaDBPlayerDataManager;
import me.nothixal.hugs.managers.data.types.mongodb.MongoDBPlayerDataManager;
import me.nothixal.hugs.managers.data.types.mysql.MySQLPlayerDataManager;
import me.nothixal.hugs.managers.data.types.sqlite.SQLitePlayerDataManager;
import me.nothixal.hugs.managers.data.types.yaml.YAMLPlayerDataManager;

public class DatabaseManager {

  private final HugsPlugin plugin;

  public DatabaseManager(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  /**
   * <p>Loads the storage method defined in the config.yml</p>
   * <p>If the plugin cannot properly determine which storage method has been configured,
   * it will default to YAML.</p>
   * */
  public void initializeDatabase() {
    String databaseType = plugin.getConfig().getString("storage", "yaml").toUpperCase();

    DatabaseType database = DatabaseType.valueOf(databaseType);

    switch (database) {
      case JSON:
        plugin.setPlayerDataManager(new JSONPlayerDataManager());
        break;
      case MYSQL:
        plugin.setPlayerDataManager(new MySQLPlayerDataManager(plugin));
        break;
      case MARIADB:
        plugin.setPlayerDataManager(new MariaDBPlayerDataManager());
        break;
      case MONGODB:
        plugin.setPlayerDataManager(new MongoDBPlayerDataManager());
        break;
      case H2:
        plugin.setPlayerDataManager(new H2PlayerDataManager());
        break;
      case SQLITE:
        plugin.setPlayerDataManager(new SQLitePlayerDataManager(plugin));
        break;
      default:
        plugin.setPlayerDataManager(new YAMLPlayerDataManager(plugin));
    }
  }

}
