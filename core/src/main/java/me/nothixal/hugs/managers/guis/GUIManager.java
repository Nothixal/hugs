package me.nothixal.hugs.managers.guis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.guis.CommandsGUI;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.guis.LeaderboardGUI;
import me.nothixal.hugs.guis.PluginArchivesGUI;
import me.nothixal.hugs.guis.adminpanel.AdminPanelGUI;
import me.nothixal.hugs.guis.adminpanel.AdvancedSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.ExperimentalSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.GeneralSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.SelectLanguageGUI;
import me.nothixal.hugs.guis.adminpanel.TechnicalSettingsGUI;
import me.nothixal.hugs.guis.adminpanel.defaults.DefaultIndicatorsGUI;
import me.nothixal.hugs.guis.adminpanel.defaults.DefaultSoundsGUI;
import me.nothixal.hugs.guis.adminpanel.defaults.DefaultVisualsGUI;
import me.nothixal.hugs.guis.adminpanel.defaults.DefaultsGUI;
import me.nothixal.hugs.guis.adminpanel.overrides.OverrideIndicatorsGUI;
import me.nothixal.hugs.guis.adminpanel.overrides.OverrideSoundsGUI;
import me.nothixal.hugs.guis.adminpanel.overrides.OverrideVisualsGUI;
import me.nothixal.hugs.guis.adminpanel.overrides.OverridesGUI;
import me.nothixal.hugs.guis.settings.backend.BroadcastSettingsGUI;
import me.nothixal.hugs.guis.settings.player.YourIndicatorsGUI;
import me.nothixal.hugs.guis.settings.player.YourPreferencesGUI;
import me.nothixal.hugs.guis.settings.player.YourSoundsGUI;
import me.nothixal.hugs.guis.settings.player.YourVisualsGUI;
import me.nothixal.hugs.guis.stats.ErosStatsGUI;
import me.nothixal.hugs.guis.stats.GlobalStatsGUI;
import me.nothixal.hugs.guis.stats.PlayerStatsGUI;
import me.nothixal.hugs.guis.stats.SilviaStatsGUI;
import me.nothixal.hugs.guis.stats.YourStatsGUI;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.text.TextUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.PluginManager;

public class GUIManager {

  private final HugsPlugin plugin;
  private List<Listener> pluginGUIs = new ArrayList<>();
  private Map<UUID, Inventory> helpGUIs = new HashMap<>();
  private Map<UUID, Inventory> leaderboardGUIs = new HashMap<>();
  private Map<UUID, Inventory> archivesGUI = new HashMap<>();
  private Map<UUID, Inventory> yourStatsGUIs = new HashMap<>();
  private Map<UUID, Inventory> yourPreferencesGUIs = new HashMap<>();
  private Map<UUID, Inventory> yourIndicatorGUIs = new HashMap<>();
  private Map<UUID, Inventory> yourSoundsGUIs = new HashMap<>();
  private Map<UUID, Inventory> yourVisualsGUIs = new HashMap<>();

  private Map<UUID, Inventory> playerStatsGUIs = new HashMap<>();
  private Map<UUID, Inventory> commandsGUIs = new HashMap<>();

  private Map<UUID, Inventory> adminPanelGUIs = new HashMap<>();
  private Map<UUID, Inventory> globalStatsGUIs = new HashMap<>();

  private Map<UUID, Inventory> generalSettingsGUIs = new HashMap<>();
  private Map<UUID, Inventory> advancedSettingsGUIs = new HashMap<>();
  private Map<UUID, Inventory> technicalSettingsGUIs = new HashMap<>();

  private Map<UUID, Inventory> defaultsSettingsGUIs = new HashMap<>();
  private Map<UUID, Inventory> defaultsSettingsVisualsGUIs = new HashMap<>();
  private Map<UUID, Inventory> defaultsSettingsSoundsGUIs = new HashMap<>();
  private Map<UUID, Inventory> defaultsSettingsIndicatorsGUIs = new HashMap<>();

  private Map<UUID, Inventory> overridesSettingsGUIs = new HashMap<>();
  private Map<UUID, Inventory> overridesSettingsVisualsGUIs = new HashMap<>();
  private Map<UUID, Inventory> overridesSettingsSoundsGUIs = new HashMap<>();
  private Map<UUID, Inventory> overridesSettingsIndicatorsGUIs = new HashMap<>();

  private Map<UUID, Inventory> experimentalSettingsGUIs = new HashMap<>();

  private Map<UUID, Inventory> temporaryGUIs = new HashMap<>();

  private Map<UUID, Inventory> erosStatsGUIs = new HashMap<>();
  private Map<UUID, Inventory> silviaStatsGUIs = new HashMap<>();

  private Inventory pluginSettingsGUI;
  private Inventory broadcastSettingsGUI;

  private String guiPrefix = PluginConstants.DEFAULT_PREFIX_COLOR_BOLD + "Hugs &8&l>> ";

  public GUIManager(HugsPlugin plugin) {
    this.plugin = plugin;
    pluginSettingsGUI = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(guiPrefix + "&8Plugin Settings"));

    broadcastSettingsGUI = Bukkit.createInventory(null, 9 * 6, TextUtils.colorText(guiPrefix + "&8Broadcast Settings"));
  }

  public void addGUIs() {
    // Plugin Help Menu
    pluginGUIs.add(new HelpGUI(plugin));
    pluginGUIs.add(new LeaderboardGUI(plugin));
    pluginGUIs.add(new CommandsGUI(plugin));
    pluginGUIs.add(new YourStatsGUI(plugin));
    pluginGUIs.add(new GlobalStatsGUI(plugin));
    pluginGUIs.add(new PlayerStatsGUI(plugin));
    pluginGUIs.add(new PluginArchivesGUI(plugin));
    pluginGUIs.add(new YourPreferencesGUI(plugin));
    pluginGUIs.add(new YourIndicatorsGUI(plugin));
    pluginGUIs.add(new SelectLanguageGUI(plugin));
    pluginGUIs.add(new YourSoundsGUI(plugin));
    pluginGUIs.add(new YourVisualsGUI(plugin));

    // Plugin Settings GUIs
    pluginGUIs.add(new AdminPanelGUI(plugin));
    pluginGUIs.add(new BroadcastSettingsGUI(plugin));

    pluginGUIs.add(new GeneralSettingsGUI(plugin));
    pluginGUIs.add(new AdvancedSettingsGUI(plugin));
    pluginGUIs.add(new TechnicalSettingsGUI(plugin));

    pluginGUIs.add(new DefaultsGUI(plugin));
    pluginGUIs.add(new DefaultVisualsGUI(plugin));
    pluginGUIs.add(new DefaultSoundsGUI(plugin));
    pluginGUIs.add(new DefaultIndicatorsGUI(plugin));

    pluginGUIs.add(new OverridesGUI(plugin));
    pluginGUIs.add(new OverrideVisualsGUI(plugin));
    pluginGUIs.add(new OverrideSoundsGUI(plugin));
    pluginGUIs.add(new OverrideIndicatorsGUI(plugin));

    pluginGUIs.add(new ExperimentalSettingsGUI(plugin));

    // Lovebird Stats GUIs
    pluginGUIs.add(new ErosStatsGUI(plugin));
    pluginGUIs.add(new SilviaStatsGUI(plugin));
  }

  public void registerListeners(PluginManager pluginManager) {
    for (Listener listener : pluginGUIs) {
      pluginManager.registerEvents(listener, plugin);
    }
  }

  public Inventory getPluginSettingsGUI() {
    return pluginSettingsGUI;
  }

  public Inventory getBroadcastSettingsGUI() {
    return broadcastSettingsGUI;
  }

  public Map<UUID, Inventory> getHelpGUIs() {
    return helpGUIs;
  }

  public Map<UUID, Inventory> getLeaderboardGUIs() {
    return leaderboardGUIs;
  }

  public Map<UUID, Inventory> getArchivesGUI() {
    return archivesGUI;
  }

  public Map<UUID, Inventory> getYourPreferencesGUIs() {
    return yourPreferencesGUIs;
  }

  public Map<UUID, Inventory> getYourStatsGUIs() {
    return yourStatsGUIs;
  }

  public Map<UUID, Inventory> getYourIndicatorGUIs() {
    return yourIndicatorGUIs;
  }

  public Map<UUID, Inventory> getPlayerStatsGUIs() {
    return playerStatsGUIs;
  }

  public Map<UUID, Inventory> getCommandsGUIs() {
    return commandsGUIs;
  }

  public Map<UUID, Inventory> getTemporaryGUIs() {
    return temporaryGUIs;
  }

  public Map<UUID, Inventory> getErosStatsGUIs() {
    return erosStatsGUIs;
  }

  public Map<UUID, Inventory> getSilviaStatsGUIs() {
    return silviaStatsGUIs;
  }

  public String getGuiPrefix() {
    return guiPrefix;
  }

  public Map<UUID, Inventory> getYourSoundsGUIs() {
    return yourSoundsGUIs;
  }

  public Map<UUID, Inventory> getAdminPanelGUIs() {
    return adminPanelGUIs;
  }

  public Map<UUID, Inventory> getGlobalStatsGUIs() {
    return globalStatsGUIs;
  }

  public Map<UUID, Inventory> getGeneralSettingsGUIs() {
    return generalSettingsGUIs;
  }

  public Map<UUID, Inventory> getAdvancedSettingsGUIs() {
    return advancedSettingsGUIs;
  }

  public Map<UUID, Inventory> getTechnicalSettingsGUIs() {
    return technicalSettingsGUIs;
  }

  public Map<UUID, Inventory> getDefaultsSettingsGUIs() {
    return defaultsSettingsGUIs;
  }

  public Map<UUID, Inventory> getOverridesSettingsGUIs() {
    return overridesSettingsGUIs;
  }

  public Map<UUID, Inventory> getExperimentalSettingsGUIs() {
    return experimentalSettingsGUIs;
  }

  public Map<UUID, Inventory> getYourVisualsGUIs() {
    return yourVisualsGUIs;
  }

  public void clearCaches() {
    helpGUIs.clear();
    leaderboardGUIs.clear();
    archivesGUI.clear();
    yourStatsGUIs.clear();
    yourPreferencesGUIs.clear();
    yourIndicatorGUIs.clear();
    yourSoundsGUIs.clear();
    playerStatsGUIs.clear();
    commandsGUIs.clear();
    adminPanelGUIs.clear();
    globalStatsGUIs.clear();
    generalSettingsGUIs.clear();
    advancedSettingsGUIs.clear();
    technicalSettingsGUIs.clear();
    defaultsSettingsGUIs.clear();
    overridesSettingsGUIs.clear();
    experimentalSettingsGUIs.clear();
    temporaryGUIs.clear();
  }

  public Map<UUID, Inventory> getDefaultsSettingsVisualsGUIs() {
    return defaultsSettingsVisualsGUIs;
  }

  public Map<UUID, Inventory> getDefaultsSettingsSoundsGUIs() {
    return defaultsSettingsSoundsGUIs;
  }

  public Map<UUID, Inventory> getDefaultsSettingsIndicatorsGUIs() {
    return defaultsSettingsIndicatorsGUIs;
  }

  public Map<UUID, Inventory> getOverridesSettingsVisualsGUIs() {
    return overridesSettingsVisualsGUIs;
  }

  public Map<UUID, Inventory> getOverridesSettingsSoundsGUIs() {
    return overridesSettingsSoundsGUIs;
  }

  public Map<UUID, Inventory> getOverridesSettingsIndicatorsGUIs() {
    return overridesSettingsIndicatorsGUIs;
  }
}
