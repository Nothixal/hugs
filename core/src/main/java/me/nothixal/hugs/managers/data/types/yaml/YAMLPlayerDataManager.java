package me.nothixal.hugs.managers.data.types.yaml;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.ConfigValues;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.enums.preferences.HuggableState;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.enums.preferences.ParticleQuality;
import me.nothixal.hugs.enums.types.DatabaseType;
import me.nothixal.hugs.enums.types.HugType;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.utils.Indicator;
import me.nothixal.hugs.utils.Interaction;
import me.nothixal.hugs.utils.cooldowns.Cooldown;
import org.bukkit.configuration.file.FileConfiguration;

public class YAMLPlayerDataManager implements PlayerDataManager {

  private final HugsPlugin plugin;

  private final Map<UUID, YAMLPlayerData> dataCache = new HashMap<>();

  public YAMLPlayerDataManager(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @Override
  public boolean playerExists(UUID uuid) {
//    System.out.println("  #playerExists() --> Entering method.");
    if (dataCache.containsKey(uuid)) {
//      System.out.println("  Data cache contains the user. Exiting method.");
      return true;
    }

    File file = new File(plugin.getFileManager().getPlayerDataDirectory(), uuid + ".yml");
//    System.out.println("  File Exists: " + file.exists() + ":" + file.getName());
//
//    System.out.println("  #playerExists() --> Exiting method.");
    return file.exists();
  }

  @Override
  public void createPlayer(UUID uuid) {
    dataCache.computeIfAbsent(uuid, k -> new YAMLPlayerData(plugin, k));

//    System.out.println("  #createPlayer() --> Creating Player!");
//    System.out.println(dataCache.get(uuid));
//    System.out.println("  #createPlayer() --> Exiting method.");
  }

  @Override
  public void deletePlayer(UUID uuid) {
    dataCache.get(uuid).deletePlayerFile();
    dataCache.remove(uuid);
  }

  @Override
  public void reloadPlayer(UUID uuid) {
    dataCache.remove(uuid);
    loadPlayer(uuid);
  }

  @Override
  public void setDefaults(UUID uuid) {

  }

  @Override
  public void loadPlayer(UUID uuid) {
//    System.out.println("  #loadPlayer() --> Loading Player...");
    dataCache.computeIfAbsent(uuid, k -> new YAMLPlayerData(plugin, k));
//    dataCache.put(uuid, new YAMLPlayerData(plugin, uuid));
//    System.out.println("  #loadPlayer() --> Exiting method.");
  }

  @Override
  public void loadDatabase() {
    File playerDataDirectory = new File(plugin.getFileManager().getPlayerDataDirectory());

    File[] files = playerDataDirectory.listFiles();

    if (files == null) {
      return;
    }

    for (File file : files) {
      if (file.isDirectory()) {
        continue;
      }

      if (!file.getName().endsWith(".yml")) {
        continue;
      }

      UUID uuid = UUID.fromString(file.getName().replace(".yml", ""));

      YAMLPlayerData playerData = new YAMLPlayerData(plugin, uuid);

      dataCache.put(uuid, playerData);
    }
  }

  @Override
  public void reloadDatabase() {
    saveDatabase();
    dataCache.clear();
    loadDatabase();
  }

  @Override
  public void saveDatabase() {
    for (YAMLPlayerData playerData : dataCache.values()) {
      if (!playerData.hasBeenModified()) {
        continue;
      }

      playerData.saveDataFile();
    }
  }

  @Override
  public void purgeDatabase() {
    dataCache.clear();
    plugin.getFileManager().deleteDirectory(new File(plugin.getFileManager().getPlayerDataDirectory()));
    plugin.getFileManager().createDirectory(new File(plugin.getFileManager().getPlayerDataDirectory()));
  }

  @Override
  public void loadData(UUID uuid) {

  }

  @Override
  public void saveData(UUID uuid) {
    dataCache.get(uuid).saveDataFile();
    dataCache.remove(uuid);
  }

  @Override
  public void reloadData(UUID uuid) {

  }

  @Override
  public void purgeData(UUID uuid) {
    dataCache.get(uuid).deletePlayerFile();
    dataCache.remove(uuid);
  }

  @Override
  public void addHugs(UUID sender, UUID receiver, HugType type, long timestamp) {

  }

  @Override
  public int getSelfHugs(UUID uuid) {
    return getPlayerConfig(uuid).getInt(ConfigValues.SELF_HUGS.getPath());
  }

  @Override
  public void setSelfHugs(UUID uuid, int value) {
    getPlayerConfig(uuid).set(ConfigValues.SELF_HUGS.getPath(), value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementSelfHugs(UUID uuid) {
    getPlayerConfig(uuid).set(ConfigValues.SELF_HUGS.getPath(), getSelfHugs(uuid) + 1);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementSelfHugs(UUID uuid, int amount) {
    getPlayerConfig(uuid).set(ConfigValues.SELF_HUGS.getPath(), getSelfHugs(uuid) + amount);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getHugsGiven(UUID uuid) {
    return getPlayerConfig(uuid).getInt(ConfigValues.HUGS_GIVEN.getPath());
  }

  @Override
  public void setHugsGiven(UUID uuid, int value) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_GIVEN.getPath(), value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementHugsGiven(UUID uuid) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_GIVEN.getPath(), getHugsGiven(uuid) + 1);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementHugsGiven(UUID uuid, int amount) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_GIVEN.getPath(), getHugsGiven(uuid) + amount);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getHugsReceived(UUID uuid) {
    return getPlayerConfig(uuid).getInt(ConfigValues.HUGS_RECEIVED.getPath());
  }

  @Override
  public void setHugsReceived(UUID uuid, int value) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_RECEIVED.getPath(), value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementHugsReceived(UUID uuid) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_RECEIVED.getPath(), getHugsReceived(uuid) + 1);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementHugsReceived(UUID uuid, int amount) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_RECEIVED.getPath(), getHugsReceived(uuid) + amount);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getMassHugsGiven(UUID uuid) {
    return getPlayerConfig(uuid).getInt(ConfigValues.MASS_HUGS_GIVEN.getPath());
  }

  @Override
  public void setMassHugsGiven(UUID uuid, int value) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_GIVEN.getPath(), value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementMassHugsGiven(UUID uuid) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_GIVEN.getPath(), getMassHugsGiven(uuid) + 1);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementMassHugsGiven(UUID uuid, int amount) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_GIVEN.getPath(), getMassHugsGiven(uuid) + amount);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getMassHugsReceived(UUID uuid) {
    return getPlayerConfig(uuid).getInt(ConfigValues.MASS_HUGS_RECEIVED.getPath());
  }

  @Override
  public void setMassHugsReceived(UUID uuid, int value) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_RECEIVED.getPath(), value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementMassHugsReceived(UUID uuid) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_RECEIVED.getPath(), getMassHugsReceived(uuid) + 1);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementMassHugsReceived(UUID uuid, int amount) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_RECEIVED.getPath(), getMassHugsReceived(uuid) + amount);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getTotalSelfHugsGiven() {
    return plugin.getDataFile().getInt("total_self_hugs_given");
  }

  @Override
  public int getTotalHugsGiven() {
    return plugin.getDataFile().getInt("total_hugs_given");
  }

  @Override
  public int getTotalMassHugsGiven() {
    return plugin.getDataFile().getInt("total_mass_hugs_given");
  }

  @Override
  public boolean wantsHugs(UUID uuid) {
    HuggableState state = getHuggability(uuid);
    return state == HuggableState.ALL;
  }

  @Override
  public boolean wantsRealisticHugs(UUID uuid) {
    HuggableState state = getHuggability(uuid);
    return state == HuggableState.REALISTIC;
  }

  @Override
  public HuggableState getHuggability(UUID uuid) {
    return HuggableState.valueOf(getPlayerConfig(uuid).getString("settings.huggability").toUpperCase());
  }

  @Override
  public void setHuggability(UUID uuid, HuggableState state) {
    getPlayerConfig(uuid).set("settings.huggability", state.getValue());
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public boolean wantsParticles(UUID uuid) {
    return getPlayerConfig(uuid).getBoolean(ConfigValues.WANTS_PARTICLES.getPath());
  }

  @Override
  public void setWantsParticles(UUID uuid, boolean bool) {
    getPlayerConfig(uuid).set(ConfigValues.WANTS_PARTICLES.getPath(), bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public String getParticleEffect(UUID uuid) {
    return getPlayerConfig(uuid).getString("settings.particles.effect");
  }

  @Override
  public void setParticleEffect(UUID uuid, String value) {
    getPlayerConfig(uuid).set("settings.particles.effect", value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public ParticleQuality getParticleQuality(UUID uuid) {
    return ParticleQuality.valueOf(getPlayerConfig(uuid).getString("settings.particles.quality", "low").toUpperCase());
  }

  @Override
  public void setParticleQuality(UUID uuid, ParticleQuality state) {
    getPlayerConfig(uuid).set("settings.particles.quality", state.name().toLowerCase());
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void increaseParticleQuality(UUID uuid) {
    setParticleQuality(uuid, ParticleQuality.valueOf(getParticleQuality(uuid).getNext().name()));
  }

  @Override
  public void decreaseParticleQuality(UUID uuid) {
    setParticleQuality(uuid, ParticleQuality.valueOf(getParticleQuality(uuid).getPrevious().name()));
  }

  @Override
  public boolean wantsHugSounds(UUID uuid) {
    return getPlayerConfig(uuid).getBoolean(ConfigValues.HUG_SOUNDS.getPath());
  }

  @Override
  public void setWantsHugSounds(UUID uuid, boolean bool) {
    getPlayerConfig(uuid).set(ConfigValues.HUG_SOUNDS.getPath(), bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public String getSoundEffect(UUID uuid) {
    return getPlayerConfig(uuid).getString("settings.sounds.effect", "ENTITY_CAT_PURR");
  }

  @Override
  public boolean wantsMenuSounds(UUID uuid) {
    return getPlayerConfig(uuid).getBoolean("settings.sounds.menu");
  }

  @Override
  public void setWantsMenuSounds(UUID uuid, boolean bool) {
    getPlayerConfig(uuid).set("settings.sounds.menu", bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public boolean wantsCommandSounds(UUID uuid) {
    return getPlayerConfig(uuid).getBoolean("settings.sounds.commands");
  }

  @Override
  public void setWantsCommandSounds(UUID uuid, boolean bool) {
    getPlayerConfig(uuid).set("settings.sounds.commands", bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public boolean hasGivenHugsBefore(UUID uuid) {
    boolean excluded = plugin.getConfig().getBoolean(Settings.EXCLUDE_SELF_HUGS_FROM_STATS.getValue());

    if (excluded) {
      return getHugsGiven(uuid) != 0 || getMassHugsGiven(uuid) != 0;
    } else {
      return getSelfHugs(uuid) != 0 || getHugsGiven(uuid) != 0 || getMassHugsGiven(uuid) != 0;
    }
  }

  @Override
  public boolean hasReceivedHugsBefore(UUID uuid) {
    return getHugsReceived(uuid) != 0 || getMassHugsReceived(uuid) != 0;
  }

  @Override
  public List<Indicator> getIndicators(UUID uuid) {
    List<Indicator> indicatorsList = new ArrayList<>();
    indicatorsList.add(new Indicator(1, IndicatorType.CHAT, getPlayerConfig(uuid).getBoolean(IndicatorType.CHAT.getPath())));
    indicatorsList.add(new Indicator(2, IndicatorType.TITLES, getPlayerConfig(uuid).getBoolean(IndicatorType.TITLES.getPath())));
    indicatorsList.add(new Indicator(3, IndicatorType.ACTIONBAR, getPlayerConfig(uuid).getBoolean(IndicatorType.ACTIONBAR.getPath())));
    indicatorsList.add(new Indicator(4, IndicatorType.BOSSBAR, getPlayerConfig(uuid).getBoolean(IndicatorType.BOSSBAR.getPath())));
    indicatorsList.add(new Indicator(5, IndicatorType.TOASTS, getPlayerConfig(uuid).getBoolean(IndicatorType.TOASTS.getPath())));
    return indicatorsList;
  }

  @Override
  public boolean getIndicator(UUID uuid, IndicatorType type) {
    return getPlayerConfig(uuid).getBoolean("settings.indicators." + type.getValue());
  }

  @Override
  public void setIndicator(UUID uuid, IndicatorType type, boolean enabled) {
    getPlayerConfig(uuid).set("settings.indicators." + type.getValue(), enabled);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public DatabaseType getDatabaseType() {
    return DatabaseType.YAML;
  }

  @Override
  public String getFirstHugGivenTo(UUID uuid) {
    return getPlayerConfig(uuid).getString("data.first_hug.given.to");
  }

  @Override
  public void setFirstHugGivenTo(UUID sender, UUID receiver) {
    getPlayerConfig(sender).set("data.first_hug.given.to", receiver.toString());
    getPlayerData(sender).setHasBeenModified(true);
  }

  @Override
  public long getFirstHugGivenTimestamp(UUID uuid) {
    return getPlayerConfig(uuid).getLong("data.first_hug.given.timestamp");
  }

  @Override
  public void setFirstHugGivenTimestamp(UUID uuid, long timestamp) {
    getPlayerConfig(uuid).set("data.first_hug.given.timestamp", timestamp);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public String getFirstHugReceivedFrom(UUID uuid) {
    return getPlayerConfig(uuid).getString("data.first_hug.received.from");
  }

  @Override
  public void setFirstHugReceivedFrom(UUID sender, UUID receiver) {
    getPlayerConfig(sender).set("data.first_hug.received.from", receiver.toString());
    getPlayerData(sender).setHasBeenModified(true);
  }

  @Override
  public long getFirstHugReceivedTimestamp(UUID uuid) {
    return getPlayerConfig(uuid).getLong("data.first_hug.received.timestamp");
  }

  @Override
  public void setFirstHugReceivedTimestamp(UUID uuid, long timestamp) {
    getPlayerConfig(uuid).set("data.first_hug.received.timestamp", timestamp);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  //
  // Last Hugs
  //
  @Override
  public String getLastHugGivenTo(UUID uuid) {
    return getPlayerConfig(uuid).getString("data.last_hug.given.to");
  }

  @Override
  public void setLastHugGivenTo(UUID sender, UUID receiver) {
    getPlayerConfig(sender).set("data.last_hug.given.to", receiver.toString());
    getPlayerData(sender).setHasBeenModified(true);
  }

  @Override
  public long getLastHugGivenTimestamp(UUID uuid) {
    return getPlayerConfig(uuid).getLong("data.last_hug.given.timestamp");
  }

  @Override
  public void setLastHugGivenTimestamp(UUID uuid, long timestamp) {
    getPlayerConfig(uuid).set("data.last_hug.given.timestamp", timestamp);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public String getLastHugReceivedFrom(UUID uuid) {
    return getPlayerConfig(uuid).getString("data.last_hug.received.from");
  }

  @Override
  public void setLastHugReceivedFrom(UUID sender, UUID receiver) {
    getPlayerConfig(sender).set("data.last_hug.received.from", receiver.toString());
    getPlayerData(sender).setHasBeenModified(true);
  }

  @Override
  public long getLastHugReceivedTimestamp(UUID uuid) {
    return getPlayerConfig(uuid).getLong("data.last_hug.received.timestamp");
  }

  @Override
  public void setLastHugReceivedTimestamp(UUID uuid, long timestamp) {
    getPlayerConfig(uuid).set("data.last_hug.received.timestamp", timestamp);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getUniqueHugsGiven(UUID uuid) {
    return getPlayerConfig(uuid).getStringList("data.unique_hugs.given").size();
  }

  @Override
  public List<String> getUniqueHugsGivenAsList(UUID uuid) {
    return getPlayerConfig(uuid).getStringList("data.unique_hugs.given");
  }

  @Override
  public void setUniqueHugsGiven(UUID sender, UUID receiver, int amount) {
    List<String> uniquePlayers = getPlayerConfig(sender).getStringList("data.unique_hugs.given");

    uniquePlayers.add(receiver.toString());

    getPlayerConfig(sender).set("data.unique_hugs.given", uniquePlayers);
    getPlayerData(sender).setHasBeenModified(true);
  }

  @Override
  public int getUniqueHugsReceived(UUID uuid) {
    return getPlayerConfig(uuid).getStringList("data.unique_hugs.received").size();
  }

  @Override
  public List<String> getUniqueHugsReceivedAsList(UUID uuid) {
    return getPlayerConfig(uuid).getStringList("data.unique_hugs.received");
  }

  @Override
  public void setUniqueHugsReceived(UUID sender, UUID receiver, int amount) {
    List<String> uniquePlayers = getPlayerConfig(sender).getStringList("data.unique_hugs.received");

    uniquePlayers.add(receiver.toString());

    getPlayerConfig(sender).set("data.unique_hugs.received", uniquePlayers);
    getPlayerData(sender).setHasBeenModified(true);
  }

  @Override
  public void addUniqueHugGiven(UUID player, UUID receiver) {
    List<String> uniqueHugsGiven = getUniqueHugsGivenAsList(player);
    uniqueHugsGiven.add(receiver.toString());

    getPlayerConfig(player).set("data.unique_hugs.given", uniqueHugsGiven);
    getPlayerData(player).setHasBeenModified(true);
  }

  @Override
  public List<Interaction> getRecentHugs(UUID player) {
    List<Interaction> interactions = new ArrayList<>();
    List<String> data = getPlayerConfig(player).getStringList("data.recent_hugs.given");

    for (String s : data) {
      String[] values = s.split(":");

      UUID sender = UUID.fromString(values[0]);
      UUID receiver = UUID.fromString(values[1]);
      HugType type = HugType.valueOf(values[2]);
      long timestamp = Long.parseLong(values[3]);

      interactions.add(new Interaction(sender, receiver, type, timestamp));
    }

    return interactions;
  }

  @Override
  public List<Interaction> getRecentHugs(UUID player, boolean allStats) {
    return null;
  }

  @Override
  public void addRecentHugGiven(UUID sender, UUID receiver, HugType type, long timestamp) {
    if (plugin.getConfig().getBoolean(Settings.EXCLUDE_SELF_HUGS_FROM_STATS.getValue())) {
      return;
    }

    List<String> test = getPlayerConfig(sender).getStringList("data.recent_hugs.given");

    test.add(0, sender + ":" + receiver + ":" + type.name() + ":" + timestamp);

    if (test.size() > 5) {
      test.remove(test.size() - 1);
    }

    getPlayerConfig(sender).set("data.recent_hugs.given", test);
    getPlayerData(sender).setHasBeenModified(true);
  }

  @Override
  public boolean prefersMenus(UUID uuid) {
    return getPlayerConfig(uuid).getBoolean("settings.prefers_menus");
  }

  @Override
  public void setPrefersMenus(UUID uuid, boolean value) {
    getPlayerConfig(uuid).set("settings.prefers_menus", value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public List<Cooldown> getCooldowns(UUID player) {
    List<Cooldown> cooldowns = new ArrayList<>();
    List<String> data = getPlayerConfig(player).getStringList("data.cooldowns");

    for (String s : data) {
      String[] values = s.split(":");

      UUID receiver = UUID.fromString(values[0]);
      HugType type = HugType.valueOf(values[1]);
      long timestamp = Long.parseLong(values[2]);
      long duration = 0;

      if (type == HugType.SELF) {
        duration = plugin.getConfig().getLong("settings.cooldowns.self_hugs") * 1000L;
      } else if (type == HugType.NORMAL) {
        duration = plugin.getConfig().getLong("settings.cooldowns.normal_hugs") * 1000L;
      } else if (type == HugType.MASS) {
        duration = plugin.getConfig().getLong("settings.cooldowns.mass_hugs") * 1000L;
      }

      duration = duration - (System.currentTimeMillis() - timestamp);

//      System.out.println(duration);

      cooldowns.add(new Cooldown(player, receiver, type, duration, timestamp));
    }

//    System.out.println("Cached Cooldown List #getCooldowns()");
//    for (Cooldown cooldown : cooldowns) {
//      System.out.println(cooldown.getSender() + ":" + cooldown.getReceiver() + ":" + cooldown.getStart() + ":" + cooldown.getDuration());
//    }
//    System.out.println("-----------------");

    return cooldowns;
  }

  @Override
  public Cooldown getCooldown(UUID player, UUID target) {
    List<Cooldown> cooldowns = getCooldowns(player);

    for (Cooldown cooldown : cooldowns) {
      if (cooldown.getSender().equals(player) && cooldown.getReceiver().equals(target)) {
        return cooldown;
      }
    }

    return null;
  }

  @Override
  public void addCooldown(UUID player, UUID target, HugType type, long duration, long start) {
    List<String> cooldowns = getPlayerConfig(player).getStringList("data.cooldowns");

//    System.out.println("#addCooldown() --------------");
//    for (String s : cooldowns) {
//      System.out.println(s);
//    }
//    System.out.println("-----------------------------");

    cooldowns.add(target + ":" + type.name() + ":" + start);

//    System.out.println("#addCooldown() 2nd --------------");
//    for (String s : cooldowns) {
//      System.out.println(s);
//    }
//    System.out.println("---------------------------------");

    getPlayerConfig(player).set("data.cooldowns", cooldowns);
    getPlayerData(player).setHasBeenModified(true);

//    System.out.println("Cached Cooldown List #add()");
//    for (String s : cooldowns) {
//      System.out.println(s);
//    }
//    System.out.println("-----------------");
  }

  @Override
  public void removeCooldown(UUID player, UUID target) {
    Cooldown cooldown = getCooldown(player, target);

    if (cooldown == null) {
      return;
    }

    List<Cooldown> cooldowns = getCooldowns(player);

    cooldowns.remove(cooldown);

    getPlayerConfig(player).set("data.cooldowns", cooldowns);
    getPlayerData(player).setHasBeenModified(true);
  }

  private void test() {
    List<UUID> players = new ArrayList<>();

    File playerDataDirectory = new File(plugin.getFileManager().getPlayerDataDirectory());

    File[] files = playerDataDirectory.listFiles();

    if (files == null) {
      return;
    }

    for (File file : files) {
      if (file.isDirectory()) {
        continue;
      }

      if (!file.getName().endsWith(".yml")) {
        continue;
      }

      UUID uuid = UUID.fromString(file.getName().replace(".yml", ""));
      players.add(uuid);
    }

    Collections.sort(players);

    for (int i = 0; i < 5; i++) {

    }
  }

  private YAMLPlayerData getPlayerData(UUID uuid) {
    return dataCache.computeIfAbsent(uuid, k -> new YAMLPlayerData(plugin, k));
  }

  private FileConfiguration getPlayerConfig(UUID uuid) {
    return dataCache.computeIfAbsent(uuid, k -> new YAMLPlayerData(plugin, k)).getPlayerConfig();
  }

}
