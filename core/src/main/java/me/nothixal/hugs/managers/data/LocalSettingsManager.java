package me.nothixal.hugs.managers.data;

public interface LocalSettingsManager {

  boolean canCheckForUpdates();

  void setCanCheckForUpdates(boolean value);

  boolean passiveModeActive();

}
