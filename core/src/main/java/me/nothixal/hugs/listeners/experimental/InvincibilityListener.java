package me.nothixal.hugs.listeners.experimental;

import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class InvincibilityListener implements Listener {

  private final HugsPlugin plugin;
  private final List<Player> players = new ArrayList<>();

  public InvincibilityListener(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onEntityDamage(EntityDamageByEntityEvent event) {
    if ((!(event.getDamager() instanceof Player)) || (!(event.getEntity() instanceof Player))) {
      return;
    }

    Player damager = (Player) event.getDamager();
    Player damaged = (Player) event.getEntity();

    if (!plugin.getConfig().getBoolean(Settings.TEMPORARY_INVINCIBILITY_ENABLED.getValue())) {
      return;
    }

    if (!plugin.getConfig().getBoolean(Settings.CANCEL_PVP.getValue())) {
      return;
    }

    if (plugin.getHugManager().getInvinciblePlayers().containsKey(damaged)) {
      event.setCancelled(true);

      if (plugin.getConfig().getBoolean(Settings.PASSIVE_MODE_ENABLED.getValue())) {
        return;
      }

      damager.sendMessage(ChatUtils.colorChat("&7You cannot harm that player!"));
    }

    if (!players.contains(damaged)) {
      players.add(damaged);

      new BukkitRunnable() {

        @Override
        public void run() {
          players.remove(damaged);
          plugin.getHugManager().getInvinciblePlayers().remove(damaged);
          cancel();
        }
      }.runTaskLater(plugin, 20 * plugin.getConfig().getInt(Settings.TEMPORARY_INVINCIBILITY_DURATION.getValue()));
    }
  }

  @EventHandler
  public void onFallDamage(EntityDamageEvent event) {
    if (!(event.getEntity() instanceof Player)) {
      return;
    }

    Player player = (Player) event.getEntity();

    if (!(event.getCause() == EntityDamageEvent.DamageCause.FALL)) {
      return;
    }

    if (!plugin.getConfig().getBoolean(Settings.TEMPORARY_INVINCIBILITY_ENABLED.getValue())) {
      return;
    }

    if (!plugin.getConfig().getBoolean(Settings.CANCEL_FALL_DAMAGE.getValue())) {
      return;
    }

    if (plugin.getHugManager().getInvinciblePlayers().containsKey(player)) {
      event.setCancelled(true);

      if (!players.contains(player)) {
        players.add(player);

        new BukkitRunnable() {

          @Override
          public void run() {
            players.remove(player);
            plugin.getHugManager().getInvinciblePlayers().remove(player);
            cancel();
          }
        }.runTaskLater(plugin, 20 * plugin.getConfig().getInt(Settings.TEMPORARY_INVINCIBILITY_DURATION.getValue()));
      }
    }
  }

}
