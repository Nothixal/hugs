package me.nothixal.hugs.listeners;

import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.utils.updater.UpdateChecker.UpdateReason;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PluginUpdateListener implements Listener {

  private final HugsPlugin plugin;

  public PluginUpdateListener(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onJoin(PlayerJoinEvent event) {
    if (plugin.getPluginUpdater().getReason() != UpdateReason.NEW_UPDATE) {
      return;
    }

    Player player = event.getPlayer();

    if (!player.hasPermission(Permissions.UPDATE.getPermissionNode())) {
      return;
    }

    if (!plugin.getFileManager().getUpdaterData().getBoolean(Settings.SEND_UPDATES_TO_OPS.getValue())) {
      return;
    }

    plugin.getPluginUpdater().sendUpdateMessage(player);
  }

}
