package me.nothixal.hugs.listeners;

import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Filter;
import me.nothixal.hugs.enums.preferences.HuggableState;
import me.nothixal.hugs.enums.preferences.IndicatorType;
import me.nothixal.hugs.enums.preferences.ParticleQuality;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinListener implements Listener {

  private final HugsPlugin plugin;

  public JoinListener(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();

//    System.out.println("=-=-=-= Entering PlayerJoinEvent (" + event.getPlayer().getName() + ") =-=-=-=");
//    System.out.println(event.getPlayer().getName() + " has joined...");

    boolean exists = plugin.getPlayerDataManager().playerExists(player.getUniqueId());

//    System.out.println("Do they exist? --> " + exists);

    if (!exists) {
      plugin.getPlayerDataManager().createPlayer(player.getUniqueId());
    } else {
      plugin.getPlayerDataManager().loadPlayer(player.getUniqueId());
    }

    plugin.getFilterList().put(player.getUniqueId(), Filter.HUGS_GIVEN);

//    System.out.println(plugin.getPlayerDataManager().wantsHugs(player.getUniqueId()));
//    System.out.println("=-=-=-= Leaving PlayerJoinEvent =-=-=-=");
//    System.out.println(" ");
  }

  @EventHandler
  public void onLeave(PlayerQuitEvent event) {
    plugin.getPlayerDataManager().saveData(event.getPlayer().getUniqueId());

    plugin.getFilterList().remove(event.getPlayer().getUniqueId());
  }

  /*
  * Logic for first joins.
  * */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onFirstJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();

//    System.out.println("=-=-=-= Entering FirstJoinEvent (" + event.getPlayer().getName() + ") =-=-=-=");

    if (player.hasPlayedBefore()) {
//      System.out.println(player.getName() + " has played before. Exiting method.");
//      System.out.println("=-=-=-= Leaving FirstJoinEvent (" + event.getPlayer().getName() + ") =-=-=-=");
//      System.out.println(" ");
      return;
    }

//    System.out.println(player.getName() + " has not played before!");

    if (!plugin.getPlayerDataManager().playerExists(player.getUniqueId())) {
      plugin.getPlayerDataManager().createPlayer(player.getUniqueId());
    } else {
      plugin.getPlayerDataManager().loadPlayer(player.getUniqueId());
    }

    plugin.getFilterList().put(player.getUniqueId(), Filter.HUGS_GIVEN);

//    System.out.println("Setting up player defaults.");
    setupDefaults(player.getUniqueId());

//    System.out.println("=-=-=-= Leaving FirstJoinEvent (" + event.getPlayer().getName() + ") =-=-=-=");
//    System.out.println(" ");
  }

  private void setupDefaults(UUID uuid) {
//    String padding = "  ";
//    System.out.println(padding + "#setupDefaults() Start");

    FileConfiguration defaults = plugin.getFileManager().getDefaultsYMLData();

//    if (defaults == null) {
//      System.out.println(padding + "ERROR: Defaults file is null.");
//    }

    String huggable = defaults.getString("defaults.huggability", "all").toUpperCase();
    String particleQuality = defaults.getString("defaults.particle_quality", "low");
    boolean prefersMenus = defaults.getBoolean("defaults.prefers_menus");

    boolean wantsHugSounds = defaults.getBoolean("defaults.sounds.hugs");
    boolean wantsMenuSounds = defaults.getBoolean("defaults.sounds.menu");
    boolean wantsCommandSounds = defaults.getBoolean("defaults.sounds.commands");

    boolean chatIndicator = defaults.getBoolean("defaults.indicators.chat");
    boolean actionbarIndicator = defaults.getBoolean("defaults.indicators.actionbar");
    boolean titleIndicator = defaults.getBoolean("defaults.indicators.titles");
    boolean bossbarIndicator = defaults.getBoolean("defaults.indicators.bossbar");
    boolean toastIndicator = defaults.getBoolean("defaults.indicators.toasts");

//    System.out.println(padding + "Chat:       " + chatIndicator);
//    System.out.println(padding + "Actionbar:  " + actionbarIndicator);
//    System.out.println(padding + "Titles:     " + titleIndicator);
//    System.out.println(padding + "Bossbar:    " + bossbarIndicator);
//    System.out.println(padding + "Toast:      " + toastIndicator);

    // General
    plugin.getPlayerDataManager().setHuggability(uuid, HuggableState.valueOf(huggable));
    plugin.getPlayerDataManager().setParticleQuality(uuid, ParticleQuality.valueOf(particleQuality.toUpperCase()));
    plugin.getPlayerDataManager().setPrefersMenus(uuid, prefersMenus);

    // Sounds
    plugin.getPlayerDataManager().setWantsHugSounds(uuid, wantsHugSounds);
    plugin.getPlayerDataManager().setWantsMenuSounds(uuid, wantsMenuSounds);
    plugin.getPlayerDataManager().setWantsCommandSounds(uuid, wantsCommandSounds);

    // Indicators
    plugin.getPlayerDataManager().setIndicator(uuid, IndicatorType.CHAT, chatIndicator);
    plugin.getPlayerDataManager().setIndicator(uuid, IndicatorType.ACTIONBAR, actionbarIndicator);
    plugin.getPlayerDataManager().setIndicator(uuid, IndicatorType.TITLES, titleIndicator);
    plugin.getPlayerDataManager().setIndicator(uuid, IndicatorType.BOSSBAR, bossbarIndicator);
    plugin.getPlayerDataManager().setIndicator(uuid, IndicatorType.TOASTS, toastIndicator);



//    System.out.println("- - - - -");
//
//    System.out.println(padding + "Chat:       " + plugin.getPlayerDataManager().getIndicator(uuid, "chat"));
//    System.out.println(padding + "Actionbar:  " + plugin.getPlayerDataManager().getIndicator(uuid, "actionbar"));
//    System.out.println(padding + "Titles:     " + plugin.getPlayerDataManager().getIndicator(uuid, "titles"));
//    System.out.println(padding + "Bossbar:    " + plugin.getPlayerDataManager().getIndicator(uuid, "bossbar"));
//    System.out.println(padding + "Toast:      " + plugin.getPlayerDataManager().getIndicator(uuid, "toast"));
//
//    System.out.println(padding + "#setupDefaults() End");
  }
}
