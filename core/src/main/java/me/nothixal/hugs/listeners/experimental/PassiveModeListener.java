package me.nothixal.hugs.listeners.experimental;

import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PassiveModeListener implements Listener {

  private final HugsPlugin plugin;
  private List<Player> players = new ArrayList<>();

  public PassiveModeListener(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onEntityDamage(EntityDamageByEntityEvent event) {
    if ((!(event.getDamager() instanceof Player)) || (!(event.getEntity() instanceof Player))) {
      return;
    }

    Player player = (Player) event.getDamager();

    if (!plugin.getConfig().getBoolean(Settings.PASSIVE_MODE_ENABLED.getValue())) {
      return;
    }

    event.setCancelled(true);
    if (!players.contains(player)) {
      players.add(player);
      player.sendMessage(ChatUtils.colorChat(Messages.PASSIVE_MODE.getLangValue()));
      new BukkitRunnable() {

        @Override
        public void run() {
          players.remove(player);
          cancel();
        }
      }.runTaskLater(plugin, 20 * plugin.getConfig().getInt(Settings.PASSIVE_MODE_MESSAGE_DELAY.getValue()));
    }
  }

}
