package me.nothixal.hugs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import me.nothixal.hugs.commands.hug.HugAllCommand;
import me.nothixal.hugs.commands.hug.HugBackCommand;
import me.nothixal.hugs.commands.hug.HugCommand;
import me.nothixal.hugs.commands.hugs.OLDHugsCommand;
import me.nothixal.hugs.commands.hugs.NewHugsCommand;
import me.nothixal.hugs.enums.Filter;
import me.nothixal.hugs.enums.Languages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.enums.holidays.Holiday;
import me.nothixal.hugs.enums.holidays.HolidayOverride;
import me.nothixal.hugs.listeners.ChatConfirmationListener;
import me.nothixal.hugs.listeners.GUIListener;
import me.nothixal.hugs.listeners.InventoryClickListener;
import me.nothixal.hugs.listeners.JoinListener;
import me.nothixal.hugs.listeners.NewHugListener;
import me.nothixal.hugs.listeners.PluginUpdateListener;
import me.nothixal.hugs.listeners.ShiftHugListener;
import me.nothixal.hugs.listeners.experimental.InvincibilityListener;
import me.nothixal.hugs.listeners.experimental.PassiveModeListener;
import me.nothixal.hugs.managers.DatabaseManager;
import me.nothixal.hugs.managers.HookManager;
import me.nothixal.hugs.managers.HugManager;
import me.nothixal.hugs.managers.LocaleManager;
import me.nothixal.hugs.managers.VerboseManager;
import me.nothixal.hugs.managers.advancements.AdvancementManager;
import me.nothixal.hugs.managers.chat.ChatManager;
import me.nothixal.hugs.managers.data.LocalSettingsManager;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.managers.data.types.local.YAMLLocalSettingsManager;
import me.nothixal.hugs.managers.files.FileManager;
import me.nothixal.hugs.managers.guis.GUIManager;
import me.nothixal.hugs.managers.guis.NewGUIManager;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.HolidayChecker;
import me.nothixal.hugs.utils.ItemStates;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.VersionChecker;
import me.nothixal.hugs.utils.Watermark;
import me.nothixal.hugs.utils.cooldowns.CooldownManager;
import me.nothixal.hugs.utils.logger.LogUtils;
import me.nothixal.hugs.utils.updater.PluginUpdater;
import me.nothixal.hugs.utils.updater.UpdateChecker;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author ShadowMasterGaming
 * @created April 19th, 2017
 */
public class HugsPlugin extends JavaPlugin {

  private HugCommand hugCommand;
  private OLDHugsCommand OLDHugsCommand;

  private NewHugsCommand newHugsCommand;

  private Watermark watermark;

  private VersionChecker versionChecker;
  private VerboseManager verboseManager;
  private FileManager fileManager;
  private DatabaseManager databaseManager;
  private CooldownManager cooldownManager;
  private PlayerDataManager playerDataManager;
  private LocalSettingsManager localSettingsManager;
  private ChatManager chatManager;
  private ItemManager itemManager;
  private AdvancementManager advancementManager;
  private HookManager hookManager;
  private GUIManager guiManager;
  private NewGUIManager newGUIManager;
  private HugManager hugManager;

  private PluginUpdater pluginUpdater;

  private ItemStates itemStates;

  private boolean initialSetupFailed = false;

  private HolidayChecker holidayChecker;
  private Holiday holiday = Holiday.NONE;
  private HolidayOverride holidayOverride = HolidayOverride.NONE;

  private final Map<UUID, Filter> filterList = new HashMap<>();
  private LocaleManager localeManager;

  @Override
  public void onLoad() {
    registerRequiredClasses();
    versionChecker.checkServerVersion();
  }

  @Override
  public void onEnable() {
    registerManagers();

    // This annoys the fuck out of me.
    // This has to be called first in order to prevent the output from being thrown in the middle of the console output.
    hookManager.registerHooks();
    fileManager.checkForAndCreateFiles();

    watermark.setUsingRandomAsciiArt(getStartupConfig().getBoolean("verbose.ascii.random", true));
    watermark.setAsciiArtValue(getStartupConfig().getInt("verbose.ascii.type", 1));

    if (getStartupConfig().getBoolean("verbose.ascii.enabled")) {
      watermark.sendPluginPreEnableWatermark();
    }

    if (getStartupConfig().getBoolean("verbose.show_file_status")) {
      fileManager.printFileStatus();
    }

    saveResources("locale");

    if (!initialSetupSuccessful()) {
      initialSetupFailed = true;
      verboseManager.sendPluginFailure();
      getServer().getPluginManager().disablePlugin(this);
      return;
    }

    databaseManager.initializeDatabase();

    registerClasses();

    scheduleEvents();

    if (fileManager.getStartupData().getBoolean(Settings.SHOW_HOOKS.getValue())) {
      hookManager.printHooks();
    }

    playerDataManager.loadDatabase();

    registerCommands();
    registerListeners();

    printVerbose();

    watermark.sendPluginEnableWatermark();

    checkForUpdates();
  }

  @Override
  public void onDisable() {
    if (!initialSetupFailed) {
      hugManager.getInvinciblePlayers().clear();
      OLDHugsCommand.getTyping().clear();
      OLDHugsCommand.getConfirming().clear();

      if (getDataFolder().exists()) {
        reloadData();
        LogUtils.logInfo("The plugin's data was reloaded.");

        saveData();
        LogUtils.logInfo("The plugin's data was saved.");
      }

      playerDataManager.saveDatabase();
      LogUtils.logInfo(" ");
    }

    watermark.sendPluginDisableWatermark();
  }

  /**
   * Loads the absolute minimum amount of classes required to check for server compatibility.
   **/
  private void registerRequiredClasses() {
    this.holidayChecker = new HolidayChecker(this);
    this.versionChecker = new VersionChecker(this);
    this.verboseManager = new VerboseManager(this);
    this.watermark = new Watermark(this);
  }

  private void registerManagers() {
    this.fileManager = new FileManager(this);
    this.hookManager = new HookManager(this);
    this.databaseManager = new DatabaseManager(this);
  }

  private void registerClasses() {
    this.localeManager = new LocaleManager(this);
    registerLocale();

    this.hugManager = new HugManager(this);
    this.guiManager = new GUIManager(this);
    guiManager.addGUIs();

    this.cooldownManager = new CooldownManager(this);

    this.newGUIManager = new NewGUIManager();

    this.itemStates = new ItemStates(this);
  }

  //TODO: Add a check to see if the language in the config is a valid language selection.
  // If it isn't, default to English.
  private void registerLocale() {
    String language = getConfig().getString("language_file", "en");

    for (Languages lang : Languages.values()) {
//      // Check to see if the user put in a standard language name.
//      // E.G. English, Español, Русский
//      if (lang.getName().equals(language)) {
//        localeManager.load(language);
//        return;
//      }

      // Check to see if the user set the value to the language extension instead.
      if (lang.getFileExtension().equals(language)) {
        localeManager.loadFromFileExtension(language);
        return;
      }
    }

    localeManager.load(language);
  }

  private void registerCommands() {
    this.hugCommand = new HugCommand(this);
    this.OLDHugsCommand = new OLDHugsCommand(this);
    this.newHugsCommand = new NewHugsCommand(this);

    getCommand("hug").setExecutor(hugCommand);
    getCommand("hugs").setExecutor(newHugsCommand);
    getCommand("hugback").setExecutor(new HugBackCommand(this));
    getCommand("hugall").setExecutor(new HugAllCommand(this));

//    // register your command executor as normal.
//    PluginCommand command = getCommand("hugtest");
//    command.setExecutor(new HugTestCommand());
//
//    PluginCommand hugCommand = getCommand("hugs");
//
//    // check if brigadier is supported
//    if (CommodoreProvider.isSupported()) {
//
//      // get a commodore instance
//      Commodore commodore = CommodoreProvider.getCommodore(this);
//
//      // register your completions.
//      registerCompletions(commodore, command);
//
//      LiteralCommandNode<?> timeCommand = null;
//      try {
//        timeCommand = CommodoreFileFormat.parse(getResource("commands/hugtest.commodore"));
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//      commodore.register(command, timeCommand);
//
//      LiteralCommandNode<?> hugsCommand = null;
//      try {
//        hugsCommand = CommodoreFileFormat.parse(getResource("commands/hugs.commodore"));
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//      commodore.register(hugsCommand);
//
//      SuggestionProvider<Object> donate = (commandContext, suggestionsBuilder) -> {
//        suggestionsBuilder.suggest("donate", () -> "Support the project!");
//        return suggestionsBuilder.buildFuture();
//      };
//
//      SuggestionProvider<Object> test = (commandContext, suggestionsBuilder) -> {
//        suggestionsBuilder.suggest("Testing123", () -> "Tooltip Test");
//        return suggestionsBuilder.buildFuture();
//      };
//
//      LiteralCommandNode<?> testingCommand = LiteralArgumentBuilder.literal("supersecretcommand")
//          .then(LiteralArgumentBuilder.literal("set")
//              .then(LiteralArgumentBuilder.literal("day"))
//              .then(LiteralArgumentBuilder.literal("noon"))
//              .then(LiteralArgumentBuilder.literal("night"))
//              .then(LiteralArgumentBuilder.literal("midnight"))
//              .then(RequiredArgumentBuilder.argument("time", IntegerArgumentType.integer())))
//          .then(LiteralArgumentBuilder.literal("add")
//              .then(RequiredArgumentBuilder.argument("time", IntegerArgumentType.integer()).suggests(test)))
//          .then(LiteralArgumentBuilder.literal("query")
//              .then(LiteralArgumentBuilder.literal("daytime"))
//              .then(LiteralArgumentBuilder.literal("gametime"))
//              .then(LiteralArgumentBuilder.literal("day"))
//          ).build();
//
//      commodore.register(testingCommand);
//
//      LiteralCommandNode<?> commodoreHugCommand = LiteralArgumentBuilder.literal("testhugcommand")
//          .then(LiteralArgumentBuilder.literal("set")
//              .then(LiteralArgumentBuilder.literal("day"))
//              .then(LiteralArgumentBuilder.literal("noon"))
//              .then(LiteralArgumentBuilder.literal("night"))
//              .then(LiteralArgumentBuilder.literal("midnight"))
//              .then(RequiredArgumentBuilder.argument("time", IntegerArgumentType.integer())))
//          .then(LiteralArgumentBuilder.literal("donate")
//              .then(RequiredArgumentBuilder.argument("time", IntegerArgumentType.integer()).suggests(donate)))
//          .then(LiteralArgumentBuilder.literal("query")
//              .then(LiteralArgumentBuilder.literal("daytime"))
//              .then(LiteralArgumentBuilder.literal("gametime"))
//              .then(LiteralArgumentBuilder.literal("day"))
//          ).build();
//
//      commodore.register(commodoreHugCommand);
//    }
//

  }

//  private static void registerCompletions(Commodore commodore, PluginCommand command) {
//    commodore.register(command, LiteralArgumentBuilder.literal("mycommand")
//        .then(RequiredArgumentBuilder.argument("some-argument", StringArgumentType.string()))
//        .then(RequiredArgumentBuilder.argument("some-other-argument", BoolArgumentType.bool()))
//    );
//  }

  private void registerListeners() {
    PluginManager pluginManager = getServer().getPluginManager();
    pluginManager.registerEvents(new InventoryClickListener(this), this);
    pluginManager.registerEvents(new ChatConfirmationListener(this), this);
    pluginManager.registerEvents(new PassiveModeListener(this), this);
    pluginManager.registerEvents(new InvincibilityListener(this), this);
    pluginManager.registerEvents(new ShiftHugListener(this), this);
    pluginManager.registerEvents(new JoinListener(this), this);
    pluginManager.registerEvents(new NewHugListener(), this);
    pluginManager.registerEvents(new GUIListener(newGUIManager), this);

    guiManager.registerListeners(pluginManager);
  }

  public void reload(CommandSender sender) {
    fileManager.checkForAndCreateFiles();

    reloadConfig();
    registerLocale();

    this.itemStates = new ItemStates(this);

//    guiManager.clearCaches();

    playerDataManager.saveDatabase();
    databaseManager.initializeDatabase();
    playerDataManager.loadDatabase();

    String senderName = (sender instanceof ConsoleCommandSender) ? getConfig().getString(Settings.CONSOLE_NAME.getValue()) : sender.getName();
    verboseManager.logVerbose(senderName + " reloaded the configuration.");

    if (getConfig().getBoolean(Settings.RELOAD_VERBOSE_ENABLED.getValue())) {
      String setting = getConfig().getString(Settings.RELOAD_VERBOSE_DISPLAY_SETTINGS.getValue(), "None");

      if (setting.equalsIgnoreCase("All")) {
        verboseManager.sendFullConfigSettings();
      } else if (setting.equalsIgnoreCase("Informative")) {
        verboseManager.sendLightConfigSettings();
      }
    }
  }

  private boolean initialSetupSuccessful() {
    this.localSettingsManager = new YAMLLocalSettingsManager(this);

    if (!versionChecker.isSupportedVersion()) {
      return false;
    }

    versionChecker.registerClasses();
    return true;
  }

  private void checkForUpdates() {
    if (fileManager.getUpdaterData().getBoolean(Settings.CHECK_FOR_UPDATES.getValue())) {
      UpdateChecker.init(this, PluginConstants.PLUGIN_ID);

      this.pluginUpdater = new PluginUpdater(this);
      getServer().getPluginManager().registerEvents(new PluginUpdateListener(this), this);

      pluginUpdater.checkForUpdate();
    }
  }

  private void printVerbose() {
    if (getStartupConfig().getBoolean(Settings.STARTUP_VERBOSE_ENABLED.getValue())) {
      String setting = getStartupConfig().getString(Settings.STARTUP_VERBOSE_DISPLAY_SETTINGS.getValue(), "None");

      if (setting.equalsIgnoreCase("All")) {
        verboseManager.sendFullConfigSettings();
      } else if (setting.equalsIgnoreCase("Informative")) {
        verboseManager.sendLightConfigSettings();
      }
    }
  }

  public void saveResources(String path) {
    String[] resources = this.getResources(HugsPlugin.class, path);

    for (String resource : resources) {
      this.saveResource(path + File.separator + resource, false);
    }
  }

  public void saveResource(String resourcePath, boolean replace) {
    if (resourcePath == null || resourcePath.equals("")) {
      throw new IllegalArgumentException("ResourcePath cannot be null or empty");
    }

    resourcePath = resourcePath.replace('\\', '/');
    InputStream in = getResource(resourcePath);
    if (in == null) {
      throw new IllegalArgumentException("The embedded resource '" + resourcePath + "' cannot be found in " + getFile());
    }

    File outFile = new File(getDataFolder(), resourcePath);
    int lastIndex = resourcePath.lastIndexOf('/');
    File outDir = new File(getDataFolder(), resourcePath.substring(0, lastIndex >= 0 ? lastIndex : 0));

    if (!outDir.exists()) {
      outDir.mkdirs();
    }

    try {
      if (!outFile.exists() || replace) {
        OutputStream out = new FileOutputStream(outFile);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
          out.write(buf, 0, len);
        }
        out.close();
        in.close();
      }
    } catch (IOException ex) {
      getLogger().log(Level.SEVERE, "Could not save " + outFile.getName() + " to " + outFile, ex);
    }
  }

  private String[] getResources(Class clazz, String path) {
    // Gets the resource path for the jar file
    URL dirURL = clazz.getClassLoader().getResource(path);

    // Create the return list of resource filenames inside path provided
    List<String> result = new ArrayList<>();

    // Get the path of the jar file
    String jarPath = dirURL.getPath().substring(5, dirURL.getPath().indexOf("!")); //strip out only the JAR file

    // Decode the compiled jar for iteration
    JarFile jar = null;
    try {
      // TODO: Replace this code with the commented one when this plugin gets released on spigot
      //  and also updates Java versions.
      jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"));
//      jar = new JarFile(URLDecoder.decode(jarPath, StandardCharsets.UTF_8));
    } catch (UnsupportedEncodingException ex) {
      getServer().getConsoleSender().sendMessage("ERROR - getResources() - couldn't decode the Jar file to index resources.");
    } catch (IOException ex) {
      getServer().getConsoleSender().sendMessage("ERROR - getResources() - couldn't perform IO operations on jar file");
    }

    // Gets all the elements in a jar file for iterating through
    Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar

    // Iterate through and add elements inside the structures folder to the resources to be moved.
    while (entries.hasMoreElements()) {
      String name = entries.nextElement().getName();
      // check that element starts with path
      if (name.startsWith(path)) {
        String entry = name.substring(path.length() + 1);
        String last = name.substring(name.length() - 1);

        // Discard if it is a directory
        if (!last.equals(File.separator)) {
          // Resource contains at least one character or number
          if (entry.matches(".*[a-zA-Z0-9].*")) {
            //getServer().getConsoleSender().sendMessage(ChatColor.LIGHT_PURPLE + "Found an element that starts with the correct path: " + name);
            //getServer().getConsoleSender().sendMessage(ChatColor.LIGHT_PURPLE + "Chopped off just the resource name: " + entry);
            result.add(entry);
          }
        }
      }
    }

    // Return the array of strings of filenames inside path.
    return result.toArray(new String[result.size()]);
  }

  private void scheduleEvents() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DAY_OF_MONTH, 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    new BukkitRunnable() {

      @Override
      public void run() {
        playerDataManager.saveDatabase();
      }
    }.runTaskTimerAsynchronously(this, 0, 20 * 60 /* * 5*/);

    new BukkitRunnable() {
      long millisTillMidnight = (calendar.getTimeInMillis() - System.currentTimeMillis());

      @Override
      public void run() {
//        if (millisTillMidnight <= 0) {
//          //System.out.println(LogUtils.getConsolePrefix() + "Millis less than 0:  " + millisTillMidnight);
//          //System.out.println("Restarting plugin to check for holidays.");
//        }

        //System.out.println(LogUtils.getConsolePrefix() + "Millis Till Midnight: " + millisTillMidnight);
        millisTillMidnight = (calendar.getTimeInMillis() - System.currentTimeMillis());
      }
    }.runTaskTimerAsynchronously(this, 0, 20 * 60);
  }

  public void saveData() {
    try {
      getDataFile().save(getFileManager().getDataFile());
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public void reloadData() {
    try {
      setDataFile(YamlConfiguration.loadConfiguration(getFileManager().getDataFile()));
    } catch (Exception ex) {
      LogUtils.logError("An error occurred whilst loading the data file!");
      LogUtils.logError("Printing stack trace:");
      ex.printStackTrace();
      LogUtils.logError("End of stack trace.");
    }
  }

  // Commands
  public HugCommand getHugCommand() {
    return hugCommand;
  }

  public OLDHugsCommand getHugsCommand() {
    return OLDHugsCommand;
  }

  // Managers
  public PlayerDataManager getPlayerDataManager() {
    return playerDataManager;
  }

  public void setPlayerDataManager(PlayerDataManager playerDataManager) {
    this.playerDataManager = playerDataManager;
  }

  public GUIManager getGUIManager() {
    return guiManager;
  }

  public NewGUIManager getNewGUIManager() {
    return newGUIManager;
  }

  public ChatManager getChatManager() {
    return chatManager;
  }

  public void setChatManager(ChatManager chatManager) {
    this.chatManager = chatManager;
  }

  // Files
  public FileManager getFileManager() {
    return fileManager;
  }

  public ItemManager getItemManager() {
    return itemManager;
  }

  public void setItemManager(ItemManager itemManager) {
    this.itemManager = itemManager;
  }

  public VerboseManager getVerboseManager() {
    return verboseManager;
  }

  public FileConfiguration getDataFile() {
    return fileManager.getData();
  }

  private void setDataFile(FileConfiguration data) {
    fileManager.dataYMLData = data;
  }

  public FileConfiguration getLangFile() {
    return localeManager.getLangFile();
  }

  public FileConfiguration getStartupConfig() {
    return fileManager.getStartupData();
  }

  // Other
  public HolidayOverride getHolidayOverride() {
    return holidayOverride;
  }

  public void setHolidayOverride(HolidayOverride override) {
    this.holidayOverride = override;
  }

  public Holiday getHoliday() {
    return holiday;
  }

  public void setHoliday(Holiday holiday) {
    this.holiday = holiday;
  }

  public HolidayChecker getHolidayChecker() {
    return holidayChecker;
  }

  public void setHolidayChecker(HolidayChecker holidayChecker) {
    this.holidayChecker = holidayChecker;
  }

  public VersionChecker getVersionChecker() {
    return versionChecker;
  }

  public Map<UUID, Filter> getFilterList() {
    return filterList;
  }


  public ItemStates getItemStates() {
    return itemStates;
  }

  public LocalSettingsManager getLocalSettingsManager() {
    return localSettingsManager;
  }

  public PluginUpdater getPluginUpdater() {
    return pluginUpdater;
  }

  public AdvancementManager getAdvancementManager() {
    return advancementManager;
  }

  public void setAdvancementManager(AdvancementManager advancementManager) {
    this.advancementManager = advancementManager;
  }

  public HookManager getHookManager() {
    return hookManager;
  }

  public HugManager getHugManager() {
    return hugManager;
  }

  public void setHugManager(HugManager hugManager) {
    this.hugManager = hugManager;
  }

  public CooldownManager getCooldownManager() {
    return cooldownManager;
  }

  public LocaleManager getLocaleManager() {
    return localeManager;
  }

  public void setLocaleManager(LocaleManager localeManager) {
    this.localeManager = localeManager;
  }
}