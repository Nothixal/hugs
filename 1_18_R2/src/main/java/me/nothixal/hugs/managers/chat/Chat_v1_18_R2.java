package me.nothixal.hugs.managers.chat;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.minecraft.network.chat.ChatType;
import net.minecraft.network.protocol.game.ClientboundChatPacket;
import org.bukkit.craftbukkit.v1_18_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Chat_v1_18_R2 implements ChatManager {

  @Override
  public void sendMessage(Player player, String message) {
    BaseComponent[] components = new ComponentBuilder()
        .append(message)
        .create();

    ClientboundChatPacket packet = new ClientboundChatPacket(null, ChatType.CHAT, player.getUniqueId());
    packet.components = components;
    ((CraftPlayer) player).getHandle().connection.send(packet);
  }

  @Override
  public void sendActionBar(Player player, String message) {
    BaseComponent[] components = new ComponentBuilder()
        .append(message)
        .create();

    ClientboundChatPacket packet = new ClientboundChatPacket(null, ChatType.GAME_INFO, player.getUniqueId());
    packet.components = components;
    ((CraftPlayer) player).getHandle().connection.send(packet);
  }
}
