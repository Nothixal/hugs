package me.nothixal.hugs.managers.advancements;

import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.enums.AdvancementBackground;
import me.nothixal.hugs.utils.json.JSONUtils;
import me.nothixal.hugs.utils.text.TextUtils;
import net.minecraft.server.v1_16_R2.Advancement;
import net.minecraft.server.v1_16_R2.AdvancementDisplay;
import net.minecraft.server.v1_16_R2.AdvancementFrameType;
import net.minecraft.server.v1_16_R2.AdvancementProgress;
import net.minecraft.server.v1_16_R2.AdvancementRewards;
import net.minecraft.server.v1_16_R2.Criterion;
import net.minecraft.server.v1_16_R2.CriterionInstance;
import net.minecraft.server.v1_16_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_16_R2.LootSerializationContext;
import net.minecraft.server.v1_16_R2.MinecraftKey;
import net.minecraft.server.v1_16_R2.PacketPlayOutAdvancements;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_16_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Advancement_v1_16_R2 implements AdvancementManager {

  @Override
  public void sendToast(ItemStack item, UUID uuid, String title, String description) {
    Player player = Bukkit.getPlayer(uuid);

    MinecraftKey pluginKey = new MinecraftKey("hugs.plugin", "notification");

    AdvancementRewards advRewards = new AdvancementRewards(0, new MinecraftKey[0], new MinecraftKey[0], null);

    Map<String, Criterion> advCriteria = new HashMap<>();
    String[][] advRequirements;

    advCriteria.put("for_free", new Criterion(new CriterionInstance() {

      @Override
      public JsonObject a(LootSerializationContext lootSerializationContext) {
        return null;
      }

      @Override
      public MinecraftKey a() {
        return new MinecraftKey("minecraft", "impossible");
      }
    }));

    ArrayList<String[]> fixedRequirements = new ArrayList<>();

    fixedRequirements.add(new String[]{"for_free"});

    advRequirements = Arrays.stream(fixedRequirements.toArray()).toArray(String[][]::new);

    AdvancementDisplay display = new AdvancementDisplay(CraftItemStack.asNMSCopy(item),
        ChatSerializer.a(JSONUtils.formatSimpleJSON(TextUtils.colorText(title))),
        ChatSerializer.a(JSONUtils.formatSimpleJSON(TextUtils.colorText(description))),
        new MinecraftKey(AdvancementBackground.SNOW.getValue()),
        AdvancementFrameType.GOAL, true, true, true);
    Advancement advancement = new Advancement(pluginKey, null, display, advRewards, advCriteria, advRequirements);

    HashMap<MinecraftKey, AdvancementProgress> prg = new HashMap<>();

    AdvancementProgress advPrg = new AdvancementProgress();
    advPrg.a(advCriteria, advRequirements);
    advPrg.getCriterionProgress("for_free").b();
    prg.put(pluginKey, advPrg);

    PacketPlayOutAdvancements packet = new PacketPlayOutAdvancements(false, Arrays.asList(advancement), new HashSet<>(), prg);
    ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);

    HashSet<MinecraftKey> rm = new HashSet<>();
    rm.add(pluginKey);
    prg.clear();
    packet = new PacketPlayOutAdvancements(false, new ArrayList<>(), rm, prg);
    ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
  }

  @Override
  public void sendToast(UUID uuid, String title, String description) {

  }

  @Override
  public void sendToast(Player player, String title, String description) {

  }

  @Override
  public void sendToast(String playerName, String title, String description) {

  }

  @Override
  public void sendGoalToast(ItemStack item, UUID uuid, String title, String description) {

  }

  @Override
  public void sendChallengeToast(ItemStack item, UUID uuid, String title, String description) {
    Player player = Bukkit.getPlayer(uuid);

    MinecraftKey notName = new MinecraftKey("hugs.plugin", "notification");

    AdvancementRewards advRewards = new AdvancementRewards(0, new MinecraftKey[0], new MinecraftKey[0], null);

    Map<String, Criterion> advCriteria = new HashMap<>();
    String[][] advRequirements;

    advCriteria.put("for_free", new Criterion(new CriterionInstance() {

      @Override
      public JsonObject a(LootSerializationContext lootSerializationContext) {
        return null;
      }

      @Override
      public MinecraftKey a() {
        return new MinecraftKey("minecraft", "impossible");
      }
    }));

    ArrayList<String[]> fixedRequirements = new ArrayList<>();

    fixedRequirements.add(new String[]{"for_free"});

    advRequirements = Arrays.stream(fixedRequirements.toArray()).toArray(String[][]::new);

    AdvancementDisplay display = new AdvancementDisplay(CraftItemStack.asNMSCopy(item),
        ChatSerializer.a(JSONUtils.formatSimpleJSON(TextUtils.colorText(title))),
        ChatSerializer.a(JSONUtils.formatSimpleJSON(TextUtils.colorText(description))),
        new MinecraftKey("minecraft:textures/block/dirt.png"),
        AdvancementFrameType.CHALLENGE, true, true, true);
    Advancement advancement = new Advancement(notName, null, display, advRewards, advCriteria, advRequirements);

    HashMap<MinecraftKey, AdvancementProgress> prg = new HashMap<>();

    AdvancementProgress advPrg = new AdvancementProgress();
    advPrg.a(advCriteria, advRequirements);
    advPrg.getCriterionProgress("for_free").b();
    prg.put(notName, advPrg);

    PacketPlayOutAdvancements packet = new PacketPlayOutAdvancements(false, Arrays.asList(advancement), new HashSet<>(), prg);
    ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
  }
}
