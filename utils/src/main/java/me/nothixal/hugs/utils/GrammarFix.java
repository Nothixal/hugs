package me.nothixal.hugs.utils;

public class GrammarFix {

  public static String getPlural(String word, boolean singular) {
    return singular ? word : word + "s";
  }

}
