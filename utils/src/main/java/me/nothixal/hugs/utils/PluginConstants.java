package me.nothixal.hugs.utils;

import java.awt.Color;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

public final class PluginConstants {

  public static final int PLUGIN_ID = 39722;

  public static final String PLUGIN_CREATOR = "Nothixal";

  public static final String PLUGIN_WEBSITE_LINK = "https://www.spigotmc.org/resources/hugs.39722/";
  public static final String PLUGIN_AUTHOR_HOMEPAGE = "https://www.spigotmc.org/resources/authors/shadowmasterg23.303901";
  public static final String PLUGIN_SOURCE_CODE_LINK = "https://gitlab.com/Nothixal/hugs";
  public static final String PLUGIN_WIKI_LINK = "https://gitlab.com/Nothixal/hugs/-/wikis/Home";
  public static final String PLUGIN_BUG_TRACKER = "https://gitlab.com/Nothixal/hugs/-/issues";

  public static final String DISCORD_INVITE_LINK = "https://discord.gg/kJyZwBj";

  public static final String PAYPAL_DONATE_LINK = "https://paypal.me/shadowmastergaming";
  public static final String CASHAPP_DONATE_LINK = "https://cash.app/$Nothixal";

  public static final String DEFAULT_PREFIX_COLOR_BOLD = "#03A9F4&l";
  public static final String DEFAULT_PREFIX_COLOR_HEX = "#03A9F4";
  public static final String SECONDARY_COLOR_HEX = "#0287c3";
  public static final String TERTIARY_COLOR_HEX = "#67cbf8";

  public static final Color PRIMARY_COLOR = new Color(3, 169, 244, 255);
  public static final Color SECONDARY_COLOR = new Color(2, 135, 195, 255);
  public static final Color TEXT_COLOR = new Color(204, 204, 204, 255);

  public static final String BACKUP_PREFIX_COLOR = "#3498DB";
  public static final String BACKUP_PREFIX_COLOR_BOLD = "#3498DB&l";
  public static final String BACKUP_PREFIX_COLOR_HEX = "#3498DB";

  public static final String TEST_COLOR_HEX = "#55CCFF";

  public static final String HEART = "❤";
  public static final String ARROW_LEFT = "←";
  public static final String ARROW_RIGHT = "→";
  public static final String THICK_ARROW_RIGHT = "➜";
  public static final String DOUBLE_ARROW_LEFT = "«";
  public static final String DOUBLE_ARROW_RIGHT = "»";
  public static final String CHECKMARK = "✔";
  public static final String X = "✖";
  public static final String OLD_DIVISION = "÷";
  public static final String PLUS_MINUS = "±";

  public static final String EROS_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzA2NmZkMjg1OWFmZmY3ZmRlMDQ4NDM1MzgwYzM4MGJlZTg2NmU1OTU2YjhhM2E1Y2NjNWUxY2Y5ZGYwZjIifX19";
  public static final String SILVIA_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2Q2ZjRhMjFlMGQ2MmFmODI0Zjg3MDhhYzYzNDEwZjFhMDFiYmI0MWQ3ZjRhNzAyZDk0NjljNjExMzIyMiJ9fX0=";

  public static final String H_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmRlNGE4OWJlMjE5N2Y4NmQyZTYxNjZhMGFjNTQxY2NjMjFkY2UyOGI3ODU0Yjc4OGQzMjlhMzlkYWVjMzIifX19";
  public static final String U_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWZkYzRmMzIxYzc4ZDY3NDg0MTM1YWU0NjRhZjRmZDkyNWJkNTdkNDU5MzgzYTRmZTlkMmY2MGEzNDMxYTc5In19fQ==";
  public static final String G_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNThjMzM2ZGVkZmUxOTdiNDM0YjVhYjY3OTg4Y2JlOWMyYzlmMjg1ZWMxODcxZmRkMWJhNDM0ODU1YiJ9fX0=";
  public static final String S_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWYyMmQ3Y2Q1M2Q1YmZlNjFlYWZiYzJmYjFhYzk0NDQzZWVjMjRmNDU1MjkyMTM5YWM5ZmJkYjgzZDBkMDkifX19";

  // Skull textures for the Plugin Info GUI.
  public static final String DISCORD_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTNiMTgzYjE0OGI5YjRlMmIxNTgzMzRhZmYzYjViYjZjMmMyZGJiYzRkNjdmNzZhN2JlODU2Njg3YTJiNjIzIn19fQ==";
  public static final String WEBSITE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGQxOWM2ODQ2MTY2NmFhY2Q3NjI4ZTM0YTFlMmFkMzlmZTRmMmJkZTMyZTIzMTk2M2VmM2IzNTUzMyJ9fX0=";
  public static final String BUG_TRACKER_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjBmNDI4OGFlMDhhMzhkMmFiOWNmMzQzMTQxNjQ3ZTRmM2JlMTZjNWE5MjdlNzIyNGEzYjFkZWNhY2ZmMjU5In19fQ==";
  public static final String WIKI_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzBjZjc0ZTI2MzhiYTVhZDMyMjM3YTM3YjFkNzZhYTEyM2QxODU0NmU3ZWI5YTZiOTk2MWU0YmYxYzNhOTE5In19fQ==";

  public static final String DEVELOPER_REWARD_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWQ5Y2M1OGFkMjVhMWFiMTZkMzZiYjVkNmQ0OTNjOGY1ODk4YzJiZjMwMmI2NGUzMjU5MjFjNDFjMzU4NjcifX19";
  public static final String SELECT_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOThkYWExZTNlZDk0ZmYzZTMzZTFkNGM2ZTQzZjAyNGM0N2Q3OGE1N2JhNGQzOGU3NWU3YzkyNjQxMDYifX19";

  public static final String ENGLISH_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNGNhYzk3NzRkYTEyMTcyNDg1MzJjZTE0N2Y3ODMxZjY3YTEyZmRjY2ExY2YwY2I0YjM4NDhkZTZiYzk0YjQifX19";
  public static final String SPANISH_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzJiZDQ1MjE5ODMzMDllMGFkNzZjMWVlMjk4NzQyODc5NTdlYzNkOTZmOGQ4ODkzMjRkYThjODg3ZTQ4NWVhOCJ9fX0=";
  public static final String RUSSIAN_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZlYWZlZjk4MGQ2MTE3ZGFiZTg5ODJhYzRiNDUwOTg4N2UyYzQ2MjFmNmE4ZmU1YzliNzM1YTgzZDc3NWFkIn19fQ==";
  public static final String GERMAN_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWU3ODk5YjQ4MDY4NTg2OTdlMjgzZjA4NGQ5MTczZmU0ODc4ODY0NTM3NzQ2MjZiMjRiZDhjZmVjYzc3YjNmIn19fQ==";
  public static final String FRENCH_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTEyNjlhMDY3ZWUzN2U2MzYzNWNhMWU3MjNiNjc2ZjEzOWRjMmRiZGRmZjk2YmJmZWY5OWQ4YjM1Yzk5NmJjIn19fQ==";
  public static final String ITALIAN_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODVjZTg5MjIzZmE0MmZlMDZhZDY1ZDhkNDRjYTQxMmFlODk5YzgzMTMwOWQ2ODkyNGRmZTBkMTQyZmRiZWVhNCJ9fX0=";
  public static final String KOREAN_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmMxYmU1ZjEyZjQ1ZTQxM2VkYTU2ZjNkZTk0ZTA4ZDkwZWRlOGUzMzljN2IxZThmMzI3OTczOTBlOWE1ZiJ9fX0=";
  public static final String DUTCH_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzIzY2YyMTBlZGVhMzk2ZjJmNWRmYmNlZDY5ODQ4NDM0ZjkzNDA0ZWVmZWFiZjU0YjIzYzA3M2IwOTBhZGYifX19";
  public static final String BRAZILIAN_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTY2OGExZmI2YWY4MWIyMzFiYmNjNGRlNWY3Zjk1ODAzYmJkMTk0ZjU4MjdkYTAyN2ZhNzAzMjFjZjQ3YyJ9fX0=";
  public static final String FINNISH_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTlmMjM0OTcyOWE3ZWM4ZDRiMTQ3OGFkZmU1Y2E4YWY5NjQ3OWU5ODNmYmFkMjM4Y2NiZDgxNDA5YjRlZCJ9fX0=";
  public static final String UKRAINIAN_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjhiOWY1MmUzNmFhNWM3Y2FhYTFlN2YyNmVhOTdlMjhmNjM1ZThlYWM5YWVmNzRjZWM5N2Y0NjVmNWE2YjUxIn19fQ==";

  public static final String LEET_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGQxOWM2ODQ2MTY2NmFhY2Q3NjI4ZTM0YTFlMmFkMzlmZTRmMmJkZTMyZTIzMTk2M2VmM2IzNTUzMyJ9fX0=";
  public static final String PIRATE_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmUxYWQyNmM0MDM4NTU1MGFjODIwOWQ3MjFlMTIxNDE2MTdjMjIwMDkxZDZjYTBlMDY1OGViOGQxMTdkYjUzNSJ9fX0=";
  public static final String OWO_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDIzNWFmMTY2ZmE1ZjQ4NmQ1YzZiODRmN2FiYjAwOTNhMWVhMDE0OTBjMzM3ZjgwNDViNDk3NmJjOTNlN2Q2MCJ9fX0=";
  public static final String ENCHANTING_TABLE_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2JiNzg4YzBmZjBmYzU3NDY5ODc4N2MzYzdmMWIzODVhMTljMTczODM1OTMyMzU2MzU0MmI1MzdkMDVmMWYyZSJ9fX0=";
  public static final String MYSTERY_LANGUAGE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWUyMjFiYjczMzE3MjIwNzMxNWFjZWRjYzVjOTk4MzZhNjM5ODYyYTM3MjdkYTZkNWRmMzZiODUxZmMxOTFjNCJ9fX0=";

  // Skull texture for the Bossbar icon in user settings.
  public static final String BOSSBAR_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTY0ZTFjM2UzMTVjOGQ4ZmZmYzM3OTg1YjY2ODFjNWJkMTZhNmY5N2ZmZDA3MTk5ZThhMDVlZmJlZjEwMzc5MyJ9fX0=";

  @SuppressWarnings("deprecation")
  public static final BaseComponent[] PAYPAL_BASE_COMPONENT = new ComponentBuilder()
      .append("[PAYPAL]").color(ChatColor.of("#2997D8")).bold(true)
      .event(new ClickEvent(Action.OPEN_URL, PAYPAL_DONATE_LINK))
      .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
          new ComponentBuilder().append("Click to donate").color(ChatColor.AQUA).create()))
      .create();

  @SuppressWarnings("deprecation")
  public static final BaseComponent[] CASHAPP_BASE_COMPONENT = new ComponentBuilder()
      .append("[CASHAPP]").color(ChatColor.of("#01d54c")).bold(true)
      .event(new ClickEvent(Action.OPEN_URL, CASHAPP_DONATE_LINK))
      .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
          new ComponentBuilder().append("Click to donate").color(ChatColor.AQUA).create()))
      .create();

  public static final BaseComponent[] VENMO_LINK = new ComponentBuilder()
      .append("[VENMO]").color(ChatColor.of("#3d95ce"))
      .create();

  public static final BaseComponent[] BITCOIN_WALLET_ADDRESS = new ComponentBuilder()
      .append("[BITCOIN]").color(ChatColor.of("#f7931a"))
      .create();
}