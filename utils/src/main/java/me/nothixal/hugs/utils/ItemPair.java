package me.nothixal.hugs.utils;

import me.nothixal.hugs.utils.inventory.ButtonSet;
import me.nothixal.hugs.utils.inventory.IconSet;

public class ItemPair {

  private IconSet iconSet;
  private ButtonSet buttonSet;

  public ItemPair(IconSet iconSet, ButtonSet buttonSet) {
    this.iconSet = iconSet;
    this.buttonSet = buttonSet;
  }

  public IconSet getIconSet() {
    return iconSet;
  }

  public void setIconSet(IconSet iconSet) {
    this.iconSet = iconSet;
  }

  public ButtonSet getButtonSet() {
    return buttonSet;
  }

  public void setButtonSet(ButtonSet buttonSet) {
    this.buttonSet = buttonSet;
  }
}
