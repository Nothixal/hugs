package me.nothixal.hugs.utils.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import me.nothixal.hugs.enums.ANSIColor;
import net.md_5.bungee.api.ChatColor;

public class TextUtils {

  public static final Pattern HEX_PATTERN = Pattern.compile("#[a-fA-F0-9]{6}");

  public static String formatHexColorCodes(String string) {
    Matcher matcher = HEX_PATTERN.matcher(string);
    while (matcher.find()) {
      string = string.replace(matcher.group(), "" + ChatColor.of(matcher.group()));
    }

    return string;
  }

  public static String colorText(String msg) {
    return ChatColor.translateAlternateColorCodes('&', msg);
  }

  public static String stripColor(String message) {
    return ChatColor.stripColor(message).replaceAll("(?i)&([a-f0-9])", "");
  }

  /**
   * Converts the color codes into ANSI color codes.
   * @param msg The message with color codes to convert.
   * */
  public static String translateANSIColorCodes(String msg) {
    return ANSIColor.translateColorCodes('&', msg);
  }

  /**
   * Converts the color codes into ANSI color codes.
   * @param character The character that will be looked for in the conversion.
   * @param msg The message with color codes to convert.
   * */
  public static String translateANSIColorCodes(char character, String msg) {
    return ANSIColor.translateColorCodes(character, msg);
  }

  public static String convertStringArrayToString(String[] array, String delimiter) {
    StringBuilder stringBuilder = new StringBuilder();
    for (String s : array)
      stringBuilder.append(s).append(delimiter);
    return stringBuilder.substring(0, stringBuilder.length() - 1);
  }

  public static String capitalize(String msg) {
    return msg.substring(0, 1).toUpperCase() + msg.substring(1);
  }

  /**
   * Capitalizes the first letter in the word you provide.
   *
   * @param msg The word you want to capitalize.
   */
  public static String capitalizeFirstLetter(String msg) {
    return msg.substring(0, 1).toUpperCase() + msg.substring(1).toLowerCase();
  }

  /**
   * Capitalizes the first letter in every word in a sentence.
   *
   * @param sentence The word you want to capitalize.
   * @return The sentence with every word capitalized.
   */
  public static String capitalizeSentence(String sentence) {
    String[] mySentenceArray = sentence.split("[ ]");

    StringBuilder builder = new StringBuilder();

    for (String s : mySentenceArray) {
      builder.append(capitalizeFirstLetter(s));
      builder.append(" ");
    }

    return builder.toString();
  }
}
