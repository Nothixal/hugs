package me.nothixal.hugs.utils;

import java.util.LinkedList;

public class LimitedQueue<E> extends LinkedList<E> {
  private int limit;

  public LimitedQueue(int limit) {
    this.limit = limit;
  }

  @Override
  public boolean add(E o) {
    super.add(0, o);
    while (size() > limit) { super.remove(size() - 1); }
    return true;
  }
}
