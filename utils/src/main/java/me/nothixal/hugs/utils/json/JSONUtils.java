package me.nothixal.hugs.utils.json;

import me.nothixal.hugs.utils.chat.ChatUtils;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;

public final class JSONUtils {

  private JSONUtils() {

  }

  public static String formatSimpleJSON(String msg) {
    return "{\"text\":\"" + msg + "\"}";
  }

  @SuppressWarnings("deprecation")
  public static void sendHelpHeader1(CommandSender sender) {
    TextComponent darkBoldArrowRight = new TextComponent(ChatUtils.colorChat("  &8&l>"));
    darkBoldArrowRight.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatUtils.colorChat("&3Next Page")).create()));
    darkBoldArrowRight.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/hugs help 2"));

    TextComponent darkAquaBoldArrowRight = new TextComponent(ChatUtils.colorChat("&3&l>"));
    darkAquaBoldArrowRight.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatUtils.colorChat("&3Next Page")).create()));
    darkAquaBoldArrowRight.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/hugs help 2"));

    TextComponent aquaBoldArrowRight = new TextComponent(ChatUtils.colorChat("&b&l>"));
    aquaBoldArrowRight.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatUtils.colorChat("&3Next Page")).create()));
    aquaBoldArrowRight.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/hugs help 2"));

    TextComponent header = new TextComponent(ChatUtils.colorChat("&8&l<<<  &8&l<&3❤&8&l>&8&m-----&8&l<< &b&lHug commands &8(&31&7/&32&8) &8&l>>&8&m-----&8&l<&3❤&8&l>"));
    header.addExtra(darkBoldArrowRight);
    header.addExtra(darkAquaBoldArrowRight);
    header.addExtra(aquaBoldArrowRight);
    sender.spigot().sendMessage(header);
  }

}