package me.nothixal.hugs.utils.chat;

import java.util.ArrayList;
import java.util.List;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

public class ClickableMessage {

  private final List<TextComponent> components = new ArrayList<>();
  private TextComponent current;

  public ClickableMessage() {

  }

  public ClickableMessage(String msg) {
    this.add(msg);
  }

  public ClickableMessage add(String msg) {
    BaseComponent[] baseComponent = new ComponentBuilder(ChatUtils.colorChat(msg)).create();
    TextComponent component = new TextComponent(baseComponent);

    this.components.add(component);
    this.current = component;
    return this;
  }

  public ClickableMessage addLegacy(String msg) {
    BaseComponent[] baseComponent = new ComponentBuilder().appendLegacy(ChatUtils.colorChat(msg)).create();
    TextComponent component = new TextComponent(baseComponent);

    this.components.add(component);
    this.current = component;
    return this;
  }

  @SuppressWarnings("deprecation")
  private void hover(TextComponent component, final String msg) {
    component.setHoverEvent(new HoverEvent(Action.SHOW_TEXT,
        new ComponentBuilder(ChatUtils.colorChat(msg)).create()));
  }

  @SuppressWarnings("deprecation")
  private void hover(TextComponent component, List<String> message) {
    StringBuilder builder = new StringBuilder();

    for (int i = 0; i < message.size(); i++) {
      builder.append(message.get(i));

      if (i != message.size() - 1) {
        builder.append("\n");
      }
    }

    component.setHoverEvent(new HoverEvent(Action.SHOW_TEXT,
        new ComponentBuilder().appendLegacy(ChatUtils.colorChat(builder.toString())).create()));
  }

  @SuppressWarnings("deprecation")
  private void hover(TextComponent component, String... message) {
    StringBuilder builder = new StringBuilder();

    for (int i = 0; i < message.length; i++) {
      builder.append(message[i]);

      if (i != message.length - 1) {
        builder.append("\n");
      }
    }

    component.setHoverEvent(new HoverEvent(Action.SHOW_TEXT,
        new ComponentBuilder().appendLegacy(ChatUtils.colorChat(builder.toString())).create()));
  }

  public ClickableMessage hover(String msg) {
    this.hover(this.current, msg);
    return this;
  }

  public ClickableMessage hover(List<String> msg) {
    this.hover(this.current, msg);
    return this;
  }

  public ClickableMessage hover(String... msg) {
    this.hover(this.current, msg);
    return this;
  }

  public ClickableMessage hoverAll(String msg) {
    this.components.forEach(component -> this.hover(component, msg));
    return this;
  }

  private void command(TextComponent component, String command) {
    component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
  }

  public void link(TextComponent component, String url) {
    component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, url));
  }

  public ClickableMessage command(String command) {
    this.command(this.current, command);
    return this;
  }

  public ClickableMessage commandAll(String command) {
    this.components.forEach(component -> this.command(component, command));
    return this;
  }

  public ClickableMessage color(String color) {
    this.current.setColor(ChatColor.getByChar(color.charAt(1)));
    return this;
  }

  public ClickableMessage color(ChatColor color) {
    this.current.setColor(color);
    return this;
  }

  public ClickableMessage style(ChatColor color) {
    if (ChatColor.UNDERLINE.equals(color)) {
      this.current.setUnderlined(true);
    } else if (ChatColor.BOLD.equals(color)) {
      this.current.setBold(true);
    } else if (ChatColor.ITALIC.equals(color)) {
      this.current.setItalic(true);
    } else if (ChatColor.MAGIC.equals(color)) {
      this.current.setObfuscated(true);
    }
    return this;
  }

  public void sendToPlayer(Player player) {
    player.spigot().sendMessage(this.components.toArray(new BaseComponent[0]));
  }

  public TextComponent getCurrent() {
    return current;
  }
}
