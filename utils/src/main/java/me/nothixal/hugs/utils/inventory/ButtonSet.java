package me.nothixal.hugs.utils.inventory;

import java.util.Arrays;
import java.util.List;
import org.bukkit.inventory.ItemStack;

public class ButtonSet {

  private List<ItemStack> buttons;

  public ButtonSet(List<ItemStack> buttons) {
    this.buttons = buttons;
  }

  public ButtonSet(ItemStack... buttons) {
    this.buttons = Arrays.asList(buttons);
  }

  public ItemStack getNext(ItemStack element) {
    int index = buttons.indexOf(element);
    if (index < 0 || index + 1 == buttons.size()) {
      return buttons.get(0);
    }
    return buttons.get(index + 1);
  }

  public ItemStack getPrevious(ItemStack element) {
    int index = buttons.indexOf(element);
    if (index <= 0) {
      return buttons.get(buttons.size() - 1);
    }
    return buttons.get(index - 1);
  }

  public List<ItemStack> getButtons() {
    return buttons;
  }

  public void setButtons(List<ItemStack> buttons) {
    this.buttons = buttons;
  }
}
