package me.nothixal.hugs.utils.chat;

import me.nothixal.hugs.utils.text.TextUtils;
import net.md_5.bungee.api.ChatColor;

public final class ChatUtils {

  public static String colorChat(String msg) {
    return TextUtils.formatHexColorCodes(ChatColor.translateAlternateColorCodes('&', msg));
  }

  public static String replaceColors(String message) {
    return message.replaceAll("(?i)&([a-f0-9])", "");
  }

  public static String stripColor(String msg) {
    return ChatColor.stripColor(msg);
  }
}