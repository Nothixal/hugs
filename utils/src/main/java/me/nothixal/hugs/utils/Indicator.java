package me.nothixal.hugs.utils;

import me.nothixal.hugs.enums.preferences.IndicatorType;

public class Indicator {

  private int priority;
  private IndicatorType type;
  private boolean enabled;

  public Indicator(int priority, IndicatorType type, boolean enabled) {
    this.priority = priority;
    this.type = type;
    this.enabled = enabled;
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  public IndicatorType getType() {
    return type;
  }

  public void setType(IndicatorType type) {
    this.type = type;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }
}
