package me.nothixal.hugs.utils.inventory;

import java.util.Arrays;
import java.util.List;
import org.bukkit.inventory.ItemStack;

public class IconSet {

  private List<ItemStack> icons;

  public IconSet(List<ItemStack> icons) {
    this.icons = icons;
  }

  public IconSet(ItemStack... items) {
    this.icons = Arrays.asList(items);
  }

  public ItemStack getNext(ItemStack element) {
    int index = icons.indexOf(element);
    if (index < 0 || index + 1 == icons.size()) {
      return icons.get(0);
    }
    return icons.get(index + 1);
  }

  public ItemStack getPrevious(ItemStack element) {
    int index = icons.indexOf(element);
    if (index <= 0) {
      return icons.get(icons.size() - 1);
    }
    return icons.get(index - 1);
  }

  public List<ItemStack> getIcons() {
    return icons;
  }

  public void setIcons(List<ItemStack> icons) {
    this.icons = icons;
  }

}
