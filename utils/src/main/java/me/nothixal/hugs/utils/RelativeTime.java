package me.nothixal.hugs.utils;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RelativeTime {

  private static final Map<String, Long> times = new LinkedHashMap<>();

  static {
    times.put("year", TimeUnit.DAYS.toMillis(365));
    times.put("month", TimeUnit.DAYS.toMillis(30));
    times.put("week", TimeUnit.DAYS.toMillis(7));
    times.put("day", TimeUnit.DAYS.toMillis(1));
    times.put("hour", TimeUnit.HOURS.toMillis(1));
    times.put("minute", TimeUnit.MINUTES.toMillis(1));
    times.put("second", TimeUnit.SECONDS.toMillis(1));
  }

  public static StringBuilder toRelative(long duration, int maxLevel) {
    StringBuilder res = new StringBuilder();
    int level = 0;
    for (Map.Entry<String, Long> time : times.entrySet()) {
      long timeDelta = duration / time.getValue();
      if (timeDelta > 0) {
        res.append(timeDelta)
            .append(" ")
            .append(time.getKey())
            .append(timeDelta > 1 ? "s" : "")
            .append(", ");
        duration -= time.getValue() * timeDelta;
        level++;
      }
      if (level == maxLevel) {
        break;
      }
    }
    if ("".equals(res.toString())) {
      res.append("0 seconds");
    } else {
      res.setLength(res.length() - 2);
    }
    return res;
  }

  public static String toRelative(long duration) {
    return toRelative(duration, times.size()).toString();
  }

  public static String toRelative(Date start, Date end) {
    assert start.after(end);
    return toRelative(end.getTime() - start.getTime());
  }

  public static String toRelative(Date start, Date end, int level) {
    assert start.after(end);
    return toRelative(end.getTime() - start.getTime(), level).toString();
  }

  public static String toRelativePast(long duration, int maxLevel) {
    StringBuilder builder = toRelative(duration, maxLevel);

    if ("".equals(builder.toString())) {
      return "0 seconds ago";
    } else {
      builder.append(" ago");
      return builder.toString();
    }
  }

  public static String toRelativePast(long duration) {
    return toRelativePast(duration, times.size());
  }

  public static String toRelativePast(Date start, Date end) {
    return toRelativePast(end.getTime() - start.getTime());
  }

  public static String toRelativePast(Date start, Date end, int level) {
    return toRelativePast(end.getTime() - start.getTime(), level);
  }

  public static String toRelativeFuture(long duration, int maxLevel) {
    StringBuilder builder = toRelative(duration, maxLevel);

    if ("".equals(builder.toString())) {
      return "0 seconds remaining";
    } else {
      builder.append(" remaining");
      return builder.toString();
    }
  }

  public static String toRelativeFuture(long duration) {
    return toRelativeFuture(duration, times.size());
  }

  public static String toRelativeFuture(Date start, Date end) {
    return toRelativeFuture(end.getTime() - start.getTime());
  }

  public static String toRelativeFuture(Date start, Date end, int level) {
    return toRelativeFuture(end.getTime() - start.getTime(), level);
  }
}