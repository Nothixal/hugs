package me.nothixal.hugs.enums.preferences;

public enum IndicatorType {

  CHAT("chat", "settings.indicators.chat"),
  TITLES("titles", "settings.indicators.titles"),
  ACTIONBAR("actionbar", "settings.indicators.actionbar"),
  BOSSBAR("bossbar", "settings.indicators.bossbar"),
  TOASTS("toasts", "settings.indicators.toasts"),
  ;

  private final String value;
  private final String path;

  IndicatorType(String value, String path) {
    this.value = value;
    this.path = path;
  }

  public String getValue() {
    return value;
  }

  public String getPath() {
    return path;
  }
}
