package me.nothixal.hugs.enums.holidays;

import me.nothixal.hugs.utils.text.TextUtils;

public enum HolidayOverride {
  // US Holidays
  NONE("\033[38;2;3;169;244mHugs &8>> ", "&7[\u001b[38;2;3;169;244mHugs&7] "),
  NEW_YEARS("&3Hugs &8>> ", "&7[&3Hugs&7] "),
  NATIONAL_HUG_DAY("&3Hugs &8>> ", "&7[&3Hugs&7] "),
  VALENTINES_DAY("&4H&cu&4g&cs &8>> ", "&7[&4H&cu&4g&cs&7] "),
  INDEPENDENCE_DAY("&4H&cu&fg&9s &8>> ", "&7[&4H&cu&fg&9s&7] "),
  HALLOWEEN("&6H7eu&6g&es &8>> ", "&7[&6H7eu&6g&es&7] "),
  VETERANS_DAY("&3Hugs &8>> ", "&7[&3Hugs&7] "),
  CHRISTMAS("&4H&cu&2g&as &8>> ", "&7[&4H&cu&2g&as&7] "),

  // Canadian Holidays
  BOXING_DAY("&3Hugs &8>> ", "&7[&3Hugs&7] ");

  private final String pluginPrefix;
  private final String consolePrefix;

  HolidayOverride(String pluginPrefix, String consolePrefix) {
    this.pluginPrefix = pluginPrefix;
    this.consolePrefix = consolePrefix;
  }

  public String getPluginPrefix() {
    return TextUtils.translateANSIColorCodes(pluginPrefix);
  }

  public String getConsolePrefix() {
    return TextUtils.translateANSIColorCodes(consolePrefix);
  }

  @Override
  public String toString() {
    return pluginPrefix;
  }
}
