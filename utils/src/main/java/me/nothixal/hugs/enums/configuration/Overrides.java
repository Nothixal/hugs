package me.nothixal.hugs.enums.configuration;

public enum Overrides {

  CHAT_MESSAGES_OVERRIDDEN("overrides.indicators.chat"),
  ACTIONBAR_MESSAGES_OVERRIDDEN("overrides.indicators.actionbar"),
  BOSSBAR_MESSAGES_OVERRIDDEN("overrides.indicators.bossbar"),
  TITLE_MESSAGES_OVERRIDDEN("overrides.indicators.titles"),
  TOAST_MESSAGES_OVERRIDDEN("overrides.indicators.toasts"),

  HUG_SOUNDS_OVERRIDDEN("overrides.sounds.hugs"),
  PARTICLES_OVERRIDDEN("overrides.particles.enabled"),
  ;

  private final String path;

  Overrides(String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }
}
