package me.nothixal.hugs.enums.configuration;

public enum Settings {
  // updater.yml paths.
  CHECK_FOR_UPDATES("updater.check_for_updates"),
  UPDATER_OUTPUT_MODE("updater.mode"),
  SEND_UPDATES_TO_OPS("updater.send-updates-to-ops"),

  // config.yml paths
  LANGUAGE_FILE("language_file"),

  OVERRIDES("settings.use_overrides"),

  USE_HELP_MENU("settings.use_help_menu"),

  DEVELOPER_REWARD("settings.developer_reward"),

  FORCE_EXACT_NAMES("settings.force_exact_names"),

  RESTORE_PLAYER_HEALTH("settings.restore_player_health"),
  RESTORE_PLAYER_HUNGER("settings.restore_player_hunger"),

  ALLOW_SELF_HUG("settings.self_hugs"),

  EXCLUDE_SELF_HUGS_FROM_STATS("settings.exclude_self_hugs_from_stats"),

  ALLOW_SHIFT_HUG("settings.shift_hugs.enabled"),
  SHIFT_HUG_USAGE("settings.shift_hugs.usage"),
  SHIFT_HUG_PERMISSION_BYPASS("settings.shift_hugs.ignore_permission_check"),

  BROADCAST_SELF_HUGS("settings.broadcasts.self_hugs"),
  BROADCAST_NORMAL_HUGS("settings.broadcasts.normal_hugs"),
  BROADCAST_MASS_HUGS("settings.broadcasts.mass_hugs"),

  TITLE_MESSAGE_ENABLED("settings.titles.enabled"),
  TITLE_FADE_IN("settings.titles.fade-in"),
  TITLE_STAY("settings.titles.stay"),
  TITLE_FADE_OUT("settings.titles.fade-out"),

  GLOBAL_PARTICLES("settings.particles.global"),
  PARTICLES_ENABLED("settings.particles.enabled"),
  PARTICLE_EFFECT("settings.particles.effect"),

  GLOBAL_SOUND("settings.sounds.global"),
  SOUND_ENABLED("settings.sounds.enabled"),
  SOUND_EFFECT("settings.sounds.sound"),
  SOUND_VOLUME("settings.sounds.volume"),
  SOUND_PITCH("settings.sounds.pitch"),

  SELF_HUG_COOLDOWN("settings.cooldowns.self_hugs"),
  NORMAL_HUG_COOLDOWN("settings.cooldowns.normal_hugs"),
  MASS_HUG_COOLDOWN("settings.cooldowns.mass_hugs"),
  USE_EXACT_TIME("settings.cooldowns.use_exact_time"),

  CONFIRMATION_ENABLED("settings.confirmations"),

  FILE_LOGGING_ENABLED("verbose.file_logging"),

  CONSOLE_VERBOSE_ENABLED("verbose.console.enabled"),
  CONSOLE_NAME("verbose.console.console_name"),

  STARTUP_VERBOSE_ENABLED("verbose.enabled"),
  STARTUP_VERBOSE_DISPLAY_SETTINGS("verbose.configuration_settings"),
  SHOW_HOOKS("verbose.show_hooks"),

  RELOAD_VERBOSE_ENABLED("verbose.reloads.enabled"),
  RELOAD_VERBOSE_DISPLAY_SETTINGS("verbose.reloads.display_settings"),

  TEMPORARY_INVINCIBILITY_ENABLED("experimental_settings.temporary_invincibility.enabled"),
  CANCEL_FALL_DAMAGE("experimental_settings.temporary_invincibility.effects.fall_damage"),
  CANCEL_PVP("experimental_settings.temporary_invincibility.effects.pvp"),
  TEMPORARY_INVINCIBILITY_DURATION("experimental_settings.temporary_invincibility.duration"),

  PASSIVE_MODE_ENABLED("experimental_settings.passive_mode.enabled"),
  PASSIVE_MODE_MESSAGE_ENABLED("experimental_settings.passive_mode.message.enabled"),
  PASSIVE_MODE_MESSAGE_DELAY("experimental_settings.passive_mode.message.delay"),

  REALISTIC_HUGS("experimental_settings.realistic_hugs.enabled"),

  // Player Indicators (Overrides.yml)
  CHAT_MESSAGES_ENABLED("chat_messages.enabled"),
  ACTIONBAR_MESSAGES_ENABLED("actionbar_messages.enabled"),
  BOSSBAR_MESSAGES_ENABLED("bossbar_messages.enabled"),
  TITLE_MESSAGES_ENABLED("title_messages.enabled"),
  TOAST_MESSAGES_ENABLED("toast_messages.enabled"),

  PARTICLES_OVERRIDE_ENABLED("particles.enabled"),
  SOUNDS_OVERRIDE_ENABLED("sounds.enabled"),
  ;

  private final String value;

  Settings(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return value;
  }

}
