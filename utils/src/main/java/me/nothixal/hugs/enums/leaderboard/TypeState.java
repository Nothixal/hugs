package me.nothixal.hugs.enums.leaderboard;

public enum TypeState {

  GIVEN(),
  RECEIVED(),
  ;

  private static final TypeState[] values = values();

  public TypeState getNext() {
    return values[(this.ordinal() + 1) % values.length];
  }

  public TypeState getPrevious() {
    if (this.ordinal() == 0) {
      return values[values.length - 1];
    }

    return values[(this.ordinal() - 1) % values.length];
  }

}
