package me.nothixal.hugs.enums.leaderboard;

public enum SortState {

  ALL_TIME(),
  TODAY(),
  THIS_WEEK(),
  THIS_MONTH(),
  ;

  private static final SortState[] values = values();

  public SortState getNext() {
    return values[(this.ordinal() + 1) % values.length];
  }

  public SortState getPrevious() {
    if (this.ordinal() == 0) {
      return values[values.length - 1];
    }

    return values[(this.ordinal() - 1) % values.length];
  }

}
