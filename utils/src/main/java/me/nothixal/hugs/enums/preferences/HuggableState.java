package me.nothixal.hugs.enums.preferences;

public enum HuggableState {

  ALL("all"),
  REALISTIC("realistic"),
  NONE("none"),
  ;

  private String value;
  private static final HuggableState[] values = values();

  HuggableState(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public HuggableState getNext() {
    return values[(this.ordinal() + 1) % values.length];
  }

  public HuggableState getPrevious() {
    if (this.ordinal() == 0) {
      return values[values.length - 1];
    }

    return values[(this.ordinal() - 1) % values.length];
  }
}
