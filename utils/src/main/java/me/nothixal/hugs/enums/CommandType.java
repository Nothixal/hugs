package me.nothixal.hugs.enums;

public enum CommandType {

  NORMAL("&bNormal"),
  ADMIN("&cAdministrative"),
  ;

  private final String value;

  CommandType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}