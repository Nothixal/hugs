package me.nothixal.hugs.enums.configuration;

public enum ConfigValues {

  SELF_HUGS("data.self_hugs"),
  HUGS_GIVEN("data.normal_hugs.given"),
  HUGS_RECEIVED("data.normal_hugs.received"),
  MASS_HUGS_GIVEN("data.mass_hugs.given"),
  MASS_HUGS_RECEIVED("data.mass_hugs.received"),
  WANTS_HUGS("settings.huggable"),
  HUG_SOUNDS("settings.sounds.hugs"),
  WANTS_PARTICLES("settings.particles"),
  ;

  private final String path;

  ConfigValues(String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }
}
