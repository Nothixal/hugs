package me.nothixal.hugs.enums.types;

public enum DatabaseType {
  YAML("YAML", "local"),
  JSON("JSON", "local"),
  SQLITE("SQLite", "local"),
  MYSQL("MySQL", "global"),
  MARIADB("MariaDB", "global"),
  MONGODB("MongoDB", "global"),
  H2("H2", "local"),
  ;

  private final String value;
  private final String type;

  DatabaseType(String value, String type) {
    this.value = value;
    this.type = type;
  }

  public String getValue() {
    return value;
  }

  public String getType() {
    return type;
  }

  public boolean isLocal() {
    return type.equalsIgnoreCase("local");
  }

  public boolean isGlobal() {
    return type.equalsIgnoreCase("global");
  }
}
