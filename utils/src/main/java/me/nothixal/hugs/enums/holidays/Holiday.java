package me.nothixal.hugs.enums.holidays;

public enum Holiday {

  // US Holidays
  NONE("", HolidayOverride.NONE),
  NEW_YEARS("HAPPY NEW YEAR!", HolidayOverride.NEW_YEARS),
  NATIONAL_HUG_DAY("It's National Hug Day! Go give someone a hug!", HolidayOverride.NATIONAL_HUG_DAY),
  VALENTINES_DAY("Happy Valentines Day! Give your special someone a hug. <3", HolidayOverride.VALENTINES_DAY),
  INDEPENDENCE_DAY("Happy Independence Day!", HolidayOverride.INDEPENDENCE_DAY),
  HALLOWEEN("Happy Halloween! Be careful, a ghost might hug you!", HolidayOverride.HALLOWEEN),
  VETERANS_DAY("Veterans Day", HolidayOverride.VETERANS_DAY),
  CHRISTMAS("Merry Christmas!", HolidayOverride.CHRISTMAS),
  // Easter

  // Canadian Holidays
  BOXING_DAY("Boxing Day", HolidayOverride.BOXING_DAY),
  ;

  private final String message;
  private final HolidayOverride override;

  Holiday(String message, HolidayOverride override) {
    this.message = message;
    this.override = override;
  }

  public String getMessage() {
    return message;
  }

  public HolidayOverride getOverride() {
    return override;
  }
}