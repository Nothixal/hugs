package me.nothixal.hugs.enums.preferences;

public enum ParticleQuality {

  NONE("off", "&c"),
  LOW("low", "&e"),
  MEDIUM("medium", "&2"),
  HIGH("high", "&3"),
  EXTREME("extreme", "&d"),
  ;

  private final String value;
  private final String color;
  private static final ParticleQuality[] values = values();

  ParticleQuality(String value, String color) {
    this.value = value;
    this.color = color;
  }

  public String getValue() {
    return value;
  }

  public ParticleQuality getNext() {
    return values[(this.ordinal() + 1) % values.length];
  }

  public ParticleQuality getPrevious() {
    if (this.ordinal() == 0) {
      return values[values.length - 1];
    }

    return values[(this.ordinal() - 1) % values.length];
  }

  public String getColor() {
    return color;
  }
}
