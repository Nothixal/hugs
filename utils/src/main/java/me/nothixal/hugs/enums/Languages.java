package me.nothixal.hugs.enums;

public enum Languages {

  ENGLISH("English", "en"),
  SPANISH("Español", "es"),
  RUSSIAN("Русский", "ru"),
  GERMAN("Deutsch", "de"),
  FRENCH("Français", "fr"),
  ITALIAN("Italiano", "it"),
  POLISH("Polski", "pl"),
  NORWEGIAN("Norsk", "no"),
  FINNISH("Suomi", ""),
  SWEDISH("Svenska", ""),
  UKRAINIAN("Українська", ""),
  KOREAN("한국어", ""),
  DUTCH("Dutch", "nl"),

  PIRATE("Pirate", "pirate"),
  OWO("OwO", "owo"),
  ENCHANTING_TABLE("Enchanting Table", "enchantingtable"),
  LEET("1337", "1337"),
  ;

  private final String name;
  private final String fileExtension;

  Languages(String name, String fileExtension) {
    this.name = name;
    this.fileExtension = fileExtension;
  }

  public String getName() {
    return name;
  }

  public String getFileExtension() {
    return fileExtension;
  }
}
