package me.nothixal.hugs.managers.chat;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.entity.Player;

public class Chat_v1_19_R2 implements ChatManager {

  @Override
  public void sendMessage(Player player, String message) {
    BaseComponent[] components = new ComponentBuilder()
        .append(message)
        .create();

    player.spigot().sendMessage(components);
  }

  @Override
  public void sendActionBar(Player player, String message) {
    BaseComponent[] components = new ComponentBuilder()
        .append(message)
        .create();

    player.spigot().sendMessage(ChatMessageType.ACTION_BAR, components);
  }
}
