package me.nothixal.hugs.managers.items;

import com.cryptomorin.xseries.XMaterial;
import java.util.List;
import java.util.UUID;
import me.nothixal.hugs.managers.ItemBuilder;
import me.nothixal.hugs.utils.PluginConstants;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.protocol.game.ClientboundOpenBookPacket;
import net.minecraft.world.InteractionHand;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_20_R2.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_20_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Item_v1_20_R2 implements ItemManager {

  @Override
  public ItemStack createItem(ItemStack item) {
    return item;
  }

  @Override
  public ItemStack createItem(ItemStack item, String displayName) {
    return new ItemBuilder(item)
        .setDisplayName(displayName)
        .build();
  }

  @Override
  public ItemStack createItem(ItemStack item, String displayName, List<String> lore) {
    return new ItemBuilder(item)
        .setDisplayName(displayName)
        .setLore(lore)
        .build();
  }

  @Override
  public ItemStack createItem(Material material) {
    return new ItemStack(material);
  }

  @Override
  public ItemStack createItem(Material material, String displayName) {
    return new ItemBuilder(material)
        .setDisplayName(displayName)
        .build();
  }

  @Override
  public ItemStack createItem(Material material, String displayName, List<String> lore) {
    return new ItemBuilder(material)
        .setDisplayName(displayName)
        .setLore(lore)
        .build();
  }

  @Override
  public ItemStack createItem(XMaterial material) {
    return material.parseItem();
  }

  @Override
  public ItemStack createItem(XMaterial material, String displayName) {
    return new ItemBuilder(material)
        .setDisplayName(displayName)
        .build();
  }

  @Override
  public ItemStack createItem(XMaterial material, String displayName, List<String> lore) {
    return new ItemBuilder(material)
        .setDisplayName(displayName)
        .setLore(lore)
        .build();
  }

  @Override
  public ItemStack createItemNoAttrib(XMaterial material) {
    return new ItemBuilder(material)
        .hideAllAttributes()
        .build();
  }

  @Override
  public ItemStack createItemNoAttrib(XMaterial material, String displayName) {
    return new ItemBuilder(material)
        .hideAllAttributes()
        .setDisplayName(displayName)
        .build();
  }

  @Override
  public ItemStack createItemNoAttrib(XMaterial material, String displayName, List<String> lore) {
    return new ItemBuilder(material)
        .hideAllAttributes()
        .setDisplayName(displayName)
        .setLore(lore)
        .build();
  }

  @Override
  public ItemStack createItemGlowing(XMaterial material) {
    return new ItemBuilder(material)
        .setGlowing(true)
        .build();
  }

  @Override
  public ItemStack createItemGlowing(XMaterial material, String displayName) {
    return new ItemBuilder(material)
        .setGlowing(true)
        .setDisplayName(displayName)
        .build();
  }

  @Override
  public ItemStack createItemGlowing(XMaterial material, String displayName, List<String> lore) {
    return new ItemBuilder(material)
        .setGlowing(true)
        .setDisplayName(displayName)
        .setLore(lore)
        .build();
  }

  @Override
  public ItemStack createItemGlowing(ItemStack item) {
    return new ItemBuilder(item)
        .setGlowing(true)
        .build();
  }

  @Override
  public ItemStack createItemGlowing(ItemStack item, String displayName) {
    return new ItemBuilder(item)
        .setGlowing(true)
        .setDisplayName(displayName)
        .build();
  }

  @Override
  public ItemStack createItemGlowing(ItemStack item, String displayName, List<String> lore) {
    return new ItemBuilder(item)
        .setGlowing(true)
        .setDisplayName(displayName)
        .setLore(lore)
        .build();
  }

  @Override
  @SuppressWarnings("deprecation")
  public ItemStack createSkullItem(String owner) {
    return new ItemBuilder(XMaterial.PLAYER_HEAD)
        .setOwningPlayer(Bukkit.getOfflinePlayer(owner))
        .build();
  }

  @Override
  @SuppressWarnings("deprecation")
  public ItemStack createSkullItem(String owner, String displayName) {
    return new ItemBuilder(XMaterial.PLAYER_HEAD)
        .setOwningPlayer(Bukkit.getOfflinePlayer(owner))
        .setDisplayName(displayName)
        .build();
  }

  @Override
  @SuppressWarnings("deprecation")
  public ItemStack createSkullItem(String owner, String displayName, List<String> lore) {
    return new ItemBuilder(XMaterial.PLAYER_HEAD)
        .setOwningPlayer(Bukkit.getOfflinePlayer(owner))
        .setDisplayName(displayName)
        .setLore(lore)
        .build();
  }

  @Override
  public ItemStack createSkullItem(UUID owner) {
    return new ItemBuilder(XMaterial.PLAYER_HEAD)
        .setOwningPlayer(Bukkit.getOfflinePlayer(owner))
        .build();
  }

  @Override
  public ItemStack createSkullItem(UUID owner, String displayName) {
    return new ItemBuilder(XMaterial.PLAYER_HEAD)
        .setOwningPlayer(Bukkit.getOfflinePlayer(owner))
        .setDisplayName(displayName)
        .build();
  }

  @Override
  public ItemStack createSkullItem(UUID owner, String displayName, List<String> lore) {
    return new ItemBuilder(XMaterial.PLAYER_HEAD)
        .setOwningPlayer(Bukkit.getOfflinePlayer(owner))
        .setDisplayName(displayName)
        .setLore(lore)
        .build();
  }

  @Override
  public ItemStack createSkullItem(ItemStack item, String displayName) {
    return new ItemBuilder(item)
        .setDisplayName(displayName)
        .build();
  }

  @Override
  public ItemStack createSkullItem(ItemStack item, String displayName, List<String> lore) {
    return new ItemBuilder(item)
        .setDisplayName(displayName)
        .setLore(lore)
        .build();
  }

  @Override
  public ItemStack createSkullItem(ItemStack item, List<String> lore) {
    return new ItemBuilder(item)
        .setLore(lore)
        .build();
  }

  @Override
  public ItemStack createNMSSkullItem(String textureValue, String displayName) {
    ItemStack item = createItem(XMaterial.PLAYER_HEAD, displayName);

    net.minecraft.world.item.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);

    CompoundTag compound = nmsStack.getTag();
    if (compound == null) {
      compound = new CompoundTag();
      nmsStack.setTag(compound);
      compound = nmsStack.getTag();
    }

    UUID uuid = UUID.randomUUID();

    long least = uuid.getMostSignificantBits();
    long most = uuid.getLeastSignificantBits();

    int[] test = new int[]{(int) (least >> 32), (int) least, (int) (most >> 32), (int) most};


    CompoundTag skullOwner = new CompoundTag();
    skullOwner.putIntArray("Id", test);
    CompoundTag properties = new CompoundTag();
    CompoundTag value = new CompoundTag();
    value.putString("Value", textureValue);
    ListTag textures = new ListTag();
    textures.add(value);
    properties.put("textures", textures);
    skullOwner.put("Properties", properties);

    compound.put("SkullOwner", skullOwner);
    nmsStack.setTag(compound);

    return CraftItemStack.asBukkitCopy(nmsStack);
  }

  @Override
  public ItemStack createNMSSkullItem(String textureValue, String displayName, List<String> lore) {
    ItemStack item = createItem(XMaterial.PLAYER_HEAD, displayName, lore);

    net.minecraft.world.item.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);

    CompoundTag compound = nmsStack.getTag();
    if (compound == null) {
      compound = new CompoundTag();
      nmsStack.setTag(compound);
      compound = nmsStack.getTag();
    }

    UUID uuid = UUID.randomUUID();

    long least = uuid.getMostSignificantBits();
    long most = uuid.getLeastSignificantBits();

    int[] test = new int[]{(int) (least >> 32), (int) least, (int) (most >> 32), (int) most};


    CompoundTag skullOwner = new CompoundTag();
    skullOwner.putIntArray("Id", test);
    CompoundTag properties = new CompoundTag();
    CompoundTag value = new CompoundTag();
    value.putString("Value", textureValue);
    ListTag textures = new ListTag();
    textures.add(value);
    properties.put("textures", textures);
    skullOwner.put("Properties", properties);

    compound.put("SkullOwner", skullOwner);
    nmsStack.setTag(compound);

    return CraftItemStack.asBukkitCopy(nmsStack);
  }

  @Override
  public ItemStack createNMSSkullItem(String textureValue, ItemStack item) {
    net.minecraft.world.item.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);

    CompoundTag compound = nmsStack.getTag();
    if (compound == null) {
      compound = new CompoundTag();
      nmsStack.setTag(compound);
      compound = nmsStack.getTag();
    }

    UUID uuid = UUID.randomUUID();

    long least = uuid.getMostSignificantBits();
    long most = uuid.getLeastSignificantBits();

    int[] test = new int[]{(int) (least >> 32), (int) least, (int) (most >> 32), (int) most};

    CompoundTag skullOwner = new CompoundTag();
    skullOwner.putIntArray("Id", test);
    CompoundTag properties = new CompoundTag();
    CompoundTag value = new CompoundTag();
    value.putString("Value", textureValue);
    CompoundTag textures = new CompoundTag();
    textures.merge(value);
    properties.put("textures", textures);
    skullOwner.put("Properties", properties);

    compound.put("SkullOwner", skullOwner);
    nmsStack.setTag(compound);

    return CraftItemStack.asBukkitCopy(nmsStack);
  }

  @Override
  public ItemStack createNMSSkullItem(String textureValue, ItemStack item, String displayName) {
    net.minecraft.world.item.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);

    CompoundTag compound = nmsStack.getTag();
    if (compound == null) {
      compound = new CompoundTag();
      nmsStack.setTag(compound);
      compound = nmsStack.getTag();
    }

    UUID uuid = UUID.randomUUID();

    long least = uuid.getMostSignificantBits();
    long most = uuid.getLeastSignificantBits();

    int[] test = new int[]{(int) (least >> 32), (int) least, (int) (most >> 32), (int) most};

    CompoundTag skullOwner = new CompoundTag();
    skullOwner.putIntArray("Id", test);
    CompoundTag properties = new CompoundTag();
    CompoundTag value = new CompoundTag();
    value.putString("Value", textureValue);
    CompoundTag textures = new CompoundTag();
    textures.merge(value);
    properties.put("textures", textures);
    skullOwner.put("Properties", properties);

    compound.put("SkullOwner", skullOwner);
    nmsStack.setTag(compound);

//        createSkullItem(CraftItemStack.asBukkitCopy(nmsStack), "");

    ItemStack itemStack = CraftItemStack.asBukkitCopy(nmsStack);
    ItemMeta meta = itemStack.getItemMeta();
    meta.setDisplayName(displayName);
    itemStack.setItemMeta(meta);

    return itemStack;
  }

  @Override
  public ItemStack getEros() {
    return createNMSSkullItem(PluginConstants.EROS_TEXTURE_VALUE, "&eEros");
  }

  @Override
  public ItemStack getSilvia() {
    return createNMSSkullItem(PluginConstants.SILVIA_TEXTURE_VALUE, "&eSilvia");
  }

  @Override
  public ItemStack getRandomLovebird() {
    if (Math.random() > 0.5) {
      return getEros();
    } else {
      return getSilvia();
    }
  }

  @Override
  public void openBook(JavaPlugin plugin, Player player, ItemStack book) {
    int slot = player.getInventory().getHeldItemSlot();
    ItemStack old = player.getInventory().getItem(slot);
    player.getInventory().setItem(slot, book);
    new BukkitRunnable() {

      @Override
      public void run() {
        ClientboundOpenBookPacket packet = new ClientboundOpenBookPacket(InteractionHand.MAIN_HAND);
        ((CraftPlayer) player).getHandle().connection.send(packet);

        player.getInventory().setItem(slot, old);
      }
    }.runTaskLater(plugin, 1);
  }

}
