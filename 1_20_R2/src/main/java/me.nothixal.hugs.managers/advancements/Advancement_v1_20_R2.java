package me.nothixal.hugs.managers.advancements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import me.nothixal.hugs.enums.AdvancementBackground;
import me.nothixal.hugs.utils.json.JSONUtils;
import me.nothixal.hugs.utils.text.TextUtils;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.advancements.AdvancementProgress;
import net.minecraft.advancements.AdvancementRequirements;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.DisplayInfo;
import net.minecraft.advancements.FrameType;
import net.minecraft.advancements.critereon.ImpossibleTrigger;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.game.ClientboundUpdateAdvancementsPacket;
import net.minecraft.resources.ResourceLocation;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_20_R2.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_20_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Advancement_v1_20_R2 implements AdvancementManager {

  @Override
  public void sendToast(ItemStack item, UUID uuid, String title, String description) {
    Player player = Bukkit.getPlayer(uuid);

    Optional<ResourceLocation> pluginKey = Optional.of(new ResourceLocation("hugs", "notification"));

    Map<String, Criterion<?>> advCriteria = new HashMap<>();

    advCriteria.put("for_free", new Criterion<>(new ImpossibleTrigger(), new ImpossibleTrigger.TriggerInstance()));

    List<String[]> fixedRequirements = new ArrayList<>();
    fixedRequirements.add(new String[]{"for_free"});

    String[][] advRequirements = Arrays.stream(fixedRequirements.toArray()).toArray(String[][]::new);
    AdvancementRequirements requirements = new AdvancementRequirements(advRequirements);

    Optional<DisplayInfo> display = Optional.of(new DisplayInfo(CraftItemStack.asNMSCopy(item),
        Component.Serializer.fromJson(JSONUtils.formatSimpleJSON(TextUtils.colorText(title))),
        Component.Serializer.fromJson(JSONUtils.formatSimpleJSON(TextUtils.colorText(description))),
        new ResourceLocation(AdvancementBackground.SNOW.getValue()),
        FrameType.GOAL, true, true, false));

    Advancement advancement = new Advancement(Optional.empty(), display, AdvancementRewards.EMPTY, advCriteria, requirements, false);

    Map<ResourceLocation, AdvancementProgress> progress = new HashMap<>();

    AdvancementProgress advPrg = new AdvancementProgress();
    advPrg.update(requirements);
    advPrg.getCriterion("for_free").grant();
    progress.put(pluginKey.get(), advPrg);

    AdvancementHolder holder = new AdvancementHolder(pluginKey.get(), advancement);

    ClientboundUpdateAdvancementsPacket packet = new ClientboundUpdateAdvancementsPacket(false, Collections.singletonList(holder), new HashSet<>(), progress);
    ((CraftPlayer) player).getHandle().connection.send(packet);

    Set<ResourceLocation> removed = new HashSet<>();
    removed.add(pluginKey.get());
    progress.clear();

    packet = new ClientboundUpdateAdvancementsPacket(false, Collections.emptyList(), removed, progress);
    ((CraftPlayer) player).getHandle().connection.send(packet);
  }

  @Override
  public void sendToast(UUID uuid, String title, String description) {

  }

  @Override
  public void sendToast(Player player, String title, String description) {

  }

  @Override
  public void sendToast(String playerName, String title, String description) {

  }

  @Override
  public void sendGoalToast(ItemStack item, UUID uuid, String title, String description) {

  }

  @Override
  public void sendChallengeToast(ItemStack item, UUID uuid, String title, String description) {
    Player player = Bukkit.getPlayer(uuid);

    Optional<ResourceLocation> pluginKey = Optional.of(new ResourceLocation("hugs", "notification"));

    Map<String, Criterion<?>> advCriteria = new HashMap<>();

    advCriteria.put("for_free", new Criterion<>(new ImpossibleTrigger(), new ImpossibleTrigger.TriggerInstance()));

    List<String[]> fixedRequirements = new ArrayList<>();
    fixedRequirements.add(new String[]{"for_free"});

    String[][] advRequirements = Arrays.stream(fixedRequirements.toArray()).toArray(String[][]::new);
    AdvancementRequirements requirements = new AdvancementRequirements(advRequirements);

    Optional<DisplayInfo> display = Optional.of(new DisplayInfo(CraftItemStack.asNMSCopy(item),
        Component.Serializer.fromJson(JSONUtils.formatSimpleJSON(TextUtils.colorText(title))),
        Component.Serializer.fromJson(JSONUtils.formatSimpleJSON(TextUtils.colorText(description))),
        new ResourceLocation(AdvancementBackground.SNOW.getValue()),
        FrameType.CHALLENGE, true, true, false));

    Advancement advancement = new Advancement(Optional.empty(), display, AdvancementRewards.EMPTY, advCriteria, requirements, false);

    Map<ResourceLocation, AdvancementProgress> progress = new HashMap<>();

    AdvancementProgress advPrg = new AdvancementProgress();
    advPrg.update(requirements);
    advPrg.getCriterion("for_free").grant();
    progress.put(pluginKey.get(), advPrg);

    AdvancementHolder holder = new AdvancementHolder(pluginKey.get(), advancement);

    ClientboundUpdateAdvancementsPacket packet = new ClientboundUpdateAdvancementsPacket(false, Collections.singletonList(holder), new HashSet<>(), progress);
    ((CraftPlayer) player).getHandle().connection.send(packet);

    Set<ResourceLocation> removed = new HashSet<>();
    removed.add(pluginKey.get());
    progress.clear();

    packet = new ClientboundUpdateAdvancementsPacket(false, Collections.emptyList(), removed, progress);
    ((CraftPlayer) player).getHandle().connection.send(packet);
  }
}
